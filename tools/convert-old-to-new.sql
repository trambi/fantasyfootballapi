INSERT INTO edition SELECT id,printf('RDVBB %d',id),day_1,day_2,round_number,current_round,first_day_round,use_finale,full_triplette,ranking_strategy,3,'[]',organiser, '{"Default":"0","Clauses":[]}','{"Default":"0","Clauses":[]}'  FROM tournament_edition ORDER BY id;

UPDATE edition SET allowed_factions = '["Orc","Skaven","Dark elf","Human","Dwarf","High elf","Goblin","Halfling","Wood elf","Chaos","Chaos dwarf","Undead","Norse","Amazon","Lizardmen"]' WHERE id = 1;

UPDATE edition SET allowed_factions = '["Orc","Skaven","Dark elf","Human","Dwarf","High elf","Goblin","Halfling","Wood elf","Chaos","Chaos dwarf","Undead","Norse","Amazon","Lizardmen","Khemri","Necromantic","Elf","Nurgle"]' WHERE id = 2;

UPDATE edition SET allowed_factions = '["Orc","Skaven","Dark elf","Human","Dwarf","High elf","Goblin","Halfling","Wood elf","Chaos","Chaos dwarf","Undead","Norse","Amazon","Lizardmen","Khemri","Necromantic","Elf","Nurgle","Ogre","Vampire"]' WHERE id IN(3,4,5,6);

UPDATE edition SET allowed_factions = '["Orc","Skaven","Dark elf","Human","Dwarf","High elf","Goblin","Halfling","Wood elf","Chaos","Chaos dwarf","Undead","Norse","Amazon","Lizardmen","Khemri","Necromantic","Elf","Nurgle","Ogre","Vampire","Underworld","Chaos Pact","Slann"]' WHERE id IN(7,8,9) ;

UPDATE edition SET allowed_factions = '["Orc","Skaven","Dark elf","Human","Dwarf","High elf","Goblin","Halfling","Wood elf","Chaos","Chaos dwarf","Undead","Norse","Amazon","Lizardmen","Khemri","Necromantic","Elf","Nurgle","Ogre","Vampire","Underworld","Chaos Pact","Slann","Skink"]' WHERE id IN(10,11,12,13,14,15);

UPDATE edition SET allowed_factions = '["Orc","Skaven","Dark elf","Human","Dwarf","High elf","Goblin","Halfling","Wood elf","Chaos","Chaos dwarf","Undead","Norse","Amazon","Lizardmen","Khemri","Necromantic","Elf","Nurgle","Ogre","Vampire","Underworld","Chaos Pact","Slann","Skink","Khorne","Bretonnian","Unknown"]' WHERE id IN(16,17);

UPDATE edition SET allowed_factions = '["Orc","Skaven","Dark elf","Human","Dwarf","High elf","Goblin","Halfling","Wood elf","Chaos","Chaos dwarf","Undead","Norse","Amazon","Lizardmen","Khemri","Necromantic","Elf","Nurgle","Ogre","Vampire","Underworld","Chaos Pact","Slann","Skink","Khorne","Old World","Imperial Nobility","Snotling","Black orc","Unknown"]' WHERE id IN(18,19);

INSERT INTO squads SELECT DISTINCT s.id,s.name,c.edition FROM tournament_coach c INNER JOIN tournament_coach_team s ON c.id_coach_team = s.id ORDER BY c.id_coach_team,c.edition;

INSERT INTO coach SELECT id,name,team_name, naf_number,ready,c.edition,r.nom_en,id_coach_team FROM tournament_coach c INNER JOIN tournament_race r ON c.id_race = r.id_race;

INSERT INTO game SELECT id,edition,round,table_number,1,finale,id_coach_1,td_1,casualties_1,completions_1,fouls_1,points_1,special_1,id_coach_2,td_2,casualties_2,completions_2,fouls_2,points_2,special_2 FROM tournament_match;

UPDATE game SET finale=0 WHERE finale=2;

UPDATE game SET table_number=id-((round-1)*24) WHERE edition_id=1;
UPDATE game SET table_number=table_number+1 WHERE edition_id=1 AND round=6;
UPDATE game set table_number=21 WHERE id=2870;
UPDATE game set table_number=22 WHERE id=2871;
UPDATE game set table_number=23 WHERE id=2881;
UPDATE game set table_number=61 WHERE id=2882;
UPDATE game set table_number=62 WHERE id=2887;
UPDATE game set table_number=63 WHERE id=2888;


INSERT INTO coach_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,1),
(NULL,'main',2,'netTd','Td','Net',1,1),
(NULL,'main',3,'casualtiesFor','Casualties','For',1,1),
(NULL,'td',1,'tdFor','Td','For',1,1),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,1),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,1),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,1),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,1);

UPDATE edition SET game_points='{"Default":"0","Clauses":[{"Condition":"{TdFor}+1={TdAgainst}","ValueIfTrue":"1"},{"Condition":"{TdFor}={TdAgainst}","ValueIfTrue":"2"},{"Condition":"{TdFor}>{TdAgainst}","ValueIfTrue":"5"}]}', confrontation_points='{"Default":"0","Clauses":[]}',coach_per_squad=0 WHERE id = 1;

INSERT INTO coach_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,2),
(NULL,'main',2,'netTd','Td','Net',1,2),
(NULL,'main',3,'casualtiesFor','Casualties','For',1,2),
(NULL,'td',1,'tdFor','Td','For',1,2),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,2),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,2),
(NULL,'comeback',1,'firstDayRanking','Ranking','Day1',0,2),
(NULL,'comeback',1,'finalRanking','Ranking','Day2',0,2),
(NULL,'main',1,'points','Points','For',1,3),
(NULL,'main',2,'netTd','Td','Net',1,3),
(NULL,'main',3,'casualtiesFor','Casualties','For',1,3),
(NULL,'td',1,'tdFor','Td','For',1,3),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,3),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,3),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,3),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,3);

UPDATE edition SET game_points='{"Default":"0","Clauses":[{"Condition":"{TdFor}+1={TdAgainst}","ValueIfTrue":"1"},{"Condition":"{TdFor}={TdAgainst}","ValueIfTrue":"3"},{"Condition":"{TdFor}>{TdAgainst}","ValueIfTrue":"5"}]}', confrontation_points='{"Default":"0","Clauses":[]}',coach_per_squad=0 WHERE id = 2 or id =3;

INSERT INTO coach_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,4),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,4),
(NULL,'main',3,'netTd','Td','Net',1,4),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,4),
(NULL,'td',1,'tdFor','Td','For',1,4),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,4),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,4),
(NULL,'comeback',1,'firstDayRanking','Ranking','Day1',0,4),
(NULL,'comeback',1,'finalRanking','Ranking','Day2',0,4),
(NULL,'main',1,'points','Points','For',1,5),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,5),
(NULL,'main',3,'netTd','Td','Net',1,5),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,5),
(NULL,'td',1,'tdFor','Td','For',1,5),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,5),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,5),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,5),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,5);

INSERT INTO squad_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,4),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,4),
(NULL,'main',3,'netTd','Td','Net',1,4),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,4),
(NULL,'main',1,'points','Points','For',1,5),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,5),
(NULL,'main',3,'netTd','Td','Net',1,5),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,5);

UPDATE edition SET game_points='{"Default":"0","Clauses":[{"Condition":"{TdFor}+1={TdAgainst}","ValueIfTrue":"1"},{"Condition":"{TdFor}={TdAgainst}","ValueIfTrue":"3"},{"Condition":"{TdFor}>{TdAgainst}","ValueIfTrue":"5"}]}', confrontation_points='{"Default":"0","Clauses":[]}' WHERE id = 4 or id =5;

INSERT INTO coach_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,6),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,6),
(NULL,'main',3,'netTd','Td','Net',1,6),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,6),
(NULL,'td',1,'tdFor','Td','For',1,6),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,6),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,6),
(NULL,'comeback',1,'firstDayRanking','Ranking','Day1',0,6),
(NULL,'comeback',1,'finalRanking','Ranking','Day2',0,6),
(NULL,'main',1,'points','Points','For',1,7),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,7),
(NULL,'main',3,'netTd','Td','Net',1,7),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,7),
(NULL,'td',1,'tdFor','Td','For',1,7),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,7),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,7),
(NULL,'comeback',1,'firstDayRanking','Ranking','Day1',0,7),
(NULL,'comeback',1,'finalRanking','Ranking','Day2',0,7),
(NULL,'main',1,'points','Points','For',1,8),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,8),
(NULL,'main',3,'netTd','Td','Net',1,8),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,8),
(NULL,'td',1,'tdFor','Td','For',1,8),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,8),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,8),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,8),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,8);

INSERT INTO squad_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,6),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,6),
(NULL,'main',3,'netTd','Td','Net',1,6),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,6),
(NULL,'main',1,'points','Points','For',1,7),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,7),
(NULL,'main',3,'netTd','Td','Net',1,7),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,7),
(NULL,'main',1,'points','Points','For',1,8),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,8),
(NULL,'main',3,'netTd','Td','Net',1,8),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,8);

UPDATE edition SET game_points='{"Default":"0","Clauses":[{"Condition":"{TdFor}+1={TdAgainst}","ValueIfTrue":"1"},{"Condition":"{TdFor}={TdAgainst}","ValueIfTrue":"3"},{"Condition":"{TdFor}>{TdAgainst}","ValueIfTrue":"5"}]}', confrontation_points='{"Default":"0","Clauses":[]}' WHERE id=6 or id=7 or id=8;

INSERT INTO coach_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,9),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,9),
(NULL,'main',3,'netTd','Td','Net',1,9),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,9),
(NULL,'td',1,'tdFor','Td','For',1,9),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,9),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,9),
(NULL,'comeback',1,'firstDayRanking','Ranking','Day1',0,9),
(NULL,'comeback',1,'finalRanking','Ranking','Day2',0,9),
(NULL,'main',1,'points','Points','For',1,10),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,10),
(NULL,'main',3,'netTd','Td','Net',1,10),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,10),
(NULL,'td',1,'tdFor','Td','For',1,10),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,10),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,10),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,10),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,10);

INSERT INTO squad_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,9),
(NULL,'main',2,'opponentCoachTeamPoints','Points','OpponentExceptOwnGame',1,9),
(NULL,'main',3,'netTd','Td','Net',1,9),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,9),
(NULL,'main',1,'points','Points','For',1,10),
(NULL,'main',2,'opponentCoachTeamPoints','Points','OpponentExceptOwnGame',1,10),
(NULL,'main',3,'netTd','Td','Net',1,10),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,10);

UPDATE edition SET game_points='{"Default":"0","Clauses":[{"Condition":"{TdFor}+1={TdAgainst}","ValueIfTrue":"1"},{"Condition":"{TdFor}={TdAgainst}","ValueIfTrue":"4"},{"Condition":"{TdFor}>{TdAgainst}","ValueIfTrue":"10"}]}', confrontation_points='{"Default":"0","Clauses":[]}' WHERE id=9 or id=10;

INSERT INTO coach_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,11),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,11),
(NULL,'main',3,'netTd','Td','Net',1,11),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,11),
(NULL,'td',1,'tdFor','Td','For',1,11),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,11),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,11),
(NULL,'comeback',1,'firstDayRanking','Ranking','Day1',0,11),
(NULL,'comeback',1,'finalRanking','Ranking','Day2',0,11),
(NULL,'main',1,'points','Points','For',1,12),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,12),
(NULL,'main',3,'netTd','Td','Net',1,12),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,12),
(NULL,'td',1,'tdFor','Td','For',1,12),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,12),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,12),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,12),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,12);

INSERT INTO squad_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,11),
(NULL,'main',2,'opponentCoachTeamPoints','Points','OpponentExceptOwnGame',1,11),
(NULL,'main',3,'netTd','Td','Net',1,11),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,11),
(NULL,'main',1,'points','Points','For',1,12),
(NULL,'main',2,'opponentCoachTeamPoints','Points','OpponentExceptOwnGame',1,12),
(NULL,'main',3,'netTd','Td','Net',1,12),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,12);

UPDATE edition SET game_points='{"Default":"0","Clauses":[{"Condition":"{TdFor}+1={TdAgainst}","ValueIfTrue":"1"},{"Condition":"{TdFor}={TdAgainst}","ValueIfTrue":"4"},{"Condition":"{TdFor}={TdAgainst}+1","ValueIfTrue":"8"},{"Condition":"{TdFor}>{TdAgainst}+1","ValueIfTrue":"9"}]}', confrontation_points='{"Default":"0","Clauses":[]}' WHERE id=11 or id=12;

INSERT INTO coach_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,13),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,13),
(NULL,'main',3,'netTd','Td','Net',1,13),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,13),
(NULL,'td',1,'tdFor','Td','For',1,13),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,13),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,13),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,13),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,13);

INSERT INTO squad_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,13),
(NULL,'main',2,'opponentCoachTeamPoints','Points','OpponentExceptOwnGame',1,13),
(NULL,'main',3,'netTd','Td','Net',1,13),
(NULL,'main',4,'casualtiesFor','Casualties','For',1,13);

UPDATE edition SET game_points='{"Default":"0","Clauses":[{"Condition":"{TdFor}={TdAgainst}","ValueIfTrue":"100"},{"Condition":"{TdFor}>{TdAgainst}","ValueIfTrue":"300"}]}',
 confrontation_points='{"Default":"{PointsFor}","Clauses":[{"Condition":"{PointsFor}={PointsAgainst}","ValueIfTrue":"{PointsFor}+50"},{"Condition":"{PointsFor}>{PointsAgainst}","ValueIfTrue":"{PointsFor}+150"}]}' WHERE id=13;

INSERT INTO coach_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,14),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,14),
(NULL,'main',3,'netTd','Td','Net',1,14),
(NULL,'main',4,'netCasualties','Casualties','Net',1,14),
(NULL,'main',1,'points','Points','For',1,15),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,15),
(NULL,'main',3,'netTd','Td','Net',1,15),
(NULL,'main',4,'netCasualties','Casualties','Net',1,15),
(NULL,'main',1,'points','Points','For',1,16),
(NULL,'main',2,'opponentsPoints','Points','OpponentExceptOwnGame',1,16),
(NULL,'main',3,'netTd','Td','Net',1,16),
(NULL,'main',4,'netCasualties','Casualties','Net',1,16);

INSERT INTO squad_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,14),
(NULL,'main',2,'opponentCoachTeamPoints','Points','OpponentExceptOwnGame',1,14),
(NULL,'main',3,'netTd','Td','Net',1,14),
(NULL,'main',4,'netCasualties','Casualties','Net',1,14),
(NULL,'td',1,'tdFor','Td','For',1,14),
(NULL,'fouls',1,'foulsFor','Fouls','For',1,14),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,14),
(NULL,'defense',1,'tdAgainst','Td','Against',0,14),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,14),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,14),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,14),
(NULL,'main',1,'points','Points','For',1,15),
(NULL,'main',2,'opponentCoachTeamPoints','Points','OpponentExceptOwnGame',1,15),
(NULL,'main',3,'netTd','Td','Net',1,15),
(NULL,'main',4,'netCasualties','Casualties','Net',1,15),
(NULL,'td',1,'tdFor','Td','For',1,15),
(NULL,'fouls',1,'foulsFor','Fouls','For',1,15),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,15),
(NULL,'defense',1,'tdAgainst','Td','Against',0,15),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,15),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,15),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,15),
(NULL,'main',1,'points','Points','For',1,16),
(NULL,'main',2,'opponentCoachTeamPoints','Points','OpponentExceptOwnGame',1,16),
(NULL,'main',3,'netTd','Td','Net',1,16),
(NULL,'main',4,'netCasualties','Casualties','Net',1,16),
(NULL,'td',1,'tdFor','Td','For',1,16),
(NULL,'fouls',1,'foulsFor','Fouls','For',1,16),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,16),
(NULL,'defense',1,'tdAgainst','Td','Against',0,16),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,16),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,16),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,16);

UPDATE edition SET game_points='{"Default":"0","Clauses":[{"Condition":"{TdFor}={TdAgainst}","ValueIfTrue":"500"},{"Condition":"{TdFor}>{TdAgainst}","ValueIfTrue":"1000"}]}', confrontation_points='{"Default":"0","Clauses":[{"Condition":"{PointsFor}={PointsAgainst}","ValueIfTrue":"500"},{"Condition":"{PointsFor}>{PointsAgainst}","ValueIfTrue":"1000"}]}' WHERE id in(14,15,16);

INSERT INTO coach_ranking_criterias VALUES
(NULL, 'main', 1, 'points', 'Points', 'For', 1, 17),
(NULL, 'main', 2, 'opponentsPoints', 'Points', 'OpponentExceptOwnGame', 1, 17),
(NULL,'main',3,'netTd','Td','Net',1,17),
(NULL,'main',4,'netCasualties','Casualties','Net',1,17);

INSERT INTO squad_ranking_criterias VALUES 
(NULL,'main',1,'points','Points','For',1,17),
(NULL,'main',2,'opponentCoachTeamPoints','Points','OpponentExceptOwnGame',1,17),
(NULL,'main',3,'netTd','Td','Net',1,17),
(NULL,'main',4,'netCasualties','Casualties','Net',1,17),
(NULL,'td',1,'tdFor','Td','For',1,17),
(NULL,'fouls',1,'foulsFor','Fouls','For',1,17),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,17),
(NULL,'defense',1,'tdAgainst','Td','Against',0,17),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,17),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,17),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,17);

UPDATE edition SET game_points='{"Default":"0","Clauses":[{"Condition":"{TdFor}={TdAgainst}","ValueIfTrue":"2"},{"Condition":"{TdFor}>{TdAgainst}","ValueIfTrue":"5"}]}',
 confrontation_points='{"Default":"0","Clauses":[{"Condition":"{PointsFor}={PointsAgainst}","ValueIfTrue":"1"},{"Condition":"{PointsFor}>{PointsAgainst}","ValueIfTrue":"2"}]}' WHERE id=17;

INSERT INTO coach_ranking_criterias VALUES
(NULL, 'main', 1, 'points', 'Points', 'For', 1, 18),
(NULL, 'main', 2, 'opponentsPoints', 'Points', 'OpponentExceptOwnGame', 1, 18),
(NULL,'main',3,'netTd','Td','Net',1,18),
(NULL,'main',4,'netCasualties','Casualties','Net',1,18),
(NULL, 'main', 1, 'points', 'Points', 'For', 1, 19),
(NULL, 'main', 2, 'opponentsPoints', 'Points', 'OpponentExceptOwnGame', 1, 19),
(NULL,'main',3,'netTd','Td','Net',1,19),
(NULL,'main',4,'netCasualties','Casualties','Net',1,19);

INSERT INTO squad_ranking_criterias VALUES 
(NULL,'main',1,'confrontationPoints','ConfrontationPoints','For',1,18),
(NULL,'main',2,'opponentConfrontationPoints','ConfrontationPoints','Opponent',1,18),
(NULL,'main',3,'points','Points','For',1,18),
(NULL,'main',4,'netTd','Td','Net',1,18),
(NULL,'main',5,'netCasualties','Casualties','Net',1,18),
(NULL,'td',1,'tdFor','Td','For',1,18),
(NULL,'fouls',1,'foulsFor','Fouls','For',1,18),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,18),
(NULL,'defense',1,'tdAgainst','Td','Against',0,18),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,18),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,18),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,18),
(NULL,'main',1,'confrontationPoints','ConfrontationPoints','For',1,19),
(NULL,'main',2,'opponentConfrontationPoints','ConfrontationPoints','Opponent',1,19),
(NULL,'main',3,'points','Points','For',1,18),
(NULL,'main',4,'netTd','Td','Net',1,19),
(NULL,'main',5,'netCasualties','Casualties','Net',1,19),
(NULL,'td',1,'tdFor','Td','For',1,19),
(NULL,'fouls',1,'foulsFor','Fouls','For',1,19),
(NULL,'casualties',1,'casualtiesFor','Casualties','For',1,19),
(NULL,'defense',1,'tdAgainst','Td','Against',0,19),
(NULL,'comeback',1,'diffRanking','Ranking','Day1MinusDay2',1,19),
(NULL,'comeback',2,'firstDayRanking','Ranking','Day1',0,19),
(NULL,'comeback',3,'finalRanking','Ranking','Day2',0,19);

UPDATE edition SET game_points='{"Default":"0","Clauses":[{"Condition":"{TdFor}={TdAgainst}","ValueIfTrue":"2"},{"Condition":"{TdFor}>{TdAgainst}","ValueIfTrue":"5"}]}',
 confrontation_points='{"Default":"0","Clauses":[{"Condition":"{PointsFor}={PointsAgainst}","ValueIfTrue":"2"},{"Condition":"{PointsFor}>{PointsAgainst}","ValueIfTrue":"5"}]}' WHERE id=18 or id=19;

UPDATE coach SET faction='Black orc' WHERE faction='d''orques noirs';