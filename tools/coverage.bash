#! /bin/bash
readonly PKG_LIST=$(go list ./internal/... ./cmd | grep -v /vendor/)
readonly coverFile=$(mktemp)
for package in ${PKG_LIST}; do
  go test -covermode=count -coverprofile "cover/${package##*/}.tmp" "$package" ;
done
echo "mode: count" > "${coverFile}"
cat cover/*.tmp | grep .| grep -v "mode" >> "${coverFile}"
go tool cover -func=${coverFile}
rm "${coverFile}"
