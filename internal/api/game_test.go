// FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func TestGameInvalidInput(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	path := s.URL + editionUrl + "/games"
	testPostWithoutBody(client, path, t)
	testPostWithInvalidJson(client, path, t)
	testPostWithUnknownJson(client, path, t)
}

func TestGameNotFound(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/games/10453"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
	req, _ := http.NewRequest("PUT", notExistentUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("POST on %v", notExistentUrl), t)
	req, _ = http.NewRequest("DELETE", notExistentUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("DELETE on %v", notExistentUrl), t)
}

func TestGameInvalidId(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	invalidUrl := s.URL + "/v2/games/AAA"
	response, _ := client.Get(invalidUrl)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("GET on %v", invalidUrl), t)
	req, _ := http.NewRequest("PUT", invalidUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("PUT on %v", invalidUrl), t)
	req, _ = http.NewRequest("DELETE", invalidUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("DELETE on %v", invalidUrl), t)
}

func TestGetGamesUnknownEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/editions/102454/games"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
}

func TestGetGamesInvalidEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/editions/AAAAA/games"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
}

func TestGetPlayedGamesUnknownEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/editions/102454/playedGames"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
}

func TestGetPlayedGamesInvalidEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/editions/AAAAA/playedGames"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
}

func TestGetPlayableGamesUnknownEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/editions/102454/playableGames"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
}

func TestGetPlayableGamesInvalidEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/editions/AAAAA/playableGames"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
}

func TestGetGames(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", (i-1%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	expectedGames := [](map[string]interface{}){}
	expectedPlayableGames := [](map[string]interface{}){}
	expectedPlayedGames := [](map[string]interface{}){}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}

		game.Played = i%2 == 0
		payload := game_to_payload(game)
		urlGame := postPayloadTo(client, payload, s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
		expectedGames = append(expectedGames, game_to_extended_map(game))
		if game.Played {
			expectedPlayedGames = append(expectedPlayedGames, game_to_extended_map(game))
		} else {
			expectedPlayableGames = append(expectedPlayableGames, game_to_extended_map(game))
		}
	}
	body := testGetOk(client, s.URL+editionUrl+"/games", t)
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw body : ", string(body))
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedGames) {
		t.Error("result of getGames is not as expected: ", cmp.Diff(receivedMaps, expectedGames))
	}
	body = testGetOk(client, s.URL+editionUrl+"/playableGames", t)
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw games : ", string(body))
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedPlayableGames) {
		t.Error("result of getPlayableGames is not as expected: ", cmp.Diff(receivedMaps, expectedPlayableGames))
	}
	body = testGetOk(client, s.URL+editionUrl+"/playedGames", t)
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw games : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(expectedPlayedGames, receivedMaps) {
		t.Error("result of getPlayedGames is not as expected: ", cmp.Diff(expectedPlayedGames, receivedMaps))
	}
}

func TestGetGamesByEditionAndRound(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", (i-1%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	expectedGames := [](map[string]interface{}){}
	expectedPlayableGames := [](map[string]interface{}){}
	expectedPlayedGames := [](map[string]interface{}){}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}

		game.Played = i%2 == 0
		payload := game_to_payload(game)
		urlGame := postPayloadTo(client, payload, s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
		if game.Round == 2 {
			expectedGames = append(expectedGames, game_to_extended_map(game))
			if game.Played {
				expectedPlayedGames = append(expectedPlayedGames, game_to_extended_map(game))
			} else {
				expectedPlayableGames = append(expectedPlayableGames, game_to_extended_map(game))
			}
		}

	}
	body := testGetOk(client, s.URL+editionUrl+"/rounds/2/games", t)
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw games : ", string(body))
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedGames) {
		t.Error("result of getGames is not as expected: ", cmp.Diff(receivedMaps, expectedGames))
	}
	body = testGetOk(client, s.URL+editionUrl+"/rounds/2/playableGames", t)
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw games : ", string(body))
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedPlayableGames) {
		t.Error("result of getPlayableGames is not as expected: ", cmp.Diff(receivedMaps, expectedPlayableGames))
	}
	body = testGetOk(client, s.URL+editionUrl+"/rounds/2/playedGames", t)
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw games : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(expectedPlayedGames, receivedMaps) {
		t.Error("result of getPlayedGames is not as expected: ", cmp.Diff(expectedPlayedGames, receivedMaps))
	}
}

func TestCreateGameWithUnknownEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	target := "/v2/editions/2565/games"
	payload := []byte(`{"round":1,"table":5}`)
	response, _ := client.Post(s.URL+target, contentType(), bytes.NewBuffer(payload))
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("POST on %v", target), t)
}

func TestCreateGameWithInvalidEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	target := "/v2/editions/AAAAA/games"
	payload := []byte(`{"round":1,"table":5}`)
	response, _ := client.Post(s.URL+target, contentType(), bytes.NewBuffer(payload))
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("POST on %v", target), t)
}

func TestUpdateGame(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coach1 := core.Coach{
		Name:      "Coach1",
		TeamName:  "Team1",
		NafNumber: 123,
		Faction:   faction,
		Ready:     true,
		Edition:   edition,
	}
	coach2 := core.Coach{
		Name:      "Coach2",
		TeamName:  "Team2",
		NafNumber: 999,
		Faction:   faction,
		Ready:     true,
		Edition:   edition,
	}
	coach1Url := postCoach(t, client, coach1, s.URL+editionUrl)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+coach1Url, t)
	})
	tempId, err = extractIdFromUrl(coach1Url)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	coach1.ID = tempId
	coach2Url := postCoach(t, client, coach2, s.URL+editionUrl)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+coach2Url, t)
	})
	tempId, err = extractIdFromUrl(coach2Url)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	coach2.ID = uint(tempId)
	game, err := core.NewGame(1, 2, coach1, coach2)
	if err != nil {
		t.Fatal("unexpected error in core.NewGame: ", err)
	}
	payload := game_to_payload(game)
	urlGame := postPayloadTo(client, payload, s.URL+editionUrl+"/games", t)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlGame, t)
	})
	tempId, err = extractIdFromUrl(urlGame)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	game.ID = tempId
	game.SetTd(1, 2)
	game.SetCasualties(3, 4)
	game.SetCompletions(5, 6)
	game.SetFouls(7, 8)
	payload = []byte(game_to_extended_payload(game))
	req, _ := http.NewRequest("PUT", s.URL+urlGame, bytes.NewBuffer(payload))
	response, _ := client.Do(req)

	checkResponseCode(http.StatusNoContent, response.StatusCode, "PUT on "+s.URL+urlGame, t)
	_, err = io.ReadAll(response.Body)
	if err != nil {
		t.Fatal("Unexpected error while reading body of PUT: ", err.Error())
	}
	expectedMap := game_to_extended_map(game)
	receivedMap := map[string]interface{}{}
	body := testGetOk(client, s.URL+urlGame, t)
	err = json.Unmarshal(body, &receivedMap)
	if err != nil {
		t.Fatal("Unexpected error while parsing body: ", err.Error())
	}
	if !cmp.Equal(expectedMap, receivedMap) {
		t.Error("received map should be equal to expected map: ", cmp.Diff(expectedMap, receivedMap))
	}
}

func TestGetGamesByCoach(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", (i-1%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	expectedGames := [](map[string]interface{}){}
	game, err := core.NewGame(1, 2, coachs[0], coachs[2])
	if err != nil {
		t.Fatal("unexpected error in core.NewGame: ", err)
	}

	payload := game_to_payload(game)
	urlGame := postPayloadTo(client, payload, s.URL+editionUrl+"/games", t)
	tempId, err = extractIdFromUrl(urlGame)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlGame, t)
	})
	game.ID = tempId
	expectedGames = append(expectedGames, game_to_extended_map(game))

	body := testGetOk(client, fmt.Sprintf("%s/v2/coachs/%d/games", s.URL, coachs[0].ID), t)
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw games : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedGames) {
		t.Error("result of getGamesBySquad is not as expected: ", cmp.Diff(receivedMaps, expectedGames))
	}
}

func TestOptionsOnGames(t *testing.T) {
	// Given
	expectedAllow := "GET, POST, OPTIONS"
	s := getTestServer()
	urlSuffix := "/v2/editions/1/games"
	url := s.URL + urlSuffix
	req, err := http.NewRequest("OPTIONS", url, nil)
	if err != nil {
		t.Fatalf("Unexpected error while OPTIONS %s, got %v", url, err.Error())
	}
	client := s.Client()
	// When
	response, _ := client.Do(req)
	// Then
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("OPTIONS on %v", urlSuffix), t)
	allow := response.Header.Get("Access-Control-Allow-Methods")
	if allow != expectedAllow {
		t.Errorf("allow header is %s, it should be %s", allow, expectedAllow)
	}
}

func TestOptionsOnGame(t *testing.T) {
	// Given
	expectedAllow := "GET, PUT, DELETE, OPTIONS"
	s := getTestServer()
	urlSuffix := "/v2/games/1"
	url := s.URL + urlSuffix
	req, err := http.NewRequest("OPTIONS", url, nil)
	if err != nil {
		t.Fatalf("Unexpected error while OPTIONS %s, got %v", url, err.Error())
	}
	client := s.Client()
	// When
	response, _ := client.Do(req)
	// Then
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("OPTIONS on %v", urlSuffix), t)
	allow := response.Header.Get("Access-Control-Allow-Methods")
	if allow != expectedAllow {
		t.Errorf("allow header is %s, it should be %s", allow, expectedAllow)
	}
}

func game_to_payload(game core.Game) []byte {
	return []byte(fmt.Sprintf(`{
		"editionId":"%d",
		"round":%d,
		"table":%d,
		"played":%t,
		"finale":false,
		"coachId1":"%d",
		"coachId2":"%d"
	}`, game.Edition.ID, game.Round, game.Table, game.Played,
		game.Coach1.ID, game.Coach2.ID))
}

func game_to_extended_map(game core.Game) map[string]interface{} {
	coach1 := map[string]interface{}{
		"name":     game.Coach1.Name,
		"teamName": game.Coach1.TeamName,
		"id":       fmt.Sprint(game.Coach1.ID),
		"faction":  string(game.Coach1.Faction),
		"squadId":  fmt.Sprint(game.Coach1.SquadID),
	}
	if game.Coach1.NafNumber != 0 {
		coach1["nafNumber"] = float64(game.Coach1.NafNumber)
	}
	coach2 := map[string]interface{}{
		"name":     game.Coach2.Name,
		"teamName": game.Coach2.TeamName,
		"id":       fmt.Sprint(game.Coach2.ID),
		"faction":  string(game.Coach2.Faction),
		"squadId":  fmt.Sprint(game.Coach2.SquadID),
	}
	if game.Coach2.NafNumber != 0 {
		coach2["nafNumber"] = float64(game.Coach2.NafNumber)
	}

	return map[string]interface{}{
		"id":        fmt.Sprint(game.ID),
		"editionId": fmt.Sprint(game.Edition.ID),
		"round":     float64(game.Round), "table": float64(game.Table),
		"played": game.Played, "finale": game.Finale,
		"td1": float64(game.Td1), "casualties1": float64(game.Casualties1),
		"completions1": float64(game.Completions1), "fouls1": float64(game.Fouls1),
		"td2": float64(game.Td2), "casualties2": float64(game.Casualties2),
		"completions2": float64(game.Completions2), "fouls2": float64(game.Fouls2),
		"special1": game.Special1, "special2": game.Special2,
		"points1": float64(game.Points1), "points2": float64(game.Points2),
		"coach1": coach1,
		"coach2": coach2,
	}
}

func game_to_extended_payload(game core.Game) string {
	value := fmt.Sprintf(`{"editionId":"%d",`, game.Edition.ID)
	value += fmt.Sprintf(`"round":%d,"table":%d,`, game.Round, game.Table)
	value += fmt.Sprintf(`"played":%t,"finale":false,`, game.Played)
	value += fmt.Sprintf(`"coach1":"%s",`, game.Coach1.Name)
	value += fmt.Sprintf(`"teamName1":"%s","coachId1":"%d",`, game.Coach1.TeamName, game.Coach1.ID)
	value += fmt.Sprintf(`"td1":%d,"casualties1":%d,`, game.Td1, game.Casualties1)
	value += fmt.Sprintf(`"completions1":%d,"fouls1":%d,`, game.Completions1, game.Fouls1)
	value += fmt.Sprintf(`"coach2":"%s",`, game.Coach2.Name)
	value += fmt.Sprintf(`"teamName2":"%s","coachId2":"%d",`, game.Coach2.TeamName, game.Coach2.ID)
	value += fmt.Sprintf(`"td2":%d,"casualties2":%d,`, game.Td2, game.Casualties2)
	value += fmt.Sprintf(`"completions2":%d,"fouls2":%d}`, game.Completions2, game.Fouls2)
	return value
}
