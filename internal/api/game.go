// FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package api

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/trambi/fantasyfootballapi/internal/api/jsonadapter"
)

func (api *API) gameHandlers() routes {
	return routes{
		route{
			"AddGame",
			"POST",
			"/v2/editions/{editionId}/games",
			api.addGameToEdition,
		},
		route{
			"DeleteGame",
			"DELETE",
			"/v2/games/{gameId}",
			api.deleteGame,
		},
		route{
			"GetGamebyID",
			"GET",
			"/v2/games/{gameId}",
			api.getGameById,
		},
		route{
			"GetGamesByEdition",
			"GET",
			"/v2/editions/{editionId}/games",
			api.getGamesByEdition,
		},
		route{
			"UpdateGame",
			"PUT",
			"/v2/games/{gameId}",
			api.updateGame,
		},
		route{
			"GetPlayableGamesByEdition",
			"GET",
			"/v2/editions/{editionId}/playableGames",
			api.getGamesByEditionAndPlayed(false),
		},
		route{
			"GetPlayedGamesByEdition",
			"GET",
			"/v2/editions/{editionId}/playedGames",
			api.getGamesByEditionAndPlayed(true),
		},
		route{
			"GetGamesByEditionAndRound",
			"GET",
			"/v2/editions/{editionId}/rounds/{round}/games",
			api.getGamesByEditionAndRound,
		},
		route{
			"GetPlayableGamesByEdition",
			"GET",
			"/v2/editions/{editionId}/rounds/{round}/playableGames",
			api.getGamesByEditionRoundAndPlayed(false),
		},
		route{
			"GetPlayedGamesByEdition",
			"GET",
			"/v2/editions/{editionId}/rounds/{round}/playedGames",
			api.getGamesByEditionRoundAndPlayed(true),
		},
		route{
			"OptionsOnGame",
			strings.ToUpper("Options"),
			"/v2/games/{gameId}",
			api.optionsOnEntity,
		},
		route{
			"OptionsOnGames",
			strings.ToUpper("Options"),
			"/v2/editions/{editionId}/games",
			api.optionsOnEntities,
		},
	}
}

// addGameToEdition adds a game to a specific edition
func (api *API) addGameToEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Expose-Headers", "Location")

	if r.Body == nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var reader jsonadapter.GameReader
	err = json.Unmarshal(body, &reader)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	editionId, err := extractEditionId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	edition, err := api.Data.GetEdition(editionId)
	if err != nil {
		log.Println("Unable to get edition")
		w.WriteHeader(http.StatusNotFound)
		return
	}
	coachId1, err := stringToUint(reader.CoachId1)
	if err != nil {
		log.Println("Unable to convert coachId1 to proper id")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	coachId2, err := stringToUint(reader.CoachId2)
	if err != nil {
		log.Println("Unable to convert coachId2 to proper id")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	coach1, err := api.Data.GetCoach(coachId1)
	if err != nil {
		log.Println("Unable to get coach1")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	coach2, err := api.Data.GetCoach(coachId2)
	if err != nil {
		log.Println("Unable to get coach2")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	game, err := reader.ToCore(edition, coach1, coach2)
	if err != nil {
		log.Printf("Receive game is invalid: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	err = game.ComputeGamePoints()
	if err != nil {
		log.Printf("Error while compute coach points: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	id, err := api.Data.CreateGame(game)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Println("created ", id)
	location := fmt.Sprintf("/v2/games/%v", id)
	w.Header().Set("Location", location)
	w.WriteHeader(http.StatusCreated)
}

func (api *API) getGameById(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	id, err := extractGameId(r)
	if err != nil {
		log.Print("In getGameById: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	game, err := api.Data.GetGame(id)
	if err != nil {
		log.Print("In getGameById: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	encoded, _ := json.Marshal(jsonadapter.NewGameWriter(game))
	io.Writer.Write(w, encoded)
}

func (api *API) updateGame(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	id, err := extractGameId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetGame(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println("can not read the body: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	var reader jsonadapter.GameReader
	err = json.Unmarshal(body, &reader)
	if err != nil {
		log.Println("error during json unmarshal: ", err.Error(), string(body))
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	coachId1, err := stringToUint(reader.CoachId1)
	if err != nil {
		log.Println("Unable to convert coachId1 to proper id")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	coachId2, err := stringToUint(reader.CoachId2)
	if err != nil {
		log.Println("Unable to convert coachId2 to proper id")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	editionId, err := stringToUint(reader.EditionId)
	if err != nil {
		log.Println("Unable to convert editionId to proper id")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	edition, err := api.Data.GetEdition(editionId)
	if err != nil {
		log.Println("Unable to get edition")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	coach1, err := api.Data.GetCoach(coachId1)
	if err != nil {
		log.Println("Unable to get coach1")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	coach2, err := api.Data.GetCoach(coachId2)
	if err != nil {
		log.Println("Unable to get coach2")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	updatedGame, err := reader.ToCore(edition, coach1, coach2)
	if err != nil {
		log.Printf("Receive game is invalid: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	updatedGame.ID = id
	err = updatedGame.ComputeGamePoints()
	if err != nil {
		log.Printf("Error while compute coach points: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = api.Data.UpdateGame(updatedGame)
	log.Println("Game updated")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func (api *API) deleteGame(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	id, err := extractGameId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetGame(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	err = api.Data.DeleteGame(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func (api *API) getGamesByEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, err := extractEditionId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetEdition(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	games, err := api.Data.GetGamesByEdition(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	adapted := []jsonadapter.GameWriter{}
	for _, game := range games {
		adapted = append(adapted, jsonadapter.NewGameWriter(game))
	}
	encoded, _ := json.Marshal(adapted)
	io.Writer.Write(w, encoded)
}

func (api *API) getGamesByEditionAndRound(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	editionId, err := extractEditionId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetEdition(editionId)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	round, err := extractRound(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	games, err := api.Data.GetGamesByEditionAndRound(editionId, round)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	adapted := []jsonadapter.GameWriter{}
	for _, game := range games {
		adapted = append(adapted, jsonadapter.NewGameWriter(game))
	}
	encoded, _ := json.Marshal(adapted)
	io.Writer.Write(w, encoded)
}

func (api *API) getGamesByEditionAndPlayed(played bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")

		id, err := extractEditionId(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		_, err = api.Data.GetEdition(id)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		games, err := api.Data.GetGamesByEditionAndPlayed(id, played)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusOK)
		adapted := []jsonadapter.GameWriter{}
		for _, game := range games {
			adapted = append(adapted, jsonadapter.NewGameWriter(game))
		}
		encoded, _ := json.Marshal(adapted)
		io.Writer.Write(w, encoded)
	}
}

func (api *API) getGamesByEditionRoundAndPlayed(played bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		id, err := extractEditionId(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		_, err = api.Data.GetEdition(id)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		round, err := extractRound(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		games, err := api.Data.GetGamesByEditionRoundAndPlayed(id, round, played)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusOK)
		adapted := []jsonadapter.GameWriter{}
		for _, game := range games {
			adapted = append(adapted, jsonadapter.NewGameWriter(game))
		}
		encoded, _ := json.Marshal(adapted)
		io.Writer.Write(w, encoded)
	}
}

func extractGameId(r *http.Request) (uint, error) {
	vars := mux.Vars(r)
	inputId, exists := vars["gameId"]
	if !exists {
		log.Println("can not find gameId parameter")
		return 0, fmt.Errorf("can not find gameId parameter")
	}
	id, err := stringToUint(inputId)
	if err != nil {
		log.Printf("can not use gameId %v as integer: %v\n", inputId, err.Error())
		return 0, fmt.Errorf("can not use gameId %v as integer: %v", inputId, err.Error())
	}
	return id, nil
}
