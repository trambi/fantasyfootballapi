// Package api FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package api

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/trambi/fantasyfootballapi/internal/api/jsonadapter"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func (api *API) editionHandlers() routes {
	return routes{
		route{
			"AddEdition",
			strings.ToUpper("Post"),
			"/v2/editions",
			api.addEdition,
		},
		route{
			"DeleteEdition",
			strings.ToUpper("Delete"),
			"/v2/editions/{editionId}",
			api.deleteEdition,
		},
		route{
			"GetCurrentEdition",
			strings.ToUpper("Get"),
			"/v2/editions/current",
			api.getCurrentEdition,
		},
		route{
			"GetEditionbyID",
			strings.ToUpper("Get"),
			"/v2/editions/{editionId}",
			api.getEdition,
		},
		route{
			"GetEditions",
			strings.ToUpper("Get"),
			"/v2/editions",
			api.getEditions,
		},
		route{
			"UpdateEdition",
			strings.ToUpper("Put"),
			"/v2/editions/{editionId}",
			api.updateEdition,
		},
		route{
			"OptionsOnEdition",
			strings.ToUpper("Options"),
			"/v2/editions/{editionId}",
			api.optionsOnEntity,
		},
		route{
			"OptionsOnEditions",
			strings.ToUpper("Options"),
			"/v2/editions",
			api.optionsOnEntities,
		},
	}
}

// addEdition creates an edition
func (api *API) addEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Expose-Headers", "Location")

	if r.Body == nil {
		log.Println("Bad request - body is empty")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println("Bad request - unable to read body:", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var adaptedEdition jsonadapter.EditionReader
	err = json.Unmarshal(body, &adaptedEdition)
	if err != nil {
		log.Println("Bad request - unable to parse body:", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	edition, err := adaptedEdition.ToCore()
	if err != nil {
		log.Println("Bad request - invalid input: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	id, err := api.Data.CreateEdition(edition)
	if err != nil {
		log.Println("Internal error - unable to create edition:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Println("created ", id)
	location := fmt.Sprintf("%v/%v", r.URL.RequestURI(), id)
	w.Header().Set("Location", location)
	w.WriteHeader(http.StatusCreated)
}

// deleteEdition deletes an edition identified by id
func (api *API) deleteEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	id, err := extractEditionId(r)
	if err != nil {
		log.Println("Bad request - unable to extract edition id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	err = api.Data.DeleteEdition(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// getCurrentEdition returns the current edition
func (api *API) getCurrentEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	editions, err := api.Data.GetEditions()
	if err != nil {
		log.Println("Internal error: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(editions) == 0 {
		log.Println("Not found: because there is no edition")
		w.WriteHeader(http.StatusNotFound)
		return
	}
	edition := core.GetCurrentEdition(editions)
	edition, _ = api.Data.GetEdition(edition.ID)
	w.WriteHeader(http.StatusOK)
	encoded, _ := json.Marshal(jsonadapter.NewEditionWriter(edition))
	io.Writer.Write(w, encoded)
}

// getEdition returns the edition identified by id
func (api *API) getEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, err := extractEditionId(r)
	if err != nil {
		log.Println("Bad request - unable to extract edition id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	edition, err := api.Data.GetEdition(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	encoded, _ := json.Marshal(jsonadapter.NewEditionWriter(edition))
	io.Writer.Write(w, encoded)

}

// getEditions returns all editions
func (api *API) getEditions(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	editions, err := api.Data.GetEditions()
	if err != nil {
		log.Println("Internal error: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	adaptedEditions := []jsonadapter.EditionWriter{}
	for _, edition := range editions {
		adaptedEditions = append(adaptedEditions, jsonadapter.NewEditionWriter(edition))
	}
	encoded, _ := json.Marshal(adaptedEditions)
	io.Writer.Write(w, encoded)
}

// updateEdition updates an edition identified by id
func (api *API) updateEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, err := extractEditionId(r)
	if err != nil {
		log.Println("Bad request - unable to extract edition id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetEdition(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println("Bad request - unable to read the body: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var adaptedEdition jsonadapter.EditionReader
	err = json.Unmarshal(body, &adaptedEdition)
	if err != nil {
		log.Println("Bad request - unable to parse the body: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	updatedEdition, err := adaptedEdition.ToCore()
	if err != nil {
		log.Println("Bad request - invalid input: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	updatedEdition.ID = id
	err = api.Data.UpdateEdition(updatedEdition)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func extractEditionId(r *http.Request) (uint, error) {
	vars := mux.Vars(r)
	inputId, exists := vars["editionId"]
	if !exists {
		log.Println("can not find editionId parameter")
		return 0, fmt.Errorf("can not find editionId parameter")
	}
	id, err := stringToUint(inputId)
	if err != nil {
		log.Printf("can not use editionId %v as integer: %v\n", inputId, err.Error())
		return 0, fmt.Errorf("can not use editionId %v as integer: %v", inputId, err.Error())
	}
	return id, nil
}

func extractRound(r *http.Request) (uint, error) {
	vars := mux.Vars(r)
	inputId, exists := vars["round"]
	if !exists {
		log.Println("can not find round parameter")
		return 0, fmt.Errorf("can not find round parameter")
	}
	id, err := stringToUint(inputId)
	if err != nil {
		log.Printf("can not use round %v as integer: %v\n", inputId, err.Error())
		return 0, fmt.Errorf("can not use round %v as integer: %v", inputId, err.Error())
	}
	return id, nil
}
