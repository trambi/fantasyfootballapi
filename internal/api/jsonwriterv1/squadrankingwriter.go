// Package api/jsonwriterv1 FantasyFootballApi
// Copyright (C) 2023-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package jsonwriterv1

import (
	"encoding/json"
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// SquadRankingWriter allow to JSON write in V1 version core.SquadRanking
type SquadRankingWriter struct {
	internal core.SquadRanking
}

func NewSquadRankingWriter(squadRanking core.SquadRanking) SquadRankingWriter {
	return SquadRankingWriter{squadRanking}
}

func (sRanking SquadRankingWriter) MarshalJSON() ([]byte, error) {
	result := [](map[string]interface{}){}
	for _, rank := range sRanking.internal.SquadRanks {
		squadForJson := NewSquadWriter(*rank.Squad)
		jsonSquad, err := json.Marshal(squadForJson)
		if err != nil {
			return []byte{}, fmt.Errorf("error while marshal squad to add extra fields: %s", err)
		}
		mapSquad := map[string]interface{}{}
		err = json.Unmarshal(jsonSquad, &mapSquad)
		if err != nil {
			return []byte{}, fmt.Errorf("error while unmarshal squad to add extra fields: %s", err)
		}
		for i, extraField := range sRanking.internal.ExtraInfoNames {
			mapSquad[extraField] = rank.ExtraInfos[i]
		}
		result = append(result, mapSquad)
	}
	return json.Marshal(result)
}
