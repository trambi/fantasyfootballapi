// Package api/jsonwriterv1 FantasyFootballApi
// Copyright (C) 2023-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package jsonwriterv1

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// SquadWriter allow to write core.Squad in Json for V1
type SquadWriter struct {
	Id      string                `json:"id"`
	Name    string                `json:"name"`
	Members []squadTeamMateWriter `json:"coachTeamMates"`
}

type squadTeamMateWriter struct {
	ID       string `json:"teamId"`
	Name     string `json:"coach"`
	TeamName string `json:"teamName"`
}

func NewSquadWriter(squad core.Squad) SquadWriter {
	teamMates := []squadTeamMateWriter{}
	for _, member := range squad.Members {
		teamMate := squadTeamMateWriter{
			ID:       fmt.Sprintf("%v", member.ID),
			Name:     member.Name,
			TeamName: member.TeamName,
		}
		teamMates = append(teamMates, teamMate)
	}
	return SquadWriter{
		Id:      fmt.Sprintf("%v", squad.ID),
		Name:    squad.Name,
		Members: teamMates,
	}
}
