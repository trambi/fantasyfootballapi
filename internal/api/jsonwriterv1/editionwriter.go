// Package jsonwriterv1 FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.

package jsonwriterv1

import (
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// EditionWriter allow to dissociate Json Marshaling and Unmarshaling from core.Edition
type EditionWriter struct {
	ID                  uint                  `json:"id"`
	Name                string                `json:"name"`
	Day1                string                `json:"day1"`
	Day2                string                `json:"day2"`
	RoundNumber         uint                  `json:"roundNumber"`
	CurrentRound        uint                  `json:"currentRound"`
	FirstDayRound       uint                  `json:"firstDayRound"`
	UseFinale           bool                  `json:"useFinale"`
	FullCoachteam       bool                  `json:"fullCoachteam"`
	RankingStrategyName string                `json:"rankingStrategyName"`
	Ranking             rankingSettingsWriter `json:"rankings,omitempty"`
}

type rankingSettingsWriter struct {
	Coach map[string][]string `json:"coach,omitempty"`
	Squad map[string][]string `json:"coachTeam,omitempty"`
}

func NewEditionWriter(edition core.Edition) EditionWriter {
	coachRankings := map[string][]string{}
	squadRankings := map[string][]string{}

	serializer := EditionWriter{
		ID:                  edition.ID,
		Name:                edition.Name,
		Day1:                edition.Day1,
		Day2:                edition.Day2,
		RoundNumber:         edition.RoundNumber,
		CurrentRound:        edition.CurrentRound,
		FirstDayRound:       edition.FirstDayRound,
		UseFinale:           edition.UseFinale,
		FullCoachteam:       edition.FullSquad,
		RankingStrategyName: edition.RankingStrategyName,
	}
	if len(edition.CoachRankings) != 0 {
		for rankingName, criterias := range edition.CoachRankings {
			labels := []string{}
			for _, criteria := range criterias {
				labels = append(labels, criteria.Label)
			}
			coachRankings[rankingName] = labels
		}
		serializer.Ranking = rankingSettingsWriter{Coach: coachRankings}
	}
	if len(edition.SquadRankings) != 0 {
		for rankingName, criterias := range edition.SquadRankings {
			labels := []string{}
			for _, criteria := range criterias {
				labels = append(labels, criteria.Label)
			}
			squadRankings[rankingName] = labels
		}
		serializer.Ranking.Squad = squadRankings
	}
	return serializer
}
