// Package jsonwriterv1 FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.

package jsonwriterv1

import "gitlab.com/trambi/fantasyfootballapi/internal/core"

// CoachWriter allow to dissociate Json Marshaling and Unmarshaling from core.Coach
type CoachWriter struct {
	ID        uint   `json:"id"`
	Name      string `json:"name"`
	TeamName  string `json:"teamName"`
	Email     string `json:"email"`
	NafNumber uint   `json:"nafNumber,omitempty"`
	Faction   string `json:"raceName"`
	Ready     bool   `json:"ready"`
	SquadName string `json:"coachTeamName,omitempty"`
	SquadId   uint   `json:"coachTeamId,omitempty"`
}

func NewCoachWriter(coach core.Coach) CoachWriter {
	return CoachWriter{ID: coach.ID, Name: coach.Name,
		TeamName: coach.TeamName, Email: "not.yourbusiness@tisseursdechimeres.org",
		NafNumber: coach.NafNumber, Faction: string(coach.Faction),
		Ready: coach.Ready, SquadId: coach.SquadID,
	}
}
