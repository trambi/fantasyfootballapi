// Package jsonwriterv1 FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.

package jsonwriterv1

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func jsonCompact(t *testing.T, toCompact string) string {
	t.Helper()
	compacted := &bytes.Buffer{}
	err := json.Compact(compacted, []byte(toCompact))
	if err != nil {
		t.Fatal("unexpected error while compacting json: ", err.Error())
	}
	return compacted.String()
}

// Helper function to get an edition serialized by getEditionAsJson
func getEdition(t *testing.T) core.Edition {
	t.Helper()
	edition, err := core.NewEdition("edition1", "2023-06-01", 5, 3, true, true, 2, []core.Faction{"Faction 1"}, "something", "orga1")
	if err != nil {
		t.Fatal("unexpected error while constructing edition: ", err.Error())
	}
	edition.ID = 1
	edition.AddCoachTdRanking()
	err = edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error while adding draw clause: ", err.Error())
	}
	err = edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error while adding win clause: ", err.Error())
	}
	err = edition.AddConfrontationPointsClause("1", "{PointsFor}={PointsAgainst}")
	if err != nil {
		t.Fatal("unexpected error while adding draw clause in confrontation: ", err.Error())
	}
	err = edition.AddConfrontationPointsClause("3", "{PointsFor}>{PointsAgainst}")
	if err != nil {
		t.Fatal("unexpected error while adding win clause in confrontation: ", err.Error())
	}
	return edition
}

// Helper function to get JSON of edition given by getEdition
func getEditionAsJson() string {
	return `{
		"id":1,
		"name":"edition1",
		"day1":"2023-06-01","day2":"2023-06-02",
		"roundNumber":5,"currentRound":0,"firstDayRound":3,
		"useFinale":true,
		"fullCoachteam":true,
		"rankingStrategyName":"something",
		"rankings":{
			"coach":{
				"td":["tdFor"]
			},
			"coachTeam":{
				"td":["tdFor"]
			}
		}
	}`
}

func TestEditionJsonWriter(t *testing.T) {
	// Given
	edition := getEdition(t)
	edition.AddSquadTdRanking()
	expected := jsonCompact(t, getEditionAsJson())
	// When
	adapter := NewEditionWriter(edition)
	// Then
	// We want to preserve < and > characters json.marshall escapes these characters
	buf := new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	enc.SetEscapeHTML(false)
	err := enc.Encode(&adapter)
	if err != nil {
		t.Fatal("unexpected error while encoding to json: ", err.Error())
	}
	result := new(bytes.Buffer)
	// We want to compare a compacted json
	err = json.Compact(result, buf.Bytes())
	if err != nil {
		t.Fatal("unexpected error while compacting json: ", err.Error())
	}
	stringResult := result.String()
	if stringResult != expected {
		t.Errorf("%s should be equal to %s: %s", stringResult, expected, cmp.Diff(stringResult, expected))
	}
}
