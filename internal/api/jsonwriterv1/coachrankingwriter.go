// Package api/jsonwriterv1 FantasyFootballApi
// Copyright (C) 2023-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package jsonwriterv1

import (
	"encoding/json"
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// CoachRankingWriter allow to JSON write core.CoachRanking
type CoachRankingWriter struct {
	internal core.CoachRanking
}

func NewCoachRankingWriter(cRanking core.CoachRanking) CoachRankingWriter {
	return CoachRankingWriter{internal: cRanking}
}

func (cRanking CoachRankingWriter) MarshalJSON() ([]byte, error) {
	result := [](map[string]interface{}){}
	for _, rank := range cRanking.internal.CoachRanks {
		coachForJson := NewCoachWriter(*rank.Coach)
		jsonCoach, err := json.Marshal(coachForJson)
		if err != nil {
			return []byte{}, fmt.Errorf("error while marshal coach to add extra fields: %s", err)
		}
		mapCoach := map[string]interface{}{}
		err = json.Unmarshal(jsonCoach, &mapCoach)
		if err != nil {
			return []byte{}, fmt.Errorf("error while unmarshal coach to add extra fields: %s", err)
		}
		for i, extraField := range cRanking.internal.ExtraInfoNames {
			mapCoach[extraField] = rank.ExtraInfos[i]
		}
		result = append(result, mapCoach)
	}
	return json.Marshal(result)
}
