// FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package api

import (
	"encoding/json"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/trambi/fantasyfootballapi/internal/appinfo"
	"gitlab.com/trambi/fantasyfootballapi/internal/logger"
	"gitlab.com/trambi/fantasyfootballapi/internal/storage"
)

const envStorageType = "STORAGE_TYPE"
const envStoragePath = "STORAGE_PATH"

// API contain basically a router and a DataProvider
type API struct {
	Router *mux.Router
	Data   *storage.DataProvider
}

// Initialize API struct
func (api *API) Initialize() {
	api.Router = mux.NewRouter()
	api.Data = storage.InitDataProvider(os.Getenv(envStorageType), os.Getenv(envStoragePath))
	api.initHandlers()
}

type route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type routes []route

func (api *API) initHandlers() {
	var apiRoutes = routes{
		route{
			"Index",
			strings.ToUpper("Get"),
			"/",
			api.index,
		},
	}
	apiRoutes = append(apiRoutes, api.editionHandlers()...)
	apiRoutes = append(apiRoutes, api.coachHandlers()...)
	apiRoutes = append(apiRoutes, api.gameHandlers()...)
	apiRoutes = append(apiRoutes, api.squadHandlers()...)
	apiRoutes = append(apiRoutes, api.rankingHandlers()...)
	apiRoutes = append(apiRoutes, api.deprecatedHandlers()...)
	for _, route := range apiRoutes {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = logger.Logger(handler, route.Name)

		api.Router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}
}

func (api *API) index(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	appInfo := appinfo.GetAppInfo()
	appInfoJSON, _ := json.Marshal(&appInfo)
	w.Write(appInfoJSON)
}

func stringToUint(id string) (uint, error) {
	idAsUint64, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		return 0, err
	}
	return uint(idAsUint64), nil
}

func (api *API) optionsOnEntities(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
	w.WriteHeader(http.StatusNoContent)
}

func (api *API) optionsOnEntity(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
	w.WriteHeader(http.StatusNoContent)
}

func (api *API) optionsOnPatchableEntity(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, DELETE, PATCH, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
	w.WriteHeader(http.StatusNoContent)
}
