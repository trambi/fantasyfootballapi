// FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func coachAsMap(id uint) map[string]any {
	return map[string]any{
		"name":     fmt.Sprintf("Coach %d", id),
		"id":       fmt.Sprint(id),
		"teamName": fmt.Sprintf("Team %d", id),
		"faction":  "Faction1",
		"ready":    true,
	}
}

func TestGetCoachTdRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddCoachTdRanking()
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	expectedRankings := [](map[string]interface{}){
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12",
			"faction":     "Faction1", "id": "1",
			"name": "Coach 1", "ready": true,
			"tdFor": float64(8), "teamName": "Team 1",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12",
			"faction":     "Faction1", "id": "4",
			"name": "Coach 4", "ready": true,
			"tdFor": float64(6), "teamName": "Team 2",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "3",
			"name": "Coach 3", "ready": true,
			"tdFor": float64(4), "teamName": "Team 1",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "2",
			"name": "Coach 2", "ready": true,
			"tdFor": float64(2), "teamName": "Team 2",
		},
	}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(1, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/coachRankings/td", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getCoachTdRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestGetCoachRankingUnknownEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	urlSuffixes := []string{
		"main",
		"td",
		"casualties",
		"defense",
		"completions",
		"fouls",
		"comeback",
	}
	for _, suffix := range urlSuffixes {
		notExistentUrl := s.URL + "/v2/editions/1/coachRankings/" + suffix
		response, _ := client.Get(notExistentUrl)
		checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
	}
}

func TestGetCoachRankingInvalidEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	urlSuffixes := []string{
		"main",
		"td",
		"casualties",
		"defense",
		"completions",
		"fouls",
		"comeback",
	}
	for _, suffix := range urlSuffixes {
		notExistentUrl := s.URL + "/v2/editions/AAAAA/coachRankings/" + suffix
		response, _ := client.Get(notExistentUrl)
		checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
	}
}

func TestGetCoachCasualtiesRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddCoachCasualtiesRanking()
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	expectedRankings := [](map[string]interface{}){
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "1",
			"name": "Coach 1", "ready": true,
			"casualtiesFor": float64(11), "teamName": "Team 1",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "4",
			"name": "Coach 4", "ready": true,
			"casualtiesFor": float64(8), "teamName": "Team 2",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "3",
			"name": "Coach 3", "ready": true,
			"casualtiesFor": float64(5), "teamName": "Team 1",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "2",
			"name": "Coach 2", "ready": true,
			"casualtiesFor": float64(2), "teamName": "Team 2",
		},
	}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetCasualties(1, int(((i-1)*3)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/coachRankings/casualties", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getCoachCasualtiesRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestGetCoachDefenseRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddCoachDefenseRanking()
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(0, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	expectedRankings := [](map[string]interface{}){
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "1",
			"name": "Coach 1", "ready": true,
			"tdAgainst": float64(1), "teamName": "Team 1",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "2",
			"name": "Coach 2", "ready": true,
			"tdAgainst": float64(3), "teamName": "Team 2",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "3",
			"name": "Coach 3", "ready": true,
			"tdAgainst": float64(5), "teamName": "Team 1",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "4",
			"name": "Coach 4", "ready": true,
			"tdAgainst": float64(7), "teamName": "Team 2",
		},
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/coachRankings/defense", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getCoachDefenseRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestGetCoachCompletionsRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.CoachRankings["completions"] = []core.RankingCriteria{
		{Label: "completions", Field: core.Completions, Type: core.For, Descending: true},
	}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetCompletions(1, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
		fmt.Printf("Game %d %s %d-%d %s\n", i, game.Coach1.Name, game.Td1, game.Td2, game.Coach2.Name)
	}
	expectedRankings := [](map[string]interface{}){
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "1",
			"name": "Coach 1", "ready": true,
			"completions": float64(8), "teamName": "Team 1",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "4",
			"name": "Coach 4", "ready": true,
			"completions": float64(6), "teamName": "Team 2",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "3",
			"name": "Coach 3", "ready": true,
			"completions": float64(4), "teamName": "Team 1",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "2",
			"name": "Coach 2", "ready": true,
			"completions": float64(2), "teamName": "Team 2",
		},
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/coachRankings/completions", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getCoachCompletionRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestGetCoachFoulsRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.CoachRankings["fouls"] = []core.RankingCriteria{
		{Label: "fouls", Field: core.Fouls, Type: core.For, Descending: true},
	}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetFouls(1, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
		fmt.Printf("Game %d %s %d-%d %s\n", i, game.Coach1.Name, game.Td1, game.Td2, game.Coach2.Name)
	}
	expectedRankings := [](map[string]interface{}){
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "1",
			"name": "Coach 1", "ready": true,
			"fouls": float64(8), "teamName": "Team 1",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "4",
			"name": "Coach 4", "ready": true,
			"fouls": float64(6), "teamName": "Team 2",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "3",
			"name": "Coach 3", "ready": true,
			"fouls": float64(4), "teamName": "Team 1",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "2",
			"name": "Coach 2", "ready": true,
			"fouls": float64(2), "teamName": "Team 2",
		},
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/coachRankings/fouls", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getCoachFoulRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestGetCoachMainRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddGamePointsClause("1", "{TdFor}+1={TdAgainst}")
	edition.AddGamePointsClause("2", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("5", "{TdFor}>{TdAgainst}")
	edition.AddCoachRankingMain([]core.RankingCriteria{
		{Label: "points", Field: core.Points, Type: core.For, Descending: true},
		{Label: "netTd", Field: core.Td, Type: core.Net, Descending: true},
		{Label: "casualtiesFor", Field: core.Casualties, Type: core.For, Descending: true},
	})
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 6; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	expectedRankings := [](map[string]interface{}){
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "5",
			"name": "Coach 5", "ready": true,
			"points": float64(10), "netTd": float64(3),
			"casualtiesFor": float64(3), "teamName": "Team 1",
		}, {
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "3",
			"name": "Coach 3", "ready": true,
			"points": float64(7), "netTd": float64(1),
			"casualtiesFor": float64(4), "teamName": "Team 1",
		}, {
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "2",
			"name": "Coach 2", "ready": true,
			"points": float64(6), "netTd": float64(0),
			"casualtiesFor": float64(3), "teamName": "Team 2",
		}, {
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "1",
			"name": "Coach 1", "ready": true,
			"points": float64(5), "netTd": float64(-1),
			"casualtiesFor": float64(2), "teamName": "Team 1",
		}, {
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "4",
			"name": "Coach 4", "ready": true,
			"points": float64(3), "netTd": float64(-1),
			"casualtiesFor": float64(3), "teamName": "Team 2",
		}, {
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "6",
			"name": "Coach 6", "ready": true,
			"points": float64(2), "netTd": float64(-2),
			"casualtiesFor": float64(4), "teamName": "Team 2",
		},
	}
	scores := []struct {
		coachIndex1 int
		td1         int
		casualties1 int
		coachIndex2 int
		td2         int
		casualties2 int
	}{
		{0, 1, 1, 1, 0, 2},
		{2, 1, 3, 3, 1, 1},
		{4, 2, 1, 5, 1, 2},
		{0, 1, 1, 4, 3, 2},
		{2, 2, 1, 5, 1, 2},
		{1, 2, 1, 3, 1, 2},
	}
	for i, score := range scores {
		game, err := core.NewGame(1, uint(1+i), coachs[score.coachIndex1], coachs[score.coachIndex2])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(score.td1, score.td2)
		game.SetCasualties(score.casualties1, score.casualties2)
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/coachRankings/main", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getMainCoachRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestGetCoachComebackRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	// To avoid to create game for three rounds
	edition.FirstDayRound = 1
	edition.AddCoachRankingMain([]core.RankingCriteria{
		{Label: "points", Field: core.Points, Type: core.For, Descending: true},
		{Label: "netTd", Field: core.Td, Type: core.Net, Descending: true},
		{Label: "casualtiesFor", Field: core.Casualties, Type: core.For, Descending: true},
	})
	edition.AddGamePointsClause("1", "{TdFor}+1={TdAgainst}")
	edition.AddGamePointsClause("2", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("5", "{TdFor}>{TdAgainst}")
	edition.AddCoachComebackRanking()
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 6; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	expectedRankings := [](map[string]interface{}){
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "2",
			"name": "Coach 2", "ready": true,
			"diffRanking": float64(3), "firstDayRanking": float64(6),
			"finalRanking": float64(3), "teamName": "Team 2",
		}, {
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "3",
			"name": "Coach 3", "ready": true,
			"diffRanking": float64(1), "firstDayRanking": float64(3),
			"finalRanking": float64(2), "teamName": "Team 1",
		},
		{
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "5",
			"name": "Coach 5", "ready": true,
			"diffRanking": float64(0), "firstDayRanking": float64(1),
			"finalRanking": float64(1), "teamName": "Team 1",
		}, {
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "4",
			"name": "Coach 4", "ready": true,
			"diffRanking": float64(-1), "firstDayRanking": float64(4),
			"finalRanking": float64(5), "teamName": "Team 2",
		}, {
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "6",
			"name": "Coach 6", "ready": true,
			"diffRanking": float64(-1), "firstDayRanking": float64(5),
			"finalRanking": float64(6), "teamName": "Team 2",
		}, {
			"editionId":   "1",
			"editionName": "Tournament of 2020-06-12", "faction": "Faction1", "id": "1",
			"name": "Coach 1", "ready": true,
			"diffRanking": float64(-2), "firstDayRanking": float64(2),
			"finalRanking": float64(4), "teamName": "Team 1",
		},
	}
	scores := []struct {
		round       uint
		coachIndex1 int
		td1         int
		casualties1 int
		coachIndex2 int
		td2         int
		casualties2 int
	}{
		{1, 0, 1, 0, 1, 0, 1}, // Coach1 5,1,0 Coach2 1,-1,1
		{1, 2, 1, 3, 3, 1, 1}, // Coach3 2,0,3 Coach4 2,0,1
		{1, 4, 2, 1, 5, 1, 2}, // Coach5 5,1,1 Coach6 1,-1,2
		// Coach5 Coach1 Coach3 Coach4 Coach6 Coach2
		{2, 0, 1, 1, 4, 3, 2}, //Coach1 5,-1,1 Coach5 10,3,3
		{2, 2, 2, 1, 5, 1, 2}, //Coach3 7,1,4  Coach6 2,-2,4
		{2, 1, 2, 1, 3, 1, 2}, //Coach2 6,0,2  Coach4 3,-1,3
		// Coach5 Coach3 Coach2 Coach1 Coach4 Coach6
		// Coach2 3 6 3
		// Coach3 1 3 2
		// Coach5 0 1 1
		// Coach4 -1 4 5
		// Coach6 -1 5 6
		// Coach1 -2 2 4
	}
	for i, score := range scores {
		game, err := core.NewGame(score.round, uint(1+i), coachs[score.coachIndex1], coachs[score.coachIndex2])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(score.td1, score.td2)
		game.SetCasualties(score.casualties1, score.casualties2)
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/coachRankings/comeback", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getMainCoachRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestGetSquadTdRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddSquadTdRanking()
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	fmt.Printf("squad1.ID: %d", lastSquad)
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	fmt.Printf("squad2.ID: %d", lastSquad)
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(1, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":        "1",
			"editionId": "1",
			"name":      "Squad 1",
			"tdFor":     float64(12),
			"teamMates": []any{
				coachAsMap(1), coachAsMap(3),
			},
		},
		{
			"id":        "2",
			"editionId": "1",
			"name":      "Squad 2",
			"tdFor":     float64(8),
			"teamMates": []any{
				coachAsMap(2), coachAsMap(4),
			},
		},
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/squadRankings/td", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getSquadTdRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestGetSquadCasualtiesRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddSquadCasualtiesRanking()
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	fmt.Printf("squad1.ID: %d", lastSquad)
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	fmt.Printf("squad2.ID: %d", lastSquad)
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetCasualties(1, int(((i-1)*3)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":            "1",
			"editionId":     "1",
			"name":          "Squad 1",
			"casualtiesFor": float64(16),
			"teamMates": []any{
				coachAsMap(1), coachAsMap(3),
			},
		},
		{
			"id":            "2",
			"editionId":     "1",
			"name":          "Squad 2",
			"casualtiesFor": float64(10),
			"teamMates": []any{
				coachAsMap(2), coachAsMap(4),
			},
		},
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/squadRankings/casualties", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getSquadCasualtiesRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestGetSquadDefenseRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddSquadDefenseRanking()
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	fmt.Printf("squad1.ID: %d", lastSquad)
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	fmt.Printf("squad2.ID: %d", lastSquad)
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(0, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":        "1",
			"editionId": "1",
			"name":      "Squad 1",
			"tdAgainst": float64(6),
			"teamMates": []any{
				coachAsMap(1), coachAsMap(3),
			},
		},
		{
			"id":        "2",
			"editionId": "1",
			"name":      "Squad 2",
			"tdAgainst": float64(10),
			"teamMates": []any{
				coachAsMap(2), coachAsMap(4),
			},
		},
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/squadRankings/defense", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getSquadDefenseRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestGetSquadCompletionsRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddSquadCompletionsRanking()
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	fmt.Printf("squad1.ID: %d", lastSquad)
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	fmt.Printf("squad2.ID: %d", lastSquad)
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetCompletions(1, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":          "1",
			"editionId":   "1",
			"name":        "Squad 1",
			"completions": float64(12),
			"teamMates": []any{
				coachAsMap(1), coachAsMap(3),
			},
		},
		{
			"id":          "2",
			"editionId":   "1",
			"name":        "Squad 2",
			"completions": float64(8),
			"teamMates": []any{
				coachAsMap(2), coachAsMap(4),
			},
		},
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/squadRankings/completions", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getSquadCompletionRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestGetSquadFoulsRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddSquadFoulsRanking()
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	fmt.Printf("squad1.ID: %d", lastSquad)
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	fmt.Printf("squad2.ID: %d", lastSquad)
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetFouls(1, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":        "1",
			"editionId": "1",
			"name":      "Squad 1",
			"fouls":     float64(12),
			"teamMates": []any{
				coachAsMap(1), coachAsMap(3),
			},
		},
		{
			"id":        "2",
			"editionId": "1",
			"name":      "Squad 2",
			"fouls":     float64(8),
			"teamMates": []any{
				coachAsMap(2), coachAsMap(4),
			},
		},
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/squadRankings/fouls", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getSquadFoulRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestGetSquadBasicMainRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddSquadMainRanking([]core.RankingCriteria{
		{Label: "points", Field: core.Points, Type: core.For, Descending: true},
		{Label: "netTd", Field: core.Td, Type: core.Net, Descending: true},
	})
	edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(0, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
		fmt.Printf("Game %d %s vs %s: %d - %d\n", i, game.Coach1.Name, game.Coach2.Name, game.Td1, game.Td2)
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":        "1",
			"finale":    float64(0),
			"editionId": "1",
			"name":      "Squad 1",
			"points":    float64(6),
			"netTd":     float64(4),
			"teamMates": []any{
				coachAsMap(1), coachAsMap(3),
			},
		},
		{
			"id":        "2",
			"finale":    float64(0),
			"editionId": "1",
			"name":      "Squad 2",
			"points":    float64(6),
			"netTd":     float64(-4),
			"teamMates": []any{
				coachAsMap(2), coachAsMap(4),
			},
		},
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/squadRankings/main", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getSquadBasicMainRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func ATestGetSquadMainRankingWithConfrontation(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddSquadMainRanking([]core.RankingCriteria{
		{Label: "points", Field: core.Points, Type: core.For, Descending: true},
		{Label: "netTd", Field: core.Td, Type: core.Net, Descending: true},
	})
	edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	edition.AddConfrontationPointsClause("1", "{PointsFor}={PointsAgainst} and {TdFor}={TdAgainst}")
	edition.AddConfrontationPointsClause("3", "{PointsFor}={PointsAgainst} and {TdFor}>{TdAgainst}")
	edition.AddConfrontationPointsClause("3", "{PointsFor}>{PointsAgainst}")
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(0, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+editionUrl+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
		fmt.Printf("Game %d %s vs %s: %d - %d\n", i, game.Coach1.Name, game.Coach2.Name, game.Td1, game.Td2)
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":        "1",
			"finale":    float64(0),
			"editionId": "1",
			"name":      "Squad 1",
			"points":    float64(3),
			"netTd":     float64(4),
			"teamMates": []any{
				coachAsMap(1), coachAsMap(3),
			},
		},
		{
			"id":        "2",
			"finale":    float64(0),
			"editionId": "1",
			"name":      "Squad 2",
			"points":    float64(3),
			"netTd":     float64(-4),
			"teamMates": []any{
				coachAsMap(2), coachAsMap(4),
			},
		},
	}
	// When
	body := testGetOk(client, s.URL+editionUrl+"/squadRankings/main", t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getSquadMainRankingWithConfrontation is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}
