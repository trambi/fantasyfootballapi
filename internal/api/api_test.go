// FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package api

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

func contentType() string {
	return "application/json"
}

func getTestServer() *httptest.Server {
	os.Setenv(envStoragePath, ":memory:")
	api := API{}
	api.Initialize()
	s := httptest.NewServer(api.Router)
	return s
}

func TestIndex(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	url := s.URL + "/"
	response, _ := client.Get(url)
	checkResponseCode(http.StatusOK, response.StatusCode, fmt.Sprintf("GET on %v", url), t)
}

func postPayloadTo(client *http.Client, payload []byte, path string, t *testing.T) string {
	t.Helper()
	response, _ := client.Post(path, contentType(), bytes.NewBuffer(payload))
	checkResponseCode(http.StatusCreated, response.StatusCode, fmt.Sprintf("POST on %v", path), t)
	var body []byte
	response.Body.Read(body)
	if len(body) != 0 {
		t.Errorf("expected an empty string. Got |%s|", body)
	}
	location := response.Header.Get("Location")
	if location == "" {
		t.Error("expected location header. Got empty")
	}
	return location
}

func checkResponseCode(expected int, actual int, testName string, t *testing.T) {
	t.Helper()
	if expected != actual {
		t.Errorf("For %v expected response code %d. Got %d\n", testName, expected, actual)
	}
}

func testPostWithoutBody(client *http.Client, url string, t *testing.T) {
	t.Helper()
	response, err := client.Post(url, contentType(), bytes.NewBuffer([]byte{}))
	if err != nil {
		t.Fatalf("Error: %v", err)
	}
	if response == nil {
		t.Fatal("Response is nil !")
	}
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("POST without body on %v", url), t)
}

func testPostWithInvalidJson(client *http.Client, url string, t *testing.T) {
	t.Helper()
	response, _ := client.Post(url, contentType(), bytes.NewBuffer([]byte("{Not a JSON]")))
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("POST with invalid JSON on %v", url), t)
}

func testPostWithUnknownJson(client *http.Client, url string, t *testing.T) {
	t.Helper()
	response, _ := client.Post(url, contentType(), bytes.NewBuffer([]byte(`{"error":"This a JSON but not an excepted object"}`)))
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("POST with unknown JSON on %v", url), t)
}

func testGetOk(client *http.Client, url string, t *testing.T) []byte {
	t.Helper()
	response, err := client.Get(url)
	if err != nil {
		t.Fatalf("Unexpected error while GET %s, got %v", url, err.Error())
	}
	checkResponseCode(http.StatusOK, response.StatusCode, fmt.Sprintf("GET on %v", url), t)
	var body []byte
	body, err = io.ReadAll(response.Body)
	if err != nil {
		t.Fatalf("unexpected error reading body while GET %s: %v", url, err)
	}
	return body
}

func testDeleteOk(client *http.Client, url string, t *testing.T) {
	t.Helper()
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		t.Fatalf("Unexpected error while DELETE %s, got %v", url, err.Error())
	}
	response, _ := client.Do(req)
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("DELETE on %v", url), t)
	_, err = io.ReadAll(response.Body)
	if err != nil {
		t.Fatalf("unexpected error reading body while DELETE %s: %v", url, err)
	}
}

func extractIdFromUrl(url string) (uint, error) {
	splittedUrl := strings.Split(url, "/")
	if splittedUrl == nil {
		return 0, errors.New("unable to split url with /")
	}
	if len(splittedUrl) == 0 {
		return 0, errors.New("unable to extract last part of url")
	}
	return stringToUint(splittedUrl[len(splittedUrl)-1])
}
