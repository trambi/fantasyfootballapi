// Package api FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package api

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/trambi/fantasyfootballapi/internal/api/jsonadapter"
)

func (api *API) coachHandlers() routes {
	return routes{
		route{
			"AddCoachToEdition",
			strings.ToUpper("Post"),
			"/v2/editions/{editionId}/coachs",
			api.addCoachToEdition,
		},
		route{
			"DeleteCoach",
			strings.ToUpper("Delete"),
			"/v2/coachs/{coachId}",
			api.deleteCoach,
		},
		route{
			"FindCoachsByEdition",
			strings.ToUpper("Get"),
			"/v2/editions/{editionId}/coachs",
			api.findCoachsByEdition,
		},
		route{
			"GetCoach",
			strings.ToUpper("Get"),
			"/v2/coachs/{coachId}",
			api.getCoach,
		},
		route{
			"UpdateCoach",
			strings.ToUpper("Put"),
			"/v2/coachs/{coachId}",
			api.updateCoach,
		},
		route{
			"PatchCoach",
			strings.ToUpper("Patch"),
			"/v2/coachs/{coachId}",
			api.patchCoach,
		},
		route{
			"GetGamesByCoach",
			"GET",
			"/v2/coachs/{coachId}/games",
			api.getGamesByCoach,
		},
		route{
			"OptionsCoach",
			strings.ToUpper("Options"),
			"/v2/coachs/{coachId}",
			api.optionsOnPatchableEntity,
		},
		route{
			"OptionsCoachsByEdition",
			strings.ToUpper("Options"),
			"/v2/editions/{editionId}/coachs",
			api.optionsOnEntities,
		},
	}
}

// addCoachToEdition adds a coach to a specific edition
func (api *API) addCoachToEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Expose-Headers", "Location")
	if r.Body == nil {
		log.Println("Bad request - empty body")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println(err.Error())
		log.Println("Bad request: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	editionId, err := extractEditionId(r)
	if err != nil {
		log.Println("Bad request: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	edition, err := api.Data.GetEdition(editionId)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	var reader jsonadapter.CoachReader
	err = json.Unmarshal(body, &reader)
	if err != nil {
		log.Println("Bad request: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	coach, err := reader.ToCore(edition)
	if err != nil {
		log.Println("Bad request - invalid coach: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	id, err := api.Data.CreateCoach(coach)
	if err != nil {
		log.Println("Internal error: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Println("created ", id)
	location := fmt.Sprintf("/v2/coachs/%v", id)
	w.Header().Set("Location", location)
	w.WriteHeader(http.StatusCreated)
}

// deleteCoach deletes a coach identified by id
func (api *API) deleteCoach(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, err := extractCoachId(r)
	if err != nil {
		log.Println("Bad request: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	err = api.Data.DeleteCoach(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusNoContent)

}

func extractCoachId(r *http.Request) (uint, error) {
	vars := mux.Vars(r)
	inputId, exists := vars["coachId"]
	if !exists {
		log.Println("can not find coachId parameter")
		return 0, fmt.Errorf("can not find coachId parameter")
	}
	id, err := stringToUint(inputId)
	if err != nil {
		log.Printf("can not use coachId %v as integer: %v\n", inputId, err.Error())
		return 0, fmt.Errorf("can not use coachId %v as integer: %v", inputId, err.Error())
	}
	return id, nil
}

func (api *API) findCoachsByEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractEditionId(r)
	if err != nil {
		log.Println("Bad request: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetEdition(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	coachs, err := api.Data.GetCoachsByEdition(id)
	if err != nil {
		log.Println("Internal server: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	adaptedCoachs := []jsonadapter.CoachWriter{}
	for _, coach := range coachs {
		adaptedCoachs = append(adaptedCoachs, jsonadapter.NewCoachWriter(coach))
	}
	encoded, _ := json.Marshal(adaptedCoachs)
	io.Writer.Write(w, encoded)
}

func (api *API) getCoach(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractCoachId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	coach, err := api.Data.GetCoach(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	encoded, _ := json.Marshal(jsonadapter.NewCoachWriter(coach))
	io.Writer.Write(w, encoded)
}

// updateCoach update a coach
func (api *API) updateCoach(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractCoachId(r)
	if err != nil {
		log.Println("Bad request: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	coachPreviousVersion, err := api.Data.GetCoach(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println("Bad request can not read the body: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var reader jsonadapter.CoachReader
	err = json.Unmarshal(body, &reader)
	if err != nil {
		log.Println("Bad request: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	updatedCoach, err := reader.ToCore(coachPreviousVersion.Edition)
	if err != nil {
		log.Println("Bad request - invalid coach: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	// We get the id from the url
	updatedCoach.ID = id
	// We get the SquadID from the previous version of coach
	updatedCoach.SquadID = coachPreviousVersion.SquadID
	err = api.Data.UpdateCoach(updatedCoach)
	if err != nil {
		log.Println("Internal error: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// patchCoach update ready field of a coach
func (api *API) patchCoach(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractCoachId(r)
	if err != nil {
		log.Println("Bad request: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetCoach(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println("Bad request can not read the body: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var reader jsonadapter.ReadyReader
	err = json.Unmarshal(body, &reader)
	if err != nil {
		log.Println("Bad request: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	err = api.Data.UpdateCoachReadiness(id, reader.Ready)
	if err != nil {
		log.Println("Internal error: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func (api *API) getGamesByCoach(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractCoachId(r)
	if err != nil {
		log.Println("Bad request: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetCoach(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	games, err := api.Data.GetGamesByCoach(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	adapted := []jsonadapter.GameWriter{}
	for _, game := range games {
		if game.Coach2.ID == id {
			adapted = append(adapted, jsonadapter.NewGameWriter(game.Reverse()))
		} else {
			adapted = append(adapted, jsonadapter.NewGameWriter(game))
		}

	}
	encoded, _ := json.Marshal(adapted)
	io.Writer.Write(w, encoded)
}
