// FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package api

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/trambi/fantasyfootballapi/internal/api/jsonadapter"
)

func (api *API) rankingHandlers() routes {
	return routes{
		route{
			"GetCoachRankingByEdition",
			"GET",
			"/v2/editions/{editionId}/coachRankings/{type}",
			api.getCoachRankingByEdition,
		},
		route{
			"GetSquadRankingByEdition",
			"GET",
			"/v2/editions/{editionId}/squadRankings/{type}",
			api.getSquadRankingByEdition,
		},
	}
}

func (api *API) getCoachRankingByEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractEditionId(r)
	if err != nil {
		log.Println("bad request in getCoachRankingByEdition: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	rankingType := mux.Vars(r)["type"]
	edition, err := api.Data.GetEdition(id)
	if err != nil {
		log.Println("Not found in getCoachRankingByEdition: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	games, err := api.Data.GetGamesByEdition(id)
	if err != nil {
		log.Println("Not found in getCoachRankingByEdition: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	ranking, err := edition.RankCoach(strings.ToLower(rankingType), games)
	if err != nil {
		log.Println("error while RankCoach: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	serializer := jsonadapter.NewCoachRankingWriter(ranking)
	encoded, _ := json.Marshal(serializer)

	io.Writer.Write(w, encoded)
	w.WriteHeader(http.StatusOK)
}

func (api *API) getSquadRankingByEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractEditionId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	rankingType := mux.Vars(r)["type"]
	edition, err := api.Data.GetEdition(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	games, _ := api.Data.GetGamesByEdition(id)
	squads, _ := api.Data.GetSquadsByEdition(id)
	ranking, err := edition.RankSquad(strings.ToLower(rankingType), games, squads)
	if err != nil {
		log.Println("error while RankSquad: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	serializer := jsonadapter.NewSquadRankingWriter(ranking)
	encoded, _ := json.Marshal(serializer)

	io.Writer.Write(w, encoded)
}
