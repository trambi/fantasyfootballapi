// FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/fantasyfootballapi/internal/api/jsonadapter"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func postCoach(t *testing.T, client *http.Client, coach core.Coach, url string) string {
	t.Helper()
	editionSerializer := jsonadapter.NewCoachWriter(coach)
	payload, err := json.Marshal(editionSerializer)
	if err != nil {
		t.Fatal("unexpected error while serializing edition json: ", err.Error())
	}
	return postPayloadTo(client, payload, url+"/coachs", t)
}

func TestCoachInvalidInput(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	path := s.URL + editionUrl + "/coachs"
	testPostWithoutBody(client, path, t)
	testPostWithInvalidJson(client, path, t)
	testPostWithUnknownJson(client, path, t)
}

func TestCoachNotFound(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/coachs/10453"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
	req, _ := http.NewRequest("PUT", notExistentUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("POST on %v", notExistentUrl), t)
	req, _ = http.NewRequest("DELETE", notExistentUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("DELETE on %v", notExistentUrl), t)
	notExistentUrl = notExistentUrl + "/games"
	response, _ = client.Get(notExistentUrl)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
}

func TestCoachInvalidId(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	invalidUrl := s.URL + "/v2/coachs/AAA"
	response, _ := client.Get(invalidUrl)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("GET on %v", invalidUrl), t)
	req, _ := http.NewRequest("PUT", invalidUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("PUT on %v", invalidUrl), t)
	req, _ = http.NewRequest("DELETE", invalidUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("DELETE on %v", invalidUrl), t)
	invalidUrl = invalidUrl + "/games"
	response, _ = client.Get(invalidUrl)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("GET on %v", invalidUrl), t)
}

func TestGetCoachsUnknownEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/editions/102454/coachs"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
}

func TestGetCoachsInvalidEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/editions/AAAAA/coachs"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
}

func TestGetCoachs(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	coach1 := core.Coach{
		Name:      "Coach1",
		TeamName:  "Team1",
		NafNumber: 123,
		Faction:   faction,
		Ready:     true,
		Edition:   edition}
	coach2 := core.Coach{
		Name:      "Coach2",
		TeamName:  "Team2",
		NafNumber: 456,
		Faction:   faction,
		Ready:     true,
		Edition:   edition}
	url1 := postCoach(t, client, coach1, s.URL+editionUrl)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url1, t)
	})
	url2 := postCoach(t, client, coach2, s.URL+editionUrl)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url2, t)
	})
	// When
	body := testGetOk(client, s.URL+editionUrl+"/coachs", t)
	// Then
	var coachsSerializer []jsonadapter.CoachWriter
	err := json.Unmarshal(body, &coachsSerializer)
	if err != nil {
		t.Log("rawCoachs: ", body)
		t.Error("Error in response body parsing")
	}
	if len(coachsSerializer) < 2 {
		t.Errorf("length of getCoachs is lower than 2: %d", len(coachsSerializer))
	}
}

func TestOptionsCoachsByEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	coach1 := core.Coach{
		Name:      "Coach1",
		TeamName:  "Team1",
		NafNumber: 123,
		Faction:   faction,
		Ready:     true,
		Edition:   edition}
	coach2 := core.Coach{
		Name:      "Coach2",
		TeamName:  "Team2",
		NafNumber: 456,
		Faction:   faction,
		Ready:     true,
		Edition:   edition}
	url1 := postCoach(t, client, coach1, s.URL+editionUrl)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url1, t)
	})
	url2 := postCoach(t, client, coach2, s.URL+editionUrl)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url2, t)
	})
	expectedAllow := "GET, POST, OPTIONS"
	req, err := http.NewRequest("OPTIONS", s.URL+editionUrl+"/coachs", nil)
	if err != nil {
		t.Fatalf("Unexpected error while OPTIONS %s, got %v", s.URL+editionUrl+"/coachs", err.Error())
	}
	// When
	response, _ := client.Do(req)
	// Then
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("OPTIONS on %v", s.URL+editionUrl+"/coachs"), t)
	allow := response.Header.Get("Access-Control-Allow-Methods")
	if allow != expectedAllow {
		t.Errorf("allow header is %s, it should be %s", allow, expectedAllow)
	}
}

func TestCreateCoachWithoutAllowedTeam(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction1 := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction1}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	coach1 := core.Coach{
		Name:      "Coach1",
		TeamName:  "Team1",
		NafNumber: 123,
		Faction:   core.Faction("NotAllowedFaction"),
		Ready:     true,
		Edition:   edition}
	coachSerializer := jsonadapter.NewCoachWriter(coach1)
	payload, err := json.Marshal(coachSerializer)
	if err != nil {
		t.Fatal("unexpected error while serializing coach in json: ", err.Error())
	}
	response, _ := client.Post(s.URL+editionUrl+"/coachs", contentType(), bytes.NewBuffer(payload))
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("POST on %v", s.URL+editionUrl+"/coachs"), t)
}

func TestCreateCoachWithInvalidEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	coach1 := core.Coach{
		Name:      "Coach1",
		TeamName:  "Team1",
		NafNumber: 123,
		Faction:   core.Faction("NotAllowedFaction"),
		Ready:     true,
		Edition:   createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))}
	coachSerializer := jsonadapter.NewCoachWriter(coach1)
	payload, err := json.Marshal(coachSerializer)
	if err != nil {
		t.Fatal("unexpected error while serializing coach in json: ", err.Error())
	}
	response, _ := client.Post(s.URL+"/v2/editions/AAAA/coachs", contentType(), bytes.NewBuffer(payload))
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("POST on %v", "/editions/2565/coachs"), t)
}

func TestCreateCoachWithUnknownEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	coach1 := core.Coach{
		Name:      "Coach1",
		TeamName:  "Team1",
		NafNumber: 123,
		Faction:   core.Faction("NotAllowedFaction"),
		Ready:     true,
		Edition:   createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))}
	coachSerializer := jsonadapter.NewCoachWriter(coach1)
	payload, err := json.Marshal(coachSerializer)
	if err != nil {
		t.Fatal("unexpected error while serializing coach in json: ", err.Error())
	}
	response, _ := client.Post(s.URL+"/editions/2565/coachs", contentType(), bytes.NewBuffer(payload))
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("POST on %v", "/editions/2565/coachs"), t)
}

func TestUpdateCoach(t *testing.T) {
	// GIVEN
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	coach1 := core.Coach{
		Name:      "Coach1",
		TeamName:  "Team1",
		NafNumber: 123,
		Faction:   faction,
		Ready:     true,
		Edition:   edition}
	url := postCoach(t, client, coach1, s.URL+editionUrl)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url, t)
	})
	tempId, err := strconv.ParseUint(strings.Split(url, "/")[3], 10, 64)
	if err != nil {
		t.Error("Unexpected error while extracting id from location", err.Error())
	}
	coach1.ID = uint(tempId)
	coach1.NafNumber = 321
	coachSerializer := jsonadapter.NewCoachWriter(coach1)
	payload, err := json.Marshal(coachSerializer)
	if err != nil {
		t.Fatal("unexpected error while serializing coach in json: ", err.Error())
	}
	// WHEN
	req, _ := http.NewRequest("PUT", s.URL+url, bytes.NewBuffer(payload))
	// THEN
	response, _ := client.Do(req)
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("PUT on %v", url), t)
	_, err = io.ReadAll(response.Body)
	if err != nil {
		t.Fatalf("Unexpected error while reading body of PUT: %s", err.Error())
	}
	var reader jsonadapter.CoachReader
	body := testGetOk(client, s.URL+url, t)
	fmt.Printf("body: %v\n", body)
	err = json.Unmarshal(body, &reader)
	if err != nil {
		t.Log("raw JSON: ", body)
		t.Fatal("Error in response body parsing")
	}
	retrievedCoach, err := reader.ToCore(edition)
	if err != nil {
		t.Errorf("Unexpected error while deserializing coach from json: %s", err.Error())
	}
	// coachJsonReader is not done to output of the API
	// it ignore the ID so we have to fix the ID
	retrievedCoach.ID = coach1.ID
	if !cmp.Equal(retrievedCoach, coach1) {
		t.Errorf("retrieved coach %v should be equal to %v: %v", retrievedCoach, coach1, cmp.Diff(retrievedCoach, coach1))
	}
}

func TestPatchCoach(t *testing.T) {
	// GIVEN
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	coach1 := core.Coach{
		Name:      "Coach1",
		TeamName:  "Team1",
		NafNumber: 123,
		Faction:   faction,
		Ready:     true,
		Edition:   edition}
	url := postCoach(t, client, coach1, s.URL+editionUrl)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url, t)
	})
	tempId, err := strconv.ParseUint(strings.Split(url, "/")[3], 10, 64)
	if err != nil {
		t.Error("Unexpected error while extracting id from location", err.Error())
	}
	// To set the coach1 as expected
	coach1.ID = uint(tempId)
	coach1.Ready = false

	payload := []byte(`{"ready":false}`)
	// WHEN
	req, _ := http.NewRequest("PATCH", s.URL+url, bytes.NewBuffer(payload))
	// THEN
	response, _ := client.Do(req)
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("PATCH on %v", url), t)
	_, err = io.ReadAll(response.Body)
	if err != nil {
		t.Fatalf("Unexpected error while reading body of PATCH: %s", err.Error())
	}
	var reader jsonadapter.CoachReader
	body := testGetOk(client, s.URL+url, t)
	fmt.Printf("body: %v\n", body)
	err = json.Unmarshal(body, &reader)
	if err != nil {
		t.Log("raw JSON: ", body)
		t.Fatal("Error in response body parsing")
	}
	retrievedCoach, err := reader.ToCore(edition)
	if err != nil {
		t.Errorf("Unexpected error while deserializing coach from json: %s", err.Error())
	}
	// coachJsonReader is not done to output of the API
	// it ignore the ID so we have to fix the ID
	retrievedCoach.ID = coach1.ID
	if !cmp.Equal(retrievedCoach, coach1) {
		t.Errorf("retrieved coach %v should be equal to %v: %v", retrievedCoach, coach1, cmp.Diff(retrievedCoach, coach1))
	}
}

func TestUpdateCoachWithSquad(t *testing.T) {
	// GIVEN
	s := getTestServer()
	client := s.Client()
	// Add Edition
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	// Add Coach
	coach := core.Coach{
		Name:      "Coach1",
		TeamName:  "Team1",
		NafNumber: 123,
		Faction:   faction,
		Ready:     false,
		Edition:   edition}
	url := postCoach(t, client, coach, s.URL+editionUrl)
	tempId, err := extractIdFromUrl(url)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url, t)
	})
	if err != nil {
		t.Error("Unexpected error while extracting id from location", err.Error())
	}
	coach.ID = uint(tempId)
	// Add Squad
	squad, err := core.NewSquad("squad 1", edition, []core.Coach{coach})
	if err != nil {
		t.Fatal("unexpected error in core.NewSquad: ", err)
	}
	payload := squad_to_payload(squad)
	urlSquad := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	tempId, err = extractIdFromUrl(urlSquad)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad, t)
	})
	squad.ID = tempId
	coach.SquadID = squad.ID
	// Modify coach
	coach.NafNumber = 321
	coach.Ready = true
	coachSerializer := jsonadapter.NewCoachWriter(coach)
	payload, err = json.Marshal(coachSerializer)
	if err != nil {
		t.Fatal("unexpected error while serializing coach in json: ", err.Error())
	}
	expectedBody := `{"id":"1","name":"Coach1","teamName":"Team1","nafNumber":321,"faction":"Faction1","ready":true,"editionName":"Tournament of 2020-06-12","editionId":"1","squadId":"1"}`
	// WHEN
	req, _ := http.NewRequest("PUT", s.URL+url, bytes.NewBuffer(payload))
	// THEN
	response, _ := client.Do(req)
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("PUT on %v", url), t)
	_, err = io.ReadAll(response.Body)
	if err != nil {
		t.Fatalf("Unexpected error while reading body of PUT: %s", err.Error())
	}
	body := string(testGetOk(client, s.URL+url, t))
	if !cmp.Equal(body, expectedBody) {
		t.Errorf("retrieved coach %v should be equal to %v: %v", body, expectedBody, cmp.Diff(body, expectedBody))
	}
}

func TestUpdateCoachWithoutAllowedTeam(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	coach1 := core.Coach{
		Name:      "Coach1",
		TeamName:  "Team1",
		NafNumber: 123,
		Faction:   faction,
		Ready:     true,
		Edition:   edition}

	url := postCoach(t, client, coach1, s.URL+editionUrl)

	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url, t)
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := strconv.ParseUint(strings.Split(url, "/")[3], 10, 64)
	if err != nil {
		t.Error("Unexpected error while extracting id from location", err.Error())
	}
	coach1.ID = uint(tempId)
	coach1.NafNumber = 321
	coach1.Faction = core.Faction("NotAllowedFaction")
	payload, _ := json.Marshal(coach1)
	req, _ := http.NewRequest("PUT", s.URL+url, bytes.NewBuffer(payload))
	response, _ := client.Do(req)

	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("PUT on %v", url), t)
}

func TestOptionsOnCoach(t *testing.T) {
	// Given
	expectedAllow := "GET, PUT, DELETE, PATCH, OPTIONS"
	s := getTestServer()
	urlSuffix := "/v2/coachs/1"
	url := s.URL + urlSuffix
	req, err := http.NewRequest("OPTIONS", url, nil)
	if err != nil {
		t.Fatalf("Unexpected error while OPTIONS %s, got %v", url, err.Error())
	}
	client := s.Client()
	// When
	response, _ := client.Do(req)
	// Then
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("OPTIONS on %v", urlSuffix), t)
	allow := response.Header.Get("Access-Control-Allow-Methods")
	if allow != expectedAllow {
		t.Errorf("allow header is %s, it should be %s", allow, expectedAllow)
	}
}

func TestOptionsOnCoachs(t *testing.T) {
	// Given
	expectedAllow := "GET, POST, OPTIONS"
	s := getTestServer()
	urlSuffix := "/v2/editions/1/coachs"
	url := s.URL + urlSuffix
	req, err := http.NewRequest("OPTIONS", url, nil)
	if err != nil {
		t.Fatalf("Unexpected error while OPTIONS %s, got %v", url, err.Error())
	}
	client := s.Client()
	// When
	response, _ := client.Do(req)
	// Then
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("OPTIONS on %v", urlSuffix), t)
	allow := response.Header.Get("Access-Control-Allow-Methods")
	if allow != expectedAllow {
		t.Errorf("allow header is %s, it should be %s", allow, expectedAllow)
	}
}
