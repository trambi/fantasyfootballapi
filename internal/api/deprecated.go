// Package api FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package api

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/trambi/fantasyfootballapi/internal/api/jsonwriterv1"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func (api *API) deprecatedHandlers() routes {
	return routes{
		route{
			"Deprecated GetCurrentEdition",
			"GET",
			"/v1/Edition/current",
			deprecateEndpoint(api.getCurrentEditionV1),
		},
		route{
			"Deprecated GetEditions",
			"GET",
			"/v1/Editions",
			deprecateEndpoint(api.getEditionsV1),
		},
		route{
			"Deprecated GetEdition",
			"GET",
			"/v1/Edition/{editionId}",
			deprecateEndpoint(api.getEditionV1),
		},
		route{
			"Deprecated findCoachsByEdition",
			"GET",
			"/v1/Coachs/{editionId}",
			deprecateEndpoint(api.findCoachsV1ByEdition),
		},
		route{
			"deprecated getCoach",
			"GET",
			"/v1/Coach/{coachId}",
			deprecateEndpoint(api.getCoachV1),
		},
		route{
			"Deprecated findGamesByEdition",
			"GET",
			"/v1/MatchList/{editionId}",
			deprecateEndpoint(api.getGamesV1ByEdition),
		},
		route{
			"Deprecated findPlayableGamesByEdition",
			"GET",
			"/v1/ToPlayMatchList/{editionId}",
			deprecateEndpoint(api.getGamesV1ByEditionAndPlayed(false)),
		},
		route{
			"Deprecated findPlayedGamesByEdition",
			"GET",
			"/v1/PlayedMatchList/{editionId}",
			deprecateEndpoint(api.getGamesV1ByEditionAndPlayed(true)),
		},
		route{
			"Deprecated GetGamesByEditionAndRound",
			"GET",
			"/v1/MatchList/{editionId}/{round}",
			deprecateEndpoint(api.getGamesV1ByEditionAndRound),
		},
		route{
			"Deprecated GetPlayableGamesByEdition",
			"GET",
			"/v1/ToPlayMatchList/{editionId}/{round}",
			deprecateEndpoint(api.getGamesV1ByEditionRoundAndPlayed(false)),
		},
		route{
			"Deprecated GetPlayedGamesByEdition",
			"GET",
			"/v1/PlayedMatchList/{editionId}/{round}",
			deprecateEndpoint(api.getGamesV1ByEditionRoundAndPlayed(true)),
		},
		route{
			"Deprecated GetSquadsByEdition",
			"GET",
			"/v1/CoachTeamList/{editionId}",
			deprecateEndpoint(api.getSquadsV1ByEdition),
		},
		route{
			"Deprecated GetSquadByID",
			"GET",
			"/v1/CoachTeam/{squadId}",
			deprecateEndpoint(api.getSquadV1),
		},
		route{
			"Deprecated GetGamesBySquad",
			"GET",
			"/v1/MatchListByCoachTeam/{squadId}",
			deprecateEndpoint(api.getGamesV1BySquad),
		},
		route{
			"Deprecated GetGamesByCoach",
			"GET",
			"/MatchListByCoach/{coachId}",
			deprecateEndpoint(api.getGamesV1ByCoach),
		},
		route{
			"Deprecated GetCoachRankingByEdition",
			"GET",
			"/v1/ranking/coach/{type}/{editionId}",
			deprecateEndpoint(api.getCoachRankingV1ByEdition),
		},
		route{
			"Deprecated GetSquadRankingByEdition",
			"GET",
			"/v1/ranking/coachTeam/{type}/{editionId}",
			deprecateEndpoint(api.getSquadRankingV1ByEdition),
		},
	}
}

func deprecateEndpoint(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("Use of deprecate endpoint")
		handler(w, r)
	}
}

func (api *API) getCoachV1(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractCoachId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	coach, err := api.Data.GetCoach(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	squadName := ""
	squad, err := api.Data.GetSquad(coach.SquadID)
	if err == nil {
		squadName = squad.Name
	}
	jsonWriter := jsonwriterv1.NewCoachWriter(coach)
	jsonWriter.SquadName = squadName

	encoded, err := json.Marshal(jsonWriter)
	if err != nil {
		log.Println("Internal server: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	io.Writer.Write(w, encoded)
}

func (api *API) findCoachsV1ByEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractEditionId(r)
	if err != nil {
		log.Println("Bad request: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetEdition(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	coachs, err := api.Data.GetCoachsByEdition(id)
	if err != nil {
		log.Println("Internal server: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	squads, err := api.Data.GetSquadsByEdition(id)
	if err != nil {
		log.Println("Internal server: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	adaptedCoachs := []jsonwriterv1.CoachWriter{}
	for _, coach := range coachs {
		squadName := ""
		for _, squad := range squads {
			if coach.SquadID == squad.ID {
				squadName = squad.Name
				break
			}
		}
		jsonWriter := jsonwriterv1.NewCoachWriter(coach)
		jsonWriter.SquadName = squadName
		adaptedCoachs = append(adaptedCoachs, jsonWriter)
	}
	encoded, _ := json.Marshal(adaptedCoachs)
	io.Writer.Write(w, encoded)
}

// getCurrentEditionV1 returns the current edition with a deprecated structure
func (api *API) getCurrentEditionV1(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	editions, err := api.Data.GetEditions()
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	edition := core.GetCurrentEdition(editions)
	edition, _ = api.Data.GetEdition(edition.ID)
	w.WriteHeader(http.StatusOK)
	encoded, _ := json.Marshal(jsonwriterv1.NewEditionWriter(edition))
	io.Writer.Write(w, encoded)
}

// getEditionV1 returns the edition identified by id with a deprecated structure
func (api *API) getEditionV1(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, err := extractEditionId(r)
	if err != nil {
		log.Println("Bad request - unable to extract edition id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	edition, err := api.Data.GetEdition(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	encoded, _ := json.Marshal(jsonwriterv1.NewEditionWriter(edition))
	io.Writer.Write(w, encoded)
}

// getEditionsV1 returns all editions with a deprecated structure
func (api *API) getEditionsV1(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	editions, err := api.Data.GetEditions()
	if err != nil {
		log.Println("Internal error: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	adaptedEditions := []jsonwriterv1.EditionWriter{}
	for _, edition := range editions {
		adaptedEditions = append(adaptedEditions, jsonwriterv1.NewEditionWriter(edition))
	}
	encoded, _ := json.Marshal(adaptedEditions)
	io.Writer.Write(w, encoded)
}

func (api *API) getSquadsV1ByEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, err := extractEditionId(r)
	if err != nil {
		log.Println("Bad request - unable to extract edition id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetEdition(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	squads, err := api.Data.GetSquadsByEdition(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	adapted := []jsonwriterv1.SquadWriter{}
	for _, squad := range squads {
		adapted = append(adapted, jsonwriterv1.NewSquadWriter(squad))
	}
	encoded, _ := json.Marshal(adapted)
	io.Writer.Write(w, encoded)
}

func (api *API) getSquadV1(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, err := extractSquadId(r)
	if err != nil {
		log.Println("Bad request - unable to extract squad id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	squad, err := api.Data.GetSquad(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	encoded, _ := json.Marshal(jsonwriterv1.NewSquadWriter(squad))
	io.Writer.Write(w, encoded)
}

func (api *API) getGamesV1ByEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, err := extractEditionId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetEdition(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	games, err := api.Data.GetGamesByEdition(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	adapted := []jsonwriterv1.GameWriter{}
	for _, game := range games {
		adapted = append(adapted, jsonwriterv1.NewGameWriter(game))
	}
	encoded, _ := json.Marshal(adapted)
	io.Writer.Write(w, encoded)
}

func (api *API) getGamesV1ByEditionRoundAndPlayed(played bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		id, err := extractEditionId(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		_, err = api.Data.GetEdition(id)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		round, err := extractRound(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		games, err := api.Data.GetGamesByEditionRoundAndPlayed(id, round, played)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusOK)
		adapted := []jsonwriterv1.GameWriter{}
		for _, game := range games {
			adapted = append(adapted, jsonwriterv1.NewGameWriter(game))
		}
		encoded, _ := json.Marshal(adapted)
		io.Writer.Write(w, encoded)
	}
}

func (api *API) getGamesV1ByEditionAndPlayed(played bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		id, err := extractEditionId(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		_, err = api.Data.GetEdition(id)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		games, err := api.Data.GetGamesByEditionAndPlayed(id, played)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusOK)
		adapted := []jsonwriterv1.GameWriter{}
		for _, game := range games {
			adapted = append(adapted, jsonwriterv1.NewGameWriter(game))
		}
		encoded, _ := json.Marshal(adapted)
		io.Writer.Write(w, encoded)
	}
}

func (api *API) getGamesV1ByEditionAndRound(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	editionId, err := extractEditionId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetEdition(editionId)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	round, err := extractRound(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	games, err := api.Data.GetGamesByEditionAndRound(editionId, round)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	adapted := []jsonwriterv1.GameWriter{}
	for _, game := range games {
		adapted = append(adapted, jsonwriterv1.NewGameWriter(game))
	}
	encoded, _ := json.Marshal(adapted)
	io.Writer.Write(w, encoded)
}

func (api *API) getGamesV1ByCoach(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractCoachId(r)
	if err != nil {
		log.Println("Bad request: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetCoach(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	games, err := api.Data.GetGamesByCoach(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	adapted := []jsonwriterv1.GameWriter{}
	for _, game := range games {
		if game.Coach2.ID == id {
			adapted = append(adapted, jsonwriterv1.NewGameWriter(game.Reverse()))
		} else {
			adapted = append(adapted, jsonwriterv1.NewGameWriter(game))
		}

	}
	encoded, _ := json.Marshal(adapted)
	io.Writer.Write(w, encoded)
}

func (api *API) getGamesV1BySquad(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractSquadId(r)
	if err != nil {
		log.Println("Bad request - unable to extract squad id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetSquad(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	games, err := api.Data.GetGamesBySquad(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	adapted := []jsonwriterv1.GameWriter{}
	for _, game := range games {
		if game.Coach2.SquadID == id {
			adapted = append(adapted, jsonwriterv1.NewGameWriter(game.Reverse()))
		} else {
			adapted = append(adapted, jsonwriterv1.NewGameWriter(game))
		}
	}
	encoded, _ := json.Marshal(adapted)
	io.Writer.Write(w, encoded)
}

func (api *API) getSquadRankingV1ByEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractEditionId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	rankingType := mux.Vars(r)["type"]
	edition, err := api.Data.GetEdition(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	games, _ := api.Data.GetGamesByEdition(id)
	squads, _ := api.Data.GetSquadsByEdition(id)
	ranking, err := edition.RankSquad(strings.ToLower(rankingType), games, squads)
	if err != nil {
		log.Println("error while RankSquad: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	serializer := jsonwriterv1.NewSquadRankingWriter(ranking)
	encoded, _ := json.Marshal(serializer)

	io.Writer.Write(w, encoded)
}

func (api *API) getCoachRankingV1ByEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractEditionId(r)
	if err != nil {
		log.Println("bad request in getCoachRankingByEdition: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	rankingType := mux.Vars(r)["type"]
	edition, err := api.Data.GetEdition(id)
	if err != nil {
		log.Println("Not found in getCoachRankingByEdition: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	games, err := api.Data.GetGamesByEdition(id)
	if err != nil {
		log.Println("Not found in getCoachRankingByEdition: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	ranking, err := edition.RankCoach(strings.ToLower(rankingType), games)
	if err != nil {
		log.Println("error while RankCoach: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	serializer := jsonwriterv1.NewCoachRankingWriter(ranking)
	encoded, _ := json.Marshal(serializer)

	io.Writer.Write(w, encoded)
	w.WriteHeader(http.StatusOK)
}
