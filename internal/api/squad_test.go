// FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func TestSquadInvalidInput(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	path := s.URL + editionUrl + "/squads"
	// testPostWithoutBody(client, path, t)
	// testPostWithInvalidJson(client, path, t)
	testPostWithUnknownJson(client, path, t)
}

func TestSquadNotFound(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/squads/10453"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
	req, _ := http.NewRequest("PUT", notExistentUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("PUT on %v", notExistentUrl), t)
	req, _ = http.NewRequest("DELETE", notExistentUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("DELETE on %v", notExistentUrl), t)
	notExistentUrl = notExistentUrl + "/games"
	response, _ = client.Get(notExistentUrl)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
}

func TestSquadInvalidId(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	invalidUrl := s.URL + "/v2/squads/AAA"
	response, _ := client.Get(invalidUrl)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("GET on %v", invalidUrl), t)
	req, _ := http.NewRequest("PUT", invalidUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("PUT on %v", invalidUrl), t)
	req, _ = http.NewRequest("DELETE", invalidUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("DELETE on %v", invalidUrl), t)
	invalidUrl = invalidUrl + "/games"
	response, _ = client.Get(invalidUrl)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("GET on %v", invalidUrl), t)
}

func TestGetSquadsUnknownEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/editions/102454/squads"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
}

func TestGetSquadsInvalidEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/editions/AAAAA/squads"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
}

func TestGetSquads(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", (i-1%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}

	expectedSquads := [](map[string]interface{}){}
	for i := uint(1); i <= 2; i++ {
		squad, err := core.NewSquad(fmt.Sprintf("squad %d", i), edition, []core.Coach{coachs[(i*2)-2], coachs[(i*2)-1]})
		if err != nil {
			t.Fatal("unexpected error in core.NewSquad: ", err)
		}
		payload := squad_to_payload(squad)
		urlSquad := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
		tempId, err = extractIdFromUrl(urlSquad)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlSquad, t)
		})
		squad.ID = tempId
		expectedSquads = append(expectedSquads, squad_to_map(squad))
	}
	body := testGetOk(client, s.URL+editionUrl+"/squads", t)
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw squads : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedSquads) {
		t.Error("result of getSquads is not as expected: ", cmp.Diff(receivedMaps, expectedSquads))
	}
}

func TestCreateSquadWithUnknownEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	target := "/v2/editions/2565/squads"
	payload := []byte(`{"round":1,"table":5}`)
	response, _ := client.Post(s.URL+target, contentType(), bytes.NewBuffer(payload))
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("POST on %v", target), t)
}

func TestCreateSquadWithInvalidEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	target := "/v2/editions/AAAAA/squads"
	payload := []byte(`{"round":1,"table":5}`)
	response, _ := client.Post(s.URL+target, contentType(), bytes.NewBuffer(payload))
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("POST on %v", target), t)
}

func TestUpdateSquad(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", (i-1%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad, err := core.NewSquad("Squad 1", edition, []core.Coach{coachs[0], coachs[1]})
	if err != nil {
		t.Fatal("unexpected error in core.NewSquad: ", err)
	}
	payload := squad_to_payload(squad)
	urlSquad := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad, t)
	})
	tempId, err = extractIdFromUrl(urlSquad)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	squad.ID = tempId
	squad.Name = "Squad 1 edited"
	squad.Members = []core.Coach{coachs[2], coachs[3]}
	payload = []byte(squad_to_payload(squad))
	req, _ := http.NewRequest("PUT", s.URL+urlSquad, bytes.NewBuffer(payload))
	response, _ := client.Do(req)

	checkResponseCode(http.StatusNoContent, response.StatusCode, "PUT on "+s.URL+urlSquad, t)
	_, err = io.ReadAll(response.Body)
	if err != nil {
		t.Fatal("Unexpected error while reading body of PUT: ", err.Error())
	}
	expectedMap := squad_to_map(squad)
	receivedMap := map[string]interface{}{}
	body := testGetOk(client, s.URL+urlSquad, t)
	err = json.Unmarshal(body, &receivedMap)
	if err != nil {
		t.Fatal("Unexpected error while parsing body: ", err.Error())
	}
	if !cmp.Equal(expectedMap, receivedMap) {
		t.Error("received map should be equal to expected map: ", cmp.Diff(expectedMap, receivedMap))
	}
}

func TestPatchSquad(t *testing.T) {
	// GIVEN
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 2; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", (i-1%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		// We update here to set expected values for the test
		coach.Ready = false
		coachs = append(coachs, coach)
	}
	squad, err := core.NewSquad("Squad 1", edition, coachs)
	if err != nil {
		t.Fatal("unexpected error in core.NewSquad: ", err)
	}
	payload := squad_to_payload(squad)
	urlSquad := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad, t)
	})
	tempId, err = extractIdFromUrl(urlSquad)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	squad.ID = tempId
	payload = []byte(`{"ready":false}`)
	// WHEN
	req, _ := http.NewRequest("PATCH", s.URL+urlSquad, bytes.NewBuffer(payload))
	response, _ := client.Do(req)
	// THEN
	checkResponseCode(http.StatusNoContent, response.StatusCode, "PATCH on "+s.URL+urlSquad, t)
	_, err = io.ReadAll(response.Body)
	if err != nil {
		t.Fatal("Unexpected error while reading body of PATCH: ", err.Error())
	}
	expectedMap := squad_to_map(squad)
	receivedMap := map[string]interface{}{}
	body := testGetOk(client, s.URL+urlSquad, t)
	err = json.Unmarshal(body, &receivedMap)
	if err != nil {
		t.Fatal("Unexpected error while parsing body: ", err.Error())
	}
	if !cmp.Equal(expectedMap, receivedMap) {
		t.Error("received map should be equal to expected map: ", cmp.Diff(expectedMap, receivedMap))
	}
}

func TestGetGamesBySquad(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	editionUrl := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+editionUrl, t)
	})
	tempId, err := extractIdFromUrl(editionUrl)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", (i-1%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+editionUrl)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	var lastSquad uint
	for i := uint(1); i <= 2; i++ {
		squad, err := core.NewSquad(fmt.Sprintf("squad %d", i), edition, []core.Coach{coachs[(i*2)-2], coachs[(i*2)-1]})
		if err != nil {
			t.Fatal("unexpected error in core.NewSquad: ", err)
		}
		payload := squad_to_payload(squad)
		urlSquad := postPayloadTo(client, payload, s.URL+editionUrl+"/squads", t)
		lastSquad, err = extractIdFromUrl(urlSquad)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlSquad, t)
		})
	}

	game, err := core.NewGame(1, 2, coachs[0], coachs[2])
	if err != nil {
		t.Fatal("unexpected error in core.NewGame: ", err)
	}

	payload := game_to_payload(game)
	urlGame := postPayloadTo(client, payload, s.URL+editionUrl+"/games", t)
	tempId, err = extractIdFromUrl(urlGame)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlGame, t)
	})
	game.ID = tempId
	game.Coach1.SquadID = lastSquad - 1
	game.Coach2.SquadID = lastSquad
	expectedGames := [](map[string]interface{}){game_to_extended_map(game.Reverse())}

	body := testGetOk(client, fmt.Sprintf("%s/v2/squads/%d/games", s.URL, lastSquad), t)
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw games : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedGames) {
		t.Error("result of getGamesBySquad is not as expected: ", cmp.Diff(receivedMaps, expectedGames))
	}
}

func TestOptionsOnSquads(t *testing.T) {
	// Given
	expectedAllow := "GET, POST, OPTIONS"
	s := getTestServer()
	urlSuffix := "/v2/editions/1/squads"
	url := s.URL + urlSuffix
	req, err := http.NewRequest("OPTIONS", url, nil)
	if err != nil {
		t.Fatalf("Unexpected error while OPTIONS %s, got %v", url, err.Error())
	}
	client := s.Client()
	// When
	response, _ := client.Do(req)
	// Then
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("OPTIONS on %v", urlSuffix), t)
	allow := response.Header.Get("Access-Control-Allow-Methods")
	if allow != expectedAllow {
		t.Errorf("allow header is %s, it should be %s", allow, expectedAllow)
	}
}

func TestOptionsOnSquad(t *testing.T) {
	// Given
	expectedAllow := "GET, PUT, DELETE, PATCH, OPTIONS"
	s := getTestServer()
	urlSuffix := "/v2/squads/1"
	url := s.URL + urlSuffix
	req, err := http.NewRequest("OPTIONS", url, nil)
	if err != nil {
		t.Fatalf("Unexpected error while OPTIONS %s, got %v", url, err.Error())
	}
	client := s.Client()
	// When
	response, _ := client.Do(req)
	// Then
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("OPTIONS on %v", urlSuffix), t)
	allow := response.Header.Get("Access-Control-Allow-Methods")
	if allow != expectedAllow {
		t.Errorf("allow header is %s, it should be %s", allow, expectedAllow)
	}
}

func squad_to_payload(squad core.Squad) []byte {
	coachTeamMates := "["
	for i, teamMate := range squad.Members {
		if i != 0 {
			coachTeamMates += ","
		}
		coachTeamMates += fmt.Sprintf(`{"id":"%d"}`, teamMate.ID)
	}
	coachTeamMates += "]"
	optionalID := ""
	if squad.ID != 0 {
		optionalID = fmt.Sprintf(`"id":"%d",`, squad.ID)
	}
	return []byte(fmt.Sprintf(`{
		%s"name":"%s","editionId":"%d",
		"teamMates":%s
	}`, optionalID, squad.Name, squad.Edition.ID, coachTeamMates))
}

func squad_to_map(squad core.Squad) map[string]interface{} {
	members := []interface{}{}
	for _, teamMate := range squad.Members {
		member := map[string]interface{}{
			"name":     teamMate.Name,
			"id":       fmt.Sprint(teamMate.ID),
			"teamName": teamMate.TeamName,
			"faction":  string(teamMate.Faction),
			"ready":    teamMate.Ready,
		}
		members = append(members, member)
	}
	return map[string]interface{}{
		"id":        fmt.Sprint(squad.ID),
		"editionId": fmt.Sprint(squad.Edition.ID),
		"name":      squad.Name,
		"teamMates": members,
	}
}
