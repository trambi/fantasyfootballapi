// FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/fantasyfootballapi/internal/api/jsonwriterv1"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func TestDeprecatedGetEditions(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	url1 := utilAddEdition(s, time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC), t)
	url2 := utilAddEdition(s, time.Date(2021, 6, 12, 0, 0, 0, 0, time.UTC), t)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url1, t)
		testDeleteOk(client, s.URL+url2, t)
	})
	body := testGetOk(client, s.URL+"/v1/Editions", t)
	var editions []jsonwriterv1.EditionWriter
	err := json.Unmarshal(body, &editions)
	if err != nil {
		t.Log("rawEditions: ", body)
		t.Fatal("Error in response body parsing")
	}
	if len(editions) < 2 {
		t.Errorf("length of getEditions is lower than 2")
	}
}

func TestDeprecatedGetCurrentEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	now := time.Now()
	oneYearBefore := now.AddDate(-1, 0, 0)
	threeDaysAfter := now.AddDate(0, 0, 3)
	oneYearAfter := now.AddDate(1, 0, 0)
	oneYearBeforeUrl := utilAddEdition(s, oneYearBefore, t)
	currentUrl := utilAddEdition(s, threeDaysAfter, t)
	oneYearAfterUrl := utilAddEdition(s, oneYearAfter, t)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+oneYearBeforeUrl, t)
		testDeleteOk(client, s.URL+currentUrl, t)
		testDeleteOk(client, s.URL+oneYearAfterUrl, t)
	})
	body := testGetOk(client, s.URL+"/v1/Edition/current", t)
	var edition jsonwriterv1.EditionWriter
	err := json.Unmarshal(body, &edition)
	if err != nil {
		t.Fatal("Error in response body parsing")
	}
	if edition.Day1 != strings.Split(threeDaysAfter.Format(time.RFC3339), "T")[0] {
		t.Errorf("getCurrentEdition.Day1 %s != %s ", edition.Day1, strings.Split(threeDaysAfter.Format(time.RFC3339), "T")[0])
	}
}

func TestDeprecatedGetCoachs(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	urlEdition := postEdition(t, s, edition)
	coach1 := core.Coach{
		Name:      "Coach1",
		TeamName:  "Team1",
		NafNumber: 123,
		Faction:   faction,
		Ready:     true,
		Edition:   edition}
	coach2 := core.Coach{
		Name:      "Coach2",
		TeamName:  "Team2",
		NafNumber: 456,
		Faction:   faction,
		Ready:     true,
		Edition:   edition}

	url1 := postCoach(t, client, coach1, s.URL+urlEdition)
	url2 := postCoach(t, client, coach2, s.URL+urlEdition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
		testDeleteOk(client, s.URL+url1, t)
		testDeleteOk(client, s.URL+url2, t)
	})
	body := testGetOk(client, s.URL+"/v1/Coachs/1", t)
	received := [](map[string]interface{}){}
	err := json.Unmarshal(body, &received)
	if err != nil {
		t.Log("raw body: ", body)
		t.Fatal("Error in response body parsing")
	}
	if len(received) < 2 {
		t.Errorf("length of getCoachs is lower than 2: %d", len(received))
	}
}

func TestDeprecateGetCoach(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	urlEdition := postEdition(t, s, edition)
	coach1 := core.Coach{
		Name:      "Coach1",
		TeamName:  "Team1",
		NafNumber: 123,
		Faction:   faction,
		Ready:     true,
		Edition:   edition}

	url := postCoach(t, client, coach1, s.URL+urlEdition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url, t)
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := strconv.ParseUint(strings.Split(url, "/")[3], 10, 64)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location: ", err.Error())
	}
	coach1.ID = uint(tempId)
	expected := `{"id":1,"name":"Coach1","teamName":"Team1","email":"not.yourbusiness@tisseursdechimeres.org","nafNumber":123,"raceName":"Faction1","ready":true}`
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/Coach/%d", s.URL, tempId), t)
	// Then
	compacted := &bytes.Buffer{}
	err = json.Compact(compacted, body)
	if err != nil {
		t.Fatal("Unexpected error while compacting json: ", err.Error())
	}
	result := compacted.String()
	if !cmp.Equal(result, expected) {
		t.Errorf("retrieved coach %v should be equal to %v: %v", result, expected, cmp.Diff(result, expected))
	}
}

func game_to_extended_map_v1(game core.Game) map[string]interface{} {
	status := "programme"
	if game.Played {
		status = "resume"
	}
	return map[string]interface{}{
		"id":        fmt.Sprintf("%d", game.ID),
		"editionId": fmt.Sprintf("%d", game.Edition.ID),
		"round":     float64(game.Round), "table": float64(game.Table),
		"status": status, "finale": game.Finale,
		"coach1": game.Coach1.Name, "teamName1": game.Coach1.TeamName, "teamId1": fmt.Sprintf("%d", game.Coach1.ID),
		"td1": float64(game.Td1), "casualties1": float64(game.Casualties1),
		"completions1": float64(game.Completions1), "fouls1": float64(game.Fouls1),
		"points1": float64(game.Points1), "special1": game.Special1,
		"coach2": game.Coach2.Name, "teamName2": game.Coach2.TeamName, "teamId2": fmt.Sprintf("%d", game.Coach2.ID),
		"td2": float64(game.Td2), "casualties2": float64(game.Casualties2),
		"completions2": float64(game.Completions2), "fouls2": float64(game.Fouls2),
		"points2": float64(game.Points2), "special2": game.Special2,
	}
}
func TestDeprecatedGetGames(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	coachNames := []string{"Coach1", "Coach 2", "Coach 3", "Coach 4"}
	for _, coachName := range coachNames {
		coach := core.Coach{
			Name:     coachName,
			TeamName: coachName,
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}

		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	game1v2, err := core.NewGame(1, 2, coachs[0], coachs[1])
	if err != nil {
		t.Fatal("unexpected error in coreNewGame: ", err)
	}
	payload := game_to_payload(game1v2)
	urlGame1v2 := postPayloadTo(client, payload, s.URL+urlEdition+"/games", t)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlGame1v2, t)
	})
	tempId, err = extractIdFromUrl(urlGame1v2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	game1v2.ID = tempId
	game3v4, err := core.NewGame(1, 3, coachs[2], coachs[3])
	if err != nil {
		t.Fatal("unexpected error in coreNewGame: ", err)
	}
	game3v4.Played = true
	payload = game_to_payload(game3v4)
	urlGame3v4 := postPayloadTo(client, payload, s.URL+urlEdition+"/games", t)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlGame3v4, t)
	})
	tempId, err = extractIdFromUrl(urlGame3v4)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	game3v4.ID = tempId
	expectedPayloadGame1v2 := game_to_extended_map_v1(game1v2)
	expectedPayloadGame3v4 := game_to_extended_map_v1(game3v4)
	expectedGames := []map[string]interface{}{expectedPayloadGame1v2, expectedPayloadGame3v4}
	expectedPlayableGames := []map[string]interface{}{expectedPayloadGame1v2}
	expectedPlayedGames := []map[string]interface{}{expectedPayloadGame3v4}
	body := testGetOk(client, fmt.Sprintf("%s/v1/MatchList/%d", s.URL, edition.ID), t)
	received := [](map[string]interface{}){}
	err = json.Unmarshal(body, &received)
	if err != nil {
		t.Fatal("Unexpected error while parsing received body", err.Error())
	}
	if !cmp.Equal(expectedGames, received) {
		t.Errorf("result of getGames is not as expected: %v", cmp.Diff(expectedGames, received))
	}
	body = testGetOk(client, fmt.Sprintf("%s/v1/ToPlayMatchList/%d", s.URL, edition.ID), t)
	received = [](map[string]interface{}){}
	err = json.Unmarshal(body, &received)
	if err != nil {
		t.Fatal("Unexpected error while parsing received body", err.Error())
	}

	if !cmp.Equal(expectedPlayableGames, received) {
		t.Errorf("result of getPlayableGames is not as expected: %v", cmp.Diff(expectedPlayableGames, received))
	}
	body = testGetOk(client, fmt.Sprintf("%s/v1/PlayedMatchList/%d", s.URL, edition.ID), t)
	received = [](map[string]interface{}){}
	err = json.Unmarshal(body, &received)
	if err != nil {
		t.Fatal("Unexpected error while parsing received body", err.Error())
	}

	if !cmp.Equal(expectedPlayedGames, received) {
		t.Errorf("result of getPlayedGames is not as expected: %v", cmp.Diff(expectedPlayedGames, received))
	}
}

func TestDeprecatedGetGamesByEditionAndRound(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", (i-1%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	expectedGames := [](map[string]interface{}){}
	expectedPlayableGames := [](map[string]interface{}){}
	expectedPlayedGames := [](map[string]interface{}){}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}

		game.Played = i%2 == 0
		payload := game_to_payload(game)
		urlGame := postPayloadTo(client, payload, s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
		if game.Round == 2 {
			expectedGames = append(expectedGames, game_to_extended_map_v1(game))
			if game.Played {
				expectedPlayedGames = append(expectedPlayedGames, game_to_extended_map_v1(game))
			} else {
				expectedPlayableGames = append(expectedPlayableGames, game_to_extended_map_v1(game))
			}
		}

	}
	body := testGetOk(client, fmt.Sprintf("%s/v1/MatchList/%d/2", s.URL, edition.ID), t)
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw games : ", string(body))
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedGames) {
		t.Fatal("result of getGames is not as expected: ", cmp.Diff(expectedGames, receivedMaps))
	}
	body = testGetOk(client, fmt.Sprintf("%s/v1/ToPlayMatchList/%d/2", s.URL, edition.ID), t)
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw games : ", string(body))
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedPlayableGames) {
		t.Fatal("result of getPlayableGames is not as expected: ", cmp.Diff(expectedPlayableGames, receivedMaps))
	}
	body = testGetOk(client, fmt.Sprintf("%s/v1/PlayedMatchList/%d/2", s.URL, edition.ID), t)
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw games : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(expectedPlayedGames, receivedMaps) {
		t.Fatal("result of getPlayedGames is not as expected: ", cmp.Diff(expectedPlayedGames, receivedMaps))
	}
}

func squad_to_map_v1(squad core.Squad) map[string]interface{} {
	members := []interface{}{}
	for _, teamMate := range squad.Members {
		member := map[string]interface{}{
			"coach":    teamMate.Name,
			"teamId":   fmt.Sprintf("%d", teamMate.ID),
			"teamName": teamMate.TeamName,
		}
		members = append(members, member)
	}
	return map[string]interface{}{
		"id":             fmt.Sprintf("%d", squad.ID),
		"name":           squad.Name,
		"coachTeamMates": members,
	}
}

func TestDeprecatedGetSquads(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", (i-1%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}

	expectedSquads := [](map[string]interface{}){}
	for i := uint(1); i <= 2; i++ {
		squad, err := core.NewSquad(fmt.Sprintf("squad %d", i), edition, []core.Coach{coachs[(i*2)-2], coachs[(i*2)-1]})
		if err != nil {
			t.Fatal("unexpected error in core.NewSquad: ", err)
		}
		payload := squad_to_payload(squad)
		urlSquad := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
		tempId, err = extractIdFromUrl(urlSquad)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlSquad, t)
		})
		squad.ID = tempId
		expectedSquads = append(expectedSquads, squad_to_map_v1(squad))
	}
	body := testGetOk(client, fmt.Sprintf("%s/v1/CoachTeamList/%d", s.URL, edition.ID), t)
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw squads : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedSquads) {
		t.Error("result of getSquads is not as expected: ", cmp.Diff(receivedMaps, expectedSquads))
	}
}

func TestDeprecatedGetSquad(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", (i-1%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad, err := core.NewSquad("Squad 1", edition, []core.Coach{coachs[0], coachs[1]})
	if err != nil {
		t.Fatal("unexpected error in core.NewSquad: ", err)
	}
	payload := squad_to_payload(squad)
	urlSquad := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad, t)
	})
	tempId, err = extractIdFromUrl(urlSquad)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	squad.ID = tempId
	expectedMap := squad_to_map_v1(squad)
	receivedMap := map[string]interface{}{}
	body := testGetOk(client, fmt.Sprintf("%s/v1/CoachTeam/%d", s.URL, tempId), t)
	err = json.Unmarshal(body, &receivedMap)
	if err != nil {
		t.Fatal("Unexpected error while parsing body: ", err.Error())
	}
	if !cmp.Equal(expectedMap, receivedMap) {
		t.Error("received map should be equal to expected map: ", cmp.Diff(expectedMap, receivedMap))
	}
}

func TestDeprecatedGetGamesBySquad(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", (i-1%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	var lastSquad uint
	for i := uint(1); i <= 2; i++ {
		squad, err := core.NewSquad(fmt.Sprintf("squad %d", i), edition, []core.Coach{coachs[(i*2)-2], coachs[(i*2)-1]})
		if err != nil {
			t.Fatal("unexpected error in core.NewSquad: ", err)
		}
		payload := squad_to_payload(squad)
		urlSquad := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
		lastSquad, err = extractIdFromUrl(urlSquad)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlSquad, t)
		})
	}
	expectedGames := [](map[string]interface{}){}

	game, err := core.NewGame(1, 2, coachs[0], coachs[2])
	if err != nil {
		t.Fatal("unexpected error in core.NewGame: ", err)
	}

	payload := game_to_payload(game)
	urlGame := postPayloadTo(client, payload, s.URL+urlEdition+"/games", t)
	tempId, err = extractIdFromUrl(urlGame)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlGame, t)
	})
	game.ID = tempId
	expectedGames = append(expectedGames, game_to_extended_map_v1(game.Reverse()))

	body := testGetOk(client, fmt.Sprintf("%s/v1/MatchListByCoachTeam/%d", s.URL, lastSquad), t)
	t.Logf("body:%v\n", string(body))
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw games : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedGames) {
		t.Error("result of getGamesBySquad is not as expected: ", cmp.Diff(receivedMaps, expectedGames))
	}
}

func TestDeprecatedGetCoachTdRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddCoachTdRanking()
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	expectedRankings := [](map[string]interface{}){
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(1),
			"name": "Coach 1", "ready": true,
			"tdFor": float64(8), "teamName": "Team 1",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(4),
			"name": "Coach 4", "ready": true,
			"tdFor": float64(6), "teamName": "Team 2",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(3),
			"name": "Coach 3", "ready": true,
			"tdFor": float64(4), "teamName": "Team 1",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(2),
			"name": "Coach 2", "ready": true,
			"tdFor": float64(2), "teamName": "Team 2",
		},
	}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(1, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coach/td/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of DeprecatedGetCoachTdRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestDeprecatedGetCoachCasualtiesRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddCoachCasualtiesRanking()
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	expectedRankings := [](map[string]interface{}){
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(1),
			"name": "Coach 1", "ready": true,
			"casualtiesFor": float64(11), "teamName": "Team 1",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(4),
			"name": "Coach 4", "ready": true,
			"casualtiesFor": float64(8), "teamName": "Team 2",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(3),
			"name": "Coach 3", "ready": true,
			"casualtiesFor": float64(5), "teamName": "Team 1",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(2),
			"name": "Coach 2", "ready": true,
			"casualtiesFor": float64(2), "teamName": "Team 2",
		},
	}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetCasualties(1, int(((i-1)*3)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coach/casualties/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of deprecatedGetCoachCasualtiesRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestDeprecatedGetCoachDefenseRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddCoachDefenseRanking()
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(0, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	expectedRankings := [](map[string]interface{}){
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(1),
			"name": "Coach 1", "ready": true,
			"tdAgainst": float64(1), "teamName": "Team 1",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(2),
			"name": "Coach 2", "ready": true,
			"tdAgainst": float64(3), "teamName": "Team 2",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(3),
			"name": "Coach 3", "ready": true,
			"tdAgainst": float64(5), "teamName": "Team 1",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(4),
			"name": "Coach 4", "ready": true,
			"tdAgainst": float64(7), "teamName": "Team 2",
		},
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coach/defense/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of deprecatedGetCoachDefenseRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestDeprecatedGetCoachCompletionsRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddCoachCompletionsRanking()
	urlEdition := postEdition(t, s, edition)

	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetCompletions(1, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
		fmt.Printf("Game %d %s %d-%d %s\n", i, game.Coach1.Name, game.Td1, game.Td2, game.Coach2.Name)
	}
	expectedRankings := [](map[string]interface{}){
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(1),
			"name": "Coach 1", "ready": true,
			"completions": float64(8), "teamName": "Team 1",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(4),
			"name": "Coach 4", "ready": true,
			"completions": float64(6), "teamName": "Team 2",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(3),
			"name": "Coach 3", "ready": true,
			"completions": float64(4), "teamName": "Team 1",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(2),
			"name": "Coach 2", "ready": true,
			"completions": float64(2), "teamName": "Team 2",
		},
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coach/completions/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of deprecatedGetCoachCompletionRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestDeprecatedGetCoachFoulsRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddCoachFoulsRanking()
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetFouls(1, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
		fmt.Printf("Game %d %s %d-%d %s\n", i, game.Coach1.Name, game.Td1, game.Td2, game.Coach2.Name)
	}
	expectedRankings := [](map[string]interface{}){
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(1),
			"name": "Coach 1", "ready": true,
			"fouls": float64(8), "teamName": "Team 1",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(4),
			"name": "Coach 4", "ready": true,
			"fouls": float64(6), "teamName": "Team 2",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(3),
			"name": "Coach 3", "ready": true,
			"fouls": float64(4), "teamName": "Team 1",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(2),
			"name": "Coach 2", "ready": true,
			"fouls": float64(2), "teamName": "Team 2",
		},
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coach/fouls/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of deprecated getCoachFoulRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestDeprecatedGetMainCoachRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddCoachRankingMain([]core.RankingCriteria{
		{Label: "points", Field: core.Points, Type: core.For, Descending: true},
		{Label: "netTd", Field: core.Td, Type: core.Net, Descending: true},
		{Label: "casualtiesFor", Field: core.Casualties, Type: core.For, Descending: true},
	})
	edition.AddGamePointsClause("1", "{TdFor}+1={TdAgainst}")
	edition.AddGamePointsClause("2", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("5", "{TdFor}>{TdAgainst}")
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 6; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	expectedRankings := [](map[string]interface{}){
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(5),
			"name": "Coach 5", "ready": true,
			"points": float64(10), "netTd": float64(3),
			"casualtiesFor": float64(3), "teamName": "Team 1",
		}, {
			"email":    "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(3),
			"name": "Coach 3", "ready": true,
			"points": float64(7), "netTd": float64(1),
			"casualtiesFor": float64(4), "teamName": "Team 1",
		}, {
			"email":    "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(2),
			"name": "Coach 2", "ready": true,
			"points": float64(6), "netTd": float64(0),
			"casualtiesFor": float64(3), "teamName": "Team 2",
		}, {
			"email":    "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(1),
			"name": "Coach 1", "ready": true,
			"points": float64(5), "netTd": float64(-1),
			"casualtiesFor": float64(2), "teamName": "Team 1",
		}, {
			"email":    "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(4),
			"name": "Coach 4", "ready": true,
			"points": float64(3), "netTd": float64(-1),
			"casualtiesFor": float64(3), "teamName": "Team 2",
		}, {
			"email":    "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(6),
			"name": "Coach 6", "ready": true,
			"points": float64(2), "netTd": float64(-2),
			"casualtiesFor": float64(4), "teamName": "Team 2",
		},
	}
	scores := []struct {
		coachIndex1 int
		td1         int
		casualties1 int
		coachIndex2 int
		td2         int
		casualties2 int
	}{
		{0, 1, 1, 1, 0, 2},
		{2, 1, 3, 3, 1, 1},
		{4, 2, 1, 5, 1, 2},
		{0, 1, 1, 4, 3, 2},
		{2, 2, 1, 5, 1, 2},
		{1, 2, 1, 3, 1, 2},
	}
	for i, score := range scores {
		game, err := core.NewGame(1, uint(1+i), coachs[score.coachIndex1], coachs[score.coachIndex2])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(score.td1, score.td2)
		game.SetCasualties(score.casualties1, score.casualties2)
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coach/main/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getMainCoachRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestDeprecatedGetComebackCoachRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddCoachRankingMain([]core.RankingCriteria{
		{Label: "points", Field: core.Points, Type: core.For, Descending: true},
		{Label: "netTd", Field: core.Td, Type: core.Net, Descending: true},
		{Label: "casualtiesFor", Field: core.Casualties, Type: core.For, Descending: true},
	})
	edition.AddGamePointsClause("1", "{TdFor}+1={TdAgainst}")
	edition.AddGamePointsClause("2", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("5", "{TdFor}>{TdAgainst}")
	edition.AddCoachComebackRanking()
	payload := []byte(`{
			"id":1,
			"name":"edition1",
			"day1":"2020-06-12","day2":"2020-06-13",
			"roundNumber":2,"currentRound":0,"firstDayRound":1,
			"useFinale":true,"fullCoachteam":true,"rankingStrategyName":"something",
			"allowedFactions":["Faction1"],"organizer":"orga1",
			"rankings":{
				"coach":{
					"main":[{
						"label":"points",
						"field":"Points",
						"type":"For",
						"descending":true
					},{
						"label":"netTd",
						"field":"Td",
						"type":"Net",
						"descending":true
					},{
						"label":"casualtiesFor",
						"field":"Casualties",
						"type":"For",
						"descending":true
					}],
					"comeback": [{
	      				"label": "diffRanking",
	      				"field": "Ranking",
	      				"type": "Day1MinusDay2",
	      				"descending": true
	    			},{
	      				"label": "firstDayRanking",
	      				"field": "Ranking",
	      				"type": "Day1",
	      				"descending": false
	    			},{
	      				"label": "finalRanking",
	      				"field": "Ranking",
	      				"type": "Day2",
	      				"descending": false
	    			}]
				}
			},
			"gamePoints":{
				"default":"0",
				"clauses":[
					{
						"valueIfTrue":"1",
						"condition":"{TdFor}+1={TdAgainst}"
					},
					{
						"valueIfTrue":"2",
						"condition":"{TdFor}={TdAgainst}"
					},
					{
						"valueIfTrue":"5",
						"condition":"{TdFor}>{TdAgainst}"
					}
				]
			}
	}`)
	urlEdition := postPayloadTo(client, payload, s.URL+"/v2/editions", t)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 6; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", ((i-1)%2)+1),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	expectedRankings := [](map[string]interface{}){
		{
			"email":    "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(2),
			"name": "Coach 2", "ready": true,
			"diffRanking": float64(3), "firstDayRanking": float64(6),
			"finalRanking": float64(3), "teamName": "Team 2",
		}, {
			"email":    "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(3),
			"name": "Coach 3", "ready": true,
			"diffRanking": float64(1), "firstDayRanking": float64(3),
			"finalRanking": float64(2), "teamName": "Team 1",
		},
		{"email": "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(5),
			"name": "Coach 5", "ready": true,
			"diffRanking": float64(0), "firstDayRanking": float64(1),
			"finalRanking": float64(1), "teamName": "Team 1",
		}, {
			"email":    "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(4),
			"name": "Coach 4", "ready": true,
			"diffRanking": float64(-1), "firstDayRanking": float64(4),
			"finalRanking": float64(5), "teamName": "Team 2",
		}, {
			"email":    "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(6),
			"name": "Coach 6", "ready": true,
			"diffRanking": float64(-1), "firstDayRanking": float64(5),
			"finalRanking": float64(6), "teamName": "Team 2",
		}, {
			"email":    "not.yourbusiness@tisseursdechimeres.org",
			"raceName": "Faction1", "id": float64(1),
			"name": "Coach 1", "ready": true,
			"diffRanking": float64(-2), "firstDayRanking": float64(2),
			"finalRanking": float64(4), "teamName": "Team 1",
		},
	}
	scores := []struct {
		round       uint
		coachIndex1 int
		td1         int
		casualties1 int
		coachIndex2 int
		td2         int
		casualties2 int
	}{
		{1, 0, 1, 0, 1, 0, 1}, // Coach1 5,1,0 Coach2 1,-1,1
		{1, 2, 1, 3, 3, 1, 1}, // Coach3 2,0,3 Coach4 2,0,1
		{1, 4, 2, 1, 5, 1, 2}, // Coach5 5,1,1 Coach6 1,-1,2
		// Coach5 Coach1 Coach3 Coach4 Coach6 Coach2
		{2, 0, 1, 1, 4, 3, 2}, //Coach1 5,-1,1 Coach5 10,3,3
		{2, 2, 2, 1, 5, 1, 2}, //Coach3 7,1,4  Coach6 2,-2,4
		{2, 1, 2, 1, 3, 1, 2}, //Coach2 6,0,2  Coach4 3,-1,3
		// Coach5 Coach3 Coach2 Coach1 Coach4 Coach6
		// Coach2 3 6 3
		// Coach3 1 3 2
		// Coach5 0 1 1
		// Coach4 -1 4 5
		// Coach6 -1 5 6
		// Coach1 -2 2 4
	}
	for i, score := range scores {
		game, err := core.NewGame(score.round, uint(1+i), coachs[score.coachIndex1], coachs[score.coachIndex2])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(score.td1, score.td2)
		game.SetCasualties(score.casualties1, score.casualties2)
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coach/comeback/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of getMainCoachRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestDeprecatedGetSquadTdRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddSquadTdRanking()
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	fmt.Printf("squad1.ID: %d", lastSquad)
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	fmt.Printf("squad2.ID: %d", lastSquad)
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(1, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":    "1",
			"name":  "Squad 1",
			"tdFor": float64(12),
			"coachTeamMates": []any{
				map[string]any{
					"coach":    "Coach 1",
					"teamId":   "1",
					"teamName": "Team 1",
				}, map[string]any{
					"coach":    "Coach 3",
					"teamId":   "3",
					"teamName": "Team 3",
				},
			},
		},
		{
			"id":    "2",
			"name":  "Squad 2",
			"tdFor": float64(8),
			"coachTeamMates": []any{
				map[string]any{
					"coach":    "Coach 2",
					"teamId":   "2",
					"teamName": "Team 2",
				}, map[string]any{
					"coach":    "Coach 4",
					"teamId":   "4",
					"teamName": "Team 4",
				},
			},
		},
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coachTeam/td/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of deprecated getSquadTdRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestDeprecatedGetSquadCasualtiesRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddSquadCasualtiesRanking()
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	fmt.Printf("squad1.ID: %d", lastSquad)
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	fmt.Printf("squad2.ID: %d", lastSquad)
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetCasualties(1, int(((i-1)*3)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":            "1",
			"name":          "Squad 1",
			"casualtiesFor": float64(16),
			"coachTeamMates": []any{
				map[string]any{
					"coach":    "Coach 1",
					"teamId":   "1",
					"teamName": "Team 1",
				}, map[string]any{
					"coach":    "Coach 3",
					"teamId":   "3",
					"teamName": "Team 3",
				},
			},
		},
		{
			"id":            "2",
			"name":          "Squad 2",
			"casualtiesFor": float64(10),
			"coachTeamMates": []any{
				map[string]any{
					"coach":    "Coach 2",
					"teamId":   "2",
					"teamName": "Team 2",
				}, map[string]any{
					"coach":    "Coach 4",
					"teamId":   "4",
					"teamName": "Team 4",
				},
			},
		},
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coachTeam/casualties/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of deprecated getSquadCasualtiesRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestDeprecatedGetSquadDefenseRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddSquadDefenseRanking()
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	fmt.Printf("squad1.ID: %d", lastSquad)
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	fmt.Printf("squad2.ID: %d", lastSquad)
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(0, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":        "1",
			"name":      "Squad 1",
			"tdAgainst": float64(6),
			"coachTeamMates": []any{
				map[string]any{
					"coach":    "Coach 1",
					"teamId":   "1",
					"teamName": "Team 1",
				}, map[string]any{
					"coach":    "Coach 3",
					"teamId":   "3",
					"teamName": "Team 3",
				},
			},
		},
		{
			"id":        "2",
			"name":      "Squad 2",
			"tdAgainst": float64(10),
			"coachTeamMates": []any{
				map[string]any{
					"coach":    "Coach 2",
					"teamId":   "2",
					"teamName": "Team 2",
				}, map[string]any{
					"coach":    "Coach 4",
					"teamId":   "4",
					"teamName": "Team 4",
				},
			},
		},
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coachTeam/defense/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of deprecated getSquadDefenseRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestDeprecatedGetSquadCompletionsRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddSquadCompletionsRanking()
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	fmt.Printf("squad1.ID: %d", lastSquad)
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	fmt.Printf("squad2.ID: %d", lastSquad)
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetCompletions(1, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":          "1",
			"name":        "Squad 1",
			"completions": float64(12),
			"coachTeamMates": []any{
				map[string]any{
					"coach":    "Coach 1",
					"teamId":   "1",
					"teamName": "Team 1",
				}, map[string]any{
					"coach":    "Coach 3",
					"teamId":   "3",
					"teamName": "Team 3",
				},
			},
		},
		{
			"id":          "2",
			"name":        "Squad 2",
			"completions": float64(8),
			"coachTeamMates": []any{
				map[string]any{
					"coach":    "Coach 2",
					"teamId":   "2",
					"teamName": "Team 2",
				}, map[string]any{
					"coach":    "Coach 4",
					"teamId":   "4",
					"teamName": "Team 4",
				},
			},
		},
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coachTeam/completions/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of deprecated getSquadCompletionRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestDeprecatedGetSquadFoulsRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddSquadFoulsRanking()
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	fmt.Printf("squad1.ID: %d", lastSquad)
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	fmt.Printf("squad2.ID: %d", lastSquad)
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetFouls(1, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":    "1",
			"name":  "Squad 1",
			"fouls": float64(12),
			"coachTeamMates": []any{
				map[string]any{
					"coach":    "Coach 1",
					"teamId":   "1",
					"teamName": "Team 1",
				}, map[string]any{
					"coach":    "Coach 3",
					"teamId":   "3",
					"teamName": "Team 3",
				},
			},
		},
		{
			"id":    "2",
			"name":  "Squad 2",
			"fouls": float64(8),
			"coachTeamMates": []any{
				map[string]any{
					"coach":    "Coach 2",
					"teamId":   "2",
					"teamName": "Team 2",
				}, map[string]any{
					"coach":    "Coach 4",
					"teamId":   "4",
					"teamName": "Team 4",
				},
			},
		},
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coachTeam/fouls/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of deprecated getSquadFoulRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}

func TestDeprecatedGetSquadBasicMainRanking(t *testing.T) {
	// Given
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction1")
	edition := createTestEdition(time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC))
	edition.AllowedFactions = []core.Faction{faction}
	edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	edition.AddSquadMainRanking([]core.RankingCriteria{
		{Label: "points", Field: core.Points, Type: core.For, Descending: true},
		{Label: "netTd", Field: core.Td, Type: core.Net, Descending: true},
	})
	urlEdition := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlEdition, t)
	})
	tempId, err := extractIdFromUrl(urlEdition)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = tempId
	coachs := []core.Coach{}
	for i := 1; i <= 4; i++ {
		coach := core.Coach{
			Name:     fmt.Sprintf("Coach %d", i),
			TeamName: fmt.Sprintf("Team %d", i),
			Faction:  faction,
			Ready:    true,
			Edition:  edition,
		}
		coachUrl := postCoach(t, client, coach, s.URL+urlEdition)
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+coachUrl, t)
		})
		tempId, err = extractIdFromUrl(coachUrl)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		coach.ID = tempId
		coachs = append(coachs, coach)
	}
	squad1 := core.Squad{Name: "Squad 1", Members: []core.Coach{coachs[0], coachs[2]}}
	payload := squad_to_payload(squad1)
	urlSquad1 := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	lastSquad, err := extractIdFromUrl(urlSquad1)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad1, t)
	})
	squad1.ID = lastSquad
	squad2 := core.Squad{Name: "Squad 2", Members: []core.Coach{coachs[1], coachs[3]}}
	payload = squad_to_payload(squad2)
	urlSquad2 := postPayloadTo(client, payload, s.URL+urlEdition+"/squads", t)
	lastSquad, err = extractIdFromUrl(urlSquad2)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlSquad2, t)
	})
	squad2.ID = lastSquad
	for i := uint(1); i <= 4; i++ {
		game, err := core.NewGame((i+1)/2, 1+i, coachs[i-1], coachs[i%uint(len(coachs))])
		if err != nil {
			t.Fatal("unexpected error in core.NewGame: ", err)
		}
		game.SetTd(0, int(((i-1)*2)+1))
		payload := game_to_extended_payload(game)
		urlGame := postPayloadTo(client, []byte(payload), s.URL+urlEdition+"/games", t)
		tempId, err = extractIdFromUrl(urlGame)
		if err != nil {
			t.Fatal("Unexpected error while extracting id from location", err.Error())
		}
		t.Cleanup(func() {
			testDeleteOk(client, s.URL+urlGame, t)
		})
		game.ID = tempId
		fmt.Printf("Game %d %s vs %s: %d - %d\n", i, game.Coach1.Name, game.Coach2.Name, game.Td1, game.Td2)
	}
	expectedRankings := [](map[string]interface{}){
		{
			"id":     "1",
			"finale": float64(0),
			"name":   "Squad 1",
			"points": float64(6),
			"netTd":  float64(4),
			"coachTeamMates": []any{
				map[string]any{
					"coach":    "Coach 1",
					"teamId":   "1",
					"teamName": "Team 1",
				}, map[string]any{
					"coach":    "Coach 3",
					"teamId":   "3",
					"teamName": "Team 3",
				},
			},
		},
		{
			"id":     "2",
			"finale": float64(0),
			"name":   "Squad 2",
			"points": float64(6),
			"netTd":  float64(-4),
			"coachTeamMates": []any{
				map[string]any{
					"coach":    "Coach 2",
					"teamId":   "2",
					"teamName": "Team 2",
				}, map[string]any{
					"coach":    "Coach 4",
					"teamId":   "4",
					"teamName": "Team 4",
				},
			},
		},
	}
	// When
	body := testGetOk(client, fmt.Sprintf("%s/v1/ranking/coachTeam/main/%d", s.URL, edition.ID), t)
	// Then
	var receivedMaps [](map[string]interface{})
	err = json.Unmarshal(body, &receivedMaps)
	if err != nil {
		t.Log("raw rankings : ", body)
		t.Fatal("Error in response body parsing")
	}
	if !cmp.Equal(receivedMaps, expectedRankings) {
		t.Error("result of TestDeprecatedGetSquadBasicMainRanking is not as expected: ", cmp.Diff(receivedMaps, expectedRankings))
	}
}
