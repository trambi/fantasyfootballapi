// Package jsonadapter FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.

package jsonadapter

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// EditionReader internal structure to read Json Marshaling to from core.Edition
type EditionReader struct {
	Name                string                 `json:"name"`
	Day1                string                 `json:"day1"`
	Day2                string                 `json:"day2"`
	RoundNumber         uint                   `json:"roundNumber"`
	CurrentRound        uint                   `json:"currentRound"`
	FirstDayRound       uint                   `json:"firstDayRound"`
	UseFinale           bool                   `json:"useFinale"`
	FullSquad           bool                   `json:"fullSquad"`
	CoachPerSquad       uint                   `json:"coachPerSquad"`
	AllowedFactions     []string               `json:"allowedFactions"`
	Organizer           string                 `json:"organizer"`
	Ranking             rankingSettingsAdapter `json:"rankings,omitempty"`
	GamePoints          pointsSettingsAdapter  `json:"gamePoints,omitempty"`
	ConfrontationPoints pointsSettingsAdapter  `json:"confrontationPoints,omitempty"`
}

// ToCore method to get a core.Edition from an editionJsonAdapter
func (s EditionReader) ToCore() (core.Edition, error) {
	factions := []core.Faction{}
	for _, faction := range s.AllowedFactions {
		factions = append(factions, core.Faction(faction))
	}

	edition, err := core.NewEdition(s.Name, s.Day1, s.RoundNumber,
		s.FirstDayRound, s.UseFinale, s.FullSquad, s.CoachPerSquad, factions,
		"unused", s.Organizer)
	if err != nil {
		return edition, err
	}
	edition.Day2 = s.Day2
	edition.CurrentRound = s.CurrentRound
	if len(s.Ranking.Coach) != 0 {
		for rankingName, criteriaAdapters := range s.Ranking.Coach {
			criterias := []core.RankingCriteria{}
			for _, criteriaAdapter := range criteriaAdapters {
				domain, err := core.NewRankingField(criteriaAdapter.Field)
				if err != nil {
					return edition, fmt.Errorf("while constructing ranking fied: %s", err.Error())
				}
				rankingType, err := core.NewRankingType(criteriaAdapter.RankingType)
				if err != nil {
					return edition, fmt.Errorf("while constructing ranking type: %s", err.Error())
				}
				criteria := core.RankingCriteria{
					Label:      criteriaAdapter.Label,
					Field:      domain,
					Type:       rankingType,
					Descending: criteriaAdapter.Descending,
				}
				criterias = append(criterias, criteria)
			}
			edition.CoachRankings[rankingName] = criterias
		}
	}
	if len(s.Ranking.Squad) != 0 {
		for rankingName, criteriaAdapters := range s.Ranking.Squad {
			criterias := []core.RankingCriteria{}
			for _, criteriaAdapter := range criteriaAdapters {
				domain, err := core.NewRankingField(criteriaAdapter.Field)
				if err != nil {
					return edition, fmt.Errorf("while constructing ranking fied: %s", err.Error())
				}
				rankingType, err := core.NewRankingType(criteriaAdapter.RankingType)
				if err != nil {
					return edition, fmt.Errorf("while constructing ranking type: %s", err.Error())
				}
				criteria := core.RankingCriteria{
					Label:      criteriaAdapter.Label,
					Field:      domain,
					Type:       rankingType,
					Descending: criteriaAdapter.Descending,
				}
				criterias = append(criterias, criteria)
			}
			edition.SquadRankings[rankingName] = criterias
		}
	}
	for _, clause := range s.GamePoints.Clauses {
		err = edition.AddGamePointsClause(clause.ValueIfTrue, clause.Condition)
		if err != nil {
			return edition, fmt.Errorf("while adding game points clause %s: %s", clause.Condition, err.Error())
		}
	}
	for _, clause := range s.ConfrontationPoints.Clauses {
		err = edition.AddConfrontationPointsClause(clause.ValueIfTrue, clause.Condition)
		if err != nil {
			return edition, fmt.Errorf("while adding confrontation points clause %s: %s", clause.Condition, err.Error())
		}
	}
	return edition, nil
}
