// Package jsonadapter FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.

package jsonadapter

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// EditionWriter internal structure to marshal Json from core.Edition
type EditionWriter struct {
	ID                  string                 `json:"id"`
	Name                string                 `json:"name"`
	Day1                string                 `json:"day1"`
	Day2                string                 `json:"day2"`
	RoundNumber         uint                   `json:"roundNumber"`
	CurrentRound        uint                   `json:"currentRound"`
	FirstDayRound       uint                   `json:"firstDayRound"`
	UseFinale           bool                   `json:"useFinale"`
	FullSquad           bool                   `json:"fullSquad"`
	CoachPerSquad       uint                   `json:"coachPerSquad"`
	AllowedFactions     []string               `json:"allowedFactions"`
	Organizer           string                 `json:"organizer"`
	Ranking             rankingSettingsAdapter `json:"rankings,omitempty"`
	GamePoints          pointsSettingsAdapter  `json:"gamePoints,omitempty"`
	ConfrontationPoints pointsSettingsAdapter  `json:"confrontationPoints,omitempty"`
}

// rankingSettingsAdapter internal structure to dissociate Json Marshaling and Unmarshaling from core.RankingSettings
type rankingSettingsAdapter struct {
	Coach map[string][]criteriaAdapter `json:"coach,omitempty"`
	Squad map[string][]criteriaAdapter `json:"squad,omitempty"`
}

// criteriaAdapter internal structure to dissociate Json Marshaling and Unmarshaling from core.Criteria
type criteriaAdapter struct {
	Label       string `json:"label"`
	Field       string `json:"field"`
	RankingType string `json:"type"`
	Descending  bool   `json:"descending"`
}

// clauseAdapter internal structure to dissociate Json Marshaling and Unmarshaling from core.Clause
type clauseAdapter struct {
	Condition   string `json:"condition"`
	ValueIfTrue string `json:"valueIfTrue"`
}

// pointsSettingsAdapter internal structure to dissociate Json Marshaling and Unmarshaling from core.PointsSettings
type pointsSettingsAdapter struct {
	Default string          `json:"default"`
	Clauses []clauseAdapter `json:"clauses"`
}

// NewEditionWriter construct editionJsonAdapter from core.Edition
func NewEditionWriter(edition core.Edition) EditionWriter {
	adaptedFactions := []string{}
	coachRankings := map[string][]criteriaAdapter{}
	squadRankings := map[string][]criteriaAdapter{}
	for _, faction := range edition.AllowedFactions {
		adaptedFactions = append(adaptedFactions, string(faction))
	}
	adapter := EditionWriter{
		ID:              fmt.Sprint(edition.ID),
		Name:            edition.Name,
		Day1:            edition.Day1,
		Day2:            edition.Day2,
		RoundNumber:     edition.RoundNumber,
		CurrentRound:    edition.CurrentRound,
		FirstDayRound:   edition.FirstDayRound,
		UseFinale:       edition.UseFinale,
		FullSquad:       edition.FullSquad,
		CoachPerSquad:   edition.CoachPerSquad,
		AllowedFactions: adaptedFactions,
		Organizer:       edition.Organizer,
	}
	if len(edition.CoachRankings) != 0 {
		for rankingName, criterias := range edition.CoachRankings {
			criteriaAdapters := []criteriaAdapter{}
			for _, criteria := range criterias {
				criteriaAdapter := criteriaAdapter{
					Label:       criteria.Label,
					Field:       criteria.Field.String(),
					RankingType: criteria.Type.String(),
					Descending:  criteria.Descending,
				}
				criteriaAdapters = append(criteriaAdapters, criteriaAdapter)
			}
			coachRankings[rankingName] = criteriaAdapters
		}
		adapter.Ranking = rankingSettingsAdapter{Coach: coachRankings}
	}
	if len(edition.SquadRankings) != 0 {
		for rankingName, criterias := range edition.SquadRankings {
			criteriaAdapters := []criteriaAdapter{}
			for _, criteria := range criterias {
				criteriaAdapter := criteriaAdapter{
					Label:       criteria.Label,
					Field:       criteria.Field.String(),
					RankingType: criteria.Type.String(),
					Descending:  criteria.Descending,
				}
				criteriaAdapters = append(criteriaAdapters, criteriaAdapter)
			}
			squadRankings[rankingName] = criteriaAdapters
		}
		adapter.Ranking.Squad = squadRankings
	}
	adapter.GamePoints.Default = edition.GamePoints.Default.String()
	adapter.GamePoints.Clauses = []clauseAdapter{}
	for _, clause := range edition.GamePoints.Clauses {
		serializedClause := clauseAdapter{ValueIfTrue: clause.ValueIfTrue.String(), Condition: clause.Condition.String()}
		adapter.GamePoints.Clauses = append(adapter.GamePoints.Clauses, serializedClause)
	}
	adapter.ConfrontationPoints.Default = edition.ConfrontationPoints.Default.String()
	adapter.ConfrontationPoints.Clauses = []clauseAdapter{}
	for _, clause := range edition.ConfrontationPoints.Clauses {
		serializedClause := clauseAdapter{ValueIfTrue: clause.ValueIfTrue.String(), Condition: clause.Condition.String()}
		adapter.ConfrontationPoints.Clauses = append(adapter.ConfrontationPoints.Clauses, serializedClause)
	}
	return adapter
}
