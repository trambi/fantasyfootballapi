// Package jsonadapter FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.

package jsonadapter

import (
	"encoding/json"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func TestCoachWriterToMinimalJson(t *testing.T) {
	// Given
	coach := core.Coach{ID: 1, Name: "coachname"}
	expected := `{"id":"1","name":"coachname"}`
	// When
	adapter := NewCoachWriter(coach)
	result, err := json.Marshal(adapter)
	// Then
	if err != nil {
		t.Fatalf("unexpected error while marshaling to json %s", err.Error())
	}
	stringResult := string(result)
	if stringResult != expected {
		t.Errorf("%s should be equal to %s: %s", stringResult, expected, cmp.Diff(stringResult, expected))
	}
}

func TestCoachJsonWriterToMaximalJson(t *testing.T) {
	// Given
	coach := core.Coach{
		ID:        1,
		Name:      "coachname",
		TeamName:  "team of coachdname",
		NafNumber: 2,
		Faction:   core.Faction("Faction 1"),
		Ready:     true,
	}
	expected := `{"id":"1","name":"coachname","teamName":"team of coachdname","nafNumber":2,"faction":"Faction 1","ready":true}`
	// When
	adapter := NewCoachWriter(coach)
	result, err := json.Marshal(adapter)
	// Then
	if err != nil {
		t.Fatal("unexpected error while marshaling to json: ", err.Error())
	}
	stringResult := string(result)
	if stringResult != expected {
		t.Errorf("%s should be equal to %s: %s", stringResult, expected, cmp.Diff(stringResult, expected))
	}
}
