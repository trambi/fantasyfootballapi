// Package jsonadapter FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.

package jsonadapter

import (
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// CoachReader allow to read JSON to  core.Coach
type CoachReader struct {
	Name      string `json:"name"`
	TeamName  string `json:"teamName,omitempty"`
	NafNumber uint   `json:"nafNumber,omitempty"`
	Faction   string `json:"faction"`
	Ready     bool   `json:"ready"`
	EditionID string `json:"editionId,omitempty"`
}

// ToCore create a core.Coach from a CoachJsonReader
func (s CoachReader) ToCore(edition core.Edition) (core.Coach, error) {
	coach, err := core.NewCoach(s.Name, core.Faction(s.Faction), edition)
	if err != nil {
		return coach, err
	}
	coach.TeamName = s.TeamName
	coach.NafNumber = s.NafNumber
	coach.Ready = s.Ready
	return coach, nil
}
