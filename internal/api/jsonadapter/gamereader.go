// Package jsonadapter FantasyFootballApi
// Copyright (C) 2023-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package jsonadapter

import (
	"errors"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// GameReader internal structure to read JSON to core.Game
type GameReader struct {
	EditionId    string  `json:"editionId,omitempty"`
	Round        uint    `json:"round"`
	Table        uint    `json:"table"`
	Played       bool    `json:"played"`
	Finale       bool    `json:"finale"`
	CoachId1     string  `json:"coachId1"`
	Td1          int     `json:"td1,omitempty"`
	Casualties1  int     `json:"casualties1,omitempty"`
	Completions1 int     `json:"completions1,omitempty"`
	Fouls1       int     `json:"fouls1,omitempty"`
	Points1      float64 `json:"points1,omitempty"`
	Special1     string  `json:"special1,omitempty"`
	CoachId2     string  `json:"coachId2"`
	Td2          int     `json:"td2,omitempty"`
	Casualties2  int     `json:"casualties2,omitempty"`
	Completions2 int     `json:"completions2,omitempty"`
	Fouls2       int     `json:"fouls2,omitempty"`
	Points2      float64 `json:"points2,omitempty"`
	Special2     string  `json:"special2,omitempty"`
}

// ToCore create a core.Game from a gameJsonAdapter
func (s GameReader) ToCore(edition core.Edition, coach1 core.Coach, coach2 core.Coach) (core.Game, error) {
	if edition.ID != coach1.Edition.ID {
		return core.Game{}, errors.New("edition id is different from edition id in coach1")
	}
	game, err := core.NewGame(s.Round, s.Table, coach1, coach2)
	if err != nil {
		return game, err
	}
	game.Played = s.Played
	game.Finale = s.Finale
	game.Td1 = s.Td1
	game.Casualties1 = s.Casualties1
	game.Completions1 = s.Completions1
	game.Fouls1 = s.Fouls1
	game.Points1 = s.Points1
	game.Special1 = s.Special1
	game.Td2 = s.Td2
	game.Casualties2 = s.Casualties2
	game.Completions2 = s.Completions2
	game.Fouls2 = s.Fouls2
	game.Points2 = s.Points2
	game.Special2 = s.Special2
	return game, nil
}
