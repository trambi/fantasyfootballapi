// Package jsonadapter FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.

package jsonadapter

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// coachInGameWriter to write a coach in game as JSON from core.Coach
type coachInGameWriter struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	TeamName  string `json:"teamName"`
	NafNumber uint   `json:"nafNumber,omitempty"`
	Faction   string `json:"faction"`
	SquadId   string `json:"squadId"`
}

// newCoachInGameWriter create a newCoachInGameWriter from a core.Coach
func newCoachInGameWriter(coach core.Coach) *coachInGameWriter {
	return &coachInGameWriter{ID: fmt.Sprint(coach.ID), Name: coach.Name,
		TeamName: coach.TeamName, NafNumber: coach.NafNumber,
		Faction: string(coach.Faction), SquadId: fmt.Sprint(coach.SquadID),
	}
}
