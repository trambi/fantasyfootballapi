// Package jsonadapter FantasyFootballApi
// Copyright (C) 2023-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package jsonadapter

import (
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// SquadReader read a core.Squad from Json
type SquadReader struct {
	Id        string              `json:"id,omitempty"`
	EditionID string              `json:"editionId,omitempty"`
	Name      string              `json:"name"`
	Members   []squadMemberReader `json:"teamMates"`
}

// squadMemberReader internal structure to json Marshaling and Unmarshaling core.Coach member of core.Squad
type squadMemberReader struct {
	ID string `json:"id"`
}

// ToCore create a core.Squad from a SquadAdapter
func (s SquadReader) ToCore(edition core.Edition, members []core.Coach) (core.Squad, error) {
	squad, err := core.NewSquad(s.Name, edition, members)
	if err != nil {
		return squad, err
	}
	return squad, nil
}
