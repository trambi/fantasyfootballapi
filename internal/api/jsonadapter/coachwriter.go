// Package jsonadapter FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.

package jsonadapter

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// CoachWriter to write JSON from core.Coach
type CoachWriter struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	TeamName  string `json:"teamName,omitempty"`
	NafNumber uint   `json:"nafNumber,omitempty"`
	Faction   string `json:"faction,omitempty"`
	Ready     bool   `json:"ready,omitempty"`
	Edition   string `json:"editionName,omitempty"`
	EditionID string `json:"editionId,omitempty"`
	SquadName string `json:"squadName,omitempty"`
	SquadId   string `json:"squadId,omitempty"`
}

// NewCoachWriter create a coachJsonWriter from a core.Coach
func NewCoachWriter(coach core.Coach) CoachWriter {
	squadId, editionId := "", ""
	if coach.SquadID != 0 {
		squadId = fmt.Sprint(coach.SquadID)
	}
	if coach.Edition.ID != 0 {
		editionId = fmt.Sprint(coach.Edition.ID)
	}
	return CoachWriter{ID: fmt.Sprint(coach.ID), Name: coach.Name,
		TeamName: coach.TeamName, NafNumber: coach.NafNumber,
		Faction: string(coach.Faction), Ready: coach.Ready,
		Edition: coach.Edition.Name, EditionID: editionId,
		SquadId: squadId,
	}
}
