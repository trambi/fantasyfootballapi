// Package jsonadapter FantasyFootballApi
// Copyright (C) 2023-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package jsonadapter

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// GameWriter internal structure to write Json of core.Game
type GameWriter struct {
	Id           string             `json:"id,omitempty"`
	EditionId    string             `json:"editionId"`
	Round        uint               `json:"round"`
	Table        uint               `json:"table"`
	Played       bool               `json:"played"`
	Finale       bool               `json:"finale"`
	Coach1       *coachInGameWriter `json:"coach1"`
	Td1          int                `json:"td1"`
	Casualties1  int                `json:"casualties1"`
	Completions1 int                `json:"completions1"`
	Fouls1       int                `json:"fouls1"`
	Points1      float64            `json:"points1"`
	Special1     string             `json:"special1"`
	Coach2       *coachInGameWriter `json:"coach2"`
	Td2          int                `json:"td2"`
	Casualties2  int                `json:"casualties2"`
	Completions2 int                `json:"completions2"`
	Fouls2       int                `json:"fouls2"`
	Points2      float64            `json:"points2"`
	Special2     string             `json:"special2"`
}

// NewGameWriter create a GameWriter from a core.Game
func NewGameWriter(game core.Game) GameWriter {
	editionId := ""
	if game.Edition != nil {
		editionId = fmt.Sprint(game.Edition.ID)
	}
	var coach1, coach2 *coachInGameWriter
	if game.Coach1 != nil {
		coach1 = newCoachInGameWriter(*game.Coach1)
	}
	if game.Coach2 != nil {
		coach2 = newCoachInGameWriter(*game.Coach2)
	}
	return GameWriter{
		Id:           fmt.Sprint(game.ID),
		EditionId:    editionId,
		Round:        game.Round,
		Table:        game.Table,
		Played:       game.Played,
		Finale:       game.Finale,
		Coach1:       coach1,
		Td1:          game.Td1,
		Casualties1:  game.Casualties1,
		Completions1: game.Completions1,
		Fouls1:       game.Fouls1,
		Points1:      game.Points1,
		Special1:     game.Special1,
		Coach2:       coach2,
		Td2:          game.Td2,
		Casualties2:  game.Casualties2,
		Completions2: game.Completions2,
		Fouls2:       game.Fouls2,
		Points2:      game.Points2,
		Special2:     game.Special2,
	}
}
