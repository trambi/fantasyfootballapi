// Package jsonadapter FantasyFootballApi
// Copyright (C) 2023-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package jsonadapter

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// SquadWriter internal structure to write core.Squad as Json
type SquadWriter struct {
	Id        string               `json:"id,omitempty"`
	EditionID string               `json:"editionId,omitempty"`
	Name      string               `json:"name"`
	Members   []coachInSquadWriter `json:"teamMates"`
}

// NewSquadWriter create a new SquadJsonAdapter from a core.Squad
func NewSquadWriter(squad core.Squad) SquadWriter {
	members := []coachInSquadWriter{}
	for _, member := range squad.Members {
		teamMate := newCoachInSquadWriter(member)
		members = append(members, *teamMate)
	}
	return SquadWriter{
		Id:        fmt.Sprint(squad.ID),
		EditionID: fmt.Sprint(squad.Edition.ID),
		Name:      squad.Name,
		Members:   members,
	}
}
