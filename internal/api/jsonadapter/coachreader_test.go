// Package jsonadapter FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.

package jsonadapter

import (
	"encoding/json"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func TestCoachReaderFromMinimalJson(t *testing.T) {
	// Given
	var adapter CoachReader
	faction := core.Faction("Faction 1")
	edition, err := core.NewEdition("edition", "2023-12-31", 5, 3, false, true, 0, []core.Faction{faction}, "test", "organizer")
	if err != nil {
		t.Fatal("unexpected error while construct edition: ", err.Error())
	}
	expected, _ := core.NewCoach("coachname", faction, edition)
	input := []byte(`{"name":"coachname","faction":"Faction 1"}`)
	err = json.Unmarshal(input, &adapter)
	if err != nil {
		t.Fatal("unexpected error while marshaling to json: ", err.Error())
	}
	// When
	result, err := adapter.ToCore(edition)
	// Then
	if err != nil {
		t.Fatal("toCore should not return an error got: ", err.Error())
	}
	if !cmp.Equal(result, expected) {
		t.Errorf("%v should be equal to %v: %v", result, expected, cmp.Diff(result, expected))
	}
}

func TestCoachReaderFromMaximalJson(t *testing.T) {
	// Given
	var adapter CoachReader
	faction := core.Faction("Faction 1")
	edition, err := core.NewEdition("edition", "2023-12-31", 5, 3, false, true, 0, []core.Faction{faction}, "test", "organizer")
	if err != nil {
		t.Fatal("unexpected error while constructing Edition: ", err.Error())
	}
	expected := core.Coach{
		ID:        0, //CoachJsonReader ignore id
		Name:      "coachname",
		TeamName:  "squadname",
		NafNumber: 2,
		Faction:   faction,
		Ready:     true,
		Edition:   edition,
	}
	input := []byte(`{"name":"coachname","teamName":"squadname","nafNumber":2,"faction":"Faction 1","ready":true}`)

	err = json.Unmarshal(input, &adapter)
	if err != nil {
		t.Fatal("unexpected error while marshaling to json: ", err.Error())
	}
	// Then
	result, err := adapter.ToCore(edition)
	// When
	if err != nil {
		t.Fatal("toCore should not return an error got: ", err.Error())
	}
	if !cmp.Equal(result, expected) {
		t.Errorf("%v should be equal to %v: %v", result, expected, cmp.Diff(result, expected))
	}
}
