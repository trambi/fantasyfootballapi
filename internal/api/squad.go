// FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package api

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/trambi/fantasyfootballapi/internal/api/jsonadapter"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func (api *API) squadHandlers() routes {
	return routes{
		route{
			"AddSquad",
			"POST",
			"/v2/editions/{editionId}/squads",
			api.addSquadToEdition,
		},
		route{
			"GetSquadsByEdition",
			"GET",
			"/v2/editions/{editionId}/squads",
			api.getSquadsByEdition,
		},
		route{
			"DeleteSquad",
			"DELETE",
			"/v2/squads/{squadId}",
			api.deleteSquad,
		},
		route{
			"GetSquad",
			"GET",
			"/v2/squads/{squadId}",
			api.getSquadById,
		},
		route{
			"UpdateSquad",
			"PUT",
			"/v2/squads/{squadId}",
			api.updateSquad,
		},
		route{
			"PatchSquad",
			"PATCH",
			"/v2/squads/{squadId}",
			api.patchSquad,
		},
		route{
			"GetGamesBySquad",
			"GET",
			"/v2/squads/{squadId}/games",
			api.getGamesBySquad,
		},
		route{
			"OptionsOnSquads",
			strings.ToUpper("Options"),
			"/v2/editions/{editionId}/squads",
			api.optionsOnEntities,
		},
		route{
			"OptionsOnSquad",
			strings.ToUpper("Options"),
			"/v2/squads/{squadId}",
			api.optionsOnPatchableEntity,
		},
	}
}

// addSquadToEdition adds a squad to a specific edition
func (api *API) addSquadToEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Expose-Headers", "Location")
	if r.Body == nil {
		log.Println("Bad request - empty body")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println("Bad request - unable to read body: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var adapter jsonadapter.SquadReader
	err = json.Unmarshal(body, &adapter)
	if err != nil {
		log.Println("Bad request - unable to unmarshal json body: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	editionId, err := extractEditionId(r)
	if err != nil {
		log.Println("Bad request - unable to extract edition id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	edition, err := api.Data.GetEdition(editionId)
	if err != nil {
		log.Println("Not found - unable to get edition")
		w.WriteHeader(http.StatusNotFound)
		return
	}
	members := []core.Coach{}
	for _, teammate := range adapter.Members {
		coachId, err := stringToUint(teammate.ID)
		if err != nil {
			log.Printf("Bad request - error while converting teammate ID in integer %s: %s\n", teammate.ID, err.Error())
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		coach, err := api.Data.GetCoach(coachId)
		if err != nil {
			log.Printf("Bad request - error while getting teammate ID |%d|: %s\n", coachId, err.Error())
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		members = append(members, coach)
	}

	squad, err := adapter.ToCore(edition, members)
	if err != nil {
		log.Printf("Bad request - Receive game is invalid: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	id, err := api.Data.CreateSquad(squad)
	if err != nil {
		log.Println("Internal error: ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Println("created ", id)
	location := fmt.Sprintf("/v2/squads/%v", id)
	w.Header().Set("Location", location)
	w.WriteHeader(http.StatusCreated)
}

func (api *API) getSquadById(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, err := extractSquadId(r)
	if err != nil {
		log.Println("Bad request - unable to extract squad id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	squad, err := api.Data.GetSquad(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	encoded, _ := json.Marshal(jsonadapter.NewSquadWriter(squad))
	io.Writer.Write(w, encoded)
}

func (api *API) updateSquad(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, err := extractSquadId(r)
	if err != nil {
		log.Println("Bad request - unable to extract squad id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	previousSquad, err := api.Data.GetSquad(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println("Bad request - can not read the body: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var adapter jsonadapter.SquadReader
	err = json.Unmarshal(body, &adapter)
	if err != nil {
		log.Println("Bad request - error during json unmarshal: ", err.Error(), string(body))
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	members := []core.Coach{}
	for _, teammate := range adapter.Members {
		coachId, err := stringToUint(teammate.ID)
		if err != nil {
			log.Printf("Bad request - error while converting teammate ID in integer %s: %s\n", teammate.ID, err.Error())
			w.WriteHeader(http.StatusBadRequest)
		}
		coach, err := api.Data.GetCoach(coachId)
		if err != nil {
			log.Printf("Bad request - error while getting teammate ID %d: %s\n", coachId, err.Error())
			w.WriteHeader(http.StatusBadRequest)
		}
		members = append(members, coach)
	}
	updatedSquad, err := adapter.ToCore(previousSquad.Edition, members)
	if err != nil {
		log.Println("Bad request - received squad is invalid: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	updatedSquad.ID = id
	err = api.Data.UpdateSquad(updatedSquad)
	log.Println("Squad updated")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func (api *API) patchSquad(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, err := extractSquadId(r)
	if err != nil {
		log.Println("Bad request - unable to extract squad id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetSquad(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println("Bad request - can not read the body: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var adapter jsonadapter.ReadyReader
	err = json.Unmarshal(body, &adapter)
	if err != nil {
		log.Println("Bad request - error during json unmarshal: ", err.Error(), string(body))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = api.Data.UpdateSquadReadiness(id, adapter.Ready)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Println("Squad patched")
	w.WriteHeader(http.StatusNoContent)
}

func (api *API) deleteSquad(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, err := extractSquadId(r)
	if err != nil {
		log.Println("Bad request - unable to extract squad id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetSquad(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}

	err = api.Data.DeleteSquad(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func (api *API) getSquadsByEdition(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractEditionId(r)
	if err != nil {
		log.Println("Bad request - unable to extract edition id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetEdition(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	squads, err := api.Data.GetSquadsByEdition(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	adapted := []jsonadapter.SquadWriter{}
	for _, squad := range squads {
		adapted = append(adapted, jsonadapter.NewSquadWriter(squad))
	}
	encoded, _ := json.Marshal(adapted)
	io.Writer.Write(w, encoded)
}

func (api *API) getGamesBySquad(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	id, err := extractSquadId(r)
	if err != nil {
		log.Println("Bad request - unable to extract squad id: ", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, err = api.Data.GetSquad(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	games, err := api.Data.GetGamesBySquad(id)
	if err != nil {
		log.Println("Not found: ", err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	adapted := []jsonadapter.GameWriter{}
	for _, game := range games {
		if game.Coach2.SquadID == id {
			adapted = append(adapted, jsonadapter.NewGameWriter(game.Reverse()))
		} else {
			adapted = append(adapted, jsonadapter.NewGameWriter(game))
		}

	}

	encoded, _ := json.Marshal(adapted)
	io.Writer.Write(w, encoded)
}

func extractSquadId(r *http.Request) (uint, error) {
	vars := mux.Vars(r)
	fieldName := "squadId"
	inputId, exists := vars[fieldName]
	if !exists {
		log.Printf("can not find %s parameter\n", fieldName)
		return 0, fmt.Errorf("can not find %s parameter", fieldName)
	}
	id, err := stringToUint(inputId)
	if err != nil {
		log.Printf("can not use %s %v as integer: %v\n", fieldName, inputId, err.Error())
		return 0, fmt.Errorf("can not use %s %v as integer: %v", fieldName, inputId, err.Error())
	}
	return id, nil
}
