// FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"

	"gitlab.com/trambi/fantasyfootballapi/internal/api/jsonadapter"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func TestEditionInvalidInput(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	path := s.URL + "/v2/editions"
	testPostWithoutBody(client, path, t)
	testPostWithInvalidJson(client, path, t)
	//testWithUnknownJson(client, url, "POST", t)
}

func TestEditionNotFound(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	notExistentUrl := s.URL + "/v2/editions/10453"
	response, _ := client.Get(notExistentUrl)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", notExistentUrl), t)
	req, _ := http.NewRequest("PUT", notExistentUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("POST on %v", notExistentUrl), t)
	req, _ = http.NewRequest("DELETE", notExistentUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("DELETE on %v", notExistentUrl), t)
}

func TestEditionInvalidId(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	invalidUrl := s.URL + "/v2/editions/AAA"
	response, _ := client.Get(invalidUrl)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("GET on %v", invalidUrl), t)
	req, _ := http.NewRequest("PUT", invalidUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("PUT on %v", invalidUrl), t)
	req, _ = http.NewRequest("DELETE", invalidUrl, nil)
	response, _ = client.Do(req)
	checkResponseCode(http.StatusBadRequest, response.StatusCode, fmt.Sprintf("DELETE on %v", invalidUrl), t)
}

func TestGetEditions(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	url1 := utilAddEdition(s, time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC), t)
	url2 := utilAddEdition(s, time.Date(2021, 6, 12, 0, 0, 0, 0, time.UTC), t)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url1, t)
		testDeleteOk(client, s.URL+url2, t)
	})
	body := testGetOk(client, s.URL+"/v2/editions", t)
	var editions []jsonadapter.EditionWriter
	err := json.Unmarshal(body, &editions)
	if err != nil {
		t.Log("rawEditions: ", body)
		t.Error("Error in response body parsing")
	}
	if len(editions) < 2 {
		t.Errorf("length of getEditions is lower than 2")
	}
}

func TestCreateGetAndDeleteEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	url := utilAddEdition(s, time.Date(2020, 6, 12, 0, 0, 0, 0, time.UTC), t)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url, t)
	})
	testGetOk(client, s.URL+url, t)
}

func TestGetCurrentEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	now := time.Now()
	day1Before := now.AddDate(-1, 0, 0)
	day1Current := now.AddDate(0, 0, 3)
	day1longAfter := now.AddDate(1, 0, 0)
	urlBefore := utilAddEdition(s, day1Before, t)
	urlCurrent := utilAddEdition(s, day1Current, t)
	urlLongAfter := utilAddEdition(s, day1longAfter, t)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+urlBefore, t)
		testDeleteOk(client, s.URL+urlCurrent, t)
		testDeleteOk(client, s.URL+urlLongAfter, t)
	})
	body := testGetOk(client, s.URL+"/v2/editions/current", t)
	var edition jsonadapter.EditionWriter
	err := json.Unmarshal(body, &edition)
	if err != nil {
		t.Error("Error in response body parsing")
	}
	if edition.Day1 != strings.Split(day1Current.Format(time.RFC3339), "T")[0] {
		t.Errorf("getCurrentEdition.Day1 %s != %s ", edition.Day1, strings.Split(day1Current.Format(time.RFC3339), "T")[0])
	}
}

func TestGetCurrentEditionNotFound(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	response, _ := client.Get(s.URL + "/v2/editions/current")
	if response == nil {
		t.Fatal("Unexpected nil response")
	}
	checkResponseCode(http.StatusNotFound, response.StatusCode, fmt.Sprintf("GET on %v", s.URL+"/v2/editions/current"), t)
}

func TestUpdateEdition(t *testing.T) {
	s := getTestServer()
	client := s.Client()
	faction := core.Faction("Faction 1")
	edition, err := core.NewEdition("Tournament", "2020-06-12", 5, 3, true, true, 2, []core.Faction{faction}, "update", "organizer")
	if err != nil {
		t.Fatal("Unexpected error while construction edition", err.Error())
	}
	url := postEdition(t, s, edition)
	t.Cleanup(func() {
		testDeleteOk(client, s.URL+url, t)
	})
	log.Printf("url: %s => id: %s", s.URL+url, strings.Split(url, "/")[2])
	tempId, err := strconv.ParseUint(strings.Split(url, "/")[3], 10, 64)
	if err != nil {
		t.Fatal("Unexpected error while extracting id from location", err.Error())
	}
	edition.ID = uint(tempId)
	edition.CurrentRound = 1
	editionAdapter := jsonadapter.NewEditionWriter(edition)
	payload, err := json.Marshal(editionAdapter)
	if err != nil {
		t.Fatal("Unexpected error while serializing edition json", err.Error())
	}
	req, _ := http.NewRequest("PUT", s.URL+url, bytes.NewBuffer(payload))
	response, _ := client.Do(req)

	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("PUT on %v", url), t)
	_, err = io.ReadAll(response.Body)
	if err != nil {
		t.Fatal("Unexpected error while reading body of PUT: ", err.Error())
	}
	body := testGetOk(client, s.URL+url, t)
	var receivedEdition jsonadapter.EditionWriter
	err = json.Unmarshal(body, &receivedEdition)
	if err != nil {
		t.Error("Error in response body parsing: ", err.Error())
	}
	if edition.CurrentRound != receivedEdition.CurrentRound {
		t.Errorf("receivedEdition.CurrentRound %d != %d ", receivedEdition.CurrentRound, edition.CurrentRound)
	}
}

func TestOptionsOnEditions(t *testing.T) {
	// Given
	expectedAllow := "GET, POST, OPTIONS"
	s := getTestServer()
	urlSuffix := "/v2/editions"
	url := s.URL + urlSuffix
	req, err := http.NewRequest("OPTIONS", url, nil)
	if err != nil {
		t.Fatalf("Unexpected error while OPTIONS %s, got %v", url, err.Error())
	}
	client := s.Client()
	// When
	response, _ := client.Do(req)
	// Then
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("OPTIONS on %v", urlSuffix), t)
	allow := response.Header.Get("Access-Control-Allow-Methods")
	if allow != expectedAllow {
		t.Errorf("allow header is %s, it should be %s", allow, expectedAllow)
	}
}

func TestOptionsOnEdition(t *testing.T) {
	// Given
	expectedAllow := "GET, PUT, DELETE, OPTIONS"
	s := getTestServer()
	urlSuffix := "/v2/editions/1"
	url := s.URL + urlSuffix
	req, err := http.NewRequest("OPTIONS", url, nil)
	if err != nil {
		t.Fatalf("Unexpected error while OPTIONS %s, got %v", url, err.Error())
	}
	client := s.Client()
	// When
	response, _ := client.Do(req)
	// Then
	checkResponseCode(http.StatusNoContent, response.StatusCode, fmt.Sprintf("OPTIONS on %v", urlSuffix), t)
	allow := response.Header.Get("Access-Control-Allow-Methods")
	if allow != expectedAllow {
		t.Errorf("allow header is %s, it should be %s", allow, expectedAllow)
	}
}

func createTestEdition(firstDate time.Time) core.Edition {
	day1 := firstDate.Format(time.DateOnly)
	edition, _ := core.NewEdition(fmt.Sprintf("Tournament of %s", day1), day1, 5, 3, true, true, 2, []core.Faction{"faction1"}, "a", "organizer")
	return edition
}

func utilAddEdition(server *httptest.Server, firstDate time.Time, t *testing.T) string {
	return postEdition(t, server, createTestEdition(firstDate))
}

func postEdition(t *testing.T, server *httptest.Server, edition core.Edition) string {
	t.Helper()
	editionSerializer := jsonadapter.NewEditionWriter(edition)
	payload, err := json.Marshal(editionSerializer)
	if err != nil {
		t.Fatal("unexpected error while serializing edition json: ", err.Error())
	}
	return postPayloadTo(server.Client(), payload, server.URL+"/v2/editions", t)
}
