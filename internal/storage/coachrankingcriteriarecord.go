// Package storage FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

type coachRankingCriteriaRecord struct {
	ID          uint
	RankingName string `gorm:"not null"`
	Order       uint   `gorm:"not null"`
	Label       string `gorm:"not null"`
	Domain      string `gorm:"not null"`
	Type        string `gorm:"not null"`
	Descending  bool   `gorm:"not null"`
	Edition     editionRecord
	EditionID   uint
}

func (coachRankingCriteriaRecord) TableName() string {
	return "coach_ranking_criterias"
}

func (record coachRankingCriteriaRecord) toCore() (core.RankingCriteria, error) {
	setting := core.RankingCriteria{}
	setting.Label = record.Label
	setting.Descending = record.Descending
	domain, err := core.NewRankingField(record.Domain)
	if err != nil {
		return setting, fmt.Errorf("error in reading ranking domain: %s", err)
	}
	setting.Field = domain
	rankingType, err := core.NewRankingType(record.Type)
	if err != nil {
		return setting, fmt.Errorf("error in reading ranking type: %s", err)
	}
	setting.Type = rankingType
	return setting, nil
}

func newCoachRankingCriteriaRecord(rankingSetting core.RankingCriteria, rankingName string, order uint) coachRankingCriteriaRecord {
	record := coachRankingCriteriaRecord{}
	record.Label = rankingSetting.Label
	record.Domain = rankingSetting.Field.String()
	record.Type = rankingSetting.Type.String()
	record.Descending = rankingSetting.Descending
	record.RankingName = rankingName
	record.Order = order
	return record
}

type coachRankingCriteriaRecords []coachRankingCriteriaRecord

func (c coachRankingCriteriaRecords) Len() int { return len(c) }

func (c coachRankingCriteriaRecords) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}
func (c coachRankingCriteriaRecords) Less(i, j int) bool {
	if c[i].RankingName != c[j].RankingName {
		return c[i].RankingName < c[j].RankingName
	}
	return c[i].Order < c[j].Order
}
