// Package storage FantasyFootballApi
// Copyright (C) 2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func TestCreateGetDeleteCoach(t *testing.T) {
	// Given
	dataProvider := InitDataProvider("sqlite", ":memory:")
	t.Cleanup(func() {
		dataProvider.close()
	})
	faction1 := core.Faction("faction1")
	edition, err := core.NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3, true, false, 0, []core.Faction{faction1}, "Something", "Founder")
	if err != nil {
		t.Fatal("Unexpected error while constructing edition: ", err.Error())
	}
	editionId, err := dataProvider.CreateEdition(edition)
	if err != nil {
		t.Fatal("Unexpected error while creating edition: ", err.Error())
	}
	edition.ID = editionId
	coach, err := core.NewCoach("Majestic Coach", faction1, edition)
	if err != nil {
		t.Fatal("Unexpected error while constructing coach: ", err.Error())
	}
	coach.TeamName = "A beautiful team"
	coach.NafNumber = 22
	// When insert coach
	coachId, err := dataProvider.CreateCoach(coach)
	// Then insert coach
	if err != nil {
		t.Fatal("Unexpected error while creating coach: ", err.Error())
	}
	coach.ID = coachId
	// When get coach
	result, err := dataProvider.GetCoach(coachId)
	// Then get coach
	if err != nil {
		t.Fatal("Unexpected error while getting coach: ", err.Error())
	}
	if cmp.Equal(result, coach) != true {
		t.Fatal("- removed from coach ,+ added to result: ", cmp.Diff(coach, result))
	}
	// When delete coach
	err = dataProvider.DeleteCoach(coachId)
	// Then delete coach
	if err != nil {
		t.Fatal("Unexpected error while deleting coach: ", err.Error())
	}
	result, err = dataProvider.GetCoach(coachId)
	if err == nil {
		t.Fatal("Unexpected return while getting coach after delete: ", result)
	}
}

func TestUpdateCoachReadiness(t *testing.T) {
	// GIVEN
	dataProvider := InitDataProvider("sqlite", ":memory:")
	t.Cleanup(func() {
		dataProvider.close()
	})
	faction1 := core.Faction("faction1")
	edition, err := core.NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3, true, false, 0, []core.Faction{faction1}, "Something", "Founder")
	if err != nil {
		t.Fatal("Unexpected error while constructing edition: ", err.Error())
	}
	editionId, err := dataProvider.CreateEdition(edition)
	if err != nil {
		t.Fatal("Unexpected error while creating edition: ", err.Error())
	}
	edition.ID = editionId
	coach, err := core.NewCoach("Majestic Coach", faction1, edition)
	if err != nil {
		t.Fatal("Unexpected error while constructing coach: ", err.Error())
	}
	coach.TeamName = "A beautiful team"
	coach.NafNumber = 22
	coachId, err := dataProvider.CreateCoach(coach)
	if err != nil {
		t.Fatal("Unexpected error while creating coach: ", err.Error())
	}
	coach.ID = coachId
	// WHEN
	err = dataProvider.UpdateCoachReadiness(coachId, true)
	// THEN
	if err != nil {
		t.Fatal("Unexpected error while update coach readiness: ", err.Error())
	}
	coach.Ready = true
	result, err := dataProvider.GetCoach(coachId)
	if err != nil {
		t.Fatal("Unexpected error while getting coach: ", err.Error())
	}
	if cmp.Equal(result, coach) != true {
		t.Fatal("- removed from coach ,+ added to result: ", cmp.Diff(coach, result))
	}

}
