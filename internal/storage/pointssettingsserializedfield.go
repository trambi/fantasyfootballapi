// Package storage FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

type clauseSerializable struct {
	Condition   string
	ValueIfTrue string
}

type pointsSettingsSerializedField struct {
	Default string
	Clauses []clauseSerializable
}

func newPointsSettingsSerializedField(pointsSettings core.PointsSettings) pointsSettingsSerializedField {
	field := pointsSettingsSerializedField{
		Default: pointsSettings.Default.String(),
		Clauses: []clauseSerializable{},
	}
	for _, clause := range pointsSettings.Clauses {
		serializable := clauseSerializable{
			ValueIfTrue: clause.ValueIfTrue.String(),
			Condition:   clause.Condition.String(),
		}
		field.Clauses = append(field.Clauses, serializable)
	}
	return field
}

func (field pointsSettingsSerializedField) toCore() (core.PointsSettings, error) {
	defaultValue, err := core.NewValuable(field.Default)
	if err != nil {
		return core.PointsSettings{}, fmt.Errorf("unable to create value from default field: %s", err.Error())
	}
	pointsSettings := core.PointsSettings{
		Default: defaultValue,
		Clauses: []core.Clause{},
	}

	for index, serializable := range field.Clauses {
		valueIfTrue, err := core.NewValuable(serializable.ValueIfTrue)
		if err != nil {
			return core.PointsSettings{}, fmt.Errorf("unable to create value from valueIfTrue %d: %s", index, err.Error())
		}
		clause := core.Clause{
			ValueIfTrue: valueIfTrue,
		}
		condition, err := core.NewCondition(serializable.Condition)
		if err != nil {
			return pointsSettings, fmt.Errorf("during parsing of condition: \"%v\": %s,", serializable.Condition, err)
		}
		clause.Condition = condition
		pointsSettings.Clauses = append(pointsSettings.Clauses, clause)
	}
	return pointsSettings, nil
}
