// Package storage defines storage structures for FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// GetCoach returns the Coach identified by id
func (dp DataProvider) GetCoach(id uint) (core.Coach, error) {
	var record coachRecord
	tx := dp.db.Preload("Edition").First(&record, id)
	if tx.Error != nil {
		return core.Coach{}, fmt.Errorf("error in GetCoach while querying database:\n%s", tx.Error.Error())
	}
	return record.toCore()
}

// CreateCoach stores a coach into storage
func (dp DataProvider) CreateCoach(coach core.Coach) (uint, error) {
	record, err := newCoachRecord(coach)
	if err != nil {
		return 0, fmt.Errorf("unable to create coach record: %v", err)
	}
	tx := dp.db.Omit("Edition").Create(&record)
	return record.ID, tx.Error
}

// UpdateCoach updates a coach and only a coach
func (dp DataProvider) UpdateCoach(coach core.Coach) error {
	record, err := newCoachRecord(coach)
	if err != nil {
		return fmt.Errorf("unable to create coach record: %v", err)
	}
	return dp.db.Save(&record).Error
}

func (dp DataProvider) UpdateCoachReadiness(id uint, readiness bool) error {
	return dp.db.Model(&coachRecord{}).Where("id = ?", id).Update("Ready", readiness).Error
}

// DeleteCoach delete a coach identified by id
func (dp DataProvider) DeleteCoach(id uint) error {
	result := dp.db.Delete(coachRecord{}, id)
	if result.Error != nil {
		return result.Error
	}
	if result.RowsAffected != 1 {
		return fmt.Errorf("unusual number of coach deleted: %v", result.RowsAffected)
	}
	return nil
}

// GetCoachsByEdition returns all coachs in an edition
func (dp DataProvider) GetCoachsByEdition(idEdition uint) ([]core.Coach, error) {
	var coachs []core.Coach
	var records []coachRecord
	result := dp.db.Preload("Edition").Where("edition_id = ?", idEdition).Find(&records)
	if result.Error != nil {
		return coachs, fmt.Errorf("error in GetCoachsInEdition while querying database:\n%s", result.Error.Error())
	}
	for _, record := range records {
		coach, err := record.toCore()
		if err != nil {
			return []core.Coach{}, fmt.Errorf("error in GetCoachsInEdition while extracting Coach record to core Coach:\n%s", err.Error())
		}
		coachs = append(coachs, coach)
	}
	return coachs, nil
}
