// Package storage FantasyFootballApi
// Copyright (C) 2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// TestCreateGetDeleteEdition
// tests a cycle of create, get, delete, get of one edition
func TestCreateGetDeleteEdition(t *testing.T) {

	dataProvider := InitDataProvider("sqlite", ":memory:")
	t.Cleanup(func() {
		dataProvider.close()
	})
	faction1 := core.Faction("faction1")
	faction2 := core.Faction("faction2")
	edition, err := core.NewEdition("Edition 1", "2023-05-13", 5, 3, true, false, 2, []core.Faction{faction1, faction2}, "Something", "organiser")
	if err != nil {
		t.Fatal("Unexpected error while constructing edition: ", err.Error())
	}
	edition.AddCoachTdRanking()
	edition.AddSquadCasualtiesRanking()
	edition.CurrentRound = 1
	err = edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during add game points for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during add game points for win: ", err.Error())
	}
	err = edition.AddConfrontationPointsClause("1", "{PointsFor}={PointsAgainst}")
	if err != nil {
		t.Fatal("unexpected error during add confrontation points for draw: ", err.Error())
	}
	err = edition.AddConfrontationPointsClause("3", "{PointsFor}>{PointsAgainst}")
	if err != nil {
		t.Fatal("unexpected error during add confrontation points for win: ", err.Error())
	}
	id, err := dataProvider.CreateEdition(edition)
	if err != nil {
		t.Fatal("Unexpected error while creating edition: ", err.Error())
	}
	// To create an expected object
	edition.ID = id
	result, err := dataProvider.GetEdition(id)
	if err != nil {
		t.Fatal("Unexpected error while getting edition: ", err.Error())
	}
	if cmp.Equal(result, edition) != true {
		t.Fatal("result differs from expected: ", cmp.Diff(result, edition))
	}
	err = dataProvider.DeleteEdition(id)
	if err != nil {
		t.Fatal("Unexpected error while deleting edition: ", err.Error())
	}
	result, err = dataProvider.GetEdition(id)
	if err == nil {
		t.Fatal("Unexpected return while getting edition after delete: ", result)
	}
}

// TestUpdateGetMultipleEdition
// tests a cycle of create two editions, update one edition,
// get all and  get current edition
func TestUpdateGetMultipleEdition(t *testing.T) {
	// Given
	var (
		id     uint
		err    error
		result []core.Edition
	)
	dataProvider := InitDataProvider("sqlite", ":memory:")
	t.Cleanup(func() {
		dataProvider.close()
	})
	faction1 := core.Faction("faction1ForEdition1")
	faction2 := core.Faction("faction2ForEdition1")
	faction3 := core.Faction("faction1ForEdition2")
	faction4 := core.Faction("faction2ForEdition2")
	edition1, err := core.NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3, true, false, 2, []core.Faction{faction1, faction2}, "Something", "Founder")
	if err != nil {
		t.Fatal("Unexpected error while constructing edition1: ", err.Error())
	}
	edition2, err := core.NewEdition("Awesome Tournament 2023", "2023-01-01", 6, 4, false, false, 2, []core.Faction{faction1, faction2}, "SomethingElse", "Founder")
	if err != nil {
		t.Fatal("Unexpected error while constructing edition2: ", err.Error())
	}
	// when insert 1
	id, err = dataProvider.CreateEdition(edition1)

	// then insert 1
	if err != nil {
		t.Fatal("Unexpected error while creating edition1: ", err.Error())
	}
	edition1.ID = id
	// when insert 2
	id, err = dataProvider.CreateEdition(edition2)
	// then insert 2
	if err != nil {
		t.Fatal("Unexpected error while creating edition2: ", err.Error())
	}
	edition2.ID = id
	edition2.Name = "Very Awesome Tournament 2023"
	edition2.Day1 = "2023-05-13"
	edition2.Day2 = "2023-05-14"
	edition2.RoundNumber = 5
	edition2.CurrentRound = 0
	edition2.FirstDayRound = 3
	edition2.UseFinale = true
	edition2.FullSquad = true
	edition2.CoachPerSquad = 3
	edition2.RankingStrategyName = "SomethingElseAgain"
	edition2.AllowedFactions = []core.Faction{faction3, faction4}
	edition2.Organizer = "Organizer5"
	expected := []core.Edition{
		edition1,
		edition2,
	}
	// when update 2
	err = dataProvider.UpdateEdition(edition2)
	// then update 2
	if err != nil {
		t.Fatal("Unexpected error while updating edition2: ", err.Error())
	}
	// when get all
	result, err = dataProvider.GetEditions()
	// then get all
	if err != nil {
		t.Fatal("Unexpected error while getting editions: ", err.Error())
	}
	if cmp.Equal(result, expected) != true {
		t.Errorf("- removed from expected ,+ added to result:\n%v\n", cmp.Diff(expected, result))
	}
}

// TestCreateGetDeleteEdition
// tests a cycle of create, update, get of one edition
func TestUpdateEdition(t *testing.T) {

	dataProvider := InitDataProvider("sqlite", ":memory:")
	t.Cleanup(func() {
		dataProvider.close()
	})
	faction1 := core.Faction("faction1")
	faction2 := core.Faction("faction2")
	edition, err := core.NewEdition("Edition 1", "2023-05-13", 5, 3, true, false, 2, []core.Faction{faction1, faction2}, "Something", "organiser")
	if err != nil {
		t.Fatal("Unexpected error while constructing edition: ", err.Error())
	}
	edition.AddCoachTdRanking()
	edition.CurrentRound = 1
	id, err := dataProvider.CreateEdition(edition)
	if err != nil {
		t.Fatal("Unexpected error while creating edition: ", err.Error())
	}
	// To create an expected object
	edition.ID = id
	t.Cleanup(func() {
		dataProvider.DeleteEdition(id)
	})
	criteria1 := core.RankingCriteria{Label: "TdAgainst", Type: core.Against, Field: core.Td, Descending: true}
	criteria2 := core.RankingCriteria{Label: "TdOpponent", Type: core.Opponent, Field: core.Td, Descending: false}
	edition.CoachRankings["Defense"] = []core.RankingCriteria{criteria1, criteria2}
	// When
	err = dataProvider.UpdateEdition(edition)
	// Then
	if err != nil {
		t.Fatal("Unexpected error while updating edition: ", err.Error())
	}
	result, err := dataProvider.GetEdition(id)
	if err != nil {
		t.Fatal("Unexpected error while getting edition: ", err.Error())
	}
	if cmp.Equal(result, edition) != true {
		fmt.Printf("result: %v", result)
		t.Fatal("result differs from expected: ", cmp.Diff(result, edition))
	}
}
