// Package storage FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// gameRecord is an adapter to store core game in storage
type gameRecord struct {
	ID           uint
	EditionID    uint
	Edition      editionRecord
	Round        uint `gorm:"not null"`
	TableNumber  uint `gorm:"not null"`
	Played       bool `gorm:"not null, default false"`
	Finale       bool `gorm:"not null, default false"`
	Coach1ID     uint
	Coach1       coachRecord
	Td1          int
	Casualties1  int
	Completions1 int
	Fouls1       int
	Points1      float64
	Special1     string
	Coach2ID     uint
	Coach2       coachRecord
	Td2          int
	Casualties2  int
	Completions2 int
	Fouls2       int
	Points2      float64
	Special2     string
}

func (gameRecord) TableName() string {
	return "game"
}

func (record gameRecord) toCore() (core.Game, error) {
	edition, err := record.Edition.toCore()
	if err != nil {
		return core.Game{}, fmt.Errorf("error while edition.ToCore in gameRecord.ToCore: \n%s", err.Error())
	}
	record.Coach1.Edition = record.Edition
	record.Coach1.EditionID = record.EditionID
	coach1, err := record.Coach1.toCore()
	if err != nil {
		return core.Game{}, fmt.Errorf("error while coach1.ToCore in gameRecord.ToCore: \n%s", err.Error())
	}
	record.Coach2.Edition = record.Edition
	record.Coach2.EditionID = record.EditionID
	coach2, err := record.Coach2.toCore()
	if err != nil {
		return core.Game{}, fmt.Errorf("error while coach2.ToCore in gameRecord.ToCore: \n%s", err.Error())
	}
	game, err := core.NewGame(record.Round, record.TableNumber, coach1, coach2)
	if err != nil {
		return core.Game{}, err
	}
	game.Edition = &edition
	game.Edition.ID = record.EditionID
	game.ID = record.ID
	game.Played = record.Played
	game.Finale = record.Finale
	game.Td1 = record.Td1
	game.Casualties1 = record.Casualties1
	game.Completions1 = record.Completions1
	game.Fouls1 = record.Fouls1
	game.Points1 = record.Points1
	game.Special1 = record.Special1
	game.Td2 = record.Td2
	game.Casualties2 = record.Casualties2
	game.Completions2 = record.Completions2
	game.Fouls2 = record.Fouls2
	game.Points2 = record.Points2
	game.Special2 = record.Special2
	return game, nil
}

func newGameRecord(game core.Game) (gameRecord, error) {
	record := gameRecord{}
	edition, err := newEditionRecord(*game.Edition)
	if err != nil {
		return record, fmt.Errorf("unable to create edition record for a coach: %v", err)
	}
	coach1, err := newCoachRecord(*game.Coach1)
	if err != nil {
		return record, fmt.Errorf("unable to create coach1 record for a game: %v", err)
	}
	coach2, err := newCoachRecord(*game.Coach2)
	if err != nil {
		return record, fmt.Errorf("unable to create coach2 record for a game: %v", err)
	}

	record.ID = game.ID
	record.Edition = edition
	record.EditionID = game.Edition.ID
	record.Round = game.Round
	record.TableNumber = game.Table
	record.Played = game.Played
	record.Finale = game.Finale
	record.Coach1 = coach1
	record.Coach1ID = game.Coach1.ID
	record.Td1 = game.Td1
	record.Casualties1 = game.Casualties1
	record.Completions1 = game.Completions1
	record.Fouls1 = game.Fouls1
	record.Points1 = game.Points1
	record.Special1 = game.Special1
	record.Coach2 = coach2
	record.Coach2ID = game.Coach2.ID
	record.Td2 = game.Td2
	record.Casualties2 = game.Casualties2
	record.Completions2 = game.Completions2
	record.Fouls2 = game.Fouls2
	record.Points2 = game.Points2
	record.Special2 = game.Special2
	return record, nil
}
