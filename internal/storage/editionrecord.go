// Package storage FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"encoding/json"
	"fmt"
	"sort"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// editionRecord is an adapter to store a core Edition
type editionRecord struct {
	ID                  uint
	Name                string `gorm:"not null"`
	Day1                string `gorm:"not null"`
	Day2                string `gorm:"not null"`
	RoundNumber         uint   `gorm:"not null,default 5"`
	CurrentRound        uint   `gorm:"not null,default 0"`
	FirstDayRound       uint   `gorm:"not null,default 3"`
	UseFinale           bool   `gorm:"not null,default false"`
	FullSquad           bool   `gorm:"not null,default false"`
	RankingStrategyName string
	CoachPerSquad       uint
	AllowedFactions     string `gorm:"not null,default '[]'"`
	Organizer           string
	CoachCriterias      []coachRankingCriteriaRecord `gorm:"foreignKey:EditionID"`
	SquadCriterias      []squadRankingCriteriaRecord `gorm:"foreignKey:EditionID"`
	GamePoints          string
	ConfrontationPoints string
}

func (editionRecord) TableName() string {
	return "edition"
}

func (record *editionRecord) toCore() (core.Edition, error) {
	var allowedFactions []core.Faction
	json.Unmarshal([]byte(record.AllowedFactions), &(allowedFactions))
	edition, err := core.NewEdition(record.Name, record.Day1, record.RoundNumber, record.FirstDayRound, record.UseFinale, record.FullSquad,
		record.CoachPerSquad, allowedFactions, record.RankingStrategyName, record.Organizer)
	if err != nil {
		return edition, err
	}
	edition.ID = record.ID
	edition.Day2 = record.Day2
	edition.CurrentRound = record.CurrentRound
	err = record.extractCoachRankingCriterias(edition)
	if err != nil {
		return edition, err
	}
	err = record.extractSquadRankingCriterias(edition)
	if err != nil {
		return edition, err
	}
	var field pointsSettingsSerializedField
	err = json.Unmarshal([]byte(record.GamePoints), &field)
	if err != nil {
		return edition, fmt.Errorf("error while parsing json game points from database: %s", err.Error())
	}
	edition.GamePoints, err = field.toCore()
	if err != nil {
		return edition, fmt.Errorf("error while converting game points field to proper points settings: %s", err.Error())
	}
	err = json.Unmarshal([]byte(record.ConfrontationPoints), &field)
	if err != nil {
		return edition, fmt.Errorf("error while parsing json confrontation points from database: %s", err.Error())
	}
	edition.ConfrontationPoints, err = field.toCore()
	if err != nil {
		return edition, fmt.Errorf("error while converting confrontation points field to proper points settings: %s", err.Error())
	}
	return edition, err
}

func (record *editionRecord) extractCoachRankingCriterias(edition core.Edition) error {
	currentRankingName := ""
	currentCriterias := []core.RankingCriteria{}
	sort.Sort(coachRankingCriteriaRecords(record.CoachCriterias))
	for _, criteriaRecord := range record.CoachCriterias {
		if currentRankingName == "" {
			currentRankingName = criteriaRecord.RankingName
		} else if currentRankingName != criteriaRecord.RankingName {
			edition.CoachRankings[currentRankingName] = currentCriterias
			currentCriterias = []core.RankingCriteria{}
			currentRankingName = criteriaRecord.RankingName
		}
		criteria, err := criteriaRecord.toCore()
		if err != nil {
			return err
		}
		currentCriterias = append(currentCriterias, criteria)
	}
	if currentRankingName != "" {
		edition.CoachRankings[currentRankingName] = currentCriterias
	}
	return nil
}

func (record *editionRecord) extractSquadRankingCriterias(edition core.Edition) error {
	currentRankingName := ""
	currentCriterias := []core.RankingCriteria{}
	sort.Sort(squadRankingCriteriaRecords(record.SquadCriterias))
	for _, criteriaRecord := range record.SquadCriterias {
		if currentRankingName == "" {
			currentRankingName = criteriaRecord.RankingName
		} else if currentRankingName != criteriaRecord.RankingName {
			edition.SquadRankings[currentRankingName] = currentCriterias
			currentCriterias = []core.RankingCriteria{}
			currentRankingName = criteriaRecord.RankingName
		}
		criteria, err := criteriaRecord.toCore()
		if err != nil {
			return err
		}
		currentCriterias = append(currentCriterias, criteria)
	}
	if currentRankingName != "" {
		edition.SquadRankings[currentRankingName] = currentCriterias
	}
	return nil
}

func newEditionRecord(edition core.Edition) (editionRecord, error) {
	record := editionRecord{}
	record.ID = edition.ID
	record.Name = edition.Name
	record.Day1 = edition.Day1
	record.Day2 = edition.Day2
	record.RoundNumber = edition.RoundNumber
	record.CurrentRound = edition.CurrentRound
	record.FirstDayRound = edition.FirstDayRound
	record.UseFinale = edition.UseFinale
	record.FullSquad = edition.FullSquad
	record.RankingStrategyName = edition.RankingStrategyName
	record.CoachPerSquad = edition.CoachPerSquad
	serializedFactions, err := json.Marshal(edition.AllowedFactions)
	if err != nil {
		return record, fmt.Errorf("unable to create json for allowed factions: %v", err)
	}
	record.AllowedFactions = string(serializedFactions)
	record.Organizer = edition.Organizer
	record.CoachCriterias = []coachRankingCriteriaRecord{}
	for name, settings := range edition.CoachRankings {
		for index, criteria := range settings {
			record.CoachCriterias = append(record.CoachCriterias, newCoachRankingCriteriaRecord(criteria, name, uint(index)))
		}
	}
	record.SquadCriterias = []squadRankingCriteriaRecord{}
	for name, settings := range edition.SquadRankings {
		for index, criteria := range settings {
			record.SquadCriterias = append(record.SquadCriterias, newSquadRankingCriteriaRecord(criteria, name, uint(index)))
		}
	}
	serialized, err := json.Marshal(newPointsSettingsSerializedField(edition.GamePoints))
	if err != nil {
		return record, fmt.Errorf("unable to create json for game points: %v", err)
	}
	record.GamePoints = string(serialized)
	serialized, err = json.Marshal(newPointsSettingsSerializedField(edition.ConfrontationPoints))
	if err != nil {
		return record, fmt.Errorf("unable to create json for confrontation points: %v", err)
	}
	record.ConfrontationPoints = string(serialized)

	return record, nil
}
