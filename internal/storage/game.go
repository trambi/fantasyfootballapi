// Package storage defines storage structures for FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// GetGame returns the game identified by id
func (dp DataProvider) GetGame(id uint) (core.Game, error) {
	var record gameRecord
	tx := dp.db.Preload("Edition").Preload("Coach1").Preload("Coach2").First(&record, id)
	if tx.Error != nil {
		return core.Game{}, fmt.Errorf("error in GetGame while querying database:\n%s", tx.Error.Error())
	}
	return record.toCore()
}

// CreateGame stores a game into storage
func (dp DataProvider) CreateGame(game core.Game) (uint, error) {
	record, err := newGameRecord(game)
	if err != nil {
		return 0, fmt.Errorf("unable to create squad record: %v", err)
	}
	tx := dp.db.Omit("Edition", "Coach1", "Coach2").Create(&record)
	return record.ID, tx.Error
}

// UpdateGame updates a game and only a game
func (dp DataProvider) UpdateGame(game core.Game) error {
	record, err := newGameRecord(game)
	if err != nil {
		return fmt.Errorf("unable to create squad record: %v", err)
	}
	return dp.db.Omit("Edition", "Coach1", "Coach2").Save(&record).Error
}

// DeleteGame delete a game identified by id
func (dp DataProvider) DeleteGame(id uint) error {
	result := dp.db.Delete(gameRecord{}, id)
	if result.Error != nil {
		return result.Error
	}
	if result.RowsAffected != 1 {
		return fmt.Errorf("unusual number of game deleted: %v", result.RowsAffected)
	}
	return nil
}

// GetGamesByEdition returns all games in an edition
func (dp DataProvider) GetGamesByEdition(idEdition uint) ([]core.Game, error) {
	var games []core.Game
	var records []gameRecord
	result := dp.db.Preload("Edition").Where("edition_id = ?", idEdition).Preload("Coach1").Preload("Coach2").Order("round, table_number").Find(&records)
	if result.Error != nil {
		return games, result.Error
	}
	for _, record := range records {
		game, err := record.toCore()
		if err != nil {
			return games, err
		}
		games = append(games, game)
	}
	return games, nil
}

// GetGamesByCoach returns all games played by coach
func (dp DataProvider) GetGamesByCoach(idCoach uint) ([]core.Game, error) {
	var games []core.Game
	var records []gameRecord
	result := dp.db.Preload("Edition").Preload("Coach1").Preload("Coach2").Where("coach1_id = ?", idCoach).Or("coach2_id = ?", idCoach).Order("round desc, table_number").Find(&records)
	if result.Error != nil {
		return games, result.Error
	}
	for _, record := range records {
		game, err := record.toCore()
		if err != nil {
			return games, err
		}
		games = append(games, game)
	}
	return games, nil
}

// GetGamesBySquad returns all games played by squad members of a given squad
func (dp DataProvider) GetGamesBySquad(idSquad uint) ([]core.Game, error) {
	var games []core.Game
	var records []gameRecord
	var tempRecords []coachRecord
	result := dp.db.Where("squad_id = ?", idSquad).Find(&tempRecords)
	if result.Error != nil {
		return games, result.Error
	}
	idCoachs := []uint{}
	for _, record := range tempRecords {
		idCoachs = append(idCoachs, record.ID)
	}
	result = dp.db.Preload("Edition").Preload("Coach1").Preload("Coach2").Where("coach1_id IN(?)", idCoachs).Or("coach2_id IN(?)", idCoachs).Order("round desc, table_number").Find(&records)
	if result.Error != nil {
		return games, result.Error
	}
	for _, record := range records {
		game, err := record.toCore()
		if err != nil {
			return games, err
		}
		games = append(games, game)
	}
	return games, nil
}

// GetGamesByEditionAndRound returns all games in an edition, a round
func (dp DataProvider) GetGamesByEditionAndRound(idEdition uint, round uint) ([]core.Game, error) {
	var squads []core.Game
	var records []gameRecord
	result := dp.db.Preload("Edition").Where("edition_id = ?", idEdition).Preload("Coach1").Preload("Coach2").Where("round = ?", round).Order("table_number").Find(&records)
	if result.Error != nil {
		return squads, result.Error
	}
	for _, record := range records {
		game, err := record.toCore()
		if err != nil {
			return squads, err
		}
		squads = append(squads, game)
	}
	return squads, nil
}

// GetGamesByEditionAndPlayed returns all games in an edition with played value
func (dp DataProvider) GetGamesByEditionAndPlayed(idEdition uint, played bool) ([]core.Game, error) {
	var squads []core.Game
	var records []gameRecord
	result := dp.db.Preload("Edition").Where("edition_id = ?", idEdition).Preload("Coach1").Preload("Coach2").Where("played = ?", played).Order("round, table_number").Find(&records)
	if result.Error != nil {
		return squads, result.Error
	}
	for _, record := range records {
		game, err := record.toCore()
		if err != nil {
			return squads, err
		}
		squads = append(squads, game)
	}
	return squads, nil
}

// GetGamesByEditionRoundAndPlayed returns all games in an edition, a round with played value
func (dp DataProvider) GetGamesByEditionRoundAndPlayed(idEdition uint, round uint, played bool) ([]core.Game, error) {
	var squads []core.Game
	var records []gameRecord
	result := dp.db.Preload("Edition").Where("edition_id = ?", idEdition).Preload("Coach1").Preload("Coach2").Where("round = ?", round).Where("played = ?", played).Order("table_number").Find(&records)
	if result.Error != nil {
		return squads, result.Error
	}
	for _, record := range records {
		game, err := record.toCore()
		if err != nil {
			return squads, err
		}
		squads = append(squads, game)
	}
	return squads, nil
}
