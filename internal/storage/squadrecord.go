// Package storage FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

type squadRecord struct {
	ID        uint
	Name      string
	Edition   editionRecord
	EditionID uint
	Members   []coachRecord `gorm:"foreignKey:SquadID"`
}

func (squadRecord) TableName() string {
	return "squads"
}

func (record squadRecord) toCore() (core.Squad, error) {
	edition, err := record.Edition.toCore()
	if err != nil {
		return core.Squad{}, fmt.Errorf("error while edition.ToCore in squadRecord.ToCore: \n%s", err.Error())
	}
	members := []core.Coach{}
	for index, coachRecord := range record.Members {
		coachRecord.Edition = record.Edition
		coachRecord.EditionID = record.EditionID
		coach, err := coachRecord.toCore()
		if err != nil {
			return core.Squad{}, fmt.Errorf("error while coach.ToCore on coach index: %d in squadRecord.ToCore: \n%s", index, err.Error())
		}
		members = append(members, coach)
	}
	squad, err := core.NewSquad(record.Name, edition, members)
	if err != nil {
		return core.Squad{}, fmt.Errorf("error while NewSquad in squadRecord.ToCore: \n%s", err.Error())
	}
	squad.ID = record.ID
	return squad, nil
}

func newSquadRecord(squad core.Squad) (squadRecord, error) {
	record := squadRecord{}
	edition, err := newEditionRecord(squad.Edition)
	if err != nil {
		return record, fmt.Errorf("unable to create edition record for a squad: %v", err)
	}

	record.ID = squad.ID
	record.Name = squad.Name
	record.Edition = edition
	record.EditionID = squad.Edition.ID
	record.Members = []coachRecord{}
	for index, member := range squad.Members {
		memberRecord, err := newCoachRecord(member)
		if err != nil {
			return record, fmt.Errorf("unable to create coach %d record for a squad: %v", index, err)
		}
		record.Members = append(record.Members, memberRecord)
	}
	return record, nil
}
