// Package storage defines storage structures for FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
	"gorm.io/gorm"
)

// GetEdition returns the edition identified by id
func (dp DataProvider) GetEdition(id uint) (core.Edition, error) {

	var record editionRecord
	tx := dp.db.Preload("CoachCriterias").Preload("SquadCriterias").First(&record, id)
	if tx.Error != nil {
		return core.Edition{}, fmt.Errorf("error in GetEdition while querying database:\n%s", tx.Error.Error())
	}
	return record.toCore()
}

// CreateEdition stores an edition into storage
func (dp DataProvider) CreateEdition(edition core.Edition) (uint, error) {
	record, err := newEditionRecord(edition)
	if err != nil {
		return 0, fmt.Errorf("unable to create edition record: %v", err)
	}
	tx := dp.db.Create(&record)
	return record.ID, tx.Error
}

// UpdateEdition updates an edition
func (dp DataProvider) UpdateEdition(edition core.Edition) error {
	record, err := newEditionRecord(edition)
	if err != nil {
		return fmt.Errorf("unable to create edition record: %v", err)
	}
	return dp.db.Transaction(func(tx *gorm.DB) error {
		// drop old criterias
		err := tx.Delete(&coachRankingCriteriaRecord{}, "edition_id = ?", edition.ID).Error
		if err != nil {
			return err
		}
		err = tx.Delete(&squadRankingCriteriaRecord{}, "edition_id = ?", edition.ID).Error
		if err != nil {
			return err
		}
		if err = tx.Save(&record).Error; err != nil {
			return err
		}
		return nil
	})
}

// DeleteEdition delete an edition identified by id
func (dp DataProvider) DeleteEdition(id uint) error {
	result := dp.db.Delete(&editionRecord{}, id)
	if result.Error != nil {
		return result.Error
	}
	if result.RowsAffected != 1 {
		return fmt.Errorf("unusual number of edition deleted: %v", result.RowsAffected)
	}
	return nil
}

// GetEditions returns all editions
func (dp DataProvider) GetEditions() ([]core.Edition, error) {
	var editions []core.Edition
	var records []editionRecord
	result := dp.db.Preload("CoachCriterias").Preload("SquadCriterias").Find(&records)
	if result.Error != nil {
		return editions, result.Error
	}
	for _, record := range records {
		edition, err := record.toCore()
		if err != nil {
			return []core.Edition{}, err
		}
		editions = append(editions, edition)
	}
	return editions, nil
}
