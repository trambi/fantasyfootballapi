// Package storage FantasyFootballApi
// Copyright (C) 2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func TestCreateGetDeleteGame(t *testing.T) {
	// Given
	// When create game
	dataProvider, game := prepareGame(t)
	result, err := dataProvider.GetGame(game.ID)
	if err != nil {
		t.Fatal("Unexpected error while getting game: ", err.Error())
	}
	if cmp.Equal(game, result) != true {
		t.Fatal("- removed from game ,+ added to result: ", cmp.Diff(game, result))
	}
	err = dataProvider.DeleteGame(game.ID)
	if err != nil {
		t.Fatal("Unexpected error while deleting game: ", err.Error())
	}
	result, err = dataProvider.GetGame(game.ID)
	if err == nil {
		t.Fatal("Unexpected return while getting game after delete: ", result)
	}
}

func TestUpdateGame(t *testing.T) {
	// Given
	dataProvider, game := prepareGame(t)
	// When
	game.Played = true
	game.Td1 = 1
	game.Td2 = 2
	game.Casualties1 = 3
	game.Casualties2 = 4
	game.Completions1 = 5
	game.Completions2 = 6
	game.Fouls1 = 7
	game.Fouls2 = 8
	game.Special1 = "foo"
	game.Special2 = "bar"
	//
	err := dataProvider.UpdateGame(game)
	if err != nil {
		t.Fatal("Unexpected error while updating game: ", err.Error())
	}
	result, err := dataProvider.GetGame(game.ID)
	if err != nil {
		t.Fatal("Unexpected error while getting game: ", err.Error())
	}
	if cmp.Equal(game, result) != true {
		t.Fatal("- removed from game ,+ added to result: ", cmp.Diff(game, result))
	}
}

func TestGetGamesByEdition(t *testing.T) {
	// Given
	dataProvider, game1 := prepareGame(t)
	game2, err := core.NewGame(1, 2, *game1.Coach1, *game1.Coach2)
	if err != nil {
		t.Fatal("Unexpected error while constructing game: ", err.Error())
	}
	id, err := dataProvider.CreateGame(game2)
	if err != nil {
		t.Fatal("Unexpected error while creating game: ", err.Error())
	}
	game2.ID = id
	expected := []core.Game{game1, game2}
	result, err := dataProvider.GetGamesByEdition(game1.Edition.ID)
	if err != nil {
		t.Fatal("Unexpected error while getting games: ", err.Error())
	}
	if cmp.Equal(expected, result) != true {
		t.Fatal("- removed from expected ,+ added to result: ", cmp.Diff(expected, result))
	}
}

func TestGetGamesByEditionAndRound(t *testing.T) {
	// Given
	dataProvider, game1 := prepareGame(t)
	game2, err := core.NewGame(3, 21, *game1.Coach1, *game1.Coach2)
	if err != nil {
		t.Fatal("Unexpected error while constructing game2: ", err.Error())
	}
	id, err := dataProvider.CreateGame(game2)
	if err != nil {
		t.Fatal("Unexpected error while creating game2: ", err.Error())
	}
	game2.ID = id
	game3, err := core.NewGame(3, 10, *game1.Coach1, *game1.Coach2)
	if err != nil {
		t.Fatal("Unexpected error while constructing game3: ", err.Error())
	}
	id, err = dataProvider.CreateGame(game3)
	if err != nil {
		t.Fatal("Unexpected error while creating game3: ", err.Error())
	}
	game3.ID = id
	expected := []core.Game{game3, game2}
	result, err := dataProvider.GetGamesByEditionAndRound(game2.Edition.ID, game2.Round)
	if err != nil {
		t.Fatal("Unexpected error while getting games: ", err.Error())
	}
	if cmp.Equal(expected, result) != true {
		t.Fatal("- removed from expected ,+ added to result: ", cmp.Diff(expected, result))
	}
}

func TestGetGamesByEditionAndPlayedOrPlayable(t *testing.T) {
	// Given
	dataProvider, game1 := prepareGame(t)
	game2, err := core.NewGame(1, 2, *game1.Coach1, *game1.Coach2)
	game2.Played = true
	if err != nil {
		t.Fatal("Unexpected error while constructing game2: ", err.Error())
	}
	id, err := dataProvider.CreateGame(game2)
	if err != nil {
		t.Fatal("Unexpected error while creating game2: ", err.Error())
	}
	game2.ID = id
	expectedPlayableGames := []core.Game{game1}
	expectedPlayedGames := []core.Game{game2}
	result, err := dataProvider.GetGamesByEditionAndPlayed(game1.Edition.ID, true)
	if err != nil {
		t.Fatal("Unexpected error while getting played games: ", err.Error())
	}
	if cmp.Equal(expectedPlayedGames, result) != true {
		t.Fatal("- removed from expected ,+ added to getting played games: ", cmp.Diff(expectedPlayedGames, result))
	}
	result, err = dataProvider.GetGamesByEditionAndPlayed(game1.Edition.ID, false)
	if err != nil {
		t.Fatal("Unexpected error while getting playable games: ", err.Error())
	}
	if cmp.Equal(expectedPlayableGames, result) != true {
		t.Fatal("- removed from expected ,+ added to getting playable games: ", cmp.Diff(expectedPlayableGames, result))
	}
}

func TestGetGamesByEditionRoundAndPlayedOrPlayable(t *testing.T) {
	// Given
	dataProvider, game1 := prepareGame(t)
	game2, err := core.NewGame(2, 1, *game1.Coach1, *game1.Coach2)
	game2.Played = true
	if err != nil {
		t.Fatal("Unexpected error while constructing game2: ", err.Error())
	}
	id, err := dataProvider.CreateGame(game2)
	if err != nil {
		t.Fatal("Unexpected error while creating game2: ", err.Error())
	}
	game2.ID = id
	game3, err := core.NewGame(2, 2, *game1.Coach1, *game1.Coach2)
	if err != nil {
		t.Fatal("Unexpected error while constructing game3: ", err.Error())
	}
	id, err = dataProvider.CreateGame(game3)
	if err != nil {
		t.Fatal("Unexpected error while creating game3: ", err.Error())
	}
	game3.ID = id
	expectedPlayableGames := []core.Game{game3}
	expectedPlayedGames := []core.Game{game2}
	result, err := dataProvider.GetGamesByEditionRoundAndPlayed(game1.Edition.ID, 2, true)
	if err != nil {
		t.Fatal("Unexpected error while getting played games: ", err.Error())
	}
	if cmp.Equal(expectedPlayedGames, result) != true {
		t.Fatal("- removed from expected ,+ added to getting played games: ", cmp.Diff(expectedPlayedGames, result))
	}
	result, err = dataProvider.GetGamesByEditionRoundAndPlayed(game1.Edition.ID, 2, false)
	if err != nil {
		t.Fatal("Unexpected error while getting playable games: ", err.Error())
	}
	if cmp.Equal(expectedPlayableGames, result) != true {
		t.Fatal("- removed from expected ,+ added to getting playable games: ", cmp.Diff(expectedPlayableGames, result))
	}
}

func prepareGame(t *testing.T) (*DataProvider, core.Game) {
	t.Helper()
	dataProvider := InitDataProvider("sqlite", ":memory:")
	t.Cleanup(func() {
		dataProvider.close()
	})
	faction1 := core.Faction("faction1")
	edition, err := core.NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3, true, false, 2, []core.Faction{faction1}, "Something", "Founder")
	if err != nil {
		t.Fatal("Unexpected error while constructing edition: ", err.Error())
	}
	id, err := dataProvider.CreateEdition(edition)
	if err != nil {
		t.Fatal("Unexpected error while creating edition: ", err.Error())
	}
	edition.ID = id
	coach1, err := core.NewCoach("Coach 1", faction1, edition)
	if err != nil {
		t.Fatal("Unexpected error while constructing coach1: ", err.Error())
	}
	coach2, err := core.NewCoach("Coach 2", faction1, edition)
	if err != nil {
		t.Fatal("Unexpected error while constructing coach2: ", err.Error())
	}
	id, err = dataProvider.CreateCoach(coach1)
	if err != nil {
		t.Fatal("Unexpected error while creating coach1: ", err.Error())
	}
	coach1.ID = id
	id, err = dataProvider.CreateCoach(coach2)
	if err != nil {
		t.Fatal("Unexpected error while creating coach2: ", err.Error())
	}
	coach2.ID = id
	game, err := core.NewGame(1, 2, coach1, coach2)
	if err != nil {
		t.Fatal("Unexpected error while constructing game: ", err.Error())
	}

	id, err = dataProvider.CreateGame(game)
	if err != nil {
		t.Fatal("Unexpected error while creating game: ", err.Error())
	}
	game.ID = id
	return dataProvider, game
}

func TestGetGamesByCoach(t *testing.T) {
	// Given
	dataProvider, game1 := prepareGame(t)
	game2, err := core.NewGame(1, 2, *game1.Coach1, *game1.Coach2)
	if err != nil {
		t.Fatal("Unexpected error while constructing game: ", err.Error())
	}
	id, err := dataProvider.CreateGame(game2)
	if err != nil {
		t.Fatal("Unexpected error while creating game: ", err.Error())
	}
	game2.ID = id
	expected := []core.Game{game1, game2}
	result, err := dataProvider.GetGamesByCoach(game1.Coach1.ID)
	if err != nil {
		t.Fatal("Unexpected error while getting games: ", err.Error())
	}
	if cmp.Equal(expected, result) != true {
		t.Fatal("- removed from expected ,+ added to result: ", cmp.Diff(expected, result))
	}
	result, err = dataProvider.GetGamesByCoach(game1.Coach2.ID)
	if err != nil {
		t.Fatal("Unexpected error while getting games: ", err.Error())
	}
	if cmp.Equal(expected, result) != true {
		t.Fatal("- removed from expected ,+ added to result: ", cmp.Diff(expected, result))
	}
}

func TestGetGamesBySquad(t *testing.T) {
	// Given
	dataProvider, game1 := prepareGame(t)
	game2, err := core.NewGame(1, 2, *game1.Coach1, *game1.Coach2)
	if err != nil {
		t.Fatal("Unexpected error while constructing game: ", err.Error())
	}
	id, err := dataProvider.CreateGame(game2)
	if err != nil {
		t.Fatal("Unexpected error while creating game: ", err.Error())
	}
	game2.ID = id
	squad1, err := core.NewSquad("squad1", game1.Coach1.Edition, []core.Coach{*game1.Coach1})
	if err != nil {
		t.Fatal("Unexpected error while constructing squad: ", err.Error())
	}
	id, err = dataProvider.CreateSquad(squad1)
	if err != nil {
		t.Fatal("Unexpected error while creating squad: ", err.Error())
	}
	squad1.ID = id
	game1.Coach1.SquadID = id
	game2.Coach1.SquadID = id
	squad2, err := core.NewSquad("squad2", game1.Coach2.Edition, []core.Coach{*game1.Coach2})
	if err != nil {
		t.Fatal("Unexpected error while constructing squad: ", err.Error())
	}
	id, err = dataProvider.CreateSquad(squad2)
	if err != nil {
		t.Fatal("Unexpected error while creating squad: ", err.Error())
	}
	squad2.ID = id
	game1.Coach2.SquadID = id
	game2.Coach2.SquadID = id
	expected := []core.Game{game1, game2}
	result, err := dataProvider.GetGamesBySquad(squad1.ID)
	if err != nil {
		t.Fatal("Unexpected error while getting games: ", err.Error())
	}
	if cmp.Equal(expected, result) != true {
		t.Fatal("- removed from expected ,+ added to result: ", cmp.Diff(expected, result))
	}
	result, err = dataProvider.GetGamesBySquad(squad2.ID)
	if err != nil {
		t.Fatal("Unexpected error while getting games: ", err.Error())
	}
	if cmp.Equal(expected, result) != true {
		t.Fatal("- removed from expected ,+ added to result: ", cmp.Diff(expected, result))
	}
}
