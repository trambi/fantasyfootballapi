// Package storage defines storage structures for FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
	"gorm.io/gorm"
)

// GetSquad returns the squad identified by id
func (dp DataProvider) GetSquad(id uint) (core.Squad, error) {
	var record squadRecord
	tx := dp.db.Preload("Edition").Preload("Members").First(&record, id)
	if tx.Error != nil {
		return core.Squad{}, fmt.Errorf("error in GetSquad while querying database:\n%s", tx.Error.Error())
	}
	return record.toCore()
}

// CreateSquad stores a squad into storage
func (dp DataProvider) CreateSquad(squad core.Squad) (uint, error) {
	record, err := newSquadRecord(squad)
	if err != nil {
		return 0, fmt.Errorf("unable to create squad record: %v", err)
	}
	coachIDToUpdate := []uint{}
	for _, member := range squad.Members {
		coachIDToUpdate = append(coachIDToUpdate, member.ID)
	}
	addMembersQuery := fmt.Sprintf("UPDATE %s SET squad_id = ? WHERE id IN ?", coachRecord{}.TableName())

	err = dp.db.Transaction(func(tx *gorm.DB) error {
		var transactionErr error
		if transactionErr = tx.Omit("Edition", "Members").Create(&record).Error; transactionErr != nil {
			return transactionErr
		}
		if transactionErr = tx.Exec(addMembersQuery, record.ID, coachIDToUpdate).Error; transactionErr != nil {
			return transactionErr
		}
		return nil
	})
	return record.ID, err
}

// UpdateSquad updates a squad and only a squad
func (dp DataProvider) UpdateSquad(squad core.Squad) error {
	record, err := newSquadRecord(squad)
	if err != nil {
		return fmt.Errorf("unable to create squad record: %v", err)
	}
	coachIDToUpdate := []uint{}
	for _, member := range squad.Members {
		coachIDToUpdate = append(coachIDToUpdate, member.ID)
	}
	removeMembersQuery := fmt.Sprintf("UPDATE %s SET squad_id = NULL WHERE squad_id = ?", coachRecord{}.TableName())
	addMembersQuery := fmt.Sprintf("UPDATE %s SET squad_id = ? WHERE id IN ?", coachRecord{}.TableName())

	return dp.db.Transaction(func(tx *gorm.DB) error {
		var err error
		if err = tx.Omit("Edition", "Members").Save(&record).Error; err != nil {
			return err
		}
		if err = tx.Exec(removeMembersQuery, record.ID).Error; err != nil {
			return err
		}
		if err = tx.Exec(addMembersQuery, record.ID, coachIDToUpdate).Error; err != nil {
			return err
		}
		return nil
	})
}

func (dp DataProvider) UpdateSquadReadiness(id uint, readiness bool) error {
	return dp.db.Model(&coachRecord{}).Where("squad_id = ?", id).Update("Ready", readiness).Error
}

// DeleteSquad delete a squad identified by id
func (dp DataProvider) DeleteSquad(id uint) error {
	result := dp.db.Delete(squadRecord{}, id)
	if result.Error != nil {
		return result.Error
	}
	if result.RowsAffected != 1 {
		return fmt.Errorf("unusual number of squad deleted: %v", result.RowsAffected)
	}
	return nil
}

// GetSquadsByEdition returns all squads in an edition
func (dp DataProvider) GetSquadsByEdition(idEdition uint) ([]core.Squad, error) {
	var squads []core.Squad
	var records []squadRecord
	result := dp.db.Preload("Edition").Where("edition_id = ?", idEdition).Preload("Members").Find(&records)
	if result.Error != nil {
		return squads, result.Error
	}
	for _, record := range records {
		squad, err := record.toCore()
		if err != nil {
			return squads, err
		}
		squads = append(squads, squad)
	}
	return squads, nil
}
