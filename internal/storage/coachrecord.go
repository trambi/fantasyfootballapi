// Package storage FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"fmt"

	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

// coachRecord is an adapter to store core.Coach
type coachRecord struct {
	ID        uint
	Name      string `gorm:"not null"`
	TeamName  string `gorm:"not null, default ''"`
	NafNumber uint
	Ready     bool `gorm:"not null, default false"`
	EditionID uint
	Edition   editionRecord
	Faction   string `gorm:"not null"`
	SquadID   uint
}

func (coachRecord) TableName() string {
	return "coach"
}

func (record coachRecord) toCore() (core.Coach, error) {
	edition, err := record.Edition.toCore()
	if err != nil {
		return core.Coach{}, fmt.Errorf("error while edition.ToCore in coachRecord.ToCore: %s", err.Error())
	}
	coach, err := core.NewCoach(record.Name, core.Faction(record.Faction), edition)
	if err != nil {
		return coach, fmt.Errorf("error while coachRecord.ToCore: %s", err.Error())
	}
	coach.ID = record.ID
	coach.TeamName = record.TeamName
	coach.NafNumber = record.NafNumber
	coach.Ready = record.Ready
	coach.SquadID = record.SquadID
	return coach, nil
}

func newCoachRecord(coach core.Coach) (coachRecord, error) {
	record := coachRecord{}
	edition, err := newEditionRecord(coach.Edition)
	if err != nil {
		return record, fmt.Errorf("unable to create edition record for a coach: %v", err)
	}
	record.ID = coach.ID
	record.Name = coach.Name
	record.TeamName = coach.TeamName
	record.NafNumber = coach.NafNumber
	record.Faction = string(coach.Faction)
	record.Ready = coach.Ready
	record.Edition = edition
	record.EditionID = coach.Edition.ID
	record.SquadID = coach.SquadID
	return record, nil
}
