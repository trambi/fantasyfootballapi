// Package storage defines storage structures for FantasyFootballApi
// Copyright (C) 2019-2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"log"

	"github.com/glebarez/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// DataProvider purpose is to bring data
type DataProvider struct {
	db *gorm.DB
}

// InitDataProvider initialize the DataProvider with
// storage type string and the path of the storage
func InitDataProvider(storage string, path string) *DataProvider {
	var (
		dp  DataProvider
		err error
	)
	if len(path) == 0 {
		path = "api.db"
	}
	log.Printf("Use sqlite in %s as database\n", path)

	dp.db, err = gorm.Open(sqlite.Open(path), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("sqlite file in %s opened\n", path)
	autoMigrate(dp)
	return &dp
}

func autoMigrate(dp DataProvider) {
	err := dp.db.AutoMigrate(&editionRecord{})
	if err != nil {
		log.Fatal("error during AutoMigrate editionRecord: ", err)
	}
	err = dp.db.AutoMigrate(&coachRecord{})
	if err != nil {
		log.Fatal("error during AutoMigrate coachRecord: ", err)
	}
	err = dp.db.AutoMigrate(&gameRecord{})
	if err != nil {
		log.Fatal("error during AutoMigrate gameRecord: ", err)
	}
	err = dp.db.AutoMigrate(&squadRecord{})
	if err != nil {
		log.Fatal("error during AutoMigrate squadRecord: ", err)
	}
	err = dp.db.AutoMigrate(&coachRankingCriteriaRecord{})
	if err != nil {
		log.Fatal("error during AutoMigrate coachRankingCriteriaRecord: ", err)
	}
	err = dp.db.AutoMigrate(&squadRankingCriteriaRecord{})
	if err != nil {
		log.Fatal("error during AutoMigrate coachRankingCriteriaRecord: ", err)
	}
}

func (dp DataProvider) close() {
	sqlDB, err := dp.db.DB()
	if err != nil {
		log.Fatal(err)
	}
	err = sqlDB.Close()
	if err != nil {
		log.Fatal(err)
	}
}
