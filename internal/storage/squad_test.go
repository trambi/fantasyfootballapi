// Package storage FantasyFootballApi
// Copyright (C) 2023  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package storage

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/fantasyfootballapi/internal/core"
)

func TestCreateGetDeleteSquad(t *testing.T) {
	// Given
	// When create squad
	dataProvider, squad := prepareSquad(t)
	result, err := dataProvider.GetSquad(squad.ID)
	if err != nil {
		t.Fatal("Unexpected error while getting squad: ", err.Error())
	}
	if cmp.Equal(squad, result) != true {
		t.Fatal("- removed from squad ,+ added to result: ", cmp.Diff(squad, result))
	}
	err = dataProvider.DeleteSquad(squad.ID)
	if err != nil {
		t.Fatal("Unexpected error while deleting squad: ", err.Error())
	}
	result, err = dataProvider.GetSquad(squad.ID)
	if err == nil {
		t.Fatal("Unexpected return while getting squad after delete: ", result)
	}
}

func TestUpdateSquad(t *testing.T) {
	// Given
	dataProvider, squad := prepareSquad(t)
	squad.Name = "Another squadname"
	// When
	err := dataProvider.UpdateSquad(squad)
	// Then
	if err != nil {
		t.Fatal("Unexpected error while updating squad: ", err.Error())
	}
	result, err := dataProvider.GetSquad(squad.ID)
	if err != nil {
		t.Fatal("Unexpected error while getting squad: ", err.Error())
	}
	if cmp.Equal(squad, result) != true {
		t.Fatal("- removed from squad ,+ added to result: ", cmp.Diff(squad, result))
	}
}

func TestGetSquadsByEdition(t *testing.T) {
	// Given
	dataProvider, squad1 := prepareSquad(t)
	coach3, err := core.NewCoach("Coach 3", squad1.Edition.AllowedFactions[0], squad1.Edition)
	if err != nil {
		t.Fatal("Unexpected error while constructing third coach: ", err.Error())
	}
	id, err := dataProvider.CreateCoach(coach3)
	if err != nil {
		t.Fatal("Unexpected error while creating third coach: ", err.Error())
	}
	coach3.ID = id
	squad2, err := core.NewSquad("Another squad", squad1.Edition, []core.Coach{coach3})
	if err != nil {
		t.Fatal("Unexpected error while constructing second squad: ", err.Error())
	}
	id, err = dataProvider.CreateSquad(squad2)
	if err != nil {
		t.Fatal("Unexpected error while creating second squad: ", err.Error())
	}
	squad2.ID = id
	squad2.Members[0].SquadID = id
	expected := []core.Squad{squad1, squad2}
	result, err := dataProvider.GetSquadsByEdition(squad1.Edition.ID)
	if err != nil {
		t.Fatal("Unexpected error while getting squads: ", err.Error())
	}
	if cmp.Equal(expected, result) != true {
		t.Fatal("- removed from expected ,+ added to result: ", cmp.Diff(expected, result))
	}
}

func TestUpdateSquadReadiness(t *testing.T) {
	// Given
	dataProvider, squad := prepareSquad(t)
	// When
	err := dataProvider.UpdateSquadReadiness(squad.ID, true)
	// Then
	if err != nil {
		t.Fatal("Unexpected error while updating squad readiness: ", err.Error())
	}
	result, err := dataProvider.GetSquad(squad.ID)
	if err != nil {
		t.Fatal("Unexpected error while getting squad: ", err.Error())
	}
	if len(result.Members) != len(squad.Members) {
		t.Fatal("Squad members length not keeped")
	}
	for index, member := range result.Members {
		if member.Ready != true {
			t.Errorf("Squad member #%d not ready", index)
		}
	}

}

func prepareSquad(t *testing.T) (*DataProvider, core.Squad) {
	t.Helper()
	dataProvider := InitDataProvider("sqlite", ":memory:")
	t.Cleanup(func() {
		dataProvider.close()
	})
	faction1 := core.Faction("faction1")
	edition, err := core.NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3, true, false, 2, []core.Faction{faction1}, "Something", "Founder")
	if err != nil {
		t.Fatal("Unexpected error while constructing edition: ", err.Error())
	}
	id, err := dataProvider.CreateEdition(edition)
	if err != nil {
		t.Fatal("Unexpected error while creating edition: ", err.Error())
	}
	edition.ID = id
	coach1, err := core.NewCoach("Coach 1", faction1, edition)
	if err != nil {
		t.Fatal("Unexpected error while constructing coach1: ", err.Error())
	}
	coach2, err := core.NewCoach("Coach 2", faction1, edition)
	if err != nil {
		t.Fatal("Unexpected error while constructing coach2: ", err.Error())
	}
	id, err = dataProvider.CreateCoach(coach1)
	if err != nil {
		t.Fatal("Unexpected error while creating coach1: ", err.Error())
	}
	coach1.ID = id
	id, err = dataProvider.CreateCoach(coach2)
	if err != nil {
		t.Fatal("Unexpected error while creating coach2: ", err.Error())
	}
	coach2.ID = id
	squad, err := core.NewSquad("squad1", edition, []core.Coach{coach1, coach2})
	if err != nil {
		t.Fatal("Unexpected error while constructing squad: ", err.Error())
	}

	id, err = dataProvider.CreateSquad(squad)
	if err != nil {
		t.Fatal("Unexpected error while creating squad: ", err.Error())
	}
	squad.ID = id
	squad.Members[0].SquadID = id
	squad.Members[1].SquadID = id
	return dataProvider, squad
}
