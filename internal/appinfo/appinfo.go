// FantasyFootballApi
// Copyright (C) 2019-2020  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package appinfo

// AppInfo contains all needed informations about the app
type AppInfo struct {
	Name    string   `json:"name"`
	Version string   `json:"version"`
	Authors string   `json:"authors"`
	License string   `json:"license"`
	Notices []string `json:"notices"`
}

// GetAppInfo Return AppInfo
func GetAppInfo() AppInfo {
	return AppInfo{
		"FantasyFootballApi",
		"0.99.4",
		"trambi",
		"Apache v2",
		[]string{"crypto/sha1, encoding/hex ,encoding/json, errors, fmt, io, io/ioutil, log, math/rand," +
			"net/http, os, path/filepath, regexp, sort, strings, time Copyright (c) 2009 The Go Authors.",
			"github.com/dgrijalva/jwt-go Copyright (c) 2012 Dave Grijalva.",
			"github.com/gorilla/mux Copyright (c) 2012-2018 The Gorilla Authors."}}
}

func (app AppInfo) String() string {
	returnValue := app.Name + " v" + app.Version + "\n"
	returnValue += "by " + app.Authors + " under " + app.License + "\n"
	returnValue += "This software use:" + "\n"
	for _, notice := range app.Notices {
		returnValue += "- " + notice + "\n"
	}
	return returnValue
}
