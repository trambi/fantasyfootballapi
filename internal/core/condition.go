// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"fmt"
	"strings"
)

// ICondition represent an operator to be tested like equality or greater
type ICondition interface {
	// Value return true if the condition is true for a given Game
	Value(provider IValueProvider) (bool, error)
	// String convert to a string
	String() string
}

// EqualCondition describe a clause true when Left and Right are equals
type EqualCondition struct {
	Left  IValuable
	Right IValuable
}

// Value return true if Left and Right values for the Game game are equal
func (ec EqualCondition) Value(provider IValueProvider) (bool, error) {
	left, leftErr := ec.Left.Evaluate(provider)
	right, rightErr := ec.Right.Evaluate(provider)
	if leftErr != nil || rightErr != nil {
		return false, fmt.Errorf("left error: %v right error:%v", leftErr, rightErr)
	}
	return left == right, nil
}

// String convert to a string
func (ec EqualCondition) String() string {
	return fmt.Sprintf("%v = %v", ec.Left, ec.Right)
}

// GreaterThanCondition describe a clause true when ToBeGreater is greater than ToBeLesser
type GreaterThanCondition struct {
	ToBeGreater IValuable
	ToBeLesser  IValuable
}

// Value return true if value of toBeGreater is greater than value of toBeLesser of the Game game
func (gc GreaterThanCondition) Value(provider IValueProvider) (bool, error) {
	toBeGreater, toBeGreaterErr := gc.ToBeGreater.Evaluate(provider)
	toBeLesser, toBeLesserErr := gc.ToBeLesser.Evaluate(provider)
	if toBeGreaterErr != nil || toBeLesserErr != nil {
		return false, fmt.Errorf("toBeGreater error: %v ToBeLesser error:%v", toBeGreaterErr, toBeLesserErr)
	}
	return toBeGreater > toBeLesser, nil
}

// String convert to a string
func (gc GreaterThanCondition) String() string {
	return fmt.Sprintf("%v > %v", gc.ToBeGreater, gc.ToBeLesser)
}

// AndCondition describe a clause true when Left and Right are true
type AndCondition struct {
	Left  ICondition
	Right ICondition
}

// Value return true if value of left and value of Right are true
func (a AndCondition) Value(provider IValueProvider) (bool, error) {
	left, leftError := a.Left.Value(provider)
	right, rightError := a.Right.Value(provider)
	if leftError != nil || rightError != nil {
		return false, fmt.Errorf("left error: %v right error:%v", leftError, rightError)
	}
	return left && right, nil
}

// String convert to a string
func (a AndCondition) String() string {
	return fmt.Sprintf("%v and %v", a.Left, a.Right)
}

func NewCondition(input string) (ICondition, error) {
	splitByAnd := strings.Split(input, " and ")
	if len(splitByAnd) == 2 {
		left, leftError := NewCondition(splitByAnd[0])
		right, rightError := NewCondition(splitByAnd[1])
		if leftError != nil || rightError != nil {
			return nil, fmt.Errorf("unable to parse and condition: %s", input)
		}
		return AndCondition{
			Left:  left,
			Right: right,
		}, nil
	}
	splitByEqual := strings.Split(input, "=")
	if len(splitByEqual) == 2 {
		left, _ := NewValuable(splitByEqual[0])
		right, _ := NewValuable(splitByEqual[1])
		return EqualCondition{
			Left:  left,
			Right: right,
		}, nil
	}
	splitByGreater := strings.Split(input, ">")
	if len(splitByGreater) == 2 {
		greater, _ := NewValuable(splitByGreater[0])
		lesser, _ := NewValuable(splitByGreater[1])
		return GreaterThanCondition{
			ToBeGreater: greater,
			ToBeLesser:  lesser,
		}, nil
	}
	return nil, fmt.Errorf("unknown condition: %s", input)
}
