// Package core FantasyFootballApi
// Copyright (C) 2019-2024 Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
//
// Package based on the series of article of Ruslan Pivak Blog https://ruslangpivak.com

package core

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestConditionValueWithValidExpression(t *testing.T) {
	testCases := []struct {
		expression string
		expected   bool
	}{
		{"1=1", true},
		{"1+2=3", true},
		{"{TdFor}={TdAgainst}", false},
		{"{TdFor}+10={TdAgainst}", true},
		{"2>1", true},
		{"1=1 and 2=3", false},
		{"1=1 and 2=2", true},
		{"{TdFor}+10={TdAgainst} and {PointsFor}={PointsAgainst}", false},
		{"{TdFor}+10={TdAgainst} and {PointsFor}+100={PointsAgainst}", true},
	}
	for _, tc := range testCases {
		// Given
		game := Game{Td1: 10, Td2: 20, Points1: 100, Points2: 200}
		condition, err := NewCondition(tc.expression)
		if err != nil {
			t.Fatalf("Unexpected error %v while parsing condition: %v\n", err.Error(), tc.expression)
		}
		// When
		result, err := condition.Value(game)
		// Then
		if err != nil {
			t.Fatalf("Unexpected error when: %v\n", err.Error())
		}
		if !cmp.Equal(tc.expected, result) {
			t.Fatalf("result %v is different from expected %v: %v", result, tc.expected, cmp.Diff(tc.expected, result))
		}
	}
}

func TestNewConditionWithEmptyInput(t *testing.T) {
	// Given
	// When
	_, err := NewCondition("")
	// Then
	if err == nil {
		t.Fatalf("Error was expected\n")
	}
}
