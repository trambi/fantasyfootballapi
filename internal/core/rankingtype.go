// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import "fmt"

type RankingType int

const (
	UndefinedType RankingType = iota
	For
	Against
	Net //For - Against
	Opponent
	OpponentExceptOwnGame
	Day1          // Ranking of day1
	Day2          // Ranking of day2
	Day1MinusDay2 // Ranking of day1 - ranking of day2
)

func (s RankingType) String() string {
	switch s {
	case For:
		return "For"
	case Against:
		return "Against"
	case Net:
		return "Net"
	case Opponent:
		return "Opponent"
	case OpponentExceptOwnGame:
		return "OpponentExceptOwnGame"
	case Day1:
		return "Day1"
	case Day2:
		return "Day2"
	case Day1MinusDay2:
		return "Day1MinusDay2"
	}
	return "unknown ranking type"
}

func NewRankingType(value string) (RankingType, error) {
	switch value {
	case "For":
		return For, nil
	case "Against":
		return Against, nil
	case "Net":
		return Net, nil
	case "Opponent":
		return Opponent, nil
	case "OpponentExceptOwnGame":
		return OpponentExceptOwnGame, nil
	case "Day1":
		return Day1, nil
	case "Day2":
		return Day2, nil
	case "Day1MinusDay2":
		return Day1MinusDay2, nil
	}
	return UndefinedType, fmt.Errorf("unknown ranking type %s", value)
}
