// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"fmt"
	"strings"
)

// Game represents a game between two coachs
// It implement IValueProvider
type Game struct {
	ID           uint
	Edition      *Edition
	Round        uint
	Table        uint
	Played       bool
	Finale       bool
	Coach1       *Coach
	Td1          int
	Casualties1  int
	Completions1 int
	Fouls1       int
	Points1      float64
	Special1     string
	Coach2       *Coach
	Td2          int
	Casualties2  int
	Completions2 int
	Fouls2       int
	Points2      float64
	Special2     string
}

// ValueByField implements IValueProvider.
func (game Game) ValueByField(field string) (float64, error) {
	switch field {
	case "TdFor":
		return float64(game.Td1), nil
	case "TdAgainst":
		return float64(game.Td2), nil
	case "CasualtiesFor":
		return float64(game.Casualties1), nil
	case "CasualtiesAgainst":
		return float64(game.Casualties2), nil
	case "CompletionsFor":
		return float64(game.Completions1), nil
	case "CompletionsAgainst":
		return float64(game.Completions2), nil
	case "FoulsFor":
		return float64(game.Fouls1), nil
	case "FoulsAgainst":
		return float64(game.Fouls2), nil
	case "PointsFor":
		return game.Points1, nil
	case "PointsAgainst":
		return game.Points2, nil
	default:
		return 0.0, fmt.Errorf("unknown field %s for game", field)
	}
}

// NewGame is Game constructor
func NewGame(round uint, table uint, coach1 Coach, coach2 Coach) (Game, error) {
	issues := []string{}
	if round == 0 {
		issues = append(issues, "round is nul")
	}
	if table == 0 {
		issues = append(issues, "table is nul")
	}
	if coach1.Edition.Name != coach2.Edition.Name {
		issues = append(issues, "coach1 edition is different from coach2 edition")
	}
	if len(issues) != 0 {
		return Game{}, fmt.Errorf("error in game constructor: %v", strings.Join(issues, ", "))
	}
	return Game{Edition: &(coach1.Edition), Round: round, Table: table, Finale: false, Played: false, Coach1: &coach1, Coach2: &coach2}, nil
}

func (game Game) GetRankingGames() [2]RankingGame {
	gameOfCoach1 := NewRankingGame(game)
	gameOfCoach2 := gameOfCoach1.Reverse()
	return [2]RankingGame{gameOfCoach1, gameOfCoach2}
}

func (game *Game) SetTd(tdFor1 int, tdFor2 int) *Game {
	game.Td1 = tdFor1
	game.Td2 = tdFor2
	game.Played = true
	return game
}

func (game *Game) SetCasualties(casualtiesFor1 int, casualtiesFor2 int) *Game {
	game.Casualties1 = casualtiesFor1
	game.Casualties2 = casualtiesFor2
	game.Played = true
	return game
}

func (game *Game) SetCompletions(completionsFor1 int, completionsFor2 int) *Game {
	game.Completions1 = completionsFor1
	game.Completions2 = completionsFor2
	game.Played = true
	return game
}

func (game *Game) SetFouls(foulsFor1 int, foulsFor2 int) *Game {
	game.Fouls1 = foulsFor1
	game.Fouls2 = foulsFor2
	game.Played = true
	return game
}

func (game *Game) SetSpecial(specialFor1 string, specialFor2 string) *Game {
	game.Special1 = specialFor1
	game.Special2 = specialFor2
	game.Played = true
	return game
}

func (game Game) Reverse() Game {
	return Game{
		ID:           game.ID,
		Edition:      game.Edition,
		Round:        game.Round,
		Table:        game.Table,
		Played:       game.Played,
		Finale:       game.Finale,
		Coach1:       game.Coach2,
		Td1:          game.Td2,
		Casualties1:  game.Casualties2,
		Completions1: game.Completions2,
		Fouls1:       game.Fouls2,
		Points1:      game.Points2,
		Special1:     game.Special2,
		Coach2:       game.Coach1,
		Td2:          game.Td1,
		Casualties2:  game.Casualties1,
		Completions2: game.Completions1,
		Fouls2:       game.Fouls1,
		Points2:      game.Points1,
		Special2:     game.Special1,
	}
}

func (game *Game) ComputeGamePoints() error {
	var err error
	points1 := game.Edition.GamePoints.Default
	reversedGame := game.Reverse()
	points2 := game.Edition.GamePoints.Default

	for _, clause := range game.Edition.GamePoints.Clauses {
		conditionForGame, err := clause.Condition.Value(*game)
		if err != nil {
			return err
		}
		conditionForReversedGame, err := clause.Condition.Value(reversedGame)
		if err != nil {
			return err
		}
		if conditionForGame {
			points1 = clause.ValueIfTrue
		}
		if conditionForReversedGame {
			points2 = clause.ValueIfTrue
		}
	}
	game.Points1, err = points1.Evaluate(game)
	if err != nil {
		return fmt.Errorf("unable to evaluate points1: %s", err.Error())
	}
	game.Points2, err = points2.Evaluate(reversedGame)
	if err != nil {
		return fmt.Errorf("unable to evaluate points2: %s", err.Error())
	}
	fmt.Printf("round %d : %s vs %s: ", game.Round, game.Coach1.Name, game.Coach2.Name)
	fmt.Printf("%d - %d\n", game.Td1, game.Td2)
	fmt.Printf("%f points - %f points\n", game.Points1, game.Points2)
	return nil
}
