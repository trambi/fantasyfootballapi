// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import "fmt"

// CoachRank represents a rank for a coach
type CoachRank struct {
	Coach      *Coach
	ExtraInfos []float64
}

type CoachRanking struct {
	ExtraInfoNames             []string
	CoachRanks                 []CoachRank
	ExtraInfoOrderIsDescending []bool
}

func (c CoachRanking) Len() int { return len(c.CoachRanks) }
func (c CoachRanking) Swap(i, j int) {
	c.CoachRanks[i], c.CoachRanks[j] = c.CoachRanks[j], c.CoachRanks[i]
}
func (c CoachRanking) Less(i, j int) bool {
	for extraInfosIndex, extraInfoForI := range c.CoachRanks[i].ExtraInfos {
		extraInfoForJ := c.CoachRanks[j].ExtraInfos[extraInfosIndex]
		if extraInfoForJ != extraInfoForI {
			if c.ExtraInfoOrderIsDescending[extraInfosIndex] {
				return extraInfoForI > extraInfoForJ
			} else {
				return extraInfoForI < extraInfoForJ
			}

		}
	}
	return false
}

func NewCoachRanking(e Edition, name string, useFinale bool) (CoachRanking, error) {
	result := CoachRanking{}
	result.ExtraInfoNames = []string{}
	result.ExtraInfoOrderIsDescending = []bool{}
	_, exists := e.CoachRankings[name]
	if !exists {
		return result, fmt.Errorf("%s is not an existing coach ranking", name)
	}
	if useFinale {
		result.ExtraInfoNames = append(result.ExtraInfoNames, "finale")
		result.ExtraInfoOrderIsDescending = append(result.ExtraInfoOrderIsDescending, true)
	}
	for _, settings := range e.CoachRankings[name] {
		result.ExtraInfoNames = append(result.ExtraInfoNames, settings.Label)
		result.ExtraInfoOrderIsDescending = append(result.ExtraInfoOrderIsDescending, settings.Descending)
	}
	return result, nil
}
