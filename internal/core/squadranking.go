// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import "fmt"

// SquadRank represents a rank for a squad
type SquadRank struct {
	Squad      *Squad
	ExtraInfos []float64
}

type SquadRanking struct {
	ExtraInfoNames             []string
	SquadRanks                 []SquadRank
	ExtraInfoOrderIsDescending []bool
}

func (s SquadRanking) Len() int { return len(s.SquadRanks) }
func (s SquadRanking) Swap(i, j int) {
	s.SquadRanks[i], s.SquadRanks[j] = s.SquadRanks[j], s.SquadRanks[i]
}
func (s SquadRanking) Less(i, j int) bool {
	for extraInfosIndex, extraInfoForI := range s.SquadRanks[i].ExtraInfos {
		extraInfoForJ := s.SquadRanks[j].ExtraInfos[extraInfosIndex]
		if extraInfoForJ != extraInfoForI {
			if s.ExtraInfoOrderIsDescending[extraInfosIndex] {
				return extraInfoForI > extraInfoForJ
			} else {
				return extraInfoForI < extraInfoForJ
			}

		}
	}
	return false
}

func NewSquadRanking(e Edition, name string, finale bool) (SquadRanking, error) {
	result := SquadRanking{}
	result.ExtraInfoNames = []string{}
	result.ExtraInfoOrderIsDescending = []bool{}
	_, exists := e.SquadRankings[name]
	if !exists {
		return result, fmt.Errorf("%s is not an existing squad ranking", name)
	}
	if finale {
		result.ExtraInfoNames = append(result.ExtraInfoNames, "finale")
		result.ExtraInfoOrderIsDescending = append(result.ExtraInfoOrderIsDescending, true)
	}
	for _, settings := range e.SquadRankings[name] {
		result.ExtraInfoNames = append(result.ExtraInfoNames, settings.Label)
		result.ExtraInfoOrderIsDescending = append(result.ExtraInfoOrderIsDescending, settings.Descending)
	}
	return result, nil
}
