// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"testing"
)

func TestRankingTypeToString(t *testing.T) {
	testCases := []struct {
		rankingtype RankingType
		expected    string
	}{
		{For, "For"},
		{Against, "Against"},
		{Net, "Net"},
		{Opponent, "Opponent"},
		{OpponentExceptOwnGame, "OpponentExceptOwnGame"},
		{Day1, "Day1"},
		{Day2, "Day2"},
		{Day1MinusDay2, "Day1MinusDay2"},
		{Day1MinusDay2 + 1, "unknown ranking type"},
	}
	for _, tc := range testCases {
		result := tc.rankingtype.String()
		if result != tc.expected {
			t.Errorf("string(%d) = %s; want %s", tc.rankingtype, result, tc.expected)
		}
	}
}

func TestRankingTypeToStringThenConstructorIsIdentity(t *testing.T) {
	testCases := []struct {
		input RankingType
	}{
		{For},
		{Against},
		{Net},
		{Opponent},
		{OpponentExceptOwnGame},
		{Day1},
		{Day2},
		{Day1MinusDay2},
	}
	for _, tc := range testCases {
		result, err := NewRankingType(tc.input.String())
		if err != nil {
			t.Fatalf("Unexpected error with %s", tc.input)
		}
		if result != tc.input {
			t.Errorf("NewRankingType(%d.string()) return %s", tc.input, result)
		}
	}
}
