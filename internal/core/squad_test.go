// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNewSquad(t *testing.T) {
	// Given
	faction1 := Faction("Something")
	edition, err := NewEdition("testedition", "2023-01-01", 5, 3, true, true, 3, []Faction{faction1}, "something", "tester")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	coach1, err := NewCoach("Coach 1", faction1, edition)
	if err != nil {
		t.Fatal("unexpected error during NewCoach: ", err.Error())
	}
	coach2, err := NewCoach("Coach2", faction1, edition)
	if err != nil {
		t.Fatal("unexpected error during NewCoach: ", err.Error())
	}
	coach3, err := NewCoach("Coach 3", faction1, edition)
	if err != nil {
		t.Fatal("unexpected error during NewCoach: ", err.Error())
	}
	target := Squad{Name: "squad1", Edition: edition, Members: []Coach{coach1, coach2, coach3}}
	// When
	result, err := NewSquad(target.Name, target.Edition, []Coach{coach1, coach2, coach3})
	// Then
	if err != nil {
		t.Fatal("unexpected error during NewSquad: ", err.Error())
	}
	if !cmp.Equal(target, result) {
		t.Error("Result is different from expected: ", cmp.Diff(target, result))
	}
}

func TestNewSquadWithHeterogenousEditionsBetweenEditionAndMembers(t *testing.T) {
	// Given
	faction1 := Faction("Something")
	edition1, err := NewEdition("first test edition", "2023-01-01", 5, 3, true, true, 3, []Faction{faction1}, "something", "tester")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	edition2, err := NewEdition("second test edition", "2023-01-01", 5, 3, true, true, 3, []Faction{faction1}, "something", "tester")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	coach1, err := NewCoach("Coach 1", faction1, edition1)
	if err != nil {
		t.Fatal("unexpected error during NewCoach: ", err.Error())
	}
	coach2, err := NewCoach("Coach2", faction1, edition1)
	if err != nil {
		t.Fatal("unexpected error during NewCoach: ", err.Error())
	}
	coach3, err := NewCoach("Coach 3", faction1, edition1)
	if err != nil {
		t.Fatal("unexpected error during NewCoach: ", err.Error())
	}
	expectedError := "at the end different editions for one squad: first test edition vs second test edition"
	// When
	_, err = NewSquad("squad1", edition2, []Coach{coach1, coach2, coach3})
	// Then
	if err == nil {
		t.Fatal("NewSquad should return an error got nil")
	}
	if err == nil {
		t.Fatal("NewSquad should return an error", err.Error())
	}
	if !cmp.Equal(err.Error(), expectedError) {
		t.Error("Error is different from expected: ", cmp.Diff(expectedError, err.Error()))
	}
}

func TestNewSquadWithHeterogenousEditionsBetweenMembers(t *testing.T) {
	// Given
	faction1 := Faction("Something")
	edition1, err := NewEdition("first test edition", "2023-01-01", 5, 3, true, true, 3, []Faction{faction1}, "something", "tester")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	edition2, err := NewEdition("second test edition", "2023-01-01", 5, 3, true, true, 3, []Faction{faction1}, "something", "tester")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	coach1, err := NewCoach("Coach 1", faction1, edition1)
	if err != nil {
		t.Fatal("unexpected error during NewCoach: ", err.Error())
	}
	coach2, err := NewCoach("Coach2", faction1, edition2)
	if err != nil {
		t.Fatal("unexpected error during NewCoach: ", err.Error())
	}
	coach3, err := NewCoach("Coach 3", faction1, edition1)
	if err != nil {
		t.Fatal("unexpected error during NewCoach: ", err.Error())
	}
	expectedError := "at 1 different editions for one squad: first test edition vs second test edition"
	// When
	_, err = NewSquad("squad1", edition1, []Coach{coach1, coach2, coach3})
	// Then
	if err == nil {
		t.Fatal("NewSquad should return an error got nil")
	}
	if err == nil {
		t.Fatal("NewSquad should return an error", err.Error())
	}
	if !cmp.Equal(err.Error(), expectedError) {
		t.Error("Error is different from expected: ", cmp.Diff(expectedError, err.Error()))
	}
}
