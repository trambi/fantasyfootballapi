// Package core FantasyFootballApi
// Copyright (C) 2019-2024 Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"errors"
	"fmt"
)

type ValuableVariable struct {
	Field string
}

func (variable ValuableVariable) Evaluate(provider IValueProvider) (float64, error) {
	return provider.ValueByField(variable.Field)
}

func (variable ValuableVariable) String() string {
	return fmt.Sprintf("{%s}", variable.Field)
}

type ConstantValue struct {
	Value float64
}

func (constant ConstantValue) Evaluate(provider IValueProvider) (float64, error) {
	return constant.Value, nil
}

func (constant ConstantValue) String() string {
	return fmt.Sprintf("%f", constant.Value)
}

type ValuableSum struct {
	Left  IValuable
	Right IValuable
}

func (sum ValuableSum) Evaluate(provider IValueProvider) (float64, error) {
	left, leftErr := sum.Left.Evaluate(provider)
	right, rightErr := sum.Right.Evaluate(provider)
	if leftErr != nil || rightErr != nil {
		return 0, fmt.Errorf("left: %v right: %v", leftErr, rightErr)
	}
	return (left + right), nil
}

func (sum ValuableSum) String() string {
	return fmt.Sprintf("%v+%v", sum.Left, sum.Right)
}

// ValuableError is a value for error case
type ValuableError struct{}

// Evaluate return value of error node always an error
func (node ValuableError) Evaluate(provider IValueProvider) (float64, error) {
	return 0, errors.New("try to value of a ValuableError")
}

func (node ValuableError) String() string {
	return "an error has occurred"
}

// NewValuable parse the given expression and return the tree of IValuable
func NewValuable(expression string) (IValuable, error) {
	parser, err := NewParser(expression)
	if err != nil {
		return ValuableError{}, err
	}
	return parser.expr()
}
