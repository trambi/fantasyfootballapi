// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import "fmt"

// Coach represents a real player
type Coach struct {
	ID        uint
	Name      string
	TeamName  string
	NafNumber uint
	Faction   Faction
	Ready     bool
	Edition   Edition
	SquadID   uint
}

// NewCoach is constructor of Coach
func NewCoach(name string, faction Faction, edition Edition) (Coach, error) {
	isAllowed := false
	for _, allowedFaction := range edition.AllowedFactions {
		if allowedFaction == faction {
			isAllowed = true
			break
		}
	}
	if !isAllowed {
		return Coach{}, fmt.Errorf("%s is not an allowed faction", faction)
	}
	return Coach{Name: name, Faction: faction, Edition: edition}, nil
}
