// Package core FantasyFootballApi
// Copyright (C) 2019-2024 Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
//
// Package based on the series of article of Ruslan Pivak Blog https://ruslangpivak.com

package core

import (
	"testing"
)

func TestEvaluateWithValidExpression(t *testing.T) {
	testCases := []struct {
		expression string
		game       Game
		expected   float64
	}{
		{"1", Game{}, 1},
		{"1+2", Game{}, 3},
		{"{TdFor}", Game{Td1: 10}, 10},
		{"{TdFor}+1", Game{Td1: 10}, 11},
		{"{CasualtiesFor}+1", Game{Casualties1: 20}, 21},
		{"{CompletionsFor}+1", Game{Completions1: 30}, 31},
		{"{FoulsFor}+1", Game{Fouls1: 40}, 41},
		{"{PointsFor}+1", Game{Points1: 50}, 51},
		{"{TdAgainst}+1", Game{Td2: 60}, 61},
		{"{CasualtiesAgainst}+1", Game{Casualties2: 70}, 71},
		{"{CompletionsAgainst}+1", Game{Completions2: 80}, 81},
		{"{FoulsAgainst}+1", Game{Fouls2: 90}, 91},
		{"{PointsAgainst}+1", Game{Points2: 100}, 101},
	}
	for _, tc := range testCases {
		// Given
		valuable, err := NewValuable(tc.expression)
		if err != nil {
			t.Fatalf("Unexpected error when constructing Valuable with %s: %v\n", tc.expression, err.Error())
		}
		// When
		result, err := valuable.Evaluate(tc.game)
		// Then
		if err != nil {
			t.Fatalf("Unexpected error when getting value from game %s: %v\n", tc.expression, err.Error())
		}
		if tc.expected != result {
			t.Fatalf("result %f is different from expected %f", result, tc.expected)
		}
	}
}

func TestValuableAsStringWithValidExpression(t *testing.T) {
	testCases := []struct {
		expression string
		expected   string
	}{
		{"1", "1.000000"},
		{"1+2", "1.000000+2.000000"},
		{"{TdFor}", "{TdFor}"},
		{"{TdFor}+1", "{TdFor}+1.000000"},
	}
	for _, tc := range testCases {
		// Given
		valuable, err := NewValuable(tc.expression)
		if err != nil {
			t.Fatalf("Unexpected error when constructing Valuable with %s: %v\n", tc.expression, err.Error())
		}
		// When
		result := valuable.String()
		// Then
		if tc.expected != result {
			t.Fatalf("result %s is different from expected %s", result, tc.expected)
		}
	}
}

func TestNewValuableWithInvalidInput(t *testing.T) {
	testCases := []struct {
		invalidExpression string
	}{
		{""},
		{"+1"},
	}
	for _, tc := range testCases {
		// Given
		// When
		_, err := NewValuable(tc.invalidExpression)
		// Then
		if err == nil {
			t.Fatalf("Error was expected for |%s|\n", tc.invalidExpression)
		}
	}
}
