// Package core FantasyFootballApi
// Copyright (C) 2019-2024 Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

// IValueProvider can be used to provide a value to IValuable.
// Structs that implement this interface must provide an implementation of the ValueByField method.
type IValueProvider interface {
	ValueByField(field string) (float64, error)
}
