// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

// RankingGame represents a game for ranking, it is from one coach perspective
type RankingGame struct {
	ID             uint
	Edition        *Edition
	Round          uint
	Table          uint
	Played         bool
	Finale         bool
	Coach          *Coach
	Opponent       *Coach
	ForByField     map[RankingField]float64
	AgainstByField map[RankingField]float64
	SpecialFor     string
	SpecialAgainst string
}

func NewRankingGame(game Game) RankingGame {
	forByField := map[RankingField]float64{}
	againstByField := map[RankingField]float64{}
	if game.Played {
		forByField[Td] = float64(game.Td1)
		forByField[Casualties] = float64(game.Casualties1)
		forByField[Completions] = float64(game.Completions1)
		forByField[Fouls] = float64(game.Fouls1)
		forByField[Points] = game.Points1
		againstByField[Td] = float64(game.Td2)
		againstByField[Casualties] = float64(game.Casualties2)
		againstByField[Completions] = float64(game.Completions2)
		againstByField[Fouls] = float64(game.Fouls2)
		againstByField[Points] = game.Points2
	}

	return RankingGame{
		ID:             game.ID,
		Edition:        game.Edition,
		Round:          game.Round,
		Table:          game.Table,
		Played:         game.Played,
		Finale:         game.Finale,
		Coach:          game.Coach1,
		Opponent:       game.Coach2,
		ForByField:     forByField,
		SpecialFor:     game.Special1,
		AgainstByField: againstByField,
		SpecialAgainst: game.Special2,
	}
}

func (game RankingGame) Reverse() RankingGame {
	return RankingGame{
		ID:             game.ID,
		Edition:        game.Edition,
		Round:          game.Round,
		Table:          game.Table,
		Played:         game.Played,
		Finale:         game.Finale,
		Coach:          game.Opponent,
		Opponent:       game.Coach,
		ForByField:     game.AgainstByField,
		SpecialFor:     game.SpecialAgainst,
		AgainstByField: game.ForByField,
		SpecialAgainst: game.SpecialFor,
	}
}
