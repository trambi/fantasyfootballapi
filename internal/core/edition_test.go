// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
)

type gameTdAndCasualties struct {
	Round       uint
	Table       uint
	Coach1Index uint
	Coach2Index uint
	Td1         int
	Td2         int
	Casualties1 int
	Casualties2 int
}

func tdAndCasualtiesToGames(t *testing.T, gameTable []gameTdAndCasualties, coachs []Coach) []Game {
	t.Helper()
	games := []Game{}
	for _, element := range gameTable {
		game, err := NewGame(element.Round, element.Table, coachs[element.Coach1Index], coachs[element.Coach2Index])
		if err != nil {
			t.Fatal("unexpected error during first NewGame: ", err.Error())
		}
		game.SetTd(element.Td1, element.Td2)
		game.SetCasualties(element.Casualties1, element.Casualties2)
		game.ComputeGamePoints()
		games = append(games, game)
	}
	return games
}

func create2SquadsOf2Coachs(edition Edition, faction Faction, t *testing.T) ([]Coach, Squad, Squad) {
	t.Helper()
	coachs := create4Coachs(edition, faction, t)
	squad1, err := NewSquad("Squad 1", edition, []Coach{coachs[0], coachs[2]})
	if err != nil {
		t.Fatal("unexpected error during first NewSquad: ", err.Error())
	}
	squad2, err := NewSquad("Squad 2", edition, []Coach{coachs[1], coachs[3]})
	if err != nil {
		t.Fatal("unexpected error during second NewSquad: ", err.Error())
	}
	return coachs, squad1, squad2
}

func create4Coachs(edition Edition, faction Faction, t *testing.T) []Coach {
	t.Helper()
	coachs := []Coach{}
	for i := 0; i < 4; i++ {
		coach, err := NewCoach(fmt.Sprintf("coach %d", i+1), faction, edition)
		if err != nil {
			t.Fatalf("unexpected error during NewCoach[%d]: %s", i, err.Error())
		}
		coachs = append(coachs, coach)
	}
	return coachs
}

func TestNewEdition(t *testing.T) {
	// Given
	expectedEdition := Edition{
		Name:                "name",
		Day1:                "2023-11-25",
		Day2:                "2023-11-26",
		RoundNumber:         5,
		CurrentRound:        0,
		FirstDayRound:       3,
		UseFinale:           false,
		FullSquad:           false,
		RankingStrategyName: "Test",
		CoachPerSquad:       2,
		AllowedFactions:     []Faction{Faction("Faction1")},
		Organizer:           "organizer",
		CoachRankings:       map[string][]RankingCriteria{},
		SquadRankings:       map[string][]RankingCriteria{},
		GamePoints:          PointsSettings{Default: ConstantValue{Value: 0}, Clauses: []Clause{}},
		ConfrontationPoints: PointsSettings{Default: ConstantValue{Value: 0}, Clauses: []Clause{}},
	}
	// When
	edition, err := NewEdition(expectedEdition.Name, expectedEdition.Day1,
		expectedEdition.RoundNumber, expectedEdition.FirstDayRound, expectedEdition.UseFinale,
		expectedEdition.FullSquad, expectedEdition.CoachPerSquad, expectedEdition.AllowedFactions, expectedEdition.RankingStrategyName,
		expectedEdition.Organizer)
	// Then
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	if !cmp.Equal(expectedEdition, edition) {
		t.Error("Result is different from expected: ", cmp.Diff(expectedEdition, edition))
	}
}

func TestNewEditionWithInvalidFirstDay(t *testing.T) {
	// Given
	expectedError := "firstDay |AAAA-DDD-MM| is not in proper format date - ISO8601"
	// When
	_, err := NewEdition("name", "AAAA-DDD-MM", 0, 1, false, false, 0,
		[]Faction{Faction("Faction1")}, "Test", "organizer")
	// Then
	if err == nil {
		t.Fatal("an error should occurs during NewEdition")
	}
	if !cmp.Equal(expectedError, err.Error()) {
		t.Error("Error is different from expected: ", cmp.Diff(expectedError, err.Error()))
	}
}

func TestGetCurrentEdition(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition1, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3,
		true, false, 0, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	edition2, err := NewEdition("Awesome Tournament 2023", "2023-01-01", 5, 4,
		false, false, 0, []Faction{faction}, "SomethingElse", "NewOrganiser")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	// When
	currentEdition := GetCurrentEdition([]Edition{edition1, edition2})
	if cmp.Equal(currentEdition, edition2) != true {
		t.Errorf("- removed from edition2 ,+ added to currentEdition:\n%v\n", cmp.Diff(edition2, currentEdition))
	}
}

func TestRankCoachTdFor(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3,
		true, false, 0, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	edition.AddCoachTdRanking()
	coachs := create4Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 0, 1, 0, 0},
		{1, 2, 2, 3, 2, 3, 0, 0},
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	coach1Rank := CoachRank{Coach: &coachs[0], ExtraInfos: []float64{0}}
	coach2Rank := CoachRank{Coach: &coachs[1], ExtraInfos: []float64{1}}
	coach3Rank := CoachRank{Coach: &coachs[2], ExtraInfos: []float64{2}}
	coach4Rank := CoachRank{Coach: &coachs[3], ExtraInfos: []float64{3}}
	expected := CoachRanking{
		ExtraInfoNames:             []string{"tdFor"},
		ExtraInfoOrderIsDescending: []bool{true},
		CoachRanks: []CoachRank{
			coach4Rank, coach3Rank,
			coach2Rank, coach1Rank,
		},
	}
	// When
	result, err := edition.RankCoach("td", games)
	if err != nil {
		t.Fatal("unexpected error during coachRank: ", err.Error())
	}
	// Then
	if cmp.Equal(result, expected) != true {
		t.Errorf("- removed from result ,+ added to expected:\n%v\n", cmp.Diff(result, expected))
	}
}

func TestRankCoachTdForWithoutTdForRanking(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3,
		true, false, 0, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	coachs := create4Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 0, 1, 0, 0},
		{1, 2, 2, 3, 2, 3, 0, 0},
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	expected := "td is not an existing coach ranking"
	// When
	_, err = edition.RankCoach("td", games)
	if err == nil {
		t.Fatal("should return an error during coachRank but it returns nil")
	}
	// Then
	if cmp.Equal(err.Error(), expected) != true {
		t.Errorf("- removed from result ,+ added to expected:\n%v\n", cmp.Diff(err.Error(), expected))
	}
}

func TestRankCoachCasualtiesFor(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3,
		true, false, 0, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	edition.AddCoachCasualtiesRanking()
	coachs := create4Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 0, 0, 3, 9},
		{1, 2, 2, 3, 0, 0, 6, 12},
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	coach1Rank := CoachRank{Coach: &coachs[0], ExtraInfos: []float64{3}}
	coach2Rank := CoachRank{Coach: &coachs[1], ExtraInfos: []float64{9}}
	coach3Rank := CoachRank{Coach: &coachs[2], ExtraInfos: []float64{6}}
	coach4Rank := CoachRank{Coach: &coachs[3], ExtraInfos: []float64{12}}
	expected := CoachRanking{
		ExtraInfoNames:             []string{"casualtiesFor"},
		ExtraInfoOrderIsDescending: []bool{true},
		CoachRanks: []CoachRank{
			coach4Rank, coach2Rank,
			coach3Rank, coach1Rank,
		},
	}
	// When
	result, err := edition.RankCoach("casualties", games)
	if err != nil {
		t.Fatal("unexpected error during coachRank: ", err.Error())
	}
	// Then
	if cmp.Equal(result, expected) != true {
		t.Errorf("- removed from result ,+ added to expected:\n%v\n", cmp.Diff(result, expected))
	}
}

func TestRankCoachDefense(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3,
		true, false, 0, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	edition.AddCoachDefenseRanking()
	coachs := create4Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 0, 1, 0, 0},
		{1, 2, 2, 3, 2, 3, 0, 0},
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	coach1Rank := CoachRank{Coach: &coachs[0], ExtraInfos: []float64{1}}
	coach2Rank := CoachRank{Coach: &coachs[1], ExtraInfos: []float64{0}}
	coach3Rank := CoachRank{Coach: &coachs[2], ExtraInfos: []float64{3}}
	coach4Rank := CoachRank{Coach: &coachs[3], ExtraInfos: []float64{2}}
	expected := CoachRanking{
		ExtraInfoNames:             []string{"tdAgainst"},
		ExtraInfoOrderIsDescending: []bool{false},
		CoachRanks: []CoachRank{
			coach2Rank, coach1Rank,
			coach4Rank, coach3Rank,
		},
	}
	// When
	result, err := edition.RankCoach("defense", games)
	if err != nil {
		t.Fatal("unexpected error during coachRank: ", err.Error())
	}
	// Then
	if cmp.Equal(result, expected) != true {
		t.Errorf("- removed from result ,+ added to expected:\n%v\n", cmp.Diff(result, expected))
	}
}

func TestRankMainCoachRankingRdvbb1(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3,
		false, false, 0, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("1", "{TdFor}+1={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for small loss: ", err.Error())
	}
	err = edition.AddGamePointsClause("2", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("5", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	pointsRanking := RankingCriteria{Label: "points", Type: For, Field: Points, Descending: true}
	netTdRanking := RankingCriteria{Label: "netTd", Type: Net, Field: Td, Descending: true}
	casualtiesForRanking := RankingCriteria{Label: "casualtiesFor", Type: For, Field: Casualties, Descending: true}
	edition.CoachRankings["main"] = []RankingCriteria{pointsRanking, netTdRanking, casualtiesForRanking}
	coachs := create4Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 0, 1, 3, 9},
		{1, 2, 2, 3, 1, 3, 6, 12},
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	coach1Rank := CoachRank{Coach: &coachs[0], ExtraInfos: []float64{1, -1, 3}}
	coach2Rank := CoachRank{Coach: &coachs[1], ExtraInfos: []float64{5, 1, 9}}
	coach3Rank := CoachRank{Coach: &coachs[2], ExtraInfos: []float64{0, -2, 6}}
	coach4Rank := CoachRank{Coach: &coachs[3], ExtraInfos: []float64{5, 2, 12}}
	expected := CoachRanking{
		ExtraInfoNames:             []string{"points", "netTd", "casualtiesFor"},
		ExtraInfoOrderIsDescending: []bool{true, true, true},
		CoachRanks: []CoachRank{
			coach4Rank, coach2Rank, coach1Rank, coach3Rank,
		},
	}
	// When
	result, err := edition.RankCoach("main", games)
	if err != nil {
		t.Fatal("unexpected error during coachRank: ", err.Error())
	}
	// Then
	if cmp.Equal(expected, result) != true {
		t.Errorf("- removed from expected ,+ added to result:\n%v\n", cmp.Diff(expected, result))
	}
}

func TestRankComebackCoachRankingRdvbb1(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 2, 1,
		false, false, 0, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("1", "{TdFor}+1={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for small loss: ", err.Error())
	}
	err = edition.AddGamePointsClause("2", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("5", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	pointsRanking := RankingCriteria{Label: "points", Type: For, Field: Points, Descending: true}
	netTdRanking := RankingCriteria{Label: "netTd", Type: Net, Field: Td, Descending: true}
	casualtiesForRanking := RankingCriteria{Label: "casualtiesFor", Type: For, Field: Casualties, Descending: true}
	edition.CoachRankings["main"] = []RankingCriteria{pointsRanking, netTdRanking, casualtiesForRanking}
	edition.AddCoachComebackRanking()
	coachs := create4Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 0, 1, 3, 9},
		{1, 2, 2, 3, 1, 3, 6, 12},
		{2, 1, 3, 1, 0, 1, 2, 4},
		{2, 2, 2, 0, 1, 3, 3, 5},
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	coach1Rank := CoachRank{Coach: &coachs[0], ExtraInfos: []float64{0, 3, 3}}
	coach2Rank := CoachRank{Coach: &coachs[1], ExtraInfos: []float64{1, 2, 1}}
	coach3Rank := CoachRank{Coach: &coachs[2], ExtraInfos: []float64{0, 4, 4}}
	coach4Rank := CoachRank{Coach: &coachs[3], ExtraInfos: []float64{-1, 1, 2}}
	expected := CoachRanking{
		ExtraInfoNames:             []string{"diffRanking", "firstDayRanking", "finalRanking"},
		ExtraInfoOrderIsDescending: []bool{true, false, false},
		CoachRanks: []CoachRank{
			coach2Rank, coach1Rank, coach3Rank, coach4Rank,
		},
	}
	// When
	result, err := edition.RankCoach("comeback", games)
	if err != nil {
		t.Fatal("unexpected error during coachRank: ", err.Error())
	}
	// Then
	if cmp.Equal(expected, result) != true {
		t.Errorf("- removed from expected ,+ added to result:\n%v\n", cmp.Diff(expected, result))
	}
}

func TestRankCoachRankingWithOpponentPoints(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 2, 1,
		false, false, 0, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	pointsRanking := RankingCriteria{Label: "points", Type: For, Field: Points, Descending: true}
	opponentPointsRanking := RankingCriteria{Label: "opponentPoints", Type: Opponent, Field: Points, Descending: true}
	netTdRanking := RankingCriteria{Label: "netTd", Type: Net, Field: Td, Descending: true}
	edition.CoachRankings["main"] = []RankingCriteria{pointsRanking, opponentPointsRanking, netTdRanking}
	coachs := create4Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 10, 11, 0, 0}, //coach 1 0pts,-1 coach 2 3pts,+1
		{1, 2, 2, 3, 11, 13, 0, 0}, //coach 3 0pts,-2 coach 4 3pts,+2
		{2, 1, 3, 1, 10, 11, 0, 0}, //coach 4 0pts,-1 coach 2 3pts,+1
		{2, 2, 2, 0, 11, 11, 0, 0}, //coach 3 1pts,0 coach 1 1pts,0
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	coach1Rank := CoachRank{Coach: &coachs[0], ExtraInfos: []float64{1, 7, -1}}
	coach2Rank := CoachRank{Coach: &coachs[1], ExtraInfos: []float64{6, 4, 2}}
	coach3Rank := CoachRank{Coach: &coachs[2], ExtraInfos: []float64{1, 4, -2}}
	coach4Rank := CoachRank{Coach: &coachs[3], ExtraInfos: []float64{3, 7, 1}}
	expected := CoachRanking{
		ExtraInfoNames:             []string{"points", "opponentPoints", "netTd"},
		ExtraInfoOrderIsDescending: []bool{true, true, true},
		CoachRanks: []CoachRank{
			coach2Rank, coach4Rank, coach1Rank, coach3Rank,
		},
	}
	// When
	result, err := edition.RankCoach("main", games)
	if err != nil {
		t.Fatal("unexpected error during coachRank: ", err.Error())
	}
	// Then
	if cmp.Equal(expected, result) != true {
		t.Errorf("- removed from expected ,+ added to result:\n%v\n", cmp.Diff(expected, result))
	}
}

func TestRankCoachRankingWithOpponentExceptOwnGamePoints(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 2, 1,
		false, false, 0, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	pointsRanking := RankingCriteria{Label: "points", Type: For, Field: Points, Descending: true}
	opponentPointsRanking := RankingCriteria{Label: "opponentPoints", Type: OpponentExceptOwnGame, Field: Points, Descending: true}
	netTdRanking := RankingCriteria{Label: "netTd", Type: Net, Field: Td, Descending: true}
	edition.CoachRankings["main"] = []RankingCriteria{pointsRanking, opponentPointsRanking, netTdRanking}
	coachs := create4Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 10, 11, 0, 0}, //coach 1 0pts,-1 coach 2 3pts,+1
		{1, 2, 2, 3, 11, 13, 0, 0}, //coach 3 0pts,-2 coach 4 3pts,+2
		{2, 1, 3, 1, 10, 11, 0, 0}, //coach 4 0pts,-1 coach 2 3pts,+1
		{2, 2, 2, 0, 11, 11, 0, 0}, //coach 3 1pts,0 coach 1 1pts,0
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	coach1Rank := CoachRank{Coach: &coachs[0], ExtraInfos: []float64{1, 3, -1}}
	coach2Rank := CoachRank{Coach: &coachs[1], ExtraInfos: []float64{6, 4, 2}}
	coach3Rank := CoachRank{Coach: &coachs[2], ExtraInfos: []float64{1, 0, -2}}
	coach4Rank := CoachRank{Coach: &coachs[3], ExtraInfos: []float64{3, 4, 1}}
	expected := CoachRanking{
		ExtraInfoNames:             []string{"points", "opponentPoints", "netTd"},
		ExtraInfoOrderIsDescending: []bool{true, true, true},
		CoachRanks: []CoachRank{
			coach2Rank, coach4Rank, coach1Rank, coach3Rank,
		},
	}
	// When
	result, err := edition.RankCoach("main", games)
	if err != nil {
		t.Fatal("unexpected error during coachRank: ", err.Error())
	}
	// Then
	if cmp.Equal(expected, result) != true {
		t.Errorf("- removed from expected ,+ added to result:\n%v\n", cmp.Diff(expected, result))
	}
}

func TestRankCoachRankingWithFinale(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 2, 1,
		true, false, 0, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	pointsRanking := RankingCriteria{Label: "points", Type: For, Field: Points, Descending: true}
	netTdRanking := RankingCriteria{Label: "netTd", Type: Net, Field: Td, Descending: true}
	edition.CoachRankings["main"] = []RankingCriteria{pointsRanking, netTdRanking}
	coachs := create4Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 10, 11, 0, 0}, //coach 1 0pts,-1 coach 2 3pts,+1
		{1, 2, 2, 3, 11, 13, 0, 0}, //coach 3 0pts,-2 coach 4 3pts,+2
		{2, 1, 3, 1, 10, 11, 0, 0}, //coach 4 0pts,-1 coach 2 3pts,+1
		{2, 2, 2, 0, 11, 14, 0, 0}, //coach 3 0pts,-3 coach 1 3pts,+3
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	games[2].Finale = true
	coach1Rank := CoachRank{Coach: &coachs[0], ExtraInfos: []float64{0, 3, 2}}
	coach2Rank := CoachRank{Coach: &coachs[1], ExtraInfos: []float64{2, 6, 2}}
	coach3Rank := CoachRank{Coach: &coachs[2], ExtraInfos: []float64{0, 0, -5}}
	coach4Rank := CoachRank{Coach: &coachs[3], ExtraInfos: []float64{1, 3, 1}}
	expected := CoachRanking{
		ExtraInfoNames:             []string{"finale", "points", "netTd"},
		ExtraInfoOrderIsDescending: []bool{true, true, true},
		CoachRanks: []CoachRank{
			coach2Rank, coach4Rank, coach1Rank, coach3Rank,
		},
	}
	// When
	result, err := edition.RankCoach("main", games)
	if err != nil {
		t.Fatal("unexpected error during coachRank: ", err.Error())
	}
	// Then
	if cmp.Equal(expected, result) != true {
		t.Errorf("- removed from expected ,+ added to result:\n%v\n", cmp.Diff(expected, result))
	}
}

func TestAddCoachComebackWithoutMainCoachRanking(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 2, 1,
		true, false, 0, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("2", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("5", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	expected := "impossible to have comeback ranking without main ranking"
	// When
	err = edition.AddCoachComebackRanking()
	// Then
	if err == nil {
		t.Fatal("Comeback coach without main coach ranking should return an error")
	}
	if err.Error() != expected {
		t.Fatalf("error should be %s - %s received", expected, err.Error())
	}
}

func TestRankComebackCoachRankingWithPlayersWhoPlayedOnlyDay2(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 2, 1,
		true, false, 0, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	pointsRanking := RankingCriteria{Label: "points", Type: For, Field: Points, Descending: true}
	netTdRanking := RankingCriteria{Label: "netTd", Type: Net, Field: Td, Descending: true}
	casualtiesForRanking := RankingCriteria{Label: "casualtiesFor", Type: For, Field: Casualties, Descending: true}
	edition.CoachRankings["main"] = []RankingCriteria{pointsRanking, netTdRanking, casualtiesForRanking}
	edition.AddCoachComebackRanking()
	coachs := create4Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 0, 1, 3, 9},
		// One game for day 1
		{2, 1, 3, 1, 0, 1, 2, 4},
		{2, 2, 2, 0, 1, 3, 3, 5},
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	coach1Rank := CoachRank{Coach: &coachs[0], ExtraInfos: []float64{0, 2, 2}}
	coach2Rank := CoachRank{Coach: &coachs[1], ExtraInfos: []float64{0, 1, 1}}
	expected := CoachRanking{
		ExtraInfoNames:             []string{"diffRanking", "firstDayRanking", "finalRanking"},
		ExtraInfoOrderIsDescending: []bool{true, false, false},
		CoachRanks: []CoachRank{
			coach2Rank, coach1Rank,
		},
	}
	// When
	result, err := edition.RankCoach("comeback", games)
	if err != nil {
		t.Fatal("unexpected error during coachRank: ", err.Error())
	}
	// Then
	if cmp.Equal(expected, result) != true {
		t.Errorf("- removed from expected ,+ added to result:\n%v\n", cmp.Diff(expected, result))
	}
}

func TestRankSquadTdFor(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3,
		true, false, 2, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	edition.AddSquadTdRanking()
	coachs, squad1, squad2 := create2SquadsOf2Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 0, 1, 0, 0},
		{1, 2, 2, 3, 2, 3, 0, 0},
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	squad1Rank := SquadRank{Squad: &squad1, ExtraInfos: []float64{2}}
	squad2Rank := SquadRank{Squad: &squad2, ExtraInfos: []float64{4}}
	expected := SquadRanking{
		ExtraInfoNames:             []string{"tdFor"},
		ExtraInfoOrderIsDescending: []bool{true},
		SquadRanks: []SquadRank{
			squad2Rank, squad1Rank,
		},
	}
	// When
	result, err := edition.RankSquad("td", games, []Squad{squad1, squad2})
	if err != nil {
		t.Fatal("unexpected error during RankSquad: ", err.Error())
	}
	// Then
	if cmp.Equal(result, expected) != true {
		t.Errorf("- removed from result ,+ added to expected:\n%v\n", cmp.Diff(result, expected))
	}
}

func TestRankMainSquadRanking(t *testing.T) { // Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3,
		true, false, 2, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	pointsRanking := RankingCriteria{Label: "points", Type: For, Field: Points, Descending: true}
	netTdRanking := RankingCriteria{Label: "netTd", Type: Net, Field: Td, Descending: true}
	edition.AddSquadMainRanking([]RankingCriteria{pointsRanking, netTdRanking})
	coachs, squad1, squad2 := create2SquadsOf2Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 0, 1, 0, 0},
		{1, 2, 2, 3, 2, 3, 0, 0},
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	squad1Rank := SquadRank{Squad: &squad1, ExtraInfos: []float64{0, -2}}
	squad2Rank := SquadRank{Squad: &squad2, ExtraInfos: []float64{6, 2}}
	expected := SquadRanking{
		ExtraInfoNames:             []string{"points", "netTd"},
		ExtraInfoOrderIsDescending: []bool{true, true},
		SquadRanks: []SquadRank{
			squad2Rank, squad1Rank,
		},
	}
	// When
	result, err := edition.RankSquad("main", games, []Squad{squad1, squad2})
	if err != nil {
		t.Fatal("unexpected error during RankSquad: ", err.Error())
	}
	// Then
	if cmp.Equal(result, expected) != true {
		t.Errorf("- removed from result ,+ added to expected:\n%v\n", cmp.Diff(result, expected))
	}
}

func TestRankSquadComebackRanking(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 2, 1,
		true, false, 2, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	pointsRanking := RankingCriteria{Label: "points", Type: For, Field: Points, Descending: true}
	netTdRanking := RankingCriteria{Label: "netTd", Type: Net, Field: Td, Descending: true}
	edition.AddSquadMainRanking([]RankingCriteria{pointsRanking, netTdRanking})
	edition.AddSquadComebackRanking()
	coachs, squad1, squad2 := create2SquadsOf2Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 0, 1, 0, 0},
		{1, 2, 2, 3, 1, 2, 0, 0},
		{2, 1, 3, 0, 0, 1, 0, 0},
		{2, 2, 1, 2, 1, 3, 0, 0},
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	squad1Rank := SquadRank{Squad: &squad1, ExtraInfos: []float64{1, 2, 1}}
	squad2Rank := SquadRank{Squad: &squad2, ExtraInfos: []float64{-1, 1, 2}}
	expected := SquadRanking{
		ExtraInfoNames:             []string{"diffRanking", "firstDayRanking", "finalRanking"},
		ExtraInfoOrderIsDescending: []bool{true, false, false},
		SquadRanks: []SquadRank{
			squad1Rank, squad2Rank,
		},
	}
	// When
	result, err := edition.RankSquad("comeback", games, []Squad{squad1, squad2})
	if err != nil {
		t.Fatal("unexpected error during coachRank: ", err.Error())
	}
	// Then
	if cmp.Equal(expected, result) != true {
		t.Errorf("- removed from expected ,+ added to result:\n%v\n", cmp.Diff(expected, result))
	}
}

func TestRankMainSquadRankingWithConfrontationPoints(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3,
		true, false, 2, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	err = edition.AddConfrontationPointsClause("1", "{PointsFor}={PointsAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddConfrontationPointsClause("3", "{PointsFor}>{PointsAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddConfrontationPointsClause for win: ", err.Error())
	}
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	pointsRanking := RankingCriteria{Label: "points", Type: For, Field: ConfrontationPoints, Descending: true}
	individualPointsRanking := RankingCriteria{Label: "memberPoints", Type: For, Field: Points, Descending: true}
	netTdRanking := RankingCriteria{Label: "netTd", Type: Net, Field: Td, Descending: true}
	edition.AddSquadMainRanking([]RankingCriteria{pointsRanking, individualPointsRanking, netTdRanking})
	coachs, squad1, squad2 := create2SquadsOf2Coachs(edition, faction, t)
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 1, 1, 0, 0},
		{1, 2, 2, 3, 2, 3, 0, 0},
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	squad1Rank := SquadRank{Squad: &squad1, ExtraInfos: []float64{0, 1, -1}}
	squad2Rank := SquadRank{Squad: &squad2, ExtraInfos: []float64{3, 4, 1}}
	expected := SquadRanking{
		ExtraInfoNames:             []string{"points", "memberPoints", "netTd"},
		ExtraInfoOrderIsDescending: []bool{true, true, true},
		SquadRanks: []SquadRank{
			squad2Rank, squad1Rank,
		},
	}
	// When
	result, err := edition.RankSquad("main", games, []Squad{squad1, squad2})
	if err != nil {
		t.Fatal("unexpected error during RankSquad: ", err.Error())
	}
	// Then
	if cmp.Equal(result, expected) != true {
		t.Errorf("- removed from result ,+ added to expected:\n%v\n", cmp.Diff(result, expected))
	}
}

func TestRankMainSquadRankingWithOpponentConfrontationPoints(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3,
		true, false, 2, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	err = edition.AddConfrontationPointsClause("1", "{PointsFor}={PointsAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddConfrontationPointsClause("3", "{PointsFor}>{PointsAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddConfrontationPointsClause for win: ", err.Error())
	}
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	pointsRanking := RankingCriteria{Label: "points", Type: For, Field: ConfrontationPoints, Descending: true}
	opponentPointsRanking := RankingCriteria{Label: "opponentPoints", Type: Opponent, Field: ConfrontationPoints, Descending: true}
	netTdRanking := RankingCriteria{Label: "netTd", Type: Net, Field: Td, Descending: true}
	edition.AddSquadMainRanking([]RankingCriteria{pointsRanking, opponentPointsRanking, netTdRanking})
	coachs, squad1, squad2 := create2SquadsOf2Coachs(edition, faction, t)
	for i := 4; i < 8; i++ {
		coach, err := NewCoach(fmt.Sprintf("coach %d", i+1), faction, edition)
		if err != nil {
			t.Fatalf("unexpected error during NewCoach[%d]: %s", i, err.Error())
		}
		coachs = append(coachs, coach)
	}
	squad3, err := NewSquad("Squad 3", edition, []Coach{coachs[4], coachs[6]})
	if err != nil {
		t.Fatal("unexpected error during third NewSquad: ", err.Error())
	}
	squad4, err := NewSquad("Squad 4", edition, []Coach{coachs[5], coachs[7]})
	if err != nil {
		t.Fatal("unexpected error during forth NewSquad: ", err.Error())
	}
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 1, 1, 0, 0},
		{1, 2, 2, 3, 0, 1, 0, 0}, // Squad 1 0pts,-1 Squad 2 3pts,1
		{1, 3, 4, 5, 1, 0, 0, 0},
		{1, 4, 6, 7, 1, 0, 0, 0}, // Squad 3 3pts,2 Squad 4 0pts,-2
		{2, 1, 3, 4, 0, 1, 0, 0},
		{2, 2, 1, 6, 1, 2, 0, 0}, // Squad 2 0pts,-2 Squad 3 3pts,2
		{2, 3, 7, 0, 0, 0, 0, 0},
		{2, 4, 5, 2, 0, 0, 0, 0}, // Squad 4 1pts,0 Squad 1 1pts,0
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	squad1Rank := SquadRank{Squad: &squad1, ExtraInfos: []float64{1, 4, -1}}
	squad2Rank := SquadRank{Squad: &squad2, ExtraInfos: []float64{3, 7, -1}}
	squad3Rank := SquadRank{Squad: &squad3, ExtraInfos: []float64{6, 4, 4}}
	squad4Rank := SquadRank{Squad: &squad4, ExtraInfos: []float64{1, 7, -2}}
	expected := SquadRanking{
		ExtraInfoNames:             []string{"points", "opponentPoints", "netTd"},
		ExtraInfoOrderIsDescending: []bool{true, true, true},
		SquadRanks: []SquadRank{
			squad3Rank, squad2Rank, squad4Rank, squad1Rank,
		},
	}
	// When
	result, err := edition.RankSquad("main", games, []Squad{squad1, squad2, squad3, squad4})
	if err != nil {
		t.Fatal("unexpected error during RankSquad: ", err.Error())
	}
	// Then
	if cmp.Equal(result, expected) != true {
		t.Errorf("- removed from result ,+ added to expected:\n%v\n", cmp.Diff(result, expected))
	}
}

func TestRankMainSquadRankingWithOpponentExceptOwnGameConfrontationPoints(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3,
		true, false, 2, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	err = edition.AddConfrontationPointsClause("1", "{PointsFor}={PointsAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddConfrontationPointsClause("3", "{PointsFor}>{PointsAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddConfrontationPointsClause for win: ", err.Error())
	}
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	pointsRanking := RankingCriteria{Label: "points", Type: For, Field: ConfrontationPoints, Descending: true}
	opponentPointsRanking := RankingCriteria{Label: "opponentPoints", Type: OpponentExceptOwnGame, Field: ConfrontationPoints, Descending: true}
	netTdRanking := RankingCriteria{Label: "netTd", Type: Net, Field: Td, Descending: true}
	edition.AddSquadMainRanking([]RankingCriteria{pointsRanking, opponentPointsRanking, netTdRanking})
	coachs, squad1, squad2 := create2SquadsOf2Coachs(edition, faction, t)
	for i := 4; i < 8; i++ {
		coach, err := NewCoach(fmt.Sprintf("coach %d", i+1), faction, edition)
		if err != nil {
			t.Fatalf("unexpected error during NewCoach[%d]: %s", i, err.Error())
		}
		coachs = append(coachs, coach)
	}
	squad3, err := NewSquad("Squad 3", edition, []Coach{coachs[4], coachs[6]})
	if err != nil {
		t.Fatal("unexpected error during third NewSquad: ", err.Error())
	}
	squad4, err := NewSquad("Squad 4", edition, []Coach{coachs[5], coachs[7]})
	if err != nil {
		t.Fatal("unexpected error during forth NewSquad: ", err.Error())
	}
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 1, 1, 0, 0},
		{1, 2, 2, 3, 0, 1, 0, 0}, // Squad 1 0pts,-1 Squad 2 3pts,1
		{1, 3, 4, 5, 1, 0, 0, 0},
		{1, 4, 6, 7, 1, 0, 0, 0}, // Squad 3 3pts,2 Squad 4 0pts,-2
		{2, 1, 3, 4, 0, 1, 0, 0},
		{2, 2, 1, 6, 1, 2, 0, 0}, // Squad 2 0pts,-2 Squad 3 3pts,2
		{2, 3, 7, 0, 0, 0, 0, 0},
		{2, 4, 5, 2, 0, 0, 0, 0}, // Squad 4 1pts,0 Squad 1 1pts,0
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	squad1Rank := SquadRank{Squad: &squad1, ExtraInfos: []float64{1, 0, -1}}
	squad2Rank := SquadRank{Squad: &squad2, ExtraInfos: []float64{3, 4, -1}}
	squad3Rank := SquadRank{Squad: &squad3, ExtraInfos: []float64{6, 4, 4}}
	squad4Rank := SquadRank{Squad: &squad4, ExtraInfos: []float64{1, 3, -2}}
	expected := SquadRanking{
		ExtraInfoNames:             []string{"points", "opponentPoints", "netTd"},
		ExtraInfoOrderIsDescending: []bool{true, true, true},
		SquadRanks: []SquadRank{
			squad3Rank, squad2Rank, squad4Rank, squad1Rank,
		},
	}
	// When
	result, err := edition.RankSquad("main", games, []Squad{squad1, squad2, squad3, squad4})
	if err != nil {
		t.Fatal("unexpected error during RankSquad: ", err.Error())
	}
	// Then
	if cmp.Equal(result, expected) != true {
		t.Errorf("- removed from result ,+ added to expected:\n%v\n", cmp.Diff(result, expected))
	}
}

func TestRankMainSquadRankingWithFinale(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("Awesome Tournament 2003", "2003-05-13", 6, 3,
		true, true, 2, []Faction{faction}, "Something", "Founder")
	if err != nil {
		t.Fatal("unexpected error during NewEdition: ", err.Error())
	}
	err = edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for win: ", err.Error())
	}
	err = edition.AddConfrontationPointsClause("1", "{PointsFor}={PointsAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	err = edition.AddConfrontationPointsClause("3", "{PointsFor}>{PointsAgainst}")
	if err != nil {
		t.Fatal("unexpected error during AddConfrontationPointsClause for win: ", err.Error())
	}
	if err != nil {
		t.Fatal("unexpected error during AddGamePointsClause for draw: ", err.Error())
	}
	pointsRanking := RankingCriteria{Label: "points", Type: For, Field: ConfrontationPoints, Descending: true}
	netTdRanking := RankingCriteria{Label: "netTd", Type: Net, Field: Td, Descending: true}
	edition.AddSquadMainRanking([]RankingCriteria{pointsRanking, netTdRanking})
	coachs, squad1, squad2 := create2SquadsOf2Coachs(edition, faction, t)
	for i := 4; i < 8; i++ {
		coach, err := NewCoach(fmt.Sprintf("coach %d", i+1), faction, edition)
		if err != nil {
			t.Fatalf("unexpected error during NewCoach[%d]: %s", i, err.Error())
		}
		coachs = append(coachs, coach)
	}
	squad3, err := NewSquad("Squad 3", edition, []Coach{coachs[4], coachs[6]})
	if err != nil {
		t.Fatal("unexpected error during third NewSquad: ", err.Error())
	}
	squad4, err := NewSquad("Squad 4", edition, []Coach{coachs[5], coachs[7]})
	if err != nil {
		t.Fatal("unexpected error during forth NewSquad: ", err.Error())
	}
	gameTable := []gameTdAndCasualties{
		{1, 1, 0, 1, 1, 1, 0, 0},
		{1, 2, 2, 3, 0, 1, 0, 0}, // Squad 1 0pts,-1 Squad 2 3pts,1
		{1, 3, 4, 5, 1, 0, 0, 0},
		{1, 4, 6, 7, 1, 0, 0, 0}, // Squad 3 3pts,2 Squad 4 0pts,-2
		{2, 1, 3, 4, 0, 1, 0, 0},
		{2, 2, 1, 6, 1, 2, 0, 0}, // Squad 2 0pts,-2 Squad 3 3pts,2
		{2, 3, 7, 0, 2, 0, 0, 0},
		{2, 4, 5, 2, 2, 0, 0, 0}, // Squad 4 3pts,+4 Squad 1 0pts,-4
	}
	games := tdAndCasualtiesToGames(t, gameTable, coachs)
	games[4].Finale = true
	squad1Rank := SquadRank{Squad: &squad1, ExtraInfos: []float64{0, 0, -5}}
	squad2Rank := SquadRank{Squad: &squad2, ExtraInfos: []float64{1, 3, -1}}
	squad3Rank := SquadRank{Squad: &squad3, ExtraInfos: []float64{2, 6, 4}}
	squad4Rank := SquadRank{Squad: &squad4, ExtraInfos: []float64{0, 3, 2}}
	expected := SquadRanking{
		ExtraInfoNames:             []string{"finale", "points", "netTd"},
		ExtraInfoOrderIsDescending: []bool{true, true, true},
		SquadRanks: []SquadRank{
			squad3Rank,
			squad2Rank, // Because of the finale, the squad 2 is ranked after the squad 3 and before the squad 4
			squad4Rank,
			squad1Rank,
		},
	}
	// When
	result, err := edition.RankSquad("main", games, []Squad{squad1, squad2, squad3, squad4})
	if err != nil {
		t.Fatal("unexpected error during RankSquad: ", err.Error())
	}
	// Then
	if cmp.Equal(result, expected) != true {
		t.Errorf("- removed from result ,+ added to expected:\n%v\n", cmp.Diff(result, expected))
	}
}
