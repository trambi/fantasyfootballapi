// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNewCoach(t *testing.T) {
	// Given
	target := Coach{
		Name:    "name",
		Faction: Faction("Something"),
		Edition: Edition{AllowedFactions: []Faction{Faction("Something"), Faction("Else")}},
	}
	// When
	result, err := NewCoach(target.Name, target.Faction, target.Edition)
	// Then
	if err != nil {
		t.Fatal("unexpected error during NewCoach: ", err.Error())
	}
	if !cmp.Equal(target, result) {
		t.Error("Result is different from expected: ", cmp.Diff(target, result))
	}
}

func TestNewCoachWithNotAllowedFaction(t *testing.T) {
	// Given
	coach := Coach{
		Name:    "name",
		Faction: Faction("another"),
		Edition: Edition{AllowedFactions: []Faction{Faction("Something"), Faction("Else")}},
	}

	expectedError := "another is not an allowed faction"
	// When
	_, err := NewCoach(coach.Name, coach.Faction, coach.Edition)
	// Then
	if err == nil {
		t.Fatal("NewCoach should return an error")
	}
	if !cmp.Equal(expectedError, err.Error()) {
		t.Error("Error is different from expected: ", cmp.Diff(expectedError, err.Error()))
	}
}
