// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"errors"
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func createTwoCoachs(t *testing.T) (Coach, Coach) {
	t.Helper()
	faction1 := Faction("faction1")
	faction2 := Faction("faction2")
	edition, err := NewEdition("myEdition", "2023-05-13", 5, 3, true, false, 0, []Faction{faction1, faction2}, "something", "someone")
	if err != nil {
		t.Fatalf("Unexpected error while creating edition: %s", err.Error())
	}
	coach1, err := NewCoach("Coach 1", faction1, edition)
	if err != nil {
		t.Fatalf("Unexpected error while creating coach 1: %s", err.Error())
	}
	coach2, err := NewCoach("Coach 2", faction2, edition)
	if err != nil {
		t.Fatalf("Unexpected error while creating coach 2: %s", err.Error())
	}
	return coach1, coach2
}

func TestNewGameWithoutRoundAndTable(t *testing.T) {
	coach1, coach2 := createTwoCoachs(t)
	expectedError := errors.New("error in game constructor: round is nul, table is nul")
	_, err := NewGame(0, 0, coach1, coach2)
	if err == nil {
		t.Fatal("Unexpected nil error")
	}
	if expectedError.Error() != err.Error() {
		t.Errorf("expected error in game constructor |%v| got: |%v|", expectedError, err)
	}
}
func TestNewGameWithIncoherentEditions(t *testing.T) {
	coach1, coach2 := createTwoCoachs(t)
	anotherEdition, err := NewEdition("myEdition-2", "2024-01-01", 5, 3, true, false, 0, coach1.Edition.AllowedFactions, "Something", "someone")
	if err != nil {
		t.Fatalf("Unexpected error while creating another edition: %s", err.Error())
	}
	coach2.Edition = anotherEdition
	expectedError := errors.New("error in game constructor: coach1 edition is different from coach2 edition")
	_, err = NewGame(1, 1, coach1, coach2)
	if err == nil {
		t.Fatal("Unexpected nil error")
	}
	if expectedError.Error() != err.Error() {
		t.Errorf("expected error in game constructor |%v| got: |%v|", expectedError, err)
	}
}

func TestNewGame(t *testing.T) {
	coach1, coach2 := createTwoCoachs(t)
	expectedGame := Game{Edition: &coach1.Edition, Round: 1, Table: 1, Finale: false, Played: false, Coach1: &coach1, Coach2: &coach2}
	game, err := NewGame(1, 1, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err.Error())
	}
	if !cmp.Equal(expectedGame, game) {
		t.Errorf("game returned by constructor is not expected: %v", cmp.Diff(expectedGame, game))
	}
}

func TestGetRankingGames(t *testing.T) {
	coach1, coach2 := createTwoCoachs(t)
	edition := coach1.Edition

	game, err := NewGame(1, 1, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err.Error())
	}
	game.SetTd(2, 7)
	game.SetCasualties(3, 8)
	game.SetCompletions(4, 9)
	game.SetFouls(5, 10)
	game.Points1 = 6
	game.Points2 = 11
	game.SetSpecial("special for coach1", "special for coach2")
	expectedRankingGame1 := RankingGame{
		Edition:  &edition,
		Round:    game.Round,
		Table:    game.Table,
		Played:   game.Played,
		Finale:   game.Finale,
		Coach:    &coach1,
		Opponent: &coach2,
		ForByField: map[RankingField]float64{
			Td:          2,
			Casualties:  3,
			Completions: 4,
			Fouls:       5,
			Points:      6,
		},
		AgainstByField: map[RankingField]float64{
			Td:          7,
			Casualties:  8,
			Completions: 9,
			Fouls:       10,
			Points:      11,
		},
		SpecialFor:     "special for coach1",
		SpecialAgainst: "special for coach2",
	}
	expectedRankingGame2 := RankingGame{
		Edition:  &edition,
		Round:    game.Round,
		Table:    game.Table,
		Played:   game.Played,
		Finale:   game.Finale,
		Coach:    &coach2,
		Opponent: &coach1,
		ForByField: map[RankingField]float64{
			Td:          7,
			Casualties:  8,
			Completions: 9,
			Fouls:       10,
			Points:      11,
		},
		AgainstByField: map[RankingField]float64{
			Td:          2,
			Casualties:  3,
			Completions: 4,
			Fouls:       5,
			Points:      6,
		},
		SpecialFor:     "special for coach2",
		SpecialAgainst: "special for coach1",
	}
	expected := [2]RankingGame{expectedRankingGame1, expectedRankingGame2}
	result := game.GetRankingGames()
	if !cmp.Equal(expected, result) {
		t.Errorf("couple of ranking games returned by GetRankingGames is not expected: %v", cmp.Diff(expected, result))
	}
}

func TestComputeGamePointsClassicFootball(t *testing.T) {
	faction1 := Faction("faction1")
	faction2 := Faction("faction2")
	edition, err := NewEdition("myEdition", "2023-05-13", 5, 3, true, false, 0, []Faction{faction1, faction2}, "something", "someone")
	if err != nil {
		t.Fatalf("Unexpected error while creating edition: %s", err.Error())
	}
	edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	coach1, err := NewCoach("Coach 1", faction1, edition)
	if err != nil {
		t.Fatalf("Unexpected error while creating coach 1: %s", err.Error())
	}
	coach2, err := NewCoach("Coach 2", faction2, edition)
	if err != nil {
		t.Fatalf("Unexpected error while creating coach 2: %s", err.Error())
	}
	game, err := NewGame(1, 1, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err.Error())
	}
	testCases := []struct {
		td1             int
		td2             int
		expectedPoints1 float64
		expectedPoints2 float64
	}{
		{0, 1, 0.0, 3.0},
		{1, 1, 1.0, 1.0},
		{1, 0, 3.0, 0.0},
	}
	for _, tc := range testCases {
		game.SetTd(tc.td1, tc.td2)
		err := game.ComputeGamePoints()
		if err != nil {
			t.Fatalf("Unexpected error: %s", err.Error())
		}
		if !cmp.Equal(tc.expectedPoints1, game.Points1) || !cmp.Equal(tc.expectedPoints2, game.Points2) {
			t.Errorf("ComputeGamePoints with td1 %d and td2 %d should return %f, %f but it returns %f,%f", tc.td1, tc.td2, tc.expectedPoints1, tc.expectedPoints2, game.Points1, game.Points2)
		}
	}
}

func TestComputeGamePointsRdvBB1(t *testing.T) {
	faction1 := Faction("faction1")
	faction2 := Faction("faction2")
	edition, err := NewEdition("rdvbb", "2023-05-13", 5, 3, true, false, 0, []Faction{faction1, faction2}, "something", "someone")
	if err != nil {
		t.Fatalf("Unexpected error while creating edition: %s", err.Error())
	}
	edition.AddGamePointsClause("1", "{TdFor}+1={TdAgainst}")
	edition.AddGamePointsClause("2", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("5", "{TdFor}>{TdAgainst}")

	coach1, err := NewCoach("Coach 1", faction1, edition)
	if err != nil {
		t.Fatalf("Unexpected error while creating coach 1: %s", err.Error())
	}
	coach2, err := NewCoach("Coach 2", faction2, edition)
	if err != nil {
		t.Fatalf("Unexpected error while creating coach 2: %s", err.Error())
	}
	game, err := NewGame(1, 1, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err.Error())
	}
	testCases := []struct {
		td1             int
		td2             int
		expectedPoints1 float64
		expectedPoints2 float64
	}{
		{0, 1, 1.0, 5.0},
		{0, 2, 0.0, 5.0},
		{1, 1, 2.0, 2.0},
		{1, 0, 5.0, 1.0},
		{2, 0, 5.0, 0.0},
	}
	for _, tc := range testCases {
		game.SetTd(tc.td1, tc.td2)
		err := game.ComputeGamePoints()
		if err != nil {
			t.Fatalf("Unexpected error: %s", err.Error())
		}
		if !cmp.Equal(tc.expectedPoints1, game.Points1) || !cmp.Equal(tc.expectedPoints2, game.Points2) {
			t.Errorf("ComputeGamePoints with td1 %d and td2 %d should return %f, %f but it returns %f,%f", tc.td1, tc.td2, tc.expectedPoints1, tc.expectedPoints2, game.Points1, game.Points2)
		}
	}
}

func TestComputeGamePointsWithVariableInPoints(t *testing.T) {
	faction1 := Faction("faction1")
	faction2 := Faction("faction2")
	edition, err := NewEdition("rdvbb", "2023-05-13", 5, 3, true, false, 0, []Faction{faction1, faction2}, "something", "someone")
	if err != nil {
		t.Fatalf("Unexpected error while creating edition: %s", err.Error())
	}
	edition.GamePoints.Default = ValuableVariable{Field: "TdFor"}
	edition.AddGamePointsClause("100+{TdFor}", "{TdFor}+1={TdAgainst}")
	edition.AddGamePointsClause("200+{TdFor}", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("500+{TdFor}", "{TdFor}>{TdAgainst}")

	coach1, err := NewCoach("Coach 1", faction1, edition)
	if err != nil {
		t.Fatalf("Unexpected error while creating coach 1: %s", err.Error())
	}
	coach2, err := NewCoach("Coach 2", faction2, edition)
	if err != nil {
		t.Fatalf("Unexpected error while creating coach 2: %s", err.Error())
	}
	game, err := NewGame(1, 1, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err.Error())
	}
	testCases := []struct {
		td1             int
		td2             int
		expectedPoints1 float64
		expectedPoints2 float64
	}{
		{0, 1, 100, 501},
		{0, 2, 0, 502},
		{1, 2, 101, 502},
		{1, 3, 1, 503},
		{1, 1, 201, 201},
		{0, 0, 200, 200},
		{2, 2, 202, 202},
		{1, 0, 501, 100},
		{2, 0, 502, 0},
		{2, 1, 502, 101},
		{3, 1, 503, 1},
	}
	for _, tc := range testCases {
		game.SetTd(tc.td1, tc.td2)
		err := game.ComputeGamePoints()
		if err != nil {
			t.Fatalf("Unexpected error: %s", err.Error())
		}
		if !cmp.Equal(tc.expectedPoints1, game.Points1) || !cmp.Equal(tc.expectedPoints2, game.Points2) {
			t.Errorf("ComputeGamePoints with td1 %d and td2 %d should return %f, %f but it returns %f,%f", tc.td1, tc.td2, tc.expectedPoints1, tc.expectedPoints2, game.Points1, game.Points2)
		}
	}
}

func TestGameImplementIValueProvider(t *testing.T) {
	// Given
	coach1, coach2 := createTwoCoachs(t)
	game, err := NewGame(1, 1, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err.Error())
	}
	game.SetTd(2, 7)
	game.SetCasualties(3, 8)
	game.SetCompletions(4, 9)
	game.SetFouls(5, 10)
	game.Points1 = 6
	game.Points2 = 11
	testCases := []struct {
		field         string
		expectedValue float64
	}{
		{"TdFor", 2},
		{"CasualtiesFor", 3},
		{"CompletionsFor", 4},
		{"FoulsFor", 5},
		{"PointsFor", 6},
		{"TdAgainst", 7},
		{"CasualtiesAgainst", 8},
		{"CompletionsAgainst", 9},
		{"FoulsAgainst", 10},
		{"PointsAgainst", 11},
	}
	for _, tc := range testCases {
		// When
		result, err := game.ValueByField(tc.field)
		if err != nil {
			t.Fatalf("Unexpected error: %s", err.Error())
		}
		// Then
		if tc.expectedValue != result {
			t.Errorf("Expect %f get %f", tc.expectedValue, result)
		}
	}
}

func TestGameImplementIValueProviderWithUnknownField(t *testing.T) {
	// Given
	coach1, coach2 := createTwoCoachs(t)
	game, err := NewGame(1, 1, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err.Error())
	}
	game.SetTd(2, 7)
	game.SetCasualties(3, 8)
	game.SetCompletions(4, 9)
	game.SetFouls(5, 10)
	game.Points1 = 6
	game.Points2 = 11
	invalidFields := []string{
		"ConfrontationPointsFor",
		"ConfrontationPointsAgainst",
		"UnknownFor",
		"UnknownAgainst",
	}
	for _, field := range invalidFields {
		expectedError := fmt.Errorf("unknown field %s for game", field)
		// When
		result, err := game.ValueByField(field)

		if err.Error() != expectedError.Error() {
			t.Fatalf("Expect error :%s get error: %s", expectedError.Error(), err.Error())
		}
		// Then
		if result != 0 {
			t.Errorf("Expect 0 get %f", result)
		}
	}
}
