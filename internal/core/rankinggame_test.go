// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNewRankingGame(t *testing.T) {
	faction1 := Faction("faction1")
	faction2 := Faction("faction2")
	edition := Edition{
		Name:                "myEdition",
		Day1:                "2023-05-13",
		Day2:                "2023-05-14",
		RoundNumber:         5,
		CurrentRound:        1,
		FirstDayRound:       3,
		UseFinale:           true,
		FullSquad:           false,
		RankingStrategyName: "Something",
		AllowedFactions:     []Faction{faction1, faction2},
	}
	coach1 := Coach{
		Name:      "Coach 1",
		TeamName:  "A beautiful team",
		NafNumber: 100,
		Faction:   faction1,
		Ready:     false,
		Edition:   edition,
	}
	coach2 := Coach{
		Name:      "Coach 2",
		TeamName:  "Another beautiful team",
		NafNumber: 1000,
		Faction:   faction2,
		Ready:     true,
		Edition:   edition,
	}
	game, err := NewGame(1, 2, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error when constructing Game: %s", err.Error())
	}
	expected := RankingGame{
		Edition:        &edition,
		Round:          1,
		Table:          2,
		Played:         false,
		Finale:         false,
		Coach:          &coach1,
		Opponent:       &coach2,
		ForByField:     map[RankingField]float64{},
		AgainstByField: map[RankingField]float64{},
	}
	result := NewRankingGame(game)
	if !cmp.Equal(expected, result) {
		t.Errorf("result of NewRankingGame is not expected: %v", cmp.Diff(expected, result))
	}
}

func TestNewRankingGamePlayed(t *testing.T) {
	faction1 := Faction("faction1")
	faction2 := Faction("faction2")
	edition := Edition{
		Name:                "myEdition",
		Day1:                "2023-05-13",
		Day2:                "2023-05-14",
		RoundNumber:         5,
		CurrentRound:        1,
		FirstDayRound:       3,
		UseFinale:           true,
		FullSquad:           false,
		RankingStrategyName: "Something",
		AllowedFactions:     []Faction{faction1, faction2},
	}
	coach1 := Coach{
		Name:      "Coach 1",
		TeamName:  "A beautiful team",
		NafNumber: 100,
		Faction:   faction1,
		Ready:     false,
		Edition:   edition,
	}
	coach2 := Coach{
		Name:      "Coach 2",
		TeamName:  "Another beautiful team",
		NafNumber: 1000,
		Faction:   faction2,
		Ready:     true,
		Edition:   edition,
	}
	game, err := NewGame(1, 2, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error when constructing Game: %s", err.Error())
	}
	game.SetTd(3, 4)
	expected := RankingGame{
		Edition:  &edition,
		Round:    1,
		Table:    2,
		Played:   true,
		Finale:   false,
		Coach:    &coach1,
		Opponent: &coach2,
		ForByField: map[RankingField]float64{
			Td:          3,
			Casualties:  0,
			Completions: 0,
			Fouls:       0,
			Points:      0,
		},
		AgainstByField: map[RankingField]float64{
			Td:          4,
			Casualties:  0,
			Completions: 0,
			Fouls:       0,
			Points:      0,
		},
	}
	result := NewRankingGame(game)
	if !cmp.Equal(expected, result) {
		t.Errorf("result of NewRankingGame is not expected: %v", cmp.Diff(expected, result))
	}
}

func TestRankingGameReverse(t *testing.T) {
	faction1 := Faction("faction1")
	faction2 := Faction("faction2")
	edition := Edition{
		Name:                "myEdition",
		Day1:                "2023-05-13",
		Day2:                "2023-05-14",
		RoundNumber:         5,
		CurrentRound:        1,
		FirstDayRound:       3,
		UseFinale:           true,
		FullSquad:           false,
		RankingStrategyName: "Something",
		AllowedFactions:     []Faction{faction1, faction2},
	}
	coach1 := Coach{
		Name:      "Coach 1",
		TeamName:  "A beautiful team",
		NafNumber: 100,
		Faction:   faction1,
		Ready:     false,
		Edition:   edition,
	}
	coach2 := Coach{
		Name:      "Coach 2",
		TeamName:  "Another beautiful team",
		NafNumber: 1000,
		Faction:   faction2,
		Ready:     true,
		Edition:   edition,
	}
	toRevert := RankingGame{
		Edition:  &edition,
		Round:    1,
		Table:    2,
		Played:   false,
		Finale:   false,
		Coach:    &coach1,
		Opponent: &coach2,
		ForByField: map[RankingField]float64{
			Td:          3,
			Casualties:  4,
			Completions: 5,
			Fouls:       6,
			Points:      7,
		},
		SpecialFor: "something",
		AgainstByField: map[RankingField]float64{
			Td:          8,
			Casualties:  9,
			Completions: 10,
			Fouls:       11,
			Points:      12,
		},
		SpecialAgainst: "another thing",
	}
	expected := RankingGame{
		Edition:  &edition,
		Round:    1,
		Table:    2,
		Played:   false,
		Finale:   false,
		Coach:    &coach2,
		Opponent: &coach1,
		ForByField: map[RankingField]float64{
			Td:          8,
			Casualties:  9,
			Completions: 10,
			Fouls:       11,
			Points:      12,
		},
		SpecialFor: "another thing",
		AgainstByField: map[RankingField]float64{
			Td:          3,
			Casualties:  4,
			Completions: 5,
			Fouls:       6,
			Points:      7,
		},
		SpecialAgainst: "something",
	}
	result := toRevert.Reverse()
	if !cmp.Equal(expected, result) {
		t.Errorf("result of Reverse is not expected: %v", cmp.Diff(expected, result))
	}
}
