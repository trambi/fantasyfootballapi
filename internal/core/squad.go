// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"fmt"
)

// Squad represents a team of real players
type Squad struct {
	ID      uint
	Name    string
	Edition Edition
	Members []Coach
}

// NewSquad is constructor of Squad
func NewSquad(name string, edition Edition, members []Coach) (Squad, error) {
	var editionNameForMembers string
	for i, member := range members {
		if i == 0 {
			editionNameForMembers = member.Edition.Name
		} else if editionNameForMembers != member.Edition.Name {
			return Squad{}, fmt.Errorf("at %d different editions for one squad: %s vs %s", i, editionNameForMembers, member.Edition.Name)
		}
	}
	if editionNameForMembers != edition.Name {
		return Squad{}, fmt.Errorf("at the end different editions for one squad: %s vs %s", editionNameForMembers, edition.Name)
	}
	return Squad{Name: name, Edition: edition, Members: members}, nil
}
