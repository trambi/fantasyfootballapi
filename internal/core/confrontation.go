// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"errors"
	"fmt"
	"sort"
)

// Confrontation is a slice of Game, it implements IValueProvider
type Confrontation struct {
	Games  []Game
	Finale bool
}

// ValueByField allows Confrontation to implements IValueProvider.
func (c Confrontation) ValueByField(field string) (float64, error) {
	value := float64(0)
	for index, game := range c.Games {
		addedValue, err := game.ValueByField(field)
		if err != nil {
			return 0, fmt.Errorf("impossible to get field %s for game %d: %s", field, index, err.Error())
		}
		value += addedValue
	}
	return value, nil
}

type Confrontations []Confrontation

func GamesToConfrontations(games []Game, squadByCoachName map[string]Squad) []Confrontation {
	confrontations := []Confrontation{}
	confrontationByRoundAndSquadId := map[string]Confrontation{}
	for _, game := range games {
		squad1, exist1 := squadByCoachName[game.Coach1.Name]
		squad2, exist2 := squadByCoachName[game.Coach2.Name]
		if exist1 && exist2 {
			key := fmt.Sprintf("%d@%s@%s", game.Round, squad1.Name, squad2.Name)
			confrontation, exist := confrontationByRoundAndSquadId[key]
			if !exist {
				confrontation = Confrontation{}
			}
			confrontation.Games = append(confrontation.Games, game)
			confrontationByRoundAndSquadId[key] = confrontation
		}
	}
	for _, confrontation := range confrontationByRoundAndSquadId {
		for _, game := range confrontation.Games {
			if game.Finale {
				confrontation.Finale = true
				break
			}
		}
		confrontations = append(confrontations, confrontation)
	}
	sort.Sort(Confrontations(confrontations))
	return confrontations
}

// Reverse returns the slice of reversed games of the Confrontation.
func (c Confrontation) Reverse() Confrontation {
	reversed := Confrontation{Finale: c.Finale}
	for _, game := range c.Games {
		reversed.Games = append(reversed.Games, game.Reverse())
	}
	return reversed
}

// ComputePoints computes the points of the confrontation.
// The points are computed according to the rules of the edition.
func (c Confrontation) ComputePoints() (float64, float64, error) {
	if len(c.Games) == 0 {
		return 0.0, 0.0, errors.New("not enough game to compute confrontation points")
	}
	points1 := c.Games[0].Edition.ConfrontationPoints.Default
	points2 := points1
	// We iterate on index because range create a copy of the slice, we want to iterate on the original slice
	for index := range c.Games {
		err := c.Games[index].ComputeGamePoints()
		if err != nil {
			return 0.0, 0.0, fmt.Errorf("unable to compute game points for game %d: %s", index, err.Error())
		}
	}
	reversed := c.Reverse()
	for _, clause := range c.Games[0].Edition.ConfrontationPoints.Clauses {
		conditionForGame, err := clause.Condition.Value(c)
		if err != nil {
			return 0.0, 0.0, fmt.Errorf("unable to evaluate condition: %s", err.Error())
		}
		conditionForReversedGame, err := clause.Condition.Value(reversed)
		if err != nil {
			return 0.0, 0.0, fmt.Errorf("unable to evaluate condition: %s", err.Error())
		}
		if conditionForGame {
			points1 = clause.ValueIfTrue
		}
		if conditionForReversedGame {
			points2 = clause.ValueIfTrue
		}
	}

	points1Value, err := points1.Evaluate(c)
	if err != nil {
		return 0.0, 0.0, fmt.Errorf("unable to evaluate confrontation points: %s", err.Error())
	}
	points2Value, err := points2.Evaluate(reversed)
	if err != nil {
		return 0.0, 0.0, fmt.Errorf("unable to evaluate confrontation points: %s", err.Error())
	}
	return points1Value, points2Value, nil

}

func (c Confrontation) addPointsToSquadRankCouple(ranks [2]EnhancedSquadRank, rankingSettings []RankingCriteria, useFinale bool) error {
	confrontationPoints1, confrontationPoints2, err := c.ComputePoints()
	if err != nil {
		return fmt.Errorf("unable to compute confrontation points: %v", err)
	}
	offsetDueToFinale := 0
	if useFinale {
		offsetDueToFinale = 1
		if c.Finale {
			const WINNER = 2
			const RUNNER_UP = 1
			if confrontationPoints1 > confrontationPoints2 {
				ranks[0].Rank.ExtraInfos[0] = WINNER
				ranks[1].Rank.ExtraInfos[0] = RUNNER_UP
				ranks[0].OpponentInfo[0] = RUNNER_UP
				ranks[1].OpponentInfo[0] = WINNER
			} else {
				ranks[0].Rank.ExtraInfos[0] = RUNNER_UP
				ranks[1].Rank.ExtraInfos[0] = WINNER
				ranks[0].OpponentInfo[0] = WINNER
				ranks[1].OpponentInfo[0] = RUNNER_UP
			}
		}
	}
	for index, setting := range rankingSettings {
		if setting.Field == ConfrontationPoints {
			added1, added2 := 0.0, 0.0
			if setting.Type == For {
				added1 = confrontationPoints1
				added2 = confrontationPoints2
			} else if setting.Type == Against {
				added1 = confrontationPoints2
				added2 = confrontationPoints1
			} else if setting.Type == Net {
				added1 = confrontationPoints1 - confrontationPoints2
				added2 = -added1
			} else if setting.Type == OpponentExceptOwnGame {
				// This is a trick to remove Opponent ConfrontationPoints of our own game
				added1 = -confrontationPoints2
				added2 = -confrontationPoints1
			}
			ranks[0].Rank.ExtraInfos[index+offsetDueToFinale] = ranks[0].Rank.ExtraInfos[index+offsetDueToFinale] + added1
			ranks[1].Rank.ExtraInfos[index+offsetDueToFinale] = ranks[1].Rank.ExtraInfos[index+offsetDueToFinale] + added2
			if setting.Type == Opponent || setting.Type == OpponentExceptOwnGame {
				ranks[0].OpponentInfo[index+offsetDueToFinale] = ranks[0].OpponentInfo[index+offsetDueToFinale] + confrontationPoints1
				ranks[1].OpponentInfo[index+offsetDueToFinale] = ranks[1].OpponentInfo[index+offsetDueToFinale] + confrontationPoints2
			}
		}
	}
	return nil
}

// Len allows Confrontations to implement sort.Interface
func (confrontations Confrontations) Len() int { return len(confrontations) }

// Swap allows Confrontations to implement sort.Interface
func (confrontations Confrontations) Swap(i, j int) {
	confrontations[i], confrontations[j] = confrontations[j], confrontations[i]
}

// Less allows Confrontations to implement sort.Interface
func (confrontations Confrontations) Less(i, j int) bool {
	gameRef1, gameRef2 := confrontations[i].Games[0], confrontations[j].Games[0]
	if gameRef1.Round < gameRef2.Round {
		return true
	}
	if gameRef1.Round == gameRef2.Round && gameRef1.Table < gameRef2.Table {
		return true
	}
	return false
}
