// Package core FantasyFootballApi
// Copyright (C) 2019-2024 Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"errors"
	"fmt"
	"sort"
	"time"
)

// Edition implements an occurrence of a tournament
type Edition struct {
	ID            uint
	Name          string
	Day1          string
	Day2          string
	RoundNumber   uint
	CurrentRound  uint
	FirstDayRound uint
	UseFinale     bool
	// FullSquad is :
	// - true if the ranking to pair is squad main ranking
	// - false if teh ranking to pair is coach main ranking
	FullSquad bool
	// RankingStrategyName is deprecated only used for v1 api
	RankingStrategyName string
	// CoachPerSquad is the member of Coach per squad, 0 or 1 means no squad
	CoachPerSquad       uint
	AllowedFactions     []Faction
	Organizer           string
	GamePoints          PointsSettings
	ConfrontationPoints PointsSettings
	CoachRankings       map[string][]RankingCriteria
	SquadRankings       map[string][]RankingCriteria
}

type Clause struct {
	Condition   ICondition
	ValueIfTrue IValuable
}

// PointsSettings represents a setting to compute coach points from a match
type PointsSettings struct {
	Default IValuable
	Clauses []Clause
}

type RankingCriteria struct {
	Label      string
	Type       RankingType
	Field      RankingField
	Descending bool
}

// NewEdition is a constructor of Edition
func NewEdition(name string, firstDay string, roundNumber uint,
	firstDayRound uint, useFinale bool, fullSquad bool, coachPerSquad uint,
	allowedFactions []Faction, rankingStrategyName string,
	organizer string) (Edition, error) {

	factions := make([]Faction, len(allowedFactions))
	copy(factions, allowedFactions)
	t, err := time.Parse(time.RFC3339, firstDay+"T12:00:00Z")
	var secondDay string
	if err != nil {
		return Edition{}, fmt.Errorf("firstDay |%s| is not in proper format date - ISO8601", firstDay)
	}
	secondDay = t.Add(time.Hour * 24).Format(time.DateOnly)
	return Edition{
		ID:                  0,
		Name:                name,
		Day1:                firstDay,
		Day2:                secondDay,
		RoundNumber:         roundNumber,
		CurrentRound:        0,
		FirstDayRound:       firstDayRound,
		UseFinale:           useFinale,
		FullSquad:           fullSquad,
		RankingStrategyName: rankingStrategyName,
		CoachPerSquad:       coachPerSquad,
		AllowedFactions:     factions,
		Organizer:           organizer,
		CoachRankings:       map[string][]RankingCriteria{},
		SquadRankings:       map[string][]RankingCriteria{},
		GamePoints: PointsSettings{
			Default: ConstantValue{Value: 0.0},
			Clauses: []Clause{},
		},
		ConfrontationPoints: PointsSettings{
			Default: ConstantValue{Value: 0.0},
			Clauses: []Clause{},
		},
	}, nil
}

// GetCurrentEdition select the closer in time edition in a slice of editions
func GetCurrentEdition(editions []Edition) Edition {
	var currentEdition Edition
	unixRef := time.Now().Unix()
	minDelta := unixRef
	for _, edition := range editions {
		t, err := time.Parse(time.RFC3339, edition.Day1+"T12:00:00Z")
		if err == nil {

			delta := t.Unix() - unixRef
			if delta < 0 {
				delta = -delta
			}
			if delta < minDelta {
				minDelta = delta
				currentEdition = edition
			}
		}
	}
	return currentEdition
}

// AddCoachRankingMain add main ranking for the coachs
func (e *Edition) AddCoachRankingMain(rankings []RankingCriteria) {
	e.CoachRankings["main"] = rankings
}

// AddCoachTdRanking add ranking of touchdown for the coachs - the more the touchdown the better
func (e *Edition) AddCoachTdRanking() {
	tdRanking := RankingCriteria{Label: "tdFor", Type: For, Field: Td, Descending: true}
	e.CoachRankings["td"] = []RankingCriteria{tdRanking}
}

// AddCoachDefenseRanking add coach ranking of best defense - the less the touchdown against the better
func (e *Edition) AddCoachDefenseRanking() {
	ranking := RankingCriteria{Label: "tdAgainst", Type: Against, Field: Td, Descending: false}
	e.CoachRankings["defense"] = []RankingCriteria{ranking}
}

// AddCoachCasualtiesRanking add ranking of casualties for the coachs - the more the casualties the better
func (e *Edition) AddCoachCasualtiesRanking() {
	ranking := RankingCriteria{Label: "casualtiesFor", Type: For, Field: Casualties, Descending: true}
	e.CoachRankings["casualties"] = []RankingCriteria{ranking}
}

// AddCoachCompletionsRanking add ranking of completions for the coachs - the more the completions the better
func (e *Edition) AddCoachCompletionsRanking() {
	ranking := RankingCriteria{Label: "completions", Type: For, Field: Completions, Descending: true}
	e.CoachRankings["completions"] = []RankingCriteria{ranking}
}

// AddCoachFoulsRanking add ranking of fouls for the coachs - the more the fouls the better
func (e *Edition) AddCoachFoulsRanking() {
	ranking := RankingCriteria{Label: "fouls", Type: For, Field: Fouls, Descending: true}
	e.CoachRankings["fouls"] = []RankingCriteria{ranking}
}

// AddCoachComebackRanking add coach ranking for best comeback the greatest difference between day 1 and day 2 of an editionAddCoachComebackRanking
func (e *Edition) AddCoachComebackRanking() error {
	_, exists := e.CoachRankings["main"]
	if !exists {
		return errors.New("impossible to have comeback ranking without main ranking")
	}
	e.CoachRankings["comeback"] = []RankingCriteria{
		{Label: "diffRanking", Type: Day1MinusDay2, Field: Ranking, Descending: true},
		{Label: "firstDayRanking", Type: Day1, Field: Ranking, Descending: false},
		{Label: "finalRanking", Type: Day2, Field: Ranking, Descending: false},
	}
	return nil
}

// AddSquadMainRanking add main ranking for the squads
func (e *Edition) AddSquadMainRanking(rankings []RankingCriteria) {
	e.SquadRankings["main"] = rankings
}

// AddSquadTdRanking add ranking of touchdown for the squads - the more the touchdown the better
func (e *Edition) AddSquadTdRanking() {
	ranking := RankingCriteria{Label: "tdFor", Type: For, Field: Td, Descending: true}
	e.SquadRankings["td"] = []RankingCriteria{ranking}
}

// AddSquadCasualtiesRanking add ranking of casualties for the squads - the more the casualties the better
func (e *Edition) AddSquadCasualtiesRanking() {
	ranking := RankingCriteria{Label: "casualtiesFor", Type: For, Field: Casualties, Descending: true}
	e.SquadRankings["casualties"] = []RankingCriteria{ranking}
}

// AddSquadCompletionsRanking add ranking of completions for the squads - the more the completions the better
func (e *Edition) AddSquadCompletionsRanking() {
	ranking := RankingCriteria{Label: "completions", Type: For, Field: Completions, Descending: true}
	e.SquadRankings["completions"] = []RankingCriteria{ranking}
}

// AddSquadFoulsRanking add ranking of fouls for the squads - the more the fouls the better
func (e *Edition) AddSquadFoulsRanking() {
	ranking := RankingCriteria{Label: "fouls", Type: For, Field: Fouls, Descending: true}
	e.SquadRankings["fouls"] = []RankingCriteria{ranking}
}

// AddSquadRankingTdAgainst add defense ranking for the squads - the less the touchdown against the better
func (e *Edition) AddSquadDefenseRanking() {
	ranking := RankingCriteria{Label: "tdAgainst", Type: Against, Field: Td, Descending: false}
	e.SquadRankings["defense"] = []RankingCriteria{ranking}
}

// AddSquadComebackRanking add squad ranking for best comeback the greatest difference between day 1 and day 2 of an edition
func (e *Edition) AddSquadComebackRanking() error {
	_, exists := e.SquadRankings["main"]
	if !exists {
		return errors.New("impossible to have comeback ranking without main ranking")
	}
	e.SquadRankings["comeback"] = []RankingCriteria{
		{Label: "diffRanking", Type: Day1MinusDay2, Field: Ranking, Descending: true},
		{Label: "firstDayRanking", Type: Day1, Field: Ranking, Descending: false},
		{Label: "finalRanking", Type: Day2, Field: Ranking, Descending: false},
	}
	return nil
}

type EnhancedCoachRank struct {
	Rank         CoachRank
	Opponents    []Coach
	OpponentInfo []float64
}

type EnhancedSquadRank struct {
	Rank         SquadRank
	Opponents    []Squad
	OpponentInfo []float64
}

// RankCoach create a CoachRanking for the edition given a coach ranking name and a slice of game
func (e Edition) RankCoach(name string, games []Game) (CoachRanking, error) {
	rankingSettings := e.CoachRankings[name]
	useFinale := e.UseFinale && !e.FullSquad && name == "main"
	offsetDueToFinale := 0
	if useFinale {
		offsetDueToFinale = 1
	}
	result, err := NewCoachRanking(e, name, useFinale)
	if err != nil {
		return result, err
	}
	if name == "comeback" {
		result = e.rankCoachByComeback(result, games)
	} else {
		enhancedRankByName := map[string]EnhancedCoachRank{}
		for _, game := range games {
			gameForCoachs := game.GetRankingGames()
			for _, gameForCoach := range gameForCoachs {
				enhancedRank, exists := enhancedRankByName[gameForCoach.Coach.Name]
				if !exists {
					enhancedRank.Rank.Coach = gameForCoach.Coach
					enhancedRank.Rank.ExtraInfos = make([]float64, len(result.ExtraInfoNames))
					enhancedRank.OpponentInfo = make([]float64, len(result.ExtraInfoNames))
				}
				if useFinale && gameForCoach.Finale {
					const WINNER = 2
					const RUNNER_UP = 1
					if gameForCoach.ForByField[Td] > gameForCoach.AgainstByField[Td] {
						enhancedRank.Rank.ExtraInfos[0] = WINNER
						enhancedRank.OpponentInfo[0] = RUNNER_UP
					} else {
						enhancedRank.Rank.ExtraInfos[0] = RUNNER_UP
						enhancedRank.OpponentInfo[0] = WINNER
					}
				}
				enhancedRank.Opponents = append(enhancedRank.Opponents, *gameForCoach.Opponent)
				for index, setting := range rankingSettings {
					added := extractValueForRankingGame(setting, gameForCoach)
					enhancedRank.Rank.ExtraInfos[index+offsetDueToFinale] = enhancedRank.Rank.ExtraInfos[index+offsetDueToFinale] + added
					if setting.Type == Opponent || setting.Type == OpponentExceptOwnGame {
						enhancedRank.OpponentInfo[index+offsetDueToFinale] = enhancedRank.OpponentInfo[index+offsetDueToFinale] + float64(gameForCoach.ForByField[setting.Field])
					}
				}
				enhancedRankByName[gameForCoach.Coach.Name] = enhancedRank
			}
		}
		for _, enhancedRank := range enhancedRankByName {
			for index, setting := range rankingSettings {
				if setting.Type == Opponent || setting.Type == OpponentExceptOwnGame {
					for _, opponent := range enhancedRank.Opponents {
						opponent := enhancedRankByName[opponent.Name]
						enhancedRank.Rank.ExtraInfos[index+offsetDueToFinale] = enhancedRank.Rank.ExtraInfos[index+offsetDueToFinale] + opponent.OpponentInfo[index]
					}
				}
			}
			result.CoachRanks = append(result.CoachRanks, enhancedRank.Rank)
		}
	}
	sort.Sort(result)
	return result, nil
}

func extractValueForRankingGame(setting RankingCriteria, gameForCoach RankingGame) float64 {
	if setting.Type == For {
		return float64(gameForCoach.ForByField[setting.Field])
	}
	if setting.Type == Against {
		return float64(gameForCoach.AgainstByField[setting.Field])
	}
	if setting.Type == Net {
		return float64(gameForCoach.ForByField[setting.Field] - gameForCoach.AgainstByField[setting.Field])
	}
	if setting.Type == OpponentExceptOwnGame {
		// This is a trick to remove Opponent stuff in our own game
		return -float64(gameForCoach.AgainstByField[setting.Field])
	}
	return 0.0
}

// RankSquad create a SquadRanking for the edition given a squad ranking name, a slice of game and a slice of squad
func (e Edition) RankSquad(name string, games []Game, squads []Squad) (SquadRanking, error) {
	rankingSettings := e.SquadRankings[name]
	useFinale := e.UseFinale && e.FullSquad && name == "main"
	offsetDueToFinale := 0
	if useFinale {
		offsetDueToFinale = 1
	}
	result, err := NewSquadRanking(e, name, useFinale)
	if err != nil {
		return result, err
	}
	if name == "comeback" {
		result = e.rankSquadByComeback(result, games, squads)
	} else {
		squadByCoachName := squadByCoachNameFromSquads(squads)
		enhancedRankByName := map[string]EnhancedSquadRank{}
		confrontations := GamesToConfrontations(games, squadByCoachName)
		for _, confrontation := range confrontations {
			ranks, err := setSquadRankCouple(confrontation, squadByCoachName, enhancedRankByName, len(result.ExtraInfoNames))
			if err != nil {
				return result, err
			}
			err = confrontation.addPointsToSquadRankCouple(ranks, rankingSettings, useFinale)
			if err != nil {
				return result, err
			}
			ranks[0].Opponents = append(ranks[0].Opponents, *ranks[1].Rank.Squad)
			ranks[1].Opponents = append(ranks[1].Opponents, *ranks[0].Rank.Squad)
			for _, game := range confrontation.Games {
				gameForCoachs := game.GetRankingGames()
				for leftOrRight, gameForCoach := range gameForCoachs {
					for index, setting := range rankingSettings {
						ranks[leftOrRight].Rank.ExtraInfos[index+offsetDueToFinale] = ranks[leftOrRight].Rank.ExtraInfos[index+offsetDueToFinale] + extractValueForRankingGame(setting, gameForCoach)
						if setting.Type == Opponent || setting.Type == OpponentExceptOwnGame {
							ranks[leftOrRight].OpponentInfo[index+offsetDueToFinale] = ranks[leftOrRight].OpponentInfo[index+offsetDueToFinale] + float64(gameForCoach.ForByField[setting.Field])
						}
					}
				}
			}
			enhancedRankByName[ranks[0].Rank.Squad.Name] = ranks[0]
			enhancedRankByName[ranks[1].Rank.Squad.Name] = ranks[1]
			fmt.Println()
		}
		for _, enhancedRank := range enhancedRankByName {
			for index, setting := range rankingSettings {
				if setting.Type == Opponent || setting.Type == OpponentExceptOwnGame {
					for _, opponent := range enhancedRank.Opponents {
						opponent := enhancedRankByName[opponent.Name]
						enhancedRank.Rank.ExtraInfos[index+offsetDueToFinale] = enhancedRank.Rank.ExtraInfos[index+offsetDueToFinale] + opponent.OpponentInfo[index]
					}
				}
			}
			result.SquadRanks = append(result.SquadRanks, enhancedRank.Rank)
		}
	}
	sort.Sort(result)
	return result, nil
}

func setSquadRankCouple(confrontation Confrontation, squadByCoachName map[string]Squad, rankByName map[string]EnhancedSquadRank, infoLength int) ([2]EnhancedSquadRank, error) {
	ranks := [2]EnhancedSquadRank{}
	for index := range ranks {
		coachName := confrontation.Games[0].Coach1.Name
		if index == 1 {
			coachName = confrontation.Games[0].Coach2.Name
		}
		squad, exists := squadByCoachName[coachName]
		if !exists {
			return ranks, fmt.Errorf("unable to find squad for %s", coachName)
		}
		ranks[index], exists = rankByName[squad.Name]
		if !exists {
			ranks[index].Rank.Squad = &squad
			ranks[index].Rank.ExtraInfos = make([]float64, infoLength)
			ranks[index].OpponentInfo = make([]float64, infoLength)
		}
	}
	return ranks, nil
}

// squadByCoachNameFromSquads create map of string squad where the key is the coach name and the value the squad
func squadByCoachNameFromSquads(squads []Squad) map[string]Squad {
	squadByCoachName := map[string]Squad{}
	for _, squad := range squads {
		for _, coach := range squad.Members {
			squadByCoachName[coach.Name] = squad
		}
	}
	return squadByCoachName
}

type comebackCoachRank struct {
	Coach         *Coach
	FirstDayRank  int
	FinalRank     int
	Day1MinusDay2 int
}

func (e Edition) rankCoachByComeback(result CoachRanking, games []Game) CoachRanking {
	comebackRankByName := map[string]comebackCoachRank{}
	comebackCriterias := e.CoachRankings["comeback"]
	comebackRankByName = e.updateCoachComebackRankByNameWithDay1Ranking(games, comebackRankByName)
	comebackRankByName = e.updateCoachComebackRankByNameWithFinalRanking(games, comebackRankByName)
	return e.updateCoachRankingWithComebackRankByName(comebackRankByName, comebackCriterias, result)
}

func (Edition) updateCoachRankingWithComebackRankByName(comebackRankByName map[string]comebackCoachRank, comebackCriterias []RankingCriteria, result CoachRanking) CoachRanking {
	for _, comebackRank := range comebackRankByName {
		if comebackRank.Coach != nil {
			coachRank := CoachRank{Coach: comebackRank.Coach}
			coachRank.ExtraInfos = make([]float64, len(comebackCriterias))
			for index, setting := range comebackCriterias {
				if setting.Type == Day1 {
					coachRank.ExtraInfos[index] = float64(comebackRank.FirstDayRank)
				} else if setting.Type == Day2 {
					coachRank.ExtraInfos[index] = float64(comebackRank.FinalRank)
				} else {
					coachRank.ExtraInfos[index] = float64(comebackRank.Day1MinusDay2)
				}
			}
			result.CoachRanks = append(result.CoachRanks, coachRank)
		}
	}
	return result
}

func (e Edition) updateCoachComebackRankByNameWithFinalRanking(games []Game, comebackRankByName map[string]comebackCoachRank) map[string]comebackCoachRank {
	finalRanking, _ := e.RankCoach("main", games)
	for i, finalRank := range finalRanking.CoachRanks {
		comebackRank := comebackRankByName[finalRank.Coach.Name]
		comebackRank.FinalRank = i + 1
		comebackRank.Day1MinusDay2 = comebackRank.FirstDayRank - comebackRank.FinalRank
		comebackRankByName[finalRank.Coach.Name] = comebackRank
	}
	return comebackRankByName
}

func (e Edition) updateCoachComebackRankByNameWithDay1Ranking(games []Game, comebackRankByName map[string]comebackCoachRank) map[string]comebackCoachRank {
	gamesOfDay1 := []Game{}
	for _, game := range games {
		if game.Round <= e.FirstDayRound {
			gamesOfDay1 = append(gamesOfDay1, game)
		}
	}
	rankingOfDay1, _ := e.RankCoach("main", gamesOfDay1)

	for i, day1rank := range rankingOfDay1.CoachRanks {
		coachRanking := comebackCoachRank{Coach: day1rank.Coach}
		coachRanking.FirstDayRank = i + 1
		comebackRankByName[day1rank.Coach.Name] = coachRanking
	}
	return comebackRankByName
}

type comebackSquadRank struct {
	Squad         *Squad
	FirstDayRank  int
	FinalRank     int
	Day1MinusDay2 int
}

func (e Edition) rankSquadByComeback(result SquadRanking, games []Game, squads []Squad) SquadRanking {
	comebackRankByName := map[string]comebackSquadRank{}
	comebackRankByName = e.updateSquadComebackRankByNameWithDay1Ranking(games, squads, comebackRankByName)
	comebackRankByName = e.updateSquadComebackRankByNameWithFinalRanking(games, squads, comebackRankByName)
	return e.updateSquadRankingWithComebackRankByName(comebackRankByName, result)
}

func (e Edition) updateSquadRankingWithComebackRankByName(comebackRankByName map[string]comebackSquadRank, result SquadRanking) SquadRanking {
	name := "comeback"
	for _, comebackRank := range comebackRankByName {
		squadRank := SquadRank{Squad: comebackRank.Squad}
		squadRank.ExtraInfos = make([]float64, len(e.SquadRankings[name]))
		for index, setting := range e.SquadRankings[name] {
			if setting.Type == Day1 {
				squadRank.ExtraInfos[index] = float64(comebackRank.FirstDayRank)
			} else if setting.Type == Day2 {
				squadRank.ExtraInfos[index] = float64(comebackRank.FinalRank)
			} else {
				squadRank.ExtraInfos[index] = float64(comebackRank.Day1MinusDay2)
			}
		}
		result.SquadRanks = append(result.SquadRanks, squadRank)
	}
	return result
}

func (e Edition) updateSquadComebackRankByNameWithFinalRanking(games []Game, squads []Squad, comebackRankByName map[string]comebackSquadRank) map[string]comebackSquadRank {
	finalRanking, _ := e.RankSquad("main", games, squads)
	for i, finalRank := range finalRanking.SquadRanks {
		comebackRank := comebackRankByName[finalRank.Squad.Name]
		comebackRank.FinalRank = i + 1
		comebackRank.Day1MinusDay2 = comebackRank.FirstDayRank - comebackRank.FinalRank
		comebackRankByName[finalRank.Squad.Name] = comebackRank
	}
	return comebackRankByName
}

func (e Edition) updateSquadComebackRankByNameWithDay1Ranking(games []Game, squads []Squad, comebackRankByName map[string]comebackSquadRank) map[string]comebackSquadRank {
	gamesOfDay1 := []Game{}
	for _, game := range games {
		if game.Round <= e.FirstDayRound {
			gamesOfDay1 = append(gamesOfDay1, game)
		}
	}
	rankingOfDay1, _ := e.RankSquad("main", gamesOfDay1, squads)

	for i, day1rank := range rankingOfDay1.SquadRanks {
		squadRanking := comebackSquadRank{Squad: day1rank.Squad}
		squadRanking.FirstDayRank = i + 1
		comebackRankByName[day1rank.Squad.Name] = squadRanking
	}
	return comebackRankByName
}

func (edition *Edition) AddGamePointsClause(valueIfTrueExpression string, conditionExpression string) error {
	condition, err := NewCondition(conditionExpression)
	if err != nil {
		return fmt.Errorf("unable to create condition while AddGamePointsClause: %s", err.Error())
	}
	valueIfTrue, err := NewValuable(valueIfTrueExpression)
	if err != nil {
		return fmt.Errorf("unable to create valuable while AddConfrontationPointsClause: %s", err.Error())
	}
	edition.GamePoints.Clauses = append(edition.GamePoints.Clauses, Clause{ValueIfTrue: valueIfTrue, Condition: condition})
	return nil
}

func (edition *Edition) AddConfrontationPointsClause(valueIfTrueExpression string, conditionExpression string) error {
	condition, err := NewCondition(conditionExpression)
	if err != nil {
		return fmt.Errorf("unable to create condition while AddConfrontationPointsClause: %s", err.Error())
	}
	valueIfTrue, err := NewValuable(valueIfTrueExpression)
	if err != nil {
		return fmt.Errorf("unable to create valuable while AddConfrontationPointsClause: %s", err.Error())
	}
	edition.ConfrontationPoints.Clauses = append(edition.ConfrontationPoints.Clauses, Clause{ValueIfTrue: valueIfTrue, Condition: condition})
	return nil
}
