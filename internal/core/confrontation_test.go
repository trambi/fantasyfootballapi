// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestGamesToConfrontations(t *testing.T) {
	faction := Faction("faction")
	edition, err := NewEdition("myEdition", "2023-05-13", 5, 3, true, false, 2, []Faction{faction}, "Something", "someone")
	if err != nil {
		t.Fatalf("error while constructing edition")
	}
	var coachs []Coach
	squadByCoachName := map[string]Squad{}
	var games []Game
	for _, i := range []int{1, 2, 3, 4, 5, 6, 7, 8} {
		coach, err := NewCoach(fmt.Sprintf("Coach %d", i), faction, edition)
		if err != nil {
			t.Fatalf("error while constructing Coach %d", i)
		}
		coachs = append(coachs, coach)
	}
	for i, letter := range []string{"A", "B", "C", "D"} {
		squad, err := NewSquad(fmt.Sprintf("Squad %s", letter), edition, coachs[(i*2):(i*2)+2])
		if err != nil {
			t.Fatalf("error while constructing Squad %s", letter)
		}
		squadByCoachName[coachs[i*2].Name] = squad
		squadByCoachName[coachs[(i*2)+1].Name] = squad
	}
	gameFillers := []struct {
		round            uint
		table            uint
		firstCoachIndex  int
		secondCoachIndex int
	}{
		{1, 1, 0, 2},
		{1, 2, 1, 3},
		{1, 3, 4, 6},
		{1, 4, 5, 7},
		{2, 1, 0, 4},
		{2, 2, 1, 5},
		{2, 3, 2, 6},
		{2, 4, 3, 7},
	}
	for _, gameFiller := range gameFillers {
		game, err := NewGame(gameFiller.round, gameFiller.table, coachs[gameFiller.firstCoachIndex], coachs[gameFiller.secondCoachIndex])
		if err != nil {
			t.Fatalf("error while constructing game %d-%d", gameFiller.round, gameFiller.table)
		}
		games = append(games, game)
	}
	expectedConfrontations := []Confrontation{
		{Games: []Game{games[0], games[1]}},
		{Games: []Game{games[2], games[3]}},
		{Games: []Game{games[4], games[5]}},
		{Games: []Game{games[6], games[7]}},
	}

	confrontations := GamesToConfrontations(games, squadByCoachName)
	if !cmp.Equal(expectedConfrontations, confrontations) {
		t.Errorf("GamesToConfrontations returns not what it was expected: %v", cmp.Diff(expectedConfrontations, confrontations))
	}
}

func TestGamesToConfrontationsWithFinale(t *testing.T) {
	// Given
	faction := Faction("faction")
	edition, err := NewEdition("myEdition", "2023-05-13", 5, 3, true, true, 2, []Faction{faction}, "Something", "someone")
	if err != nil {
		t.Fatalf("error while constructing edition")
	}
	var coachs []Coach
	squadByCoachName := map[string]Squad{}
	var games []Game
	for _, i := range []int{1, 2, 3, 4, 5, 6, 7, 8} {
		coach, err := NewCoach(fmt.Sprintf("Coach %d", i), faction, edition)
		if err != nil {
			t.Fatalf("error while constructing Coach %d", i)
		}
		coachs = append(coachs, coach)
	}
	for i, letter := range []string{"A", "B", "C", "D"} {
		squad, err := NewSquad(fmt.Sprintf("Squad %s", letter), edition, coachs[(i*2):(i*2)+2])
		if err != nil {
			t.Fatalf("error while constructing Squad %s", letter)
		}
		squadByCoachName[coachs[i*2].Name] = squad
		squadByCoachName[coachs[(i*2)+1].Name] = squad
	}
	gameFillers := []struct {
		round            uint
		table            uint
		firstCoachIndex  int
		secondCoachIndex int
	}{
		{1, 1, 0, 2},
		{1, 2, 1, 3},
		{1, 3, 4, 6},
		{1, 4, 5, 7},
		{2, 1, 0, 4},
		{2, 2, 1, 5},
		{2, 3, 2, 6},
		{2, 4, 3, 7},
	}
	for _, gameFiller := range gameFillers {
		game, err := NewGame(gameFiller.round, gameFiller.table, coachs[gameFiller.firstCoachIndex], coachs[gameFiller.secondCoachIndex])
		if err != nil {
			t.Fatalf("error while constructing game %d-%d", gameFiller.round, gameFiller.table)
		}
		games = append(games, game)
	}
	games[4].Finale = true
	expectedConfrontations := []Confrontation{
		{Games: []Game{games[0], games[1]}},
		{Games: []Game{games[2], games[3]}},
		{Games: []Game{games[4], games[5]}, Finale: true},
		{Games: []Game{games[6], games[7]}},
	}
	// When
	confrontations := GamesToConfrontations(games, squadByCoachName)
	// Then
	if !cmp.Equal(expectedConfrontations, confrontations) {
		t.Errorf("GamesToConfrontations returns not what it was expected: %v", cmp.Diff(expectedConfrontations, confrontations))
	}
}

func TestConfrontationComputePointsClassicFootball(t *testing.T) {
	faction := Faction("faction")
	edition, err := NewEdition("myEdition", "2023-05-13", 5, 3, true, false, 3, []Faction{faction}, "something", "someone")
	if err != nil {
		t.Fatalf("error while constructing Edition")
	}
	edition.AddGamePointsClause("1", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("3", "{TdFor}>{TdAgainst}")
	edition.AddConfrontationPointsClause("1", "{PointsFor}={PointsAgainst} and {TdFor}={TdAgainst}")
	edition.AddConfrontationPointsClause("3", "{PointsFor}>{PointsAgainst}")
	edition.AddConfrontationPointsClause("3", "{TdFor}>{TdAgainst} and {PointsFor}={PointsAgainst}")

	coachs := []Coach{}
	for _, i := range []int{1, 2, 3, 4, 5, 6} {
		coach, err := NewCoach(fmt.Sprintf("Coach %d", i), faction, edition)
		if err != nil {
			t.Fatalf("error while constructing Coach %d", i)
		}
		coachs = append(coachs, coach)
	}
	testCases := []struct {
		td1             [3]int
		td2             [3]int
		expectedPoints1 float64
		expectedPoints2 float64
	}{
		{[3]int{1, 1, 1}, [3]int{0, 0, 0}, 3.0, 0.0},
		{[3]int{1, 1, 1}, [3]int{1, 0, 0}, 3.0, 0.0},
		{[3]int{1, 1, 1}, [3]int{1, 1, 0}, 3.0, 0.0},
		{[3]int{1, 1, 1}, [3]int{1, 1, 1}, 1.0, 1.0},
		{[3]int{1, 1, 1}, [3]int{1, 1, 2}, 0.0, 3.0},
		{[3]int{1, 1, 1}, [3]int{1, 2, 2}, 0.0, 3.0},
		{[3]int{1, 1, 1}, [3]int{2, 2, 2}, 0.0, 3.0},
		{[3]int{1, 1, 1}, [3]int{2, 1, 0}, 1.0, 1.0},
		{[3]int{1, 1, 0}, [3]int{0, 0, 3}, 3.0, 0.0},
	}
	for _, tc := range testCases {
		confrontation := Confrontation{}
		for i := range []int{0, 1, 2} {
			game, err := NewGame(1, uint(i+1), coachs[i], coachs[i+3])
			if err != nil {
				t.Fatalf("error while constructing game %d-%d", 1, i+1)
			}
			game.SetTd(tc.td1[i], tc.td2[i])
			confrontation.Games = append(confrontation.Games, game)
		}
		points1, points2, err := confrontation.ComputePoints()
		if err != nil {
			t.Fatalf("Unexpected error: %s", err.Error())
		}
		if !cmp.Equal(tc.expectedPoints1, points1) || !cmp.Equal(tc.expectedPoints2, points2) {
			t.Errorf("ComputePoints with td1 %v and td2 %v should return %f, %f but it returns %f,%f", tc.td1, tc.td2, tc.expectedPoints1, tc.expectedPoints2, points1, points2)
		}
	}
}

func TestConfrontationComputePointsWithVariableInPoints(t *testing.T) {
	faction := Faction("faction")
	edition, err := NewEdition("myEdition", "2023-05-13", 5, 3, true, false, 3, []Faction{faction}, "something", "someone")
	if err != nil {
		t.Fatalf("error while constructing Edition")
	}
	edition.AddGamePointsClause("100", "{TdFor}={TdAgainst}")
	edition.AddGamePointsClause("300", "{TdFor}>{TdAgainst}")
	edition.ConfrontationPoints.Default = ValuableVariable{Field: "PointsFor"}
	edition.AddConfrontationPointsClause("{PointsFor}+50", "{PointsFor}={PointsAgainst} and {TdFor}={TdAgainst}")
	edition.AddConfrontationPointsClause("{PointsFor}+150", "{PointsFor}>{PointsAgainst}")
	edition.AddConfrontationPointsClause("{PointsFor}+150", "{TdFor}>{TdAgainst} and {PointsFor}={PointsAgainst}")

	coachs := []Coach{}
	for _, i := range []int{1, 2, 3, 4, 5, 6} {
		coach, err := NewCoach(fmt.Sprintf("Coach %d", i), faction, edition)
		if err != nil {
			t.Fatalf("error while constructing Coach %d", i)
		}
		coachs = append(coachs, coach)
	}
	testCases := []struct {
		td1             [3]int
		td2             [3]int
		expectedPoints1 float64
		expectedPoints2 float64
	}{
		{[3]int{1, 1, 1}, [3]int{0, 0, 0}, 1050, 0},
		{[3]int{1, 1, 1}, [3]int{1, 0, 0}, 850, 100},
		{[3]int{1, 1, 1}, [3]int{1, 1, 0}, 650, 200},
		{[3]int{1, 1, 1}, [3]int{1, 1, 1}, 350, 350},
		{[3]int{1, 1, 1}, [3]int{1, 1, 2}, 200, 650},
		{[3]int{1, 1, 1}, [3]int{1, 2, 2}, 100, 850},
		{[3]int{1, 1, 1}, [3]int{2, 2, 2}, 0, 1050},
		{[3]int{1, 1, 1}, [3]int{2, 1, 0}, 450, 450},
	}
	for _, tc := range testCases {
		confrontation := Confrontation{}
		for i := range []int{0, 1, 2} {
			game, err := NewGame(1, uint(i+1), coachs[i], coachs[i+3])
			if err != nil {
				t.Fatalf("error while constructing game %d-%d", 1, i+1)
			}
			game.SetTd(tc.td1[i], tc.td2[i])
			confrontation.Games = append(confrontation.Games, game)
		}
		points1, points2, err := confrontation.ComputePoints()
		if err != nil {
			t.Fatalf("Unexpected error: %s", err.Error())
		}
		if !cmp.Equal(tc.expectedPoints1, points1) || !cmp.Equal(tc.expectedPoints2, points2) {
			t.Errorf("ComputePoints with td1 %v and td2 %v should return %f, %f but it returns %f,%f", tc.td1, tc.td2, tc.expectedPoints1, tc.expectedPoints2, points1, points2)
		}
	}
}

func TestConfrontationImplementIValueProvider(t *testing.T) {
	// Given
	coach1, coach2 := createTwoCoachs(t)
	game1, err := NewGame(1, 1, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err.Error())
	}
	game1.SetTd(2, 7)
	game1.SetCasualties(3, 8)
	game1.SetCompletions(4, 9)
	game1.SetFouls(5, 10)
	game1.Points1 = 6
	game1.Points2 = 11
	game2, err := NewGame(12, 12, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err.Error())
	}
	game2.SetTd(13, 18)
	game2.SetCasualties(14, 19)
	game2.SetCompletions(15, 20)
	game2.SetFouls(16, 21)
	game2.Points1 = 17
	game2.Points2 = 22
	confrontation := Confrontation{Games: []Game{game1, game2}}
	testCases := []struct {
		field         string
		expectedValue float64
	}{
		{"TdFor", 15},
		{"CasualtiesFor", 17},
		{"CompletionsFor", 19},
		{"FoulsFor", 21},
		{"PointsFor", 23},
		{"TdAgainst", 25},
		{"CasualtiesAgainst", 27},
		{"CompletionsAgainst", 29},
		{"FoulsAgainst", 31},
		{"PointsAgainst", 33},
	}
	for _, tc := range testCases {
		// When
		result, err := confrontation.ValueByField(tc.field)
		if err != nil {
			t.Fatalf("Unexpected error: %s", err.Error())
		}
		// Then
		if tc.expectedValue != result {
			t.Fatalf("Expect %f get %f", tc.expectedValue, result)
		}
	}
}

func TestConfrontationImplementIValueProviderWithUnknownField(t *testing.T) {
	// Given
	coach1, coach2 := createTwoCoachs(t)
	game1, err := NewGame(1, 1, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err.Error())
	}
	game1.SetTd(2, 7)
	game1.SetCasualties(3, 8)
	game1.SetCompletions(4, 9)
	game1.SetFouls(5, 10)
	game1.Points1 = 6
	game1.Points2 = 11
	game2, err := NewGame(12, 12, coach1, coach2)
	if err != nil {
		t.Fatalf("Unexpected error: %s", err.Error())
	}
	game2.SetTd(13, 18)
	game2.SetCasualties(14, 19)
	game2.SetCompletions(15, 20)
	game2.SetFouls(16, 21)
	game2.Points1 = 17
	game2.Points2 = 22
	confrontation := Confrontation{Games: []Game{game1, game2}}
	invalidFields := []string{
		"ConfrontationPointsFor",
		"ConfrontationPointsAgainst",
		"UnknownFor",
		"UnknownAgainst",
	}
	for _, field := range invalidFields {
		expectedError := fmt.Errorf("impossible to get field %s for game 0: unknown field %s for game", field, field)
		// When
		result, err := confrontation.ValueByField(field)

		if err.Error() != expectedError.Error() {
			t.Fatalf("Expect error :%s get error: %s", expectedError.Error(), err.Error())
		}
		// Then
		if result != 0 {
			t.Errorf("Expect 0 get %f", result)
		}
	}
}
