// Package core FantasyFootballApi
// Copyright (C) 2019-2024 Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
//
// File based on the series of article of Ruslan Pivak Blog https://ruslanspivak.com
package core

import (
	"errors"
	"fmt"
	"unicode"
)

// TokenType is a enum to represent the possible token type
type TokenType int

const (
	// NUMBER is the token type for integer
	NUMBER TokenType = iota
	// PLUS is the token type for the plus symbol '+' used for addition
	PLUS
	// MINUS is the token type for the minus symbol '-' for subtraction
	MINUS
	// MUL is the token type for the star symbol '*' for multiplication
	MUL
	// LPAREN is the token type for the left parenthesis symbol (
	LPAREN
	// RPAREN is the token type for the right parenthesis symbol (
	RPAREN
	// LCBRACKET is the token tyke for the left curly bracket symbol {
	LCBRACKET
	// RCBRACKET is the token type for the right curly bracket symbol }
	RCBRACKET
	// VARIABLE is the token type for variable
	VARIABLE
	// EOF is the token that indicated there is no more symbol for tokenizer
	EOF
)

func (tt TokenType) String() string {
	return [...]string{
		"NUMBER",
		"PLUS",
		"MINUS",
		"MULTIPLICITY",
		"LPAREN",
		"RPAREN",
		"LCBRACKET",
		"RCBRACKET",
		"VARIABLE",
		"EOF"}[tt]
}

// Token describe a basic component of the string to evaluate
type Token struct {
	tokenType TokenType
	value     string
}

func (token Token) String() string {
	return fmt.Sprintf("Token({%v},{%v}", token.tokenType, token.value)
}

// Tokenizer cut the string Text into token
type Tokenizer struct {
	Text         string
	Pos          int
	CurrentToken Token
	CurrentByte  byte
}

// NewTokenizer create a Tokenizer from input
func NewTokenizer(input string) (Tokenizer, error) {
	if len(input) > 0 {
		return Tokenizer{input, 0, Token{EOF, "EOF"}, input[0]}, nil
	}
	return Tokenizer{}, errors.New("unable to create an tokenizer from empty input")

}

func (tokenizer *Tokenizer) advance() {
	tokenizer.Pos++
	if tokenizer.Pos >= len(tokenizer.Text) {
		tokenizer.CurrentByte = 0
	} else {
		tokenizer.CurrentByte = tokenizer.Text[tokenizer.Pos]
	}
}

func (tokenizer *Tokenizer) skipWhiteSpace() {
	for ; tokenizer.CurrentByte != 0 && unicode.IsSpace(rune(tokenizer.CurrentByte)); tokenizer.advance() {
	}
}

func byteValidForDecimal(input byte) bool {
	if input == 0 {
		return false
	}
	return unicode.IsDigit(rune(input)) || input == '.'
}

func (tokenizer *Tokenizer) decimal() string {
	result := []byte{}
	for ; byteValidForDecimal(tokenizer.CurrentByte); tokenizer.advance() {
		result = append(result, tokenizer.CurrentByte)
	}
	return string(result)
}

func isVariable(byteToCheck byte) bool {
	runeToCheck := rune(byteToCheck)
	return byteToCheck != 0 && (byteToCheck == '_' || unicode.IsDigit(runeToCheck) || unicode.IsLetter(runeToCheck))
}

func (tokenizer *Tokenizer) variable() string {
	result := []byte{}
	for ; isVariable(tokenizer.CurrentByte); tokenizer.advance() {
		result = append(result, tokenizer.CurrentByte)
	}
	return string(result)
}

func (tokenizer *Tokenizer) nextToken() (Token, error) {
	for tokenizer.CurrentByte != 0 {
		if unicode.IsSpace(rune(tokenizer.CurrentByte)) {
			tokenizer.skipWhiteSpace()
			continue
		}
		if unicode.IsDigit(rune(tokenizer.CurrentByte)) {
			return Token{NUMBER, tokenizer.decimal()}, nil
		}
		if unicode.IsLetter(rune(tokenizer.CurrentByte)) {
			return Token{VARIABLE, tokenizer.variable()}, nil
		}
		if tokenizer.CurrentByte == '+' {
			tokenizer.advance()
			return Token{PLUS, "+"}, nil
		}
		if tokenizer.CurrentByte == '-' {
			tokenizer.advance()
			return Token{MINUS, "-"}, nil
		}
		if tokenizer.CurrentByte == '*' {
			tokenizer.advance()
			return Token{MUL, "*"}, nil
		}
		if tokenizer.CurrentByte == '(' {
			tokenizer.advance()
			return Token{LPAREN, "("}, nil
		}
		if tokenizer.CurrentByte == ')' {
			tokenizer.advance()
			return Token{RPAREN, ")"}, nil
		}
		if tokenizer.CurrentByte == '{' {
			tokenizer.advance()
			return Token{LCBRACKET, "{"}, nil
		}
		if tokenizer.CurrentByte == '}' {
			tokenizer.advance()
			return Token{RCBRACKET, "}"}, nil
		}
		return Token{}, errors.New("unknown token")
	}
	return Token{EOF, "EOF"}, nil
}
