// Package core FantasyFootballApi
// Copyright (C) 2019-2024 Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
//
// File based on the series of article of Ruslan Pivak Blog https://ruslanspivak.com
package core

import (
	"errors"
	"fmt"
	"strconv"
)

// Parser transform tokens to tree of IValuable
type Parser struct {
	tokenizer    *Tokenizer
	CurrentToken Token
}

// NewParser create an parser from an input string
func NewParser(input string) (Parser, error) {
	tokenizer, err := NewTokenizer(input)
	if err == nil {
		return Parser{tokenizer: &tokenizer}, nil
	}
	return Parser{}, errors.New("unable to create an tokenizer for the parser")
}

func (parser *Parser) eat(tokenType TokenType) error {
	if parser.CurrentToken.tokenType == tokenType {
		var err error
		parser.CurrentToken, err = parser.tokenizer.nextToken()
		return err
	}
	return fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType)
}

// term return an NUMBER token value
func (parser *Parser) term() (IValuable, error) {
	// term : factor (MUL factor)*
	node, err := parser.factor()
	if err != nil {
		return ValuableError{}, fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType.String())
	}
	for parser.CurrentToken.tokenType == MUL {
		op := parser.CurrentToken
		if op.tokenType == MUL {
			parser.eat(MUL)
			//var factor IValuable
			//factor, err = parser.factor()
			if err != nil {
				return ValuableError{}, fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType.String())
			}
			//node = BinOpNode{op, node, factor}
			node = ValuableError{}
		} else {
			return ValuableError{}, fmt.Errorf("invalid operator! - Token type: %v", op.tokenType.String())
		}
	}
	return node, nil
}

// factor returns an NUMBER token or expr token
func (parser *Parser) factor() (IValuable, error) {
	// factor: NUMBER | LPAREN expr RPAREN | LCBRACKET VARIABLE RCBRACKET
	token := parser.CurrentToken
	if token.tokenType == NUMBER {
		err := parser.eat(NUMBER)
		if err != nil {
			return ValuableError{}, err
		}
		value, err := strconv.ParseFloat(token.value, 64)
		return ConstantValue{Value: value}, err
	} else if token.tokenType == LPAREN {
		err := parser.eat(LPAREN)
		if err != nil {
			return ValuableError{}, err
		}
		node, err := parser.expr()
		if err != nil {
			return ValuableError{}, err
		}
		err = parser.eat(RPAREN)
		if err != nil {
			return ValuableError{}, err
		}
		return node, nil
	} else if token.tokenType == LCBRACKET {
		err := parser.eat(LCBRACKET)
		if err != nil {
			return ValuableError{}, err
		}
		token = parser.CurrentToken
		value := token.value
		err = parser.eat(VARIABLE)
		if err != nil {
			return ValuableError{}, err
		}
		err = parser.eat(RCBRACKET)
		if err != nil {
			return ValuableError{}, err
		}
		return ValuableVariable{Field: value}, nil
	}
	return ValuableError{}, fmt.Errorf("unknown token for a term: %v", token)
}

func (parser *Parser) expr() (IValuable, error) {
	// expr: term ((PLUS|MINUS) term)*
	// term: factor(MUL factor)*
	// factor: NUMBER | LPAREN expr RPAREN
	var err error
	parser.CurrentToken, err = parser.tokenizer.nextToken()
	if err != nil {
		return ValuableError{}, err
	}
	node, err := parser.term()
	if err != nil {
		return ValuableError{}, fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType.String())
	}
	for parser.CurrentToken.tokenType == PLUS || parser.CurrentToken.tokenType == MINUS {
		op := parser.CurrentToken
		if op.tokenType == PLUS {
			err = parser.eat(PLUS)
			if err != nil {
				return ValuableError{}, fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType.String())
			}

			// } else if op.tokenType == MINUS {
			// 	err = parser.eat(MINUS)
			// 	if err != nil {
			// 		return ErrorValue{}, fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType.String())
			// 	}
		} else {
			return ValuableError{}, fmt.Errorf("invalid operator! - Token type: %v", op.tokenType.String())
		}
		//return ErrorValue{}, fmt.Errorf("invalid operator! - Token type: %v", op.tokenType.String())
		var term IValuable
		term, err = parser.term()
		if err != nil {
			return ValuableError{}, fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType.String())
		}
		node = ValuableSum{Left: node, Right: term}
	}
	return node, nil
}
