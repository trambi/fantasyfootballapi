// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"fmt"
)

type RankingField int

const (
	UndefinedDomain RankingField = iota
	Points
	Td
	Casualties
	Completions
	Fouls
	Comeback
	Special
	Ranking
	ConfrontationPoints
)

func (s RankingField) String() string {
	switch s {
	case Points:
		return "Points"
	case Td:
		return "Td"
	case Casualties:
		return "Casualties"
	case Completions:
		return "Completions"
	case Fouls:
		return "Fouls"
	case Comeback:
		return "Comeback"
	case Special:
		return "Special"
	case Ranking:
		return "Ranking"
	case ConfrontationPoints:
		return "ConfrontationPoints"
	}
	return "unknown ranking field"
}

func NewRankingField(value string) (RankingField, error) {
	switch value {
	case "Points":
		return Points, nil
	case "Td":
		return Td, nil
	case "Casualties":
		return Casualties, nil
	case "Completions":
		return Completions, nil
	case "Fouls":
		return Fouls, nil
	case "Comeback":
		return Comeback, nil
	case "Special":
		return Special, nil
	case "Ranking":
		return Ranking, nil
	case "ConfrontationPoints":
		return ConfrontationPoints, nil
	}
	return UndefinedDomain, fmt.Errorf("unknown ranking field :%s", value)
}
