// Package core FantasyFootballApi
// Copyright (C) 2019-2024  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package core

import (
	"testing"
)

func TestRankingFieldToString(t *testing.T) {
	testCases := []struct {
		domain   RankingField
		expected string
	}{
		{Points, "Points"},
		{Td, "Td"},
		{Casualties, "Casualties"},
		{Completions, "Completions"},
		{Fouls, "Fouls"},
		{Comeback, "Comeback"},
		{Special, "Special"},
		{Ranking, "Ranking"},
		{ConfrontationPoints, "ConfrontationPoints"},
		{ConfrontationPoints + 1, "unknown ranking field"},
	}
	for _, tc := range testCases {
		result := tc.domain.String()
		if result != tc.expected {
			t.Errorf("string(%v) = %s; want %s", tc.domain, result, tc.expected)
		}
	}
}

func TestRankingFieldToStringThenConstructorIsIdentity(t *testing.T) {
	testCases := []struct {
		input RankingField
	}{
		{Points},
		{Td},
		{Casualties},
		{Completions},
		{Fouls},
		{Comeback},
		{Special},
		{Ranking},
		{ConfrontationPoints},
	}
	for _, tc := range testCases {
		result, err := NewRankingField(tc.input.String())
		if err != nil {
			t.Fatalf("Unexpected error with %s", tc.input)
		}
		if result != tc.input {
			t.Errorf("NewRankingField(%d.string()) return %s", tc.input, result)
		}
	}
}
