// Package auth defines authentication structures for FantasyFootballApi
// Copyright (C) 2019-2020  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package auth

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

func getJwtLifetime() time.Duration {
	const jwTLifetimeinMinute time.Duration = 30
	return jwTLifetimeinMinute * time.Minute
}

func getJwtKey() []byte {
	key, _ := ioutil.ReadFile("jwt_secret_key")
	return key
}

func checkUserPassword(user string, password string) bool {
	var printedPassword = strings.Repeat("*", len(password))
	log.Printf("checkUserPassword(%v,%v)", user, printedPassword)
	checked := false
	var content []byte
	var err error
	content, err = ioutil.ReadFile("users.json")
	if err != nil {
		log.Fatal(err)
	}
	var objmap map[string]*json.RawMessage
	err = json.Unmarshal(content, &objmap)
	if err != nil {
		log.Fatal(err)
	}

	usercontent, userexist := objmap[user]
	if userexist {
		var str string
		err = json.Unmarshal(*usercontent, &str)
		if err == nil {
			var saltedPassword = sha1.Sum([]byte(password + "anothersalt"))
			log.Printf("saltedPassword(%v)", hex.EncodeToString(saltedPassword[:]))
			if hex.EncodeToString(saltedPassword[:]) == strings.Trim(str, "\n") {
				checked = true
			}
		}
	} else {
		log.Printf("user:%s does not exist", user)
	}
	log.Printf("checkUserPassword(%v,%v) return %t", user, printedPassword, checked)
	return checked
}

// Create a struct to read the username and password from the request body
type Credentials struct {
	Password string `json:"password"`
	Username string `json:"username"`
}

// Create a struct that will be encoded to a JWT.
// We add jwt.StandardClaims as an embedded type, to provide fields like expiry time
type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

// Create the Signin handler
func signinHandler(w http.ResponseWriter, r *http.Request) {
	var creds Credentials
	// Get the JSON body and decode into credentials
	err := json.NewDecoder(r.Body).Decode(&creds)
	if err != nil {
		// If the structure of the body is wrong, return an HTTP error
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// If a password exists for the given user
	// AND, if it is the same as the password we received, the we can move ahead
	// if NOT, then we return an "Unauthorized" status
	if checkUserPassword(creds.Username, creds.Password) == false {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// Declare the expiration time of the token
	// here, we have kept it as 5 minutes
	expirationTime := time.Now().Add(getJwtLifetime())
	// Create the JWT claims, which includes the username and expiry time
	claims := &Claims{
		Username: creds.Username,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}

	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString(getJwtKey())
	if err != nil {
		// If there is an error in creating the JWT return an internal server error
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Finally, we set the client cookie for "token" as the JWT we just generated
	// we also set an expiry time which is the same as the token itself
	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   tokenString,
		Expires: expirationTime,
	})
}

func checkJwt(r *http.Request) bool {
	checked := false
	log.Printf("checkJwt()")

	// We can obtain the session token from the requests cookies, which come with every request
	c, err := r.Cookie("token")
	if err == nil {
		// Get the JWT string from the cookie
		tknStr := c.Value

		// Initialize a new instance of `Claims`
		claims := &Claims{}

		// Parse the JWT string and store the result in `claims`.
		// Note that we are passing the key in this method as well. This method will return an error
		// if the token is invalid (if it has expired according to the expiry time we set on sign in),
		// or if the signature does not match
		tkn, _ := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
			return getJwtKey(), nil
		})
		if tkn.Valid {
			checked = true
		}
	}
	log.Printf("checkJwt() return %t", checked)
	return checked
}

func Refresh(w http.ResponseWriter, r *http.Request) {
	if checkJwt(r) == false {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	claims := &Claims{}
	// The code up-till this point is the same as the first part of the `Welcome` route

	// We ensure that a new token is not issued until enough time has elapsed
	// In this case, a new token will only be issued if the old token is within
	// 30 seconds of expiry. Otherwise, return a bad request status
	if time.Unix(claims.ExpiresAt, 0).Sub(time.Now()) > 30*time.Second {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Now, create a new token for the current use, with a renewed expiration time
	expirationTime := time.Now().Add(getJwtLifetime())
	claims.ExpiresAt = expirationTime.Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(getJwtKey())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Set the new token as the users `token` cookie
	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   tokenString,
		Expires: expirationTime,
	})
}
