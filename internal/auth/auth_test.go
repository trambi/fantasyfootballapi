// Package auth defines authentication structures for FantasyFootballApi
// Copyright (C) 2019-2020  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package auth

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestGetJwtKey(t *testing.T) {
	needToMove := true
	want := []byte("test")
	err := os.Rename("jwt_secret_key", "jwt_secret_key.not4test")
	if err != nil {
		needToMove = false
	}
	err = ioutil.WriteFile("jwt_secret_key", want, 0644)
	if got := getJwtKey(); string(got) != string(want) {
		t.Errorf("getJwtKey() = %q, want %q", got, want)
	}
	os.Remove("jwt_secret_key")
	if needToMove {
		os.Rename("jwt_secret_key.not4test", "jwt_secret_key")
	}
}

func TestCheckUserPassword(t *testing.T) {
	const filename string = "users.json"
	const suffix string = "not4test"
	needToMove := true
	want := []byte("{\"user1\":\"0e8251b0e64c9eaaa7cc5507a50428e348987614\",\"user2\":\"1166726892dc4fc0d2de237888239505eecee041\"}")
	err := os.Rename(filename, filename+suffix)
	if err != nil {
		needToMove = false
	}
	err = ioutil.WriteFile(filename, want, 0644)
	if err != nil {
		t.Errorf("unable to write users.json")
	}
	if checkUserPassword("user1", "password1") == false {
		t.Error("checkUserPassword(user1,password1) false")
	}
	if checkUserPassword("user2", "password3") {
		t.Error("checkUserPassword(user2,password3) true")
	}
	if checkUserPassword("user3", "password3") {
		t.Error("checkUserPassword(user3,password3) true")
	}
	os.Remove(filename)
	if needToMove {
		os.Rename(filename+suffix, filename)
	}
}
