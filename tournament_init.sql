DROP TABLE IF EXISTS squad;
CREATE TABLE squad (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL
);

DROP TABLE IF EXISTS edition;
CREATE TABLE edition (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  day_1 TEXT NOT NULL,
  day_2 TEXT NOT NULL,
  round_number INTEGER NOT NULL,
  current_round INTEGER NOT NULL,
  use_finale INTEGER NOT NULL DEFAULT 1,
  full_triplette INTEGER NOT NULL DEFAULT 1,
  ranking_strategy TEXT NOT NULL,
  first_day_round INTEGER NOT NULL DEFAULT 3,
  allowed_factions TEXT DEFAULT '[]',
  organizer TEXT DEFAULT NULL
);

DROP TABLE IF EXISTS coach;
CREATE TABLE coach (
  id INTEGER PRIMARY KEY,
  team_name TEXT NOT NULL,
  name TEXT NOT NULL,
  id_faction INTEGER DEFAULT NULL,
  email TEXT NOT NULL,
  points INTEGER NOT NULL,
  opponents_points INTEGER NOT NULL,
  net_td INTEGER NOT NULL,
  casualties INTEGER NOT NULL,
  edition INTEGER NOT NULL,
  naf_number INTEGER NOT NULL,
  id_squad INTEGER DEFAULT NULL,
  ready INTEGER NOT NULL,
  FOREIGN KEY(edition) REFERENCES edition(id),
  FOREIGN KEY(id_squad) REFERENCES squad(id),
  FOREIGN KEY(id_faction) REFERENCES faction(id)
);

--
-- Table structure for table edition
--

INSERT INTO edition VALUES (18,'2022-05-08 09:00:00.000','2022-05-09  09:00:00.000',5,0,1,1,'Rdvbb18',3,'Sebotouno');

--
-- Table structure for table game
--

DROP TABLE IF EXISTS game;
CREATE TABLE game (
  id INTEGER PRIMARY KEY,
  id_coach_1 INTEGER DEFAULT NULL,
  id_coach_2 INTEGER DEFAULT NULL,
  td_1 INTEGER NOT NULL,
  td_2 INTEGER NOT NULL,
  round INTEGER NOT NULL,
  points_1 INTEGER DEFAULT NULL,
  points_2 INTEGER DEFAULT NULL,
  casualties_1 INTEGER DEFAULT NULL,
  casualties_2 INTEGER DEFAULT NULL,
  completions_1 INTEGER DEFAULT NULL,
  completions_2 INTEGER DEFAULT NULL,
  fouls_1 INTEGER DEFAULT NULL,
  fouls_2 INTEGER DEFAULT NULL,
  special_1 TEXT DEFAULT NULL,
  special_2 TEXT DEFAULT NULL,
  finale INTEGER NOT NULL,
  edition INTEGER NOT NULL,
  status TEXT NOT NULL,
  table_number INTEGER NOT NULL,
  FOREIGN KEY(id_coach_1) REFERENCES coach(id),
  FOREIGN KEY(id_coach_2) REFERENCES coach(id)
);


