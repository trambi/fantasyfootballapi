// FantasyFootballApi
// Copyright (C) 2019-2020  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package main

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"testing"

	"gitlab.com/trambi/fantasyfootballapi/internal/api"
)

var a api.API

func TestMain(m *testing.M) {
	dir, err := os.MkdirTemp("", "test_")
	if err != nil {
		log.Fatal(err)
	}
	dbPath := path.Join(dir, "api.db")
	os.Setenv("STORAGE_PATH", dbPath)
	//defer os.RemoveAll(dir) // clean up
	a = api.API{}
	a.Initialize()

	code := m.Run()

	os.Exit(code)
}

func TestGetIndexAnswerSomethingInJson(t *testing.T) {
	// Given
	receivedMap := map[string]interface{}{}
	expectedStatus := http.StatusOK
	// When
	req, _ := http.NewRequest("GET", "/", nil)
	response := executeRequest(req)
	// Then
	if http.StatusOK != response.Code {
		t.Errorf("For GET on / expected response code %d. Got %d\n", expectedStatus, response.Code)
	}
	if response.Body.String() == "" {
		t.Error("Unexpected an empty string.")
	}
	encoded := response.Body.Bytes()
	err := json.Unmarshal(encoded, &receivedMap)
	if err != nil {
		t.Fatal("Unexpected error while parsing body: ", err.Error())
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}
