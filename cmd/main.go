// FantasyFootballApi
// Copyright (C) 2019-2020  Bertrand Madet
// Licensed under the Apache License, Version 2.0 (the "License");
//
//	you may not use this file except in compliance with the License.
//	You may obtain a copy of the License at
//
//	    http://www.apache.org/licenses/LICENSE-2.0
//
//	Unless required by applicable law or agreed to in writing, software
//	distributed under the License is distributed on an "AS IS" BASIS,
//	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//	See the License for the specific language governing permissions and
//	limitations under the License.
package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/trambi/fantasyfootballapi/internal/api"
	"gitlab.com/trambi/fantasyfootballapi/internal/appinfo"
)

// our main function
func main() {
	port := 8000
	api := api.API{}
	fmt.Println(appinfo.GetAppInfo())
	api.Initialize()
	listeningAddress := fmt.Sprintf(":%d", port)
	fmt.Printf("will listen on %s\n", listeningAddress)
	err := http.ListenAndServe(listeningAddress, api.Router)
	if err != nil {
		log.Fatalf("server failed to start: %v", err)
	}

}
