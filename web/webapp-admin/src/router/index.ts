import { createRouter, createWebHistory } from 'vue-router'
import MainPage from '@/views/MainPage.vue';
import ViewCoach from '@/views/ViewCoach.vue';
import ViewSquad from '@/views/ViewSquad.vue';
import AddCoach from '@/views/AddCoach.vue';
import EditCoach from '@/views/EditCoach.vue';
import AddSquad from '@/views/AddSquad.vue';
import AddGame from '@/views/AddGame.vue';
import EditSquad from '@/views/EditSquad.vue';
import ProposeGames from '@/views/ProposeGames.vue';
import EditGame from '@/views/EditGame.vue';
import LoadSquad from '@/views/LoadSquad.vue';
import ViewCoachRanking from '@/views/ViewCoachRanking.vue';
import ViewSquadRanking from '@/views/ViewSquadRanking.vue';
import EditEdition from '@/views/EditEdition.vue';
import AddEdition from '@/views/AddEdition.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: MainPage
    },
    {
      path: '/main/:id/:round',
      name: 'main',
      component: MainPage
    },
    {
      path: '/games/propose',
      name: 'propose-games',
      component: ProposeGames
    },
    {
      path: '/coachs/view/:id',
      name: 'coach-view',
      component: ViewCoach
    },
    {
      path: '/coachs/edit/:id',
      name: 'coach-edit',
      component: EditCoach
    },
    {
      path: '/editions/:id/coachs/add',
      name: 'coach-add',
      component: AddCoach
    },
    {
      path: '/editions/:id/coachs/rankings/:rankingName',
      name: 'coach-ranking',
      component:ViewCoachRanking
    },
    {
      path: '/squads/view/:id',
      name: 'squad-view',
      component: ViewSquad
    },
    {
      path: '/squads/edit/:id',
      name: 'squad-edit',
      component: EditSquad
    },
    {
      path: '/editions/:id/squads/add',
      name: 'squad-add',
      component: AddSquad
    },
    {
      path: '/editions/:id/squads/load',
      name: 'squad-load',
      component: LoadSquad
    },
    {
      path: '/editions/:id/squads/rankings/:rankingName',
      name: 'squad-ranking',
      component:ViewSquadRanking
    },
    {
      path: '/editions/:id/games/add',
      name: 'game-add',
      component: AddGame
    },
    {
      path: '/games/resume/:id',
      name: 'game-resume',
      component: EditGame
    },
    {
      path: '/editions/:id/edit',
      name: 'edition-edit',
      component: EditEdition
    },
    {
      path: '/editions/add',
      name: 'edition-add',
      component: AddEdition
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router
