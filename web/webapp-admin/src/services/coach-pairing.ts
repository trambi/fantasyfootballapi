import type { ToBeCreatedGame } from "./game";
import { randomPair, swissPair } from "./pair";

interface PairableCoach {
    editionId: string;
    id: string;
    name: string;
    squadId?: string | undefined;
    squadName?: string | undefined;
}

export function pairByCoachRandomly(coachs: PairableCoach[], round: number, startingTable: number, forbidden: Map<string, string[]>): ToBeCreatedGame[] {
    const alreadyPairedId: string[][] = []
    const toBePairedId = coachs.map(coach => coach.id);
    const pairedId = randomPair(alreadyPairedId, toBePairedId, forbidden);
    if (pairedId === null) {
        console.error("Error while pairing coachs randomly");
        return [] as ToBeCreatedGame[];
    };
    return pairedIdsToGames(pairedId, coachs, round, startingTable);
}

export function pairByCoachOrderly(coachs: PairableCoach[], round: number, startingTable: number, forbidden: Map<string, string[]>): ToBeCreatedGame[] {
    const alreadyPairedId: string[][] = []
    const toBePairedId = coachs.map(coach => coach.id);
    const pairedId = swissPair(alreadyPairedId, toBePairedId, forbidden);
    if (pairedId === null) {
        console.error("Error while pairing coachs by ranking");
        return [] as ToBeCreatedGame[];
    };
    return pairedIdsToGames(pairedId, coachs, round, startingTable);
}

function pairedIdsToGames(pairedId: string[][], coachs: PairableCoach[], round: number, startingTable: number,): ToBeCreatedGame[] {
    let table = startingTable;
    const games: ToBeCreatedGame[] = [];
    for (const pairOfId of pairedId) {
        const coach1 = coachs.find(coach => coach.id === pairOfId[0]);
        const coach2 = coachs.find(coach => coach.id === pairOfId[1]);
        if (coach1 && coach2) {
            games.push({
                editionId: coach1.editionId,
                round,
                finale: false,
                table,
                coachId1: coach1.id,
                coachId2: coach2.id,
                coachName1: coach1.name,
                coachName2: coach2.name,
                squadId1: coach1.squadId ?? '',
                squadId2: coach2.squadId ?? '',
                squadName1: coach1.squadName || '',
                squadName2: coach2.squadName || '',
            });
            table = table + 1;
        }
    }
    return games;
}