import { z } from "zod";

const rankingFieldValidator = z.object({
    label: z.string(),
    field: z.string(),
    type: z.string(),
    descending: z.boolean()
});

export const rankingConfigurationValidator = z.object({
    coach: z.object({
        main: z.array(rankingFieldValidator).optional(),
        comeback: z.array(rankingFieldValidator).optional(),
        td: z.array(rankingFieldValidator).optional(),
        casualties: z.array(rankingFieldValidator).optional(),
        fouls: z.array(rankingFieldValidator).optional(),
        defense: z.array(rankingFieldValidator).optional(),
        completions: z.array(rankingFieldValidator).optional(),
    }).optional(),
    squad: z.object({
        main: z.array(rankingFieldValidator).optional(),
        comeback: z.array(rankingFieldValidator).optional(),
        td: z.array(rankingFieldValidator).optional(),
        casualties: z.array(rankingFieldValidator).optional(),
        fouls: z.array(rankingFieldValidator).optional(),
        defense: z.array(rankingFieldValidator).optional(),
        completions: z.array(rankingFieldValidator).optional(),
    }).optional(),
});

export type RankingField = z.infer<typeof rankingFieldValidator>;
export type RankingConfiguration = z.infer<typeof rankingConfigurationValidator>;

export type RankingType = 'coach'|'squad';
export type RankingName = 'main' | 'comeback'|'td'|'casualties'|'fouls'|'defense'|'completions';

export const possibleRankingFields = () => [
    "Points",
    "Td",
    "Casualties",
    "Completions",
    "Fouls",
    "Comeback",
    "Special",
    "Ranking",
    "ConfrontationPoints",
  ]
  
  export const possibleRankingTypes = () => [
    "For",
    "Against",
    "Net",
    "Opponent",
    "OpponentExceptOwnGame",
    "Day1",
    "Day2",
    "Day1MinusDay2",
  ]
  
  export const summarizeRankingField = (ranking: RankingField) => {
    return `${ranking.label}: Based on ${ranking.field} ${ranking.type} ${ranking.descending ? 'descending' : 'ascending'}`;
  }