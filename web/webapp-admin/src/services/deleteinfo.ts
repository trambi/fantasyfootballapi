export interface DeleteInfo {
    target:'game'|'coach'|'squad';
    id: string;
    name: string;
  };