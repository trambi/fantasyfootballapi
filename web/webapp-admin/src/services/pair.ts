import { shuffle } from "./shuffle";

/**
 * Recursively pairs elements in the `toBePaired` array, ensuring that the pairings do not violate the `forbidden` constraints.
 *
 * @template T - The type of the elements to be paired.
 * @param {T[][]} alreadyPaired - An array of arrays representing the pairs that have already been formed.
 * @param {T[]} toBePaired - An array of elements that still need to be paired.
 * @param {Map<T, T[]>} forbidden - A Map that stores pairs of elements that are not allowed to be paired together.
 * @returns {T[][] | null} - The updated `alreadyPaired` array if a valid pairing is found, or `null` if no valid pairing is possible.
 */
export function swissPair<T>(alreadyPaired: T[][], toBePaired: T[], forbidden: Map<T, T[]>): T[][] | null {
    const numberToBePaired = toBePaired.length;
    if (numberToBePaired === 2) {
        if (isAllowed(toBePaired[0], toBePaired[1], forbidden)) {
            alreadyPaired.push(toBePaired);
            return alreadyPaired;
        }
        return null;
    }
    if (numberToBePaired === 1) {
        return alreadyPaired;
    }
    const candidateToBePaired = toBePaired[0];
    for (let i = 1; i < numberToBePaired; i++) {
        const candidateOpponent = toBePaired[i];
        if (isAllowed(candidateToBePaired, candidateOpponent, forbidden)) {
            alreadyPaired.push([candidateToBePaired, candidateOpponent]);
            const newToBePaired = removeFirstAndIFromToBePaired(toBePaired, i);
            const nextToBePaired = swissPair(alreadyPaired, newToBePaired, forbidden);
            // Check if the next pairing is valid
            if (nextToBePaired === null) {
                // If not, remove the last pairing and with the next candidate opponent
                alreadyPaired.pop();
                continue;
            }
            return nextToBePaired;
        }
    }
    return null;
}

export function randomPair<T>(alreadyPaired: T[][], toBePaired: T[], forbidden: Map<T, T[]>): T[][] | null {
    const shuffledToBePaired = shuffle(toBePaired);
    return swissPair(alreadyPaired, shuffledToBePaired, forbidden);
}

function isAllowed<T>(candidate: T, opponent: T, forbidden: Map<T, T[]>): boolean {
    return !forbidden.get(candidate)?.includes(opponent);
}

function removeFirstAndIFromToBePaired<T>(toBePaired: T[], i: number): T[] {
    return toBePaired.slice(1, i).concat(toBePaired.slice(i + 1));
} 