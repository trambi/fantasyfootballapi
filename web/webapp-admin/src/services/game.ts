import { z } from "zod";
import { fake6Coachs } from "./coach";

export const coachInGameValidator = z.object({
    id: z.string(),
    name: z.string(),
    nafNumber: z.number().int().nonnegative().optional(),
    faction: z.string(),
    teamName: z.string().optional(),
    squadId: z.string().optional(),
    squadName: z.string().optional(),
})

const baseGameValidator = z.object({
    id: z.string(),
    editionId: z.string(),
    round: z.number().int().positive(),
    table: z.number().int().positive(),
    finale: z.boolean(),
    coach1: coachInGameValidator,
    coach2: coachInGameValidator
});

export const notPlayedGameValidator = baseGameValidator.merge(z.object({
    played: z.literal(false),
}));
export const notPlayedGamesValidator = z.array(notPlayedGameValidator);

export const playedGameValidator = baseGameValidator.merge(z.object({
    played: z.literal(true),
    td1: z.number().int(),
    casualties1: z.number().int(),
    completions1: z.number().int(),
    fouls1: z.number().int(),
    special1: z.string(),
    points1: z.number(),
    td2: z.number().int(),
    casualties2: z.number().int(),
    completions2: z.number().int(),
    fouls2: z.number().int(),
    special2: z.string(),
    points2: z.number(),
}));
export const playedGamesValidator = z.array(playedGameValidator);

export const gameValidator = playedGameValidator.or(notPlayedGameValidator);
export const gamesValidator = z.array(gameValidator);

export type NotPlayedGame = z.infer<typeof notPlayedGameValidator>;
export type PlayedGame = z.infer<typeof playedGameValidator>;
export type Game = z.infer<typeof gameValidator>;
export interface ToBeCreatedGame {
    editionId: string;
    round: number;
    table: number;
    finale: boolean;
    coachId1: string;
    coachName1: string;
    squadId1: string;
    squadName1: string;
    coachId2: string;
    coachName2: string;
    squadId2: string;
    squadName2: string;
};

export interface GameForCreation extends Omit<ToBeCreatedGame, 'coachName1' | 'squadId1' | 'squadName1' | 'coachName2' | 'squadId2' | 'squadName2'> {
    played: boolean;
};

export interface GameForUpdate extends GameForCreation {
    id: string;
    td1: number;
    td2: number;
    casualties1: number;
    casualties2: number;
    completions1: number;
    completions2: number;
    fouls1: number;
    fouls2: number;
    special1: string;
    special2: string;
};

export function gameToBeCreatedToGameForCreation(toBeCreated: ToBeCreatedGame): GameForCreation {
    return {
        editionId: toBeCreated.editionId,
        round: toBeCreated.round,
        table: toBeCreated.table,
        finale: toBeCreated.finale,
        played: false,
        coachId1: toBeCreated.coachId1,
        coachId2: toBeCreated.coachId2,
    };
}

export function gameToGameForUpdate(game: Game): GameForUpdate {
    return {
        id: game.id,
        editionId: game.editionId,
        round: game.round,
        table: game.table,
        finale: game.finale,
        played: game.played,
        coachId1: game.coach1.id,
        coachId2: game.coach2.id,
        td1: game.played ? game.td1 : 0,
        td2: game.played ? game.td2 : 0,
        casualties1: game.played ? game.casualties1 : 0,
        casualties2: game.played ? game.casualties2 : 0,
        completions1: game.played ? game.completions1 : 0,
        completions2: game.played ? game.completions2 : 0,
        fouls1: game.played ? game.fouls1 : 0,
        fouls2: game.played ? game.fouls2 : 0,
        special1: game.played ? game.special1 : '',
        special2: game.played ? game.special2 : '',
    };
}


export function fakeToBeCreatedGame(): ToBeCreatedGame {
    const coachs = fake6Coachs()
    return {
        editionId: "2",
        round: 1,
        table: 2,
        finale: false,
        coachId1: coachs[0].id,
        coachId2: coachs[2].id,
        coachName1: coachs[0].name,
        coachName2: coachs[2].name,
        squadId1: coachs[0].squadId ?? '',
        squadId2: coachs[2].squadId ?? '',
        squadName1: coachs[0].squadName ?? '',
        squadName2: coachs[2].squadName ?? ''
    }
}

export function fake2ToBeCreatedGames(): ToBeCreatedGame[] {
    const coachs = fake6Coachs()
    return [fakeToBeCreatedGame(),
    {
        editionId: "2",
        round: 1,
        table: 3,
        finale: false,
        coachId1: coachs[1].id,
        coachId2: coachs[3].id,
        coachName1: coachs[1].name,
        coachName2: coachs[3].name,
        squadId1: coachs[1].squadId ?? '',
        squadId2: coachs[3].squadId ?? '',
        squadName1: coachs[1].squadName ?? '',
        squadName2: coachs[3].squadName ?? ''
    }
    ]
}

export function fakeNotPlayedGame(): NotPlayedGame {
    const coachs = fake6Coachs();
    const { ready: _ready1, editionId: _editionId1, editionName: _editionName1, ...coach1 } = coachs[0];
    const { ready: _ready2, editionId: _editionId2, editionName: _editionName2, ...coach2 } = coachs[2];
    return {
        id: "101",
        editionId: "2",
        round: 1,
        table: 2,
        finale: false,
        coach1,
        coach2,
        played: false
    }
}

export function fake2NotPlayedGames(): NotPlayedGame[] {
    const coachs = fake6Coachs();
    const { ready: _ready1, editionId: _editionId1, editionName: _editionName1, ...coach1 } = coachs[1];
    const { ready: _ready2, editionId: _editionId2, editionName: _editionName2, ...coach2 } = coachs[3];
    return [fakeNotPlayedGame(),
    {
        id: "102",
        editionId: "2",
        round: 1,
        table: 3,
        finale: false,
        coach1,
        coach2,
        played: false
    }
    ]
}

export function fakePlayedGame(): PlayedGame {
    const coachs = fake6Coachs();
    const { ready: _ready1, editionId: _editionId1, editionName: _editionName1, ...coach1 } = coachs[0];
    const { ready: _ready2, editionId: _editionId2, editionName: _editionName2, ...coach2 } = coachs[2];
    return {
        id: "101",
        editionId: "2",
        round: 1,
        table: 2,
        finale: false,
        coach1,
        coach2,
        played: true,
        td1: 6,
        td2: 7,
        casualties1: 8,
        casualties2: 9,
        completions1: 10,
        completions2: 11,
        fouls1: 12,
        fouls2: 13,
        special1: "special1",
        special2: "special2",
        points1: 14,
        points2: 15
    }
}

export function fake2PlayedGames(): PlayedGame[] {
    const coachs = fake6Coachs()
    const { ready: _ready1, editionId: _editionId1, editionName: _editionName1, ...coach1 } = coachs[1];
    const { ready: _ready2, editionId: _editionId2, editionName: _editionName2, ...coach2 } = coachs[3];
    return [fakePlayedGame(),
    {
        id: "102",
        editionId: "2",
        round: 1,
        table: 3,
        finale: false,
        coach1,
        coach2,
        played: true,
        td1: 6,
        td2: 7,
        casualties1: 8,
        casualties2: 9,
        completions1: 10,
        completions2: 11,
        fouls1: 12,
        fouls2: 13,
        special1: "special1",
        special2: "special2",
        points1: 14,
        points2: 15
    }
    ]
}