import { fake6Coachs } from "./coach";
import type { GameForCreation } from "./game";

export interface ToBeCreatedConfrontation {
    editionId: string;
    round: number;
    finale: boolean;
    squadId1: string;
    squadName1: string;
    squadId2: string;
    squadName2: string;
    games: {
        table: number;
        coachId1: string;
        coachName1: string;
        coachId2: string;
        coachName2: string;
    }[];
}

export function fakeToBeCreatedConfrontation(): ToBeCreatedConfrontation {
    const coachs = fake6Coachs()
    return {
        editionId: "2",
        round: 1,
        finale: false,
        squadId1: coachs[0].squadId ?? '',
        squadName1: coachs[0].squadName ?? '',
        squadId2: coachs[2].squadId ?? '',
        squadName2: coachs[2].squadName ?? '',
        games: [
            {

                table: 2,
                coachId1: coachs[0].id,
                coachId2: coachs[2].id,
                coachName1: coachs[0].name,
                coachName2: coachs[2].name,
            }, {
                table: 3,
                coachId1: coachs[1].id,
                coachId2: coachs[3].id,
                coachName1: coachs[1].name,
                coachName2: coachs[3].name,
            }
        ]
    }
}

export function confrontationToBeCreatedToGamesForCreation(confrontation: ToBeCreatedConfrontation): GameForCreation[] {
    const played = false;
    const { editionId, round, finale } = confrontation;
    return confrontation.games.map(game => ({
        editionId,
        round,
        table: game.table,
        played,
        coachId1: game.coachId1,
        coachId2: game.coachId2,
        finale,
    }))
}