export function get_iso_8660_date(input: Date){
  const month = (input.getUTCMonth()+1).toFixed().padStart(2,'0');
  const day = input.getUTCDate().toFixed().padStart(2,'0');
  const year = input.getUTCFullYear().toFixed().padStart(4,'0');
  return `${year}-${month}-${day}`;
}

export function add_days(input: Date, delta: number): Date{
  const millisecondSinceEpoq = input.getTime() + delta * 24 * 3_600_000;
  return new Date(millisecondSinceEpoq);
}