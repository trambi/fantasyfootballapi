import { fake6Coachs, fakeCoach, type Coach } from "./coach";
import { fakeSquad, fakeSquads, type Squad } from "./squad";

export interface CoachRank extends Coach {
  position: number;
  params: Map<string, number>
};

export function fakeCoachRank(): CoachRank {
  const { id, name, faction, teamName, editionId, editionName } = fakeCoach();
  const params = new Map();
  params.set('netCasualties', 4);
  params.set('netTd', 2);
  params.set('opponentsPoints', 0);
  params.set('points', 5);
  return {
    id,
    name,
    faction,
    teamName,
    editionId,
    editionName,
    position: 4,
    params
  };
};

export function fakeCoachRanking(): CoachRank[] {
  const coachs = fake6Coachs();
  const params = new Array(6).fill(new Map());
  params[0].set('netCasualties', 4);
  params[0].set('netTd', 2);
  params[0].set('opponentsPoints', 10);
  params[0].set('points', 15);
  params[1].set('netCasualties', -4);
  params[1].set('netTd', 2);
  params[1].set('opponentsPoints', 10);
  params[1].set('points', 15);
  params[2].set('netCasualties', 6);
  params[2].set('netTd', -2);
  params[2].set('opponentsPoints', 10);
  params[2].set('points', 15);
  params[3].set('netCasualties', -1);
  params[3].set('netTd', 3);
  params[3].set('opponentsPoints', 8);
  params[3].set('points', 15);
  params[4].set('netCasualties', 7);
  params[4].set('netTd', -3);
  params[4].set('opponentsPoints', 6);
  params[4].set('points', 8);
  params[5].set('netCasualties', 4);
  params[5].set('netTd', 2);
  params[5].set('opponentsPoints', 7);
  params[5].set('points', 0);
  return coachs.map(({ id, name, faction, teamName, editionId, editionName }, index) => {
    return {
      id,
      name,
      faction,
      teamName,
      editionId,
      editionName,
      position: index + 1,
      params: params[index]
    };
  });
};

export interface SquadRank extends Squad {
  position: number;
  params: Map<string, number>
};

export const fakeSquadRank = () => {
  const squad = fakeSquad();
  const params = new Map();
  params.set('netCasualties', 4);
  params.set('netTd', 2);
  params.set('opponentsPoints', 0);
  params.set('points', 5);
  return {
    ...squad,
    position: 4,
    params
  };
};


export function fakeSquadRanking(): SquadRank[] {
  const squads = fakeSquads();
  const params = new Array(6).fill(new Map());
  params[0].set('netCasualties', 4);
  params[0].set('netTd', 2);
  params[0].set('opponentsPoints', 10);
  params[0].set('points', 15);
  params[1].set('netCasualties', -4);
  params[1].set('netTd', 2);
  params[1].set('opponentsPoints', 10);
  params[1].set('points', 15);
  return squads.map((squad, index) => ({ ...squad, position: index + 1, params: params[index] }));
};
