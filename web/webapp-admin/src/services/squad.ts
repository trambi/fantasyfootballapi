import { z } from "zod";
import { fake6SummaryCoachs, summaryCoachValidator } from "./coach";

export const squadValidator = z.object({
    id: z.string(),
    editionId: z.string(),
    name: z.string(),
    teamMates: z.array(summaryCoachValidator),
});

export const squadsValidator = z.array(squadValidator);

export type Squad = z.infer<typeof squadValidator>;

export interface SquadForCreation {
    editionId: string;
    name: string,
    teamMates: {
        id: string,
    }[]
};

export interface SquadForUpdate extends SquadForCreation {
    id: string;
};

export interface TeamMateInSquadForBulkCreation {
    name: string,
    nafNumber: number,
    faction: string,
};

export interface SquadForBulkCreation {
    editionId: string;
    name: string,
    teamMates: TeamMateInSquadForBulkCreation[];
};

export function displayMembers(squad: Squad) {
    const begin = squad.teamMates.slice(0, -1).map(member => member.name).join(', ')
    const end = squad.teamMates.slice(-1).map(member => member.name).join('')
    return `${begin} & ${end}`;
}

export function isReady(squad: Squad) {
    return squad.teamMates.every(member => member.ready);
}


export function fakeSquad(): Squad {
    const coachs = fake6SummaryCoachs();
    return {
        id: "30",
        editionId: "2",
        name: "Squad A",
        teamMates: [coachs[0], coachs[1], coachs[4]]
    }
}

export function fakeSquads(): Squad[] {
    const coachs = fake6SummaryCoachs();
    return [fakeSquad(), {
        id: "31",
        editionId: "2",
        name: "Squad B",
        teamMates: [coachs[2], coachs[3], coachs[5]]
    }]
}

export function fakeSquadForBulkCreation(): SquadForBulkCreation {
    const squad = fakeSquad();
    return {
        editionId: squad.editionId,
        name: squad.name,
        teamMates: [
            {
                name: squad.teamMates[0].name,
                nafNumber: squad.teamMates[0].nafNumber ?? 0,
                faction: squad.teamMates[0].faction
            },
            {
                name: squad.teamMates[1].name,
                nafNumber: squad.teamMates[1].nafNumber ?? 0,
                faction: squad.teamMates[1].faction
            },
            {
                name: squad.teamMates[2].name,
                nafNumber: squad.teamMates[2].nafNumber ?? 0,
                faction: squad.teamMates[2].faction
            }]
    }
}