import { z } from "zod";
import type { ToBeCreatedConfrontation } from "./confrontation";
import type { PlayedGame, ToBeCreatedGame } from "./game";
import type { Coach } from "./coach";
import type { Squad } from "./squad";
import { pairByRanking, pairRandomly } from "./squad-pairing";
import { pairByCoachOrderly, pairByCoachRandomly } from "./coach-pairing";
import { pointConfigurationValidator } from "./point-configuration";
import { rankingConfigurationValidator } from "./ranking-configuration";

// zod object for edition as written in Go
export const editionValidator = z.object({
  id: z.string(),
  name: z.string(),
  day1: z.string(),
  day2: z.string(),
  roundNumber: z.number().int(),
  currentRound: z.number().int(),
  firstDayRound: z.number().int(),
  useFinale: z.boolean(),
  fullSquad: z.boolean(),
  coachPerSquad: z.number().int().nonnegative(),
  allowedFactions: z.array(z.string()),
  organizer: z.string(),
  rankings: rankingConfigurationValidator,
  gamePoints: pointConfigurationValidator,
  confrontationPoints: pointConfigurationValidator,
});

export type Edition = z.infer<typeof editionValidator>;

export function fakeEdition(): Edition {
  return {
    id: "2",
    name: "Fake 2",
    day1: "2023-05-13",
    day2: "2023-05-14",
    roundNumber: 5,
    currentRound: 5,
    firstDayRound: 3,
    useFinale: true,
    fullSquad: true,
    coachPerSquad: 3,
    allowedFactions: ["faction 1", "faction 2"],
    organizer: "Awesome 1 and Awesome 2",
    rankings: {
      coach: {
        main: [
          { label: "points", field: "Points", type: "For", descending: true },
          { label: "opponentsPoints", field: "Points", type: "Opponent", descending: true },
          { label: "netTd", field: "Td", type: "Net", descending: true },
          { label: "netCasualties", field: "Casualties", type: "Net", descending: true }]
      },
      squad: {
        casualties: [{ label: "casualtiesFor", field: "Casualties", type: "For", descending: true }],
        comeback: [
          { label: "diffRanking", field: "Ranking", type: "Day1MinusDay2", descending: true },
          { label: "firstDayRanking", field: "Ranking", type: "Day1", descending: false },
          { label: "finalRanking", field: "Ranking", type: "Day2", descending: false }
        ],
        defense: [{ label: "tdAgainst", field: "Td", type: "Against", descending: false }],
        fouls: [{ label: "foulsFor", field: "Fouls", type: "For", descending: true }],
        main: [
          { label: "points", field: "Points", type: "For", descending: true },
          { label: "opponentCoachTeamPoints", field: "Points", type: "Opponent", descending: true },
          { label: "netTd", field: "Td", type: "Net", descending: true },
          { label: "netCasualties", field: "Casualties", type: "Net", descending: true }
        ],
        td: [{ label: "tdFor", field: "Td", type: "For", descending: true }]
      },
    },
    gamePoints: {
      default: "0.000000",
      clauses: [
        { condition: "{TdFor} = {TdAgainst}", valueIfTrue: "2.000000" },
        { condition: "{TdFor} \u003e {TdAgainst}", valueIfTrue: "5.000000" }
      ]
    },
    confrontationPoints: {
      default: "0.000000",
      clauses: [
        { condition: "{PointsFor} = {PointsAgainst}", valueIfTrue: "2.000000" },
        { condition: "{PointsFor} \u003e {PointsAgainst}", valueIfTrue: "5.000000" }
      ]
    }
  };
}



export function fake2Editions(): Edition[] {
  const fakeEdition1 = fakeEdition();
  fakeEdition1.id = "1";
  fakeEdition1.name = "Fake 1";
  fakeEdition1.day1 = "2022-05-12";
  fakeEdition1.day2 = "2022-05-12";
  return [fakeEdition1, fakeEdition()]
}


export function prepareNextRound(edition: Edition, playedGames: PlayedGame[], coachs: Coach[], squads: Squad[]): ToBeCreatedConfrontation[] | ToBeCreatedGame[] {
  if (edition.currentRound >= edition.roundNumber) {
    return [];
  }
  if (edition.fullSquad) {
    if (edition.currentRound === 0) {
      console.debug("By squad - randomly");
      return pairRandomly(squads, edition.currentRound + 1, 1, new Map());
    } else {
      console.debug("By squad - by ranking");
      const forbidden = playedGamesToForbiddenSquadMap(playedGames);
      if (edition.useFinale && edition.currentRound === edition.roundNumber - 1) {
        console.debug("with finale");
        const squad1 = squads.shift();
        const squad2 = squads.shift();
        if (squad1 === undefined || squad2 === undefined) {
          console.error("Error while pairing squads");
          return [] as ToBeCreatedConfrontation[];
        }
        const finaleConfrontation = createFinaleConfrontation(edition, squad1, squad2, coachs);
        const table = finaleConfrontation.games.length + 1;
        addToForbiddenSquadMap(squad1.id, squad2.id, forbidden)
        const otherConfrontations = pairByRanking(squads, edition.currentRound + 1, table, forbidden);
        return [finaleConfrontation, ...otherConfrontations];
      }
      return pairByRanking(squads, edition.currentRound + 1, 1, forbidden);
    }
  } else {
    if (edition.currentRound === 0) {
      console.debug("By coach - randomly");
      return pairByCoachRandomly(coachs, edition.currentRound + 1, 1, new Map());
    } else {
      console.debug("By coach - by ranking");
      const forbidden = playedGamesToForbiddenCoachMap(playedGames);
      if (edition.useFinale && edition.currentRound === edition.roundNumber - 1) {
        console.debug("with finale");
        const coach1 = coachs.shift();
        const coach2 = coachs.shift();
        if (coach1 === undefined || coach2 === undefined) {
          console.error("Error while pairing coachs");
          return [] as ToBeCreatedGame[];
        }
        const finaleGame = createFinaleGame(edition, coach1, coach2);
        const table = 2;
        addToForbiddenCoachMap(coach1.id, coach2.id, forbidden);
        const otherGames = pairByCoachOrderly(coachs, edition.currentRound + 1, table, forbidden);
        return [finaleGame, ...otherGames];
      }
      return pairByCoachOrderly(coachs, edition.currentRound + 1, 1, forbidden);
    }
  }
}

function createFinaleGame(edition: Edition, coach1: Coach, coach2: Coach): ToBeCreatedGame {
  return {
    table: 1,
    editionId: edition.id,
    round: edition.roundNumber,
    finale: true,
    coachId1: coach1.id,
    coachName1: coach1.name,
    squadId1: coach1.squadId ?? '',
    squadName1: coach1.squadName ?? '',
    coachId2: coach2.id,
    coachName2: coach2.name,
    squadId2: coach2.squadId ?? '',
    squadName2: coach2.squadName ?? ''
  };
}

function createFinaleConfrontation(edition: Edition, squad1: Squad, squad2: Squad, coachs: Coach[]): ToBeCreatedConfrontation {
  const finaleConfrontation: ToBeCreatedConfrontation = {
    editionId: edition.id,
    round: edition.roundNumber,
    finale: true,
    squadId1: squad1.id,
    squadName1: squad1.name,
    squadId2: squad2.id,
    squadName2: squad2.name,
    games: []
  };
  let table = 1;
  const sortedSquad1Coachs = coachs.filter((coach) => coach.squadId === squad1.id);
  const sortedSquad2Coachs = coachs.filter((coach) => coach.squadId === squad2.id);
  for (let i = 0; i < sortedSquad1Coachs.length; i++) {
    finaleConfrontation.games.push({
      table,
      coachId1: sortedSquad1Coachs[i].id,
      coachName1: sortedSquad1Coachs[i].name,
      coachId2: sortedSquad2Coachs[i].id,
      coachName2: sortedSquad2Coachs[i].name,
    });
    table = table + 1;
  }
  return finaleConfrontation;
}

function playedGamesToForbiddenSquadMap(alreadyPlayedGames: PlayedGame[]): Map<string, string[]> {
  const forbidden = new Map<string, string[]>();
  for (const game of alreadyPlayedGames) {
    addToForbiddenSquadMap(game.coach1.squadId, game.coach2.squadId, forbidden);
  }
  return forbidden;
}

function addToForbiddenSquadMap(squadId1: string | undefined, squadId2: string | undefined, forbidden: Map<string, string[]>) {
  if (squadId1 && squadId2) {
    const alreadyPlayedBySquad1 = forbidden.get(squadId1) || [];
    const alreadyPlayedBySquad2 = forbidden.get(squadId2) || [];
    alreadyPlayedBySquad1.push(squadId2);
    alreadyPlayedBySquad2.push(squadId1);
    forbidden.set(squadId1, [...new Set(alreadyPlayedBySquad1)]);
    forbidden.set(squadId2, [...new Set(alreadyPlayedBySquad2)]);
  }
}

function playedGamesToForbiddenCoachMap(
  alreadyPlayedGames: PlayedGame[]
): Map<string, string[]> {
  const forbidden = new Map<string, string[]>();
  for (const game of alreadyPlayedGames) {
    addToForbiddenCoachMap(game.coach1.id, game.coach1.id, forbidden);
  }
  return forbidden;
}

function addToForbiddenCoachMap(coachId1: string | undefined, coachId2: string | undefined, forbidden: Map<string, string[]>) {
  if (coachId1 && coachId2) {
    const alreadyPlayedByCoach1 = forbidden.get(coachId1) || [];
    const alreadyPlayedByCoach2 = forbidden.get(coachId2) || [];
    alreadyPlayedByCoach1.push(coachId1);
    alreadyPlayedByCoach2.push(coachId2);
    forbidden.set(coachId1, [...new Set(alreadyPlayedByCoach1)]);
    forbidden.set(coachId2, [...new Set(alreadyPlayedByCoach2)]);
  }
}
