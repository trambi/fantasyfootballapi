import type { ToBeCreatedConfrontation } from "./confrontation";
import { randomPair, swissPair } from "./pair";
import { shuffle } from "./shuffle";

interface PairableSquad {
    editionId: string;
    id: string;
    name: string;
    teamMates: PairableMate[];
}

interface PairableMate {
    id: string;
    name: string;
}

export function pairRandomly(squads: PairableSquad[], round: number, startingTable: number, forbidden: Map<string, string[]>): ToBeCreatedConfrontation[] {
    const alreadyPairedId: string[][] = []
    const toBePairedId = squads.map(squad => squad.id);
    const pairedId = randomPair(alreadyPairedId, toBePairedId, forbidden);
    if (pairedId === null) {
        console.error("Error while pairing squads");
        return [] as ToBeCreatedConfrontation[];
    };
    return pairedIdsToConfrontations(pairedId, squads, round, startingTable, shuffle);
}

export function pairByRanking(squads: PairableSquad[], round: number, startingTable: number, forbidden: Map<string, string[]>): ToBeCreatedConfrontation[] {
    const alreadyPairedId: string[][] = []
    const toBePairedId = squads.map(squad => squad.id);
    const pairedId = swissPair(alreadyPairedId, toBePairedId, forbidden);
    if (pairedId === null) {
        console.error("Error while pairing squads");
        return [] as ToBeCreatedConfrontation[];
    };
    return pairedIdsToConfrontations(pairedId, squads, round, startingTable, (mates) => mates);
}

function pairedIdsToConfrontations(pairedId: string[][], squads: PairableSquad[], round: number, startingTable: number, orderFunction: (array: PairableMate[]) => PairableMate[]): ToBeCreatedConfrontation[] {
    let table = startingTable;
    const confrontations: ToBeCreatedConfrontation[] = [];
    for (const pairOfId of pairedId) {
        const squad1 = squads.find(squad => squad.id === pairOfId[0]);
        const squad2 = squads.find(squad => squad.id === pairOfId[1]);
        if (squad1 && squad2) {
            const confrontation: ToBeCreatedConfrontation = {
                editionId: squad1.editionId,
                round,
                finale: false,
                squadId1: squad1.id,
                squadId2: squad2.id,
                squadName1: squad1.name,
                squadName2: squad2.name,
                games: [],
            }
            const squad1Members = orderFunction(squad1.teamMates);
            const squad2Members = orderFunction(squad2.teamMates);
            squad1Members.forEach((member, i) => {
                confrontation.games.push({
                    table,
                    coachId1: member.id,
                    coachId2: squad2Members[i].id,
                    coachName1: member.name,
                    coachName2: squad2Members[i].name,
                });
                table = table + 1;
            });
            confrontations.push(confrontation);
        }
    }
    return confrontations;
}