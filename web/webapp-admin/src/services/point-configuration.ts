import { z } from "zod";

const clauseValidator = z.object({
    condition: z.string(),
    valueIfTrue: z.string(),
});

export const pointConfigurationValidator = z.object({
    default: z.string(),
    clauses: z.array(clauseValidator),
});

export type Clause = z.infer<typeof clauseValidator>;
export type PointConfiguration = z.infer<typeof pointConfigurationValidator>;

export function fakePointConfiguration(): PointConfiguration {
    return {
        default: "0",
        clauses: [
            { condition: "{TdFor} = {TdAgainst}", valueIfTrue: "2" },
            { condition: "{TdFor} > {TdAgainst}", valueIfTrue: "5" }
        ]
    }
}

export function emptyPointConfiguration(): PointConfiguration {
    return {
        default: "",
        clauses: []
    }
}

export function emptyClause(): Clause{
    return {
        condition: "",
        valueIfTrue: ""
    }
}

export function summarizePointConfiguration(conf: PointConfiguration): string[]{
    const defaultLine = `Default value: ${conf.default}`;
    const clauses = conf.clauses.map((clause)=>`If ${clause.condition} then value: ${clause.valueIfTrue}`);
    return [defaultLine, ...clauses];
}
