import { describe, expect, it } from "vitest";
import { swissPair, randomPair } from "../pair";


describe('swissPair', () => {
    it.each([
        [new Map<string, string[]>(),
        [["1", "2"], ["3", "4"]]],
        [new Map<string, string[]>([["1", ["2"]], ["2", ["1"]], ["3", ["4"]], ["4", ["3"]]]),
        [["1", "3"], ["2", "4"]]],
        [new Map<string, string[]>([["1", ["2", "3"]], ["2", ["1", "4"]], ["3", ["4", "1"]], ["4", ["3", "2"]]]),
        [["1", "4"], ["2", "3"]]],

    ])('should pair according to order and forbidden', (forbidden: Map<string, string[]>, expected: string[][]) => {
        // Given
        const paired: string[][] = [];
        const toBePaired = ["1", "2", "3", "4"];
        // When
        const result = swissPair(paired, toBePaired, forbidden);
        // Then
        expect(result).toEqual(expected);
    });
});

describe('randomPair', () => {
    it('should pair randomly', () => {
        // Given
        const paired: string[][] = [];
        const toBePaired = ["1", "2", "3", "4"];
        const expectedSet = new Set(toBePaired);
        // When
        const result = randomPair(paired, toBePaired, new Map<string, string[]>());
        // Then
        expect(result).not.toBeNull();
        expect(result).toHaveLength(2);
        expect(new Set(result?.flat())).toEqual(expectedSet);
    });
    it('should pair randomly with taking into account paired and forbidden', () => {
        // Given
        const paired: string[][] = [["1", "2"]];
        const toBePaired = ["3", "4", "5", "6", "7", "8"];
        const expectedSet = new Set(["1", "2", "3", "4", "5", "6", "7", "8"]);
        const forbidden = new Map<string, string[]>([["7", ["8"]], ["8", ["7"]]]);
        // When
        const result = randomPair(paired, toBePaired, forbidden);
        // Then
        expect(result).not.toBeNull();
        expect(result).toHaveLength(4);
        // Take into account paired
        expect(result ? result[0] : []).toEqual(paired[0]);
        expect(new Set(result?.flat())).toEqual(expectedSet);
        // Take into account forbidden
        expect(result ? result.some(([a, b]) => (a === '7' && b === '8') || (b === '7' && a === '8')) : true).toBeFalsy();
    });
});