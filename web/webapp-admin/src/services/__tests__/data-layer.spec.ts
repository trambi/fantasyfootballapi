import { describe, expect, it, vi } from "vitest";
import { DataLayer } from "../data-layer";
import { fake2NotPlayedGames, fake2PlayedGames, fakeNotPlayedGame, fakePlayedGame, gameToGameForUpdate } from "../game";

describe("DataLayer", () => {
    describe('getCurrentEdition', () => {
        it("should call fetch function with suffix path /editions/current", async () => {
            // Given
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(`{
    "id": "19",
    "name": "RDVBB 19",
    "day1": "2023-05-13",
    "day2": "2023-05-14",
    "roundNumber": 5,
    "currentRound": 5,
    "firstDayRound": 3,
    "useFinale": true,
    "fullSquad": true,
    "coachPerSquad":3,
    "allowedFactions": ["Orc", "Skaven"],
    "organizer": "Old Dwarf and Rremy",
    "rankings": {
        "coach": {
            "main": [
                { "label": "points", "field": "Points", "type": "For", "descending": true },
                { "label": "opponentsPoints", "field": "Points", "type": "Opponent", "descending": true },
                { "label": "netTd", "field": "Td", "type": "Net", "descending": true },
                { "label": "netCasualties", "field": "Casualties", "type": "Net", "descending": true }
            ]
        }, 
        "squad": { 
            "comeback": [
                { "label": "diffRanking", "field": "Ranking", "type": "Day1MinusDay2", "descending": true },
                { "label": "firstDayRanking", "field": "Ranking", "type": "Day1", "descending": false },
                { "label": "finalRanking", "field": "Ranking", "type": "Day2", "descending": false }
            ],
            "main": [
                { "label": "points", "field": "Points", "type": "For", "descending": true },
                { "label": "opponentCoachTeamPoints", "field": "Points", "type": "Opponent", "descending": true },
                { "label": "netTd", "field": "Td", "type": "Net", "descending": true },
                { "label": "netCasualties", "field": "Casualties", "type": "Net", "descending": true }
            ]
        }
    },
    "gamePoints": { "default": "0.000000", "clauses": [{ "condition": "{TdFor} = {TdAgainst}", "valueIfTrue": "2.000000" }, { "condition": "{TdFor} \u003e {TdAgainst}", "valueIfTrue": "5.000000" }] },
    "confrontationPoints": { "default": "0.000000", "clauses": [{ "condition": "{PointsFor} = {PointsAgainst}", "valueIfTrue": "2.000000" }, { "condition": "{PointsFor} \u003e {PointsAgainst}", "valueIfTrue": "5.000000" }] }
}`              );
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.getCurrentEdition();
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/current");
            expect(result.id).toEqual("19");
            expect(result.coachPerSquad).toEqual(3);
        });
    });
    describe('getEdition', () => {
        it("should call fetch function with suffix path /editions/1", async () => {
            // Given
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(`{
    "id": "1",
    "name": "RDVBB",
    "day1": "2003-05-13",
    "day2": "2003-05-14",
    "roundNumber": 5,
    "currentRound": 5,
    "firstDayRound": 3,
    "useFinale": true,
    "fullSquad": true,
    "coachPerSquad":3,
    "rankingStrategyName": "Rdvbb18",
    "allowedFactions": ["Orc", "Skaven"],
    "organizer": "Old Dwarf and Rremy",
    "rankings": {
        "coach": {
            "main": [
                { "label": "points", "field": "Points", "type": "For", "descending": true },
                { "label": "opponentsPoints", "field": "Points", "type": "Opponent", "descending": true },
                { "label": "netTd", "field": "Td", "type": "Net", "descending": true },
                { "label": "netCasualties", "field": "Casualties", "type": "Net", "descending": true }
            ]
        }, 
        "squad": { 
            "comeback": [
                { "label": "diffRanking", "field": "Ranking", "type": "Day1MinusDay2", "descending": true },
                { "label": "firstDayRanking", "field": "Ranking", "type": "Day1", "descending": false },
                { "label": "finalRanking", "field": "Ranking", "type": "Day2", "descending": false }
            ],
            "main": [
                { "label": "points", "field": "Points", "type": "For", "descending": true },
                { "label": "opponentCoachTeamPoints", "field": "Points", "type": "Opponent", "descending": true },
                { "label": "netTd", "field": "Td", "type": "Net", "descending": true },
                { "label": "netCasualties", "field": "Casualties", "type": "Net", "descending": true }
            ]
        }
    },
    "gamePoints": { "default": "0.000000", "clauses": [{ "condition": "{TdFor} = {TdAgainst}", "valueIfTrue": "2.000000" }, { "condition": "{TdFor} \u003e {TdAgainst}", "valueIfTrue": "5.000000" }] },
    "confrontationPoints": { "default": "0.000000", "clauses": [{ "condition": "{PointsFor} = {PointsAgainst}", "valueIfTrue": "2.000000" }, { "condition": "{PointsFor} \u003e {PointsAgainst}", "valueIfTrue": "5.000000" }] }
}`              );
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            await layer.getEdition("1");
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/1");
        });
    });
    describe('updateEdition', () => {
        it('should call fetch function with PUT and suffix path editions/1', async () => {
            // Given
            const edition = {
                id: "19",
                name: "RDVBB 19",
                day1: "2023-05-13",
                day2: "2023-05-14",
                roundNumber: 5,
                currentRound: 5,
                firstDayRound: 3,
                useFinale: true,
                fullSquad: true,
                coachPerSquad: 3,
                allowedFactions: ["Orc", "Skaven"],
                organizer: "Old Dwarf and Rremy",
                rankings: {
                    coach: {
                        main: [
                            { label: "points", field: "Points", type: "For", descending: true },
                            { label: "opponentsPoints", field: "Points", type: "Opponent", "descending": true },
                            { label: "netTd", field: "Td", type: "Net", descending: true },
                            { label: "netCasualties", field: "Casualties", type: "Net", descending: true }
                        ]
                    },
                    squad: {
                        comeback: [
                            { label: "diffRanking", field: "Ranking", type: "Day1MinusDay2", descending: true },
                            { label: "firstDayRanking", field: "Ranking", type: "Day1", descending: false },
                            { label: "finalRanking", field: "Ranking", type: "Day2", descending: false }
                        ],
                        main: [
                            { label: "points", field: "Points", type: "For", descending: true },
                            { label: "opponentCoachTeamPoints", field: "Points", type: "Opponent", descending: true },
                            { label: "netTd", field: "Td", type: "Net", descending: true },
                            { label: "netCasualties", field: "Casualties", type: "Net", descending: true }
                        ]
                    }
                },
                gamePoints: { default: "0.000000", clauses: [{ condition: "{TdFor} = {TdAgainst}", valueIfTrue: "2.000000" }, { condition: "{TdFor} \u003e {TdAgainst}", valueIfTrue: "5.000000" }] },
                confrontationPoints: { default: "0.000000", clauses: [{ condition: "{PointsFor} = {PointsAgainst}", valueIfTrue: "2.000000" }, { condition: "{PointsFor} \u003e {PointsAgainst}", valueIfTrue: "5.000000" }] }
            };
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = JSON.stringify(edition);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 204
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.updateEdition(edition);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/19", { method: 'PUT', headers, body: expectedBody });
            expect(result).toBe(true);
        });
    });
    describe('getCoachsByEdition', () => {
        it("should call fetch function with suffix path /editions/1/coachs when asked getCoachsByEdition(1)", async () => {
            // Given
            const id = "1";
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(`[
    {
        "id":"1",
        "editionId":"1",
        "editionName":"An edition",
        "name":"a coach",
        "teamName":"a team",
        "email":"valid.address@email.com",
        "nafNumber":2,
        "faction":"Faction 1",
        "ready":true,
        "squadId": "3",
        "squadName": "a squad"
    },
    {
        "id":"2",
        "editionId":"1",
        "editionName":"An edition",
        "name":"another coach",
        "teamName":"a team",
        "email":"valid.address@email.com",
        "nafNumber":2,
        "faction":"Faction 2",
        "ready":true,
        "squadId": "4",
        "squadName": "another squad"
    }
]`              );
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            await layer.getCoachsByEdition(id);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/1/coachs");
        });
    });
    describe('getCoach', () => {
        it("should call fetch function with suffix path /coachs/1", async () => {
            // Given
            const id = "1";
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(`{
            "id":"1",
            "editionId":"1",
            "editionName":"An edition",
            "name":"a coach",
            "teamName":"a team",
            "email":"valid.address@email.com",
            "nafNumber":2,
            "faction":"Faction 1",
            "ready":true,
            "squadId": "3",
            "squadName": "a squad"
            }`);
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            await layer.getCoach(id);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/coachs/1");
        });
    });
    describe('getSquad', () => {
        it('should call fetch function with suffix path squads/1', async () => {
            // Given
            const id = "1";
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(`{
            "id":"1",
            "editionId":"2",
            "name":"a squad",
            "teamMates":[
                {"id":"2","name":"a coach","teamName":"a team","ready":true,"faction":"faction 1"},
                {"id":"3","name":"another coach","teamName":"another team","ready":true,"faction":"faction 1"}
            ]
            
            }`);
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            await layer.getSquad(id);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/squads/1");
        });
    });
    describe('getSquadsByEdition', () => {
        it('should call fetch function with suffix path editions/1/squads', async () => {
            // Given
            const id = "1";
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(`[
    {
        "id":"1",
        "editionId":"2",
        "name":"a squad",
        "teamMates":[
            {"id":"2","name":"a coach","teamName":"a team","ready":true,"faction":"faction 1"},
            {"id":"3","name":"another coach","teamName":"another team","ready":true,"faction":"faction 1"}
        ]

    },
    {
        "id":"2",
        "editionId":"2",
        "name":"another squad",
        "teamMates":[
            {"id":"4","name":"a coach","teamName":"a team","ready":true,"faction":"faction 2"},
            {"id":"5","name":"another coach","teamName":"another team","ready":true,"faction":"faction 2"}
        ]

    }
]`        );
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            await layer.getSquadsByEdition(id);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/1/squads");
        });
    });
    describe('createCoachForEdition', () => {
        it('should call fetch function with POST and suffix path editions/1/coachs', async () => {
            // Given
            const coach = {
                "id": "0",
                "editionId": "3",
                "editionName": "An edition",
                "name": "a coach",
                "teamName": "a team",
                "nafNumber": 2,
                "faction": "Faction 1",
                "ready": true,
                "squadId": "3",
                "squadName": "a squad"
            };
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = JSON.stringify(coach);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 201, headers: {
                        'Location': 'https://something/coachs/2'
                    }
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.createCoachForEdition(coach);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/3/coachs", { method: 'POST', headers, body: expectedBody });
            expect(result).toBe('2');
        });
        it('should return null if fetch function returns different from 201', async () => {
            // Given
            const coach = {
                "id": "0",
                "editionId": "3",
                "editionName": "An edition",
                "name": "a coach",
                "teamName": "a team",
                "nafNumber": 2,
                "faction": "Faction 1",
                "ready": true,
                "squadId": "3",
                "squadName": "a squad"
            };
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 500
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.createCoachForEdition(coach);
            // Then
            expect(result).toBe(null);
        });
    });
    describe('updateCoach', () => {
        it('should call fetch function with PUT and suffix path coachs/1', async () => {
            // Given
            const coach = {
                "id": "1",
                "editionId": "1",
                "editionName": "Some edition",
                "name": "a coach",
                "teamName": "a team",
                "nafNumber": 2,
                "faction": "Faction 1",
                "ready": true,
                "squadId": "3",
                "squadName": "a squad"
            };
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = JSON.stringify(coach);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 204
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.updateCoach(coach);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/coachs/1", { method: 'PUT', headers, body: expectedBody });
            expect(result).toBe(true);
        });
    });
    describe('updateCoachReadiness', () => {
        it('should call fetch function with PATCH and suffix path coachs/1', async () => {
            // Given
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = '{"ready":true}';
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 204
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.updateCoachReadiness('1',true);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/coachs/1", { method: 'PATCH', headers, body: expectedBody });
            expect(result).toBe(true);
        });
    });
    describe('createSquadForEdition', () => {
        it('call fetch function with POST and suffix path editions/1/squads', async () => {
            // Given
            const squad = {
                id: "0",
                editionId: "3",
                name: "a squad",
                teamMates: [
                    { id: "2" },
                    { id: "3" }
                ]
            };
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = JSON.stringify(squad);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 201, headers: {
                        'Location': 'https://something/squads/2'
                    }
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.createSquadForEdition(squad);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/3/squads", { method: 'POST', headers, body: expectedBody });
            expect(result).toBe('2');
        });
        it('should return null if fetch function return non 201 status', async () => {
            // Given
            const squad = {
                id: "0",
                editionId: "3",
                name: "a squad",
                teamMates: [
                    { id: "2" },
                    { id: "3" }
                ]
            };
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = JSON.stringify(squad);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, { status: 500 });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.createSquadForEdition(squad);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/3/squads", { method: 'POST', headers, body: expectedBody });
            expect(result).toBe(null);
        });
    });
    describe('updateSquad', () => {
        it('should call fetch function with PUT and suffix path squads/1', async () => {
            // Given
            const squad = {
                id: "1",
                editionId: "2",
                name: "a squad",
                teamMates: [
                    { id: "2", name: "a coach", teamName: "a team", "ready": true, faction: "a faction" },
                    { id: "3", name: "another coach", teamName: "another team", ready: true, faction: "a faction" }
                ]
            };
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = JSON.stringify(squad);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 204
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.updateSquad(squad);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/squads/1", { method: 'PUT', headers, body: expectedBody });
            expect(result).toBe(true);
        });
        it('should return false if fetch function return non 204 status', async () => {
            // Given
            const squad = {
                id: "1",
                editionId: "2",
                name: "a squad",
                teamMates: [
                    { id: "2", name: "a coach", teamName: "a team", ready: true, faction: "a faction" },
                    { id: "3", name: "another coach", teamName: "another team", ready: true, faction: "a faction" }
                ]
            };
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = JSON.stringify(squad);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 500
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.updateSquad(squad);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/squads/1", { method: 'PUT', headers, body: expectedBody });
            expect(result).toBe(false);
        });
    });
    describe('updateSquadReadiness', () => {
        it('should call fetch function with PATCH and suffix path squads/1', async () => {
            // Given
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = '{"ready":true}';
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 204
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.updateSquadReadiness('1',true);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/squads/1", { method: 'PATCH', headers, body: expectedBody });
            expect(result).toBe(true);
        });
        it('should return false if fetch function return non 204 status', async () => {
            // Given
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = '{"ready":true}';
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 500
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.updateSquadReadiness('1',true);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/squads/1", { method: 'PATCH', headers, body: expectedBody });
            expect(result).toBe(false);
        });
    });
    describe('createGameForEdition', () => {
        const gameHelper = () => ({
            editionId: "3",
            editionName: "An edition",
            round: 1,
            table: 2,
            coachId1: "3",
            coachId2: "4",
            played: false,
            finale: false
        });
        it('should call fetch function with POST and suffix path editions/1/games', async () => {
            // Given
            const game = gameHelper();
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = JSON.stringify(game);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 201, headers: {
                        'Location': 'https://something/games/2'
                    }
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.createGameForEdition(game);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/3/games", { method: 'POST', headers, body: expectedBody });
            expect(result).toBe('2');
        });
        it('should return null if fetch function returns different from 201', async () => {
            // Given
            const game = gameHelper();
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 500
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.createGameForEdition(game);
            // Then
            expect(result).toBe(null);
        });
    });
    describe('getGamesByEdition',()=>{
        it('should call fetch function with suffix path editions/1/games',async ()=>{
            // Given
            const games = fake2NotPlayedGames();
            const responseBody = JSON.stringify(games);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(responseBody, {
                    status: 200
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.getGamesByEdition("2");
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/2/games");
            expect(result).toEqual(games);
        });
    });
    describe('getPlayedGamesByEdition',()=>{
        it('should call fetch function with suffix path editions/1/playedGames',async ()=>{
            // Given
            const games = fake2PlayedGames();
            const responseBody = JSON.stringify(games);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(responseBody, {
                    status: 200
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.getPlayedGamesByEdition("2");
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/2/playedGames");
            expect(result).toEqual(games);
        });
    });
    describe('getGame', () => {
        it('should call fetch function with suffix path games/101', async () => {
            // Given
            const game = fakeNotPlayedGame();
            const responseBody = JSON.stringify(game);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(responseBody, {
                    status: 200
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            await layer.getGame(game.id);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/games/101");
        });
    });
    describe('updateGame', () => {
        it('should call fetch function with PUT and suffix path games/101', async () => {
            // Given
            const game = gameToGameForUpdate(fakePlayedGame());
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = JSON.stringify(game);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 204
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.updateGame(game);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/games/101", { method: 'PUT', headers, body: expectedBody });
            expect(result).toBe(true);
        });
        it('should return false if fetch function return non 204 status', async () => {
            // Given
            const game = gameToGameForUpdate(fakePlayedGame());
            const headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            const expectedBody = JSON.stringify(game);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 500
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.updateGame(game);
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/games/101", { method: 'PUT', headers, body: expectedBody });
            expect(result).toBe(false);
        });
    });
    describe('deleteGame',()=>{
        it('should call fetch function with DELETE and suffix path games/101',async ()=>{
            // Given
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 204
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.deleteGame("101");
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/games/101",{method:'DELETE'});
            expect(result).toBe(true);
        });
        it('should return false if fetch function return non 204 status',async ()=>{
            // Given
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(null, {
                    status: 500
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.deleteGame("101");
            // Then
            expect(result).toBe(false);
        });
    });
    describe('getCoachRanking', () => {
        it('should call fetch function with suffix path editions/1/coachRankings/main', async () => {
            // Given
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(`[
{
    "id": "101",
    "editionId":"2",
    "editionName":"Fake 2",
    "name": "Coach A",
    "teamName": "team A",
    "faction": "faction 1",
    "nafNumber":102,
    "netCasualties":5,
    "netTd":6,
    "opponentsPoints":7,
    "points":8,
    "squadId":"31"
},
{
    "id": "103",
    "editionId":"2",
    "editionName":"Fake 2",
    "name": "Coach B",
    "teamName": "team B",
    "faction": "faction 1",
    "nafNumber":104,
    "netCasualties":9,
    "netTd":10,
    "opponentsPoints":11,
    "points":12,
    "squadId":"32"
}]`, {
                    status: 200
                });
            });
            const expected = [
                {
                    position: 1,
                    id: '101',
                    editionId:"2",
                    editionName:"Fake 2",
                    name: 'Coach A',
                    nafNumber: 102,
                    faction: 'faction 1',
                    teamName: 'team A',
                    squadId:"31",
                    params: new Map<string,number>().set('netCasualties',5).set('netTd',6).set('opponentsPoints',7).set('points',8)
                },
                {
                    position: 2,
                    id: '103',
                    editionId:"2",
                    editionName:"Fake 2",
                    name: 'Coach B',
                    nafNumber: 104,
                    faction: 'faction 1',
                    teamName: 'team B',
                    squadId:"32",
                    params: new Map<string,number>().set('netCasualties', 9).set('netTd', 10).set('opponentsPoints', 11).set('points', 12)
                }
            ]
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.getCoachRanking("2","main");
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/2/coachRankings/main");
            expect(result).toEqual(expected);
        });
    });
    describe('getSquadRanking', () => {
        it('should call fetch function with suffix path editions/1/squadRankings/main', async () => {
            // Given
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(`[
{
    "id": "30",
    "editionId":"2",
    "name": "Squad A",
    "netCasualties":5,
    "netTd":6,
    "opponentsPoints":7,
    "points":8,
    "teamMates": [
        {
            "id":"101",
            "name":"Coach A",
            "faction": "faction 1",
            "nafNumber":102,
            "ready": true,
            "teamName": "team A"
        },{
            "id":"103",
            "name":"Coach B",
            "faction": "faction 1",
            "nafNumber":104,
            "ready": true,
            "teamName": "team B"
        },{
            "id":"105",
            "name":"Coach C",
            "faction": "faction 1",
            "nafNumber":106,
            "ready": true,
            "teamName": "team C"
        }
    ]
},
{
    "id": "31",
    "editionId":"2",
    "name": "Squad B",
    "netCasualties":9,
    "netTd":10,
    "opponentsPoints":11,
    "points":12,
    "teamMates":[
        {
            "id":"107",
            "name":"Coach D",
            "faction": "faction 1",
            "nafNumber":108,
            "ready": true,
            "teamName": "team D"
        },{
            "id":"109",
            "name":"Coach E",
            "faction": "faction 1",
            "nafNumber":110,
            "ready": true,
            "teamName": "team E"
        },{
            "id":"111",
            "name":"Coach F",
            "faction": "faction 1",
            "nafNumber":112,
            "ready": true,
            "teamName": "team F"
        }
    ]
}]`, {
                    status: 200
                });
            });
            const expected = [
                {
                    position: 1,
                    id: '30',
                    editionId:"2",
                    name: 'Squad A',
                    teamMates:[
                        {
                            id:"101",
                            name:"Coach A",
                            faction: "faction 1",
                            nafNumber:102,
                            ready: true,
                            teamName: "team A"
                        },{
                            id:"103",
                            name:"Coach B",
                            faction: "faction 1",
                            nafNumber:104,
                            ready: true,
                            teamName: "team B"
                        },{
                            id:"105",
                            name:"Coach C",
                            faction: "faction 1",
                            nafNumber:106,
                            ready: true,
                            teamName: "team C"
                        }
                    ],
                    params: new Map<string,number>().set('netCasualties',5).set('netTd',6).set('opponentsPoints',7).set('points',8)
                },
                {
                    position: 2,
                    id: '31',
                    editionId:"2",
                    name: 'Squad B',
                    teamMates:[
                        {
                            id:"107",
                            name:"Coach D",
                            faction: "faction 1",
                            nafNumber:108,
                            ready: true,
                            teamName: "team D"
                        },{
                            id:"109",
                            name:"Coach E",
                            faction: "faction 1",
                            nafNumber:110,
                            ready: true,
                            teamName: "team E"
                        },{
                            id:"111",
                            name:"Coach F",
                            faction: "faction 1",
                            nafNumber:112,
                            ready: true,
                            teamName: "team F"
                        }
                    ],
                    params: new Map<string,number>().set('netCasualties', 9).set('netTd', 10).set('opponentsPoints', 11).set('points', 12)
                }
            ]
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.getSquadRanking("2","main");
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/editions/2/squadRankings/main");
            expect(result).toEqual(expected);
        });
    });
    describe('getGamesBySquad',()=>{
        it('should call fetch function with suffix path squads/30/games',async ()=>{
            // Given
            const games = fake2NotPlayedGames();
            const responseBody = JSON.stringify(games);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(responseBody, {
                    status: 200
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.getGamesBySquad("30");
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/squads/30/games");
            expect(result).toEqual(games);
        });
    });
    describe('getGamesByCoach',()=>{
        it('should call fetch function with suffix path coaches/101/games',async ()=>{
            // Given
            const games = fake2NotPlayedGames();
            const responseBody = JSON.stringify(games);
            const mockedFetch = vi.fn(async (_url, _something) => {
                return new Response(responseBody, {
                    status: 200
                });
            });
            const layer = new DataLayer("https://something", mockedFetch);
            // When
            const result = await layer.getGamesByCoach("101");
            // Then
            expect(mockedFetch).toHaveBeenCalledWith("https://something/coachs/101/games");
            expect(result).toEqual(games);
        });
    });
});