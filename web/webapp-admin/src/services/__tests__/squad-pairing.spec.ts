import { describe, expect, it } from "vitest";
import { pairByRanking, pairRandomly } from "../squad-pairing";
import { confrontationToBeCreatedToGamesForCreation } from "../confrontation";

describe('pairRandomly', () => {
    it('should return random confrontation', () => {
        // Given
        const squads = [{
            id: "1",
            editionId: "1",
            name: "Squad A",
            teamMates: [
                { id: "11", name: "Amerigo", teamName: "Team of Amerigo" },
                { id: "21", name: "Ania", teamName: "Team of Ania" }
            ]
        }, {
            id: "2",
            editionId: "1",
            name: "Squad B",
            teamMates: [
                { id: "12", name: "Bernardo", teamName: "Team of Bernardo" },
                { id: "22", name: "Biancana", teamName: "Team of Biancana" }
            ]
        }, {
            id: "3",
            editionId: "1",
            name: "Squad C",
            teamMates: [
                { id: "13", name: "Carlito", teamName: "Team of Carlito" },
                { id: "23", name: "Carla", teamName: "Team of Carla" }
            ]
        }, {
            id: "4",
            editionId: "1",
            name: "Squad D",
            teamMates: [
                { id: "14", name: "Daniele", teamName: "Team of Daniele" },
                { id: "24", name: "Dominica", teamName: "Team of Dominica" }
            ]
        }];
        const round = 3;
        // When
        const confrontations = pairRandomly(squads, round, 1, new Map<string, string[]>());
        const result = confrontations.flatMap(confrontationToBeCreatedToGamesForCreation);
        // Then
        expect(result).toHaveLength(4);
        // Games used the given round
        expect(result.every(game => game.round === round)).toBe(true);
        // Games implied not people from different squads
        expect(result.every(game => +(game.coachId1) !== +(game.coachId2) % 10)).toBe(true);
        // Games implied different names
        const usedCoachIds = result.flatMap((game) => [game.coachId1, game.coachId2]);
        expect(new Set(usedCoachIds)).toHaveLength(8);
        // Table is increased by 1 for each game
        expect(result.map(game => game.table)).toEqual([1, 2, 3, 4]);
    });
});
describe('pairByRanking', () => {
    it('should return games by squad', () => {
        // Given
        const editionId = "1";
        const forbidden = new Map<string, string[]>(
            [
                ["1", ["3"]],
                ["2", ["4"]],
                ["3", ["1"]],
                ["4", ["2"]],
            ]
        );
        const squads = [{
            id: "1",
            editionId,
            name: "Squad A",
            teamMates: [
                { id: "11", name: "Amerigo", teamName: "Team of Amerigo" },
                { id: "21", name: "Ania", teamName: "Team of Ania" }
            ]
        }, {
            id: "2",
            editionId,
            name: "Squad B",
            teamMates: [
                { id: "12", name: "Bernardo", teamName: "Team of Bernardo" },
                { id: "22", name: "Biancana", teamName: "Team of Biancana" }
            ]
        }, {
            id: "3",
            editionId,
            name: "Squad C",
            teamMates: [
                { id: "13", name: "Carlito", teamName: "Team of Carlito" },
                { id: "23", name: "Carla", teamName: "Team of Carla" }
            ]
        }, {
            id: "4",
            editionId,
            name: "Squad D",
            teamMates: [
                { id: "14", name: "Daniele", teamName: "Team of Daniele" },
                { id: "24", name: "Dominica", teamName: "Team of Dominica" }
            ]
        }];
        const round = 3;
        const expectedConfrontations = [
            {
                editionId,
                round,
                finale: false,
                squadId1: "1",
                squadId2: "2",
                squadName1: "Squad A",
                squadName2: "Squad B",
                games: [
                    {
                        table: 1,
                        coachId1: "11",
                        coachId2: "12",
                        coachName1: "Amerigo",
                        coachName2: "Bernardo",
                    }, {
                        table: 2,
                        coachId1: "21",
                        coachId2: "22",
                        coachName1: "Ania",
                        coachName2: "Biancana",
                    }
                ]
            },
            {
                editionId,
                round,
                finale: false,
                squadId1: "3",
                squadId2: "4",
                squadName1: "Squad C",
                squadName2: "Squad D",
                games: [
                    {
                        table: 3,
                        coachId1: "13",
                        coachId2: "14",
                        coachName1: "Carlito",
                        coachName2: "Daniele",
                    }, {
                        table: 4,
                        coachId1: "23",
                        coachId2: "24",
                        coachName1: "Carla",
                        coachName2: "Dominica",
                    }
                ]
            }
        ]
        // When
        const result = pairByRanking(squads, round, 1, forbidden);
        // Then
        expect(result).toEqual(expectedConfrontations);
    });
});