import { describe, expect, test } from "vitest";
import { squadValidator, displayMembers, isReady, fakeSquad } from "../squad";

describe('SquadValidator', () => {
    test('should validate squad', () => {
        // Given
        const input = fakeSquad();
        // When
        const result = squadValidator.safeParse(input);
        // Then
        expect(result.success).toBe(true);
    });
});

describe('displayMembers', () => {
    test('should display members separated by comma except the last one with &', () => {
        // Given
        const input = fakeSquad();
        // When
        const result = displayMembers(input);
        // Then
        expect(result).toBe("Coach A, Coach B & Coach E");
    });
 });

describe('isReady', () => {
    test.each([
        [true, true, true],
        [false, false, false],
        [false, true, false],
        [false, false, false]
    ])('should return %s when coach1 is ready:%s and coach2 is ready:%s', (expected, coach1Ready, coach2Ready) => {
        // Given
        const squad = {
            id: "1",
            editionId: "1",
            editionName: "An edition",
            name: "A Squad",
            teamMates: [
                { id: "1", name: "Coach 1", teamName: "A team", ready: coach1Ready, faction: "a faction" },
                { id: "2", name: "Coach 2", teamName: "Another team", ready: coach2Ready, faction: "a faction" }
            ],
        };
        // When
        const result = isReady(squad);
        // Then
        expect(result).toBe(expected);
    });
});