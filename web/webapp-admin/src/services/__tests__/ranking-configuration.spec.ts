import { describe, expect, it } from "vitest";
import { summarizeRankingField } from "../ranking-configuration";

describe('summarize_ranking_field', () => {
    it.each([
        [{
            label:"TD",
            field: "Touchdown",
            type: "For",
            descending: true
        },'TD: Based on Touchdown For descending'],
        [{
            label:"Defense",
            field: "Touchdown",
            type: "Against",
            descending: false
        },'Defense: Based on Touchdown Against ascending'],
    ])('should summarize given ranking', (field,expected) => {
        // Given    
        // When
        const result = summarizeRankingField(field);
        // Then
        expect(result).toContain(expected);

    });
});