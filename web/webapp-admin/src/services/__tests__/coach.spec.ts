import { describe, expect, it } from "vitest";
import { coachValidator, fakeCoach, fakeSummaryCoach, summaryCoachValidator } from "../coach";

describe('CoachValidator', () => {
    it('should validate fakeCoach', () => {
        // Given
        const input = fakeCoach();
        // When
        const result = coachValidator.safeParse(input);
        // Then
        expect(result.success).toBe(true);
    });
    it('should invalidate fakeCoach without name', () => {
        // Given
        const { name, ...input } = fakeCoach();
        // When
        const result = coachValidator.safeParse(input);
        // Then
        expect(result.success).toBe(false);
    });
    it('should invalidate fakeSummaryCoach', () => {
        // Given
        const input = fakeSummaryCoach();
        // When
        const result = coachValidator.safeParse(input);
        // Then
        expect(result.success).toBe(false);
    });
});

describe('SummaryCoachValidator', () => {
    it('should validate fakeSummaryCoach', () => {
        // Given
        const input = fakeSummaryCoach();
        // When
        const result = summaryCoachValidator.safeParse(input);
        // Then
        expect(result.success).toBe(true);
    });
    it('should validate fakeCoach', () => {
        // Given
        const input = fakeCoach();
        // When
        const result = summaryCoachValidator.safeParse(input);
        // Then
        expect(result.success).toBe(true);
    });
    it('should invalidate fakeSummaryCoach without name', () => {
        // Given
        const { name, ...input } = fakeSummaryCoach();
        // When
        const result = summaryCoachValidator.safeParse(input);
        // Then
        expect(result.success).toBe(false);
    });
});