import { beforeEach, describe, expect, it, vi } from "vitest";
import { fakeEdition } from "../edition";
import { cvsToSquadsForBulkCreation, saveSquads } from "../loader";
import type { IDataLayer } from "../i-data-layer";

describe('cvsToSquadsForBulkCreation', () => {
    it('should take a csv content with email and return the adapted SquadAndCoachForCreation array', () => {
        // Given
        const csvContent = `coach_team_name,email,coach_1_name,coach_1_naf,coach_1_race,coach_2_name,coach_2_naf,coach_2_race,coach_3_name,coach_3_naf,coach_3_race,
        crazy team,crazy@team.org,crazy_one,101547,faction 1,crazy_two,101548,faction 2,crazy_three,101549,faction 1,
        socks_bowl,socks@bowl.org,trambi,99,faction 2,longshot,101,faction 1,biel,0,faction 1,`;
        const edition = fakeEdition();
        const expected = {
            data: [
                {
                    name: 'crazy team',
                    editionId: '2',
                    teamMates: [
                        { name: 'crazy_one', nafNumber: 101547, faction: 'faction 1' },
                        { name: 'crazy_two', nafNumber: 101548, faction: 'faction 2' },
                        { name: 'crazy_three', nafNumber: 101549, faction: 'faction 1' },
                    ]
                }, {
                    name: 'socks_bowl',
                    editionId: '2',
                    teamMates: [
                        { name: 'trambi', nafNumber: 99, faction: 'faction 2' },
                        { name: 'longshot', nafNumber: 101, faction: 'faction 1' },
                        { name: 'biel', nafNumber: 0, faction: 'faction 1' },
                    ]
                }
            ],
            errors: []
        };
        // When
        const result = cvsToSquadsForBulkCreation(csvContent, edition);
        // Then
        expect(result).toEqual(expected)
    });
    it('should take a csv content without email and return the adapted SquadAndCoachForCreation array', () => {
        // Given
        const csvContent = `coach_team_name,coach_1_name,coach_1_naf,coach_1_race,coach_2_name,coach_2_naf,coach_2_race,coach_3_name,coach_3_naf,coach_3_race,
        crazy team,crazy_one,101547,faction 1,crazy_two,101548,faction 2,crazy_three,101549,faction 1,
        socks_bowl,trambi,99,faction 2,longshot,101,faction 1,biel,0,faction 1,`;
        const edition = fakeEdition();
        const expected = {
            data: [
                {
                    name: 'crazy team',
                    editionId: '2',
                    teamMates: [
                        { name: 'crazy_one', nafNumber: 101547, faction: 'faction 1' },
                        { name: 'crazy_two', nafNumber: 101548, faction: 'faction 2' },
                        { name: 'crazy_three', nafNumber: 101549, faction: 'faction 1' },
                    ]
                }, {
                    name: 'socks_bowl',
                    editionId: '2',
                    teamMates: [
                        { name: 'trambi', nafNumber: 99, faction: 'faction 2' },
                        { name: 'longshot', nafNumber: 101, faction: 'faction 1' },
                        { name: 'biel', nafNumber: 0, faction: 'faction 1' },
                    ]
                }
            ],
            errors: []
        };
        // When
        const result = cvsToSquadsForBulkCreation(csvContent, edition);
        // Then
        expect(result).toEqual(expected)
    });
});
// Unit tests for saveSquads function
describe('saveSquads', () => {
    let mockDataLayer: IDataLayer;

    beforeEach(() => {
        mockDataLayer = {
            createCoachForEdition: vi.fn(),
            createSquadForEdition: vi.fn()
        };
    });

    it('should save coaches and squads correctly', async () => {
        // Given
        const editionId = 'edition1';
        const squads = [{
            name: 'Squad 1',
            editionId: '1',
            teamMates: [
                { name: 'Coach 1', nafNumber: 1, faction: 'Orcs' },
                { name: 'Coach 2', nafNumber: 2, faction: 'Humans' }
            ]
        }];
        const [expectedCoach1, expectedCoach2] = [{
            name: 'Coach 1',
            nafNumber: 1,
            faction: 'Orcs',
            editionId: 'edition1'
        }, {
            name: 'Coach 2',
            nafNumber: 2,
            faction: 'Humans',
            editionId: 'edition1'
        }
        ];
        const expectedSquad = {
            name: 'Squad 1',
            editionId: '1',
            teamMates: [
                { name: 'Coach 1', nafNumber: 1, faction: 'Orcs', id: 'coach1_id' },
                { name: 'Coach 2', nafNumber: 2, faction: 'Humans', id: 'coach2_id' }
            ]
        }
        mockDataLayer.createCoachForEdition = vi.fn().mockResolvedValueOnce('coach1_id').mockResolvedValueOnce('coach2_id');

        // When
        await saveSquads(mockDataLayer, editionId, squads);

        // Then
        expect(mockDataLayer.createCoachForEdition).toHaveBeenCalledTimes(2);
        expect(mockDataLayer.createCoachForEdition).toHaveBeenCalledWith(expectedCoach1);
        expect(mockDataLayer.createCoachForEdition).toHaveBeenCalledWith(expectedCoach2);
        expect(mockDataLayer.createSquadForEdition).toHaveBeenCalledTimes(1);
        expect(mockDataLayer.createSquadForEdition).toHaveBeenCalledWith(expectedSquad);
    });

    it('should handle null coach ids', async () => {
        // Given
        const editionId = 'edition1';
        const squads = [{
            name: 'Squad 1',
            editionId: '1',
            teamMates: [
                { name: 'Coach 1', nafNumber: 1, faction: 'Orcs' }
            ]
        }];

        mockDataLayer.createCoachForEdition = vi.fn().mockResolvedValueOnce(null);

        // When
        await saveSquads(mockDataLayer, editionId, squads);

        // Then
        expect(mockDataLayer.createCoachForEdition).toHaveBeenCalledTimes(1);
        expect(mockDataLayer.createSquadForEdition).toHaveBeenCalledTimes(1);
    });
});

