import { describe, expect, it } from "vitest";
import { editionValidator, fakeEdition, prepareNextRound } from "../edition";
import { fakeSquads } from "../squad";
import { fake6Coachs } from "../coach";

describe('EditionValidator', () => {
    it('should validate', () => {
        // Given
        const input = fakeEdition();
        // When
        const result = editionValidator.safeParse(input);
        // Then
        expect(result.success).toBe(true);
    });
});

describe('prepareNextRound()',()=>{
    it.each([
        [0],[1],[2],[3],[4]
    ])('should return confrontations to be created if the edition is fullSquad and currentRound %s under roundNumber', (currentRound)=>{
        // Given
        const edition = fakeEdition();
        edition.fullSquad = true;
        edition.currentRound = currentRound;
        const squads = fakeSquads();
        const coachs = fake6Coachs();
        // When
        const result = prepareNextRound(edition,[],coachs,squads);
        // Then
        expect(result).toHaveLength(1);
    });
    it.each([
        [0],[1],[2],[3],[4]
    ])('should return games to be created if the edition is not fullSquad and currentRound %s under roundNumber', (currentRound)=>{
        // Given
        const edition = fakeEdition();
        edition.fullSquad = false;
        edition.currentRound = currentRound;
        const squads = fakeSquads();
        const coachs = fake6Coachs();
        // When
        const result = prepareNextRound(edition,[],coachs,squads);
        // Then
        expect(result).toHaveLength(3);
    });
    it.each([
        [true],[false]
    ])('should return empty array for currentRound >= roundNumber with fullSquad:%s',(fullSquad: boolean)=>{
        // Given
        const edition = fakeEdition();
        edition.fullSquad = fullSquad;
        edition.currentRound = edition.roundNumber;
        const squads = fakeSquads();
        const coachs = fake6Coachs();
        // When
        const result = prepareNextRound(edition,[],coachs,squads);
        // Then
        expect(result).toHaveLength(0);
    });
    it.each([
        [true],[false]
    ])('should return first object as finale if useFinale is true and currentRound = roundNumber -1 with fullSquad:%s',(fullSquad: boolean)=>{
        // Given
        const edition = fakeEdition();
        edition.fullSquad = fullSquad;
        edition.useFinale = true;
        edition.currentRound = edition.roundNumber - 1;
        const squads = fakeSquads();
        const coachs = fake6Coachs();
        // When
        const result = prepareNextRound(edition, [], coachs, squads);
        // Then
        expect(result.length).toBeGreaterThanOrEqual(1);
        expect(result[0].finale).toBe(true);
    });
});