import { describe, expect, test } from "vitest";
import { add_days, get_iso_8660_date} from '../date';

describe('get_iso_8660_date',()=>{
  test.each([
    ['13 January 2025','2025-01-13'],
    ['1 January 2025','2025-01-01'],
    ['31 December 2024','2024-12-31'],
    ['31 December 999','0999-12-31'],
  ])('Given %s should return %s',(plainDate: string, expected: string)=>{
    // Given
    const date = new Date(plainDate + ' 00:00Z');
    // When
    const result = get_iso_8660_date(date);
    // Then
    expect(result).toEqual(expected);
  })
});

describe('add_days',()=>{
  test.each([
    ['1 January 2024',1,'2 January 2024'],
    ['31 January 2024',1,'1 February 2024'],
    ['31 December 2024',1,'1 January 2025'],
    ['31 December 2024',3,'3 January 2025'],
  ])('Given %s and %d should return %s',(initialDate: string, delta: number, expected: string)=>{
    // Given
    const date = new Date(initialDate + ' 00:00Z');
    const expectedDate = new Date(expected + ' 00:00Z');
    // When
    const result = add_days(date,delta);
    //Then
    expect(result).toEqual(expectedDate);
  })
});