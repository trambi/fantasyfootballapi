import { describe, expect, it } from "vitest";
import { gameValidator, notPlayedGameValidator, playedGameValidator, gameToBeCreatedToGameForCreation, fakeToBeCreatedGame, fakeNotPlayedGame, fakePlayedGame, gameToGameForUpdate } from "../game";

describe('Validators', () => {
    describe('gameValidator', () => {
        it('should invalidate a to be created game', () => {
            // Given
            const input = fakeToBeCreatedGame();
            // When
            const result = gameValidator.safeParse(input);
            // Then
            expect(result.success).toBe(false);
        });
        it('should validate a unplayed game', () => {
            // Given
            const input = fakeNotPlayedGame();
            // When
            const result = gameValidator.safeParse(input);
            // Then
            if (!result.success) {
                console.log(result.error);
                expect(false).toBe(true)
            } else {
                expect(result.success).toBe(true);
                expect(result.data).toEqual(input);
            }
        });
        it('should validate a played game', () => {
            // Given
            const input = fakePlayedGame();
            // When
            const result = gameValidator.safeParse(input);
            // Then
            if (!result.success) {
                console.log(result.error);
                expect(false).toBe(true)
            } else {
                expect(result.success).toBe(true);
                expect(result.data).toEqual(input);
            }
        });
    });
    describe('notPlayedGameValidator',()=>{
        it('should invalidate a to be created game', () => {
            // Given
            const input = fakeToBeCreatedGame();
            // When
            const result = notPlayedGameValidator.safeParse(input);
            // Then
            expect(result.success).toBe(false);
        });
        it('should validate a unplayed game', () => {
            // Given
            const input = fakeNotPlayedGame();
            // When
            const result = notPlayedGameValidator.safeParse(input);
            // Then
            if (!result.success) {
                console.log(result.error);
                expect(false).toBe(true)
            } else {
                expect(result.success).toBe(true);
                expect(result.data).toEqual(input);
            }
        });
        it('should invalidate a played game', () => {
            // Given
            const input = fakePlayedGame();
            // When
            const result = notPlayedGameValidator.safeParse(input);
            // Then
            expect(result.success).toBe(false);
        });
    });
    describe('playedGameValidator',()=>{
        it('should invalidate a to be created game', () => {
            // Given
            const input = fakeToBeCreatedGame();
            // When
            const result = playedGameValidator.safeParse(input);
            // Then
            expect(result.success).toBe(false);
        });
        it('should invalidate a unplayed game', () => {
            // Given
            const input = fakeNotPlayedGame();
            // When
            const result = playedGameValidator.safeParse(input);
            // Then
            expect(result.success).toBe(false);
        });
        it('should validate a played game', () => {
            // Given
            const input = fakePlayedGame();
            // When
            const result = playedGameValidator.safeParse(input);
            // Then
            // Then
            if (!result.success) {
                console.log(result.error);
                expect(false).toBe(true)
            } else {
                expect(result.success).toBe(true);
                expect(result.data).toEqual(input);
            }
        });
    });
});

describe('gameToBeCreatedToGameForCreation',()=>{
    it('should convert a to be created game to a game for creation', () => {
        // Given
        const input = fakeToBeCreatedGame();
        const expected = {
            editionId:input.editionId,
            round:input.round,
            table:input.table,
            played:false,
            finale:input.finale,
            coachId1:input.coachId1,
            coachId2:input.coachId2,
        };
        // When
        const result = gameToBeCreatedToGameForCreation(input);
        // Then
        expect(result).toEqual(expected);
    });
});

describe('gameToGameForUpdate',()=>{
    it('should convert a played game to gameForUpdate',()=>{
        // Given
        const input = fakePlayedGame();
        const expected = {
            id: input.id,
            editionId:input.editionId,
            round:input.round,
            table:input.table,
            played:input.played,
            finale:input.finale,
            coachId1:input.coach1.id,
            coachId2:input.coach2.id,
            td1: input.td1,
            td2: input.td2,
            casualties1: input.casualties1,
            casualties2: input.casualties2,
            completions1: input.completions1,
            completions2: input.completions2,
            fouls1: input.fouls1,
            fouls2: input.fouls2,
            special1: input.special1,
            special2: input.special2,
        };
        // When
        const result = gameToGameForUpdate(input);
        // Then
        expect(result).toEqual(expected);
    });
    it('should convert a not played game to gameForUpdate',()=>{
        // Given
        const input = fakeNotPlayedGame();
        const expected = {
            id: input.id,
            editionId:input.editionId,
            round:input.round,
            table:input.table,
            played:input.played,
            finale:input.finale,
            coachId1:input.coach1.id,
            coachId2:input.coach2.id,
            td1: 0,
            td2: 0,
            casualties1: 0,
            casualties2: 0,
            completions1: 0,
            completions2: 0,
            fouls1: 0,
            fouls2: 0,
            special1: '',
            special2: '',
        };
        // When
        const result = gameToGameForUpdate(input);
        // Then
        expect(result).toEqual(expected);
    });
})