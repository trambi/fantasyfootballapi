import { describe, expect, it } from "vitest";
import { fakePointConfiguration, summarizePointConfiguration } from "../point-configuration";

describe('summarizePointConfiguration', () => {
    it('should summarize default point configuration', () => {
        // Given  
        const expected = [
            'Default value: 0',
            'If {TdFor} = {TdAgainst} then value: 2',
            'If {TdFor} > {TdAgainst} then value: 5',
        ]
        // When
        const result = summarizePointConfiguration(fakePointConfiguration());
        // Then
        expect(result).toEqual(expected);

    });
});