import { describe, expect, it } from "vitest";
import { shuffle } from "../shuffle";

describe('shuffle', () => {
    it('should keep same length and same elements', () => {
        // Given
        const arr = [1, 2, 3, 4, 5];
        // When
        const result = shuffle(arr);
        // Then
        expect(result.length).toBe(arr.length);
        expect(result.sort()).toEqual(arr);
    });
});
