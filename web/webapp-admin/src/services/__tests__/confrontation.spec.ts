import { describe, expect, it } from "vitest";
import { confrontationToBeCreatedToGamesForCreation, fakeToBeCreatedConfrontation } from "../confrontation";

describe('confrontationToBeCreatedToGameForCreation', () => {
    it.each([[true], [false]])('should convert a to be created confrontation to array of game for creation with finale:%s', (finale: boolean) => {
        // Given
        const input = fakeToBeCreatedConfrontation();
        input.finale = finale;
        const played = false;
        const expected = [
            {
                editionId: input.editionId,
                round: input.round,
                table: 2,
                finale,
                played,
                coachId1: '100',
                coachId2: '111',
            }, {
                editionId: input.editionId,
                round: input.round,
                table: 3,
                finale,
                played,
                coachId1: '101',
                coachId2: '112',
            }];
        // When
        const result = confrontationToBeCreatedToGamesForCreation(input);
        // Then
        expect(result).toEqual(expected);
    });
});