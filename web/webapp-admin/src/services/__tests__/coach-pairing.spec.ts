import { describe, expect, it } from "vitest";
import { fake6Coachs } from "../coach";
import { pairByCoachOrderly, pairByCoachRandomly } from "../coach-pairing";

describe('pairByCoachRandomly', () => {
    it('should return random game', () => {
        // Given
        const coachs = fake6Coachs();
        const round = 3;
        // When
        const games = pairByCoachRandomly(coachs, round, 1, new Map<string, string[]>());
        // Then
        expect(games).toHaveLength(3);
        // Games used the given round
        expect(games.every(game => game.round === round)).toBe(true);
        // Games implied not people from different squads
        expect(games.every(game => +(game.coachId1) !== +(game.coachId2) % 10)).toBe(true);
        // Games implied different names
        const usedCoachIds = games.flatMap((game) => [game.coachId1, game.coachId2]);
        expect(new Set(usedCoachIds)).toHaveLength(6);
        // Table is increased by 1 for each game
        expect(games.map(game => game.table)).toEqual([1, 2, 3]);
    });
});
describe('pairByCoachOrderly', () => {
    it('should return games by coach', () => {
        // Given
        const editionId = "2";
        const coachs = fake6Coachs();
        const forbidden = new Map<string, string[]>(
            [
                [coachs[0].id, [coachs[1].id, coachs[2].id, coachs[4].id]],
                [coachs[1].id, [coachs[0].id, coachs[4].id]],
                [coachs[2].id, [coachs[1].id, coachs[3].id, coachs[5].id]],
                [coachs[3].id, [coachs[2].id, coachs[5].id]],
                [coachs[4].id, [coachs[0].id, coachs[1].id]],
                [coachs[5].id, [coachs[2].id, coachs[3].id]],
            ]
        );
        const round = 3;
        const finale = false;
        const expectedGames = [
            {
                editionId,
                round,
                finale,
                table: 1,
                coachId1: coachs[0].id,
                coachId2: coachs[3].id,
                coachName1: coachs[0].name,
                coachName2: coachs[3].name,
                squadId1: coachs[0].squadId,
                squadId2: coachs[3].squadId,
                squadName1: coachs[0].squadName,
                squadName2: coachs[3].squadName,
            },
            {
                editionId,
                round,
                finale,
                table: 2,
                coachId1: coachs[1].id,
                coachId2: coachs[2].id,
                coachName1: coachs[1].name,
                coachName2: coachs[2].name,
                squadId1: coachs[1].squadId,
                squadId2: coachs[2].squadId,
                squadName1: coachs[1].squadName,
                squadName2: coachs[2].squadName,
            },
            {
                editionId,
                round,
                finale,
                table: 3,
                coachId1: coachs[4].id,
                coachId2: coachs[5].id,
                coachName1: coachs[4].name,
                coachName2: coachs[5].name,
                squadId1: coachs[4].squadId,
                squadId2: coachs[5].squadId,
                squadName1: coachs[4].squadName,
                squadName2: coachs[5].squadName,
            },
        ]
        // When
        const result = pairByCoachOrderly(coachs, round, 1, forbidden);
        // Then
        expect(result).toEqual(expectedGames);
    });
});