import { z } from "zod";

export const coachValidator = z.object({
  id: z.string(),
  teamName: z.string().optional(),
  name: z.string(),
  nafNumber: z.number().int().nonnegative().optional(),
  faction: z.string(),
  squadId: z.string().optional(),
  squadName: z.string().optional(),
  ready: z.boolean().optional(),
  editionId: z.string(),
  editionName: z.string()
});

export const summaryCoachValidator = z.object({
  id: z.string(),
  name: z.string(),
  nafNumber: z.number().int().nonnegative().optional(),
  faction: z.string(),
  ready: z.boolean(),
  teamName: z.string().optional(),
})

export type SummaryCoach = z.infer<typeof summaryCoachValidator>;

export const coachsValidator = z.array(coachValidator);

export type Coach = z.infer<typeof coachValidator>;

export type CoachForUpdate = Omit<Coach, 'squadId'|'squadName'|'editionName'> ;

export type CoachForCreation = Omit<CoachForUpdate, 'id'> ;

export function fakeCoach(): Coach {
  return {
    id: '100',
    teamName: 'team A',
    name: 'Coach A',
    nafNumber: 4524,
    faction: 'faction 1',
    squadId: '30',
    squadName: 'Squad A',
    ready: true,
    editionId: '2',
    editionName: 'Fake 2'
  };
};

export function fake6Coachs(): Coach[] {
  return [fakeCoach(),{
    id: '101',
    teamName: 'team B',
    name: 'Coach B',
    nafNumber: 4521,
    faction: 'faction 1',
    squadId: '30',
    squadName: 'Squad A',
    ready: true,
    editionId: '2',
    editionName: 'Fake 2'
  },{
    id: '111',
    teamName: 'team C',
    name: 'Coach C',
    nafNumber: 11782,
    faction: 'faction 2',
    squadId: '31',
    squadName: 'Squad B',
    ready: true,
    editionId: '2',
    editionName: 'Fake 2'
  },{
    id: '112',
    teamName: 'team D',
    name: 'Coach D',
    nafNumber: 13580,
    faction: 'faction 2',
    squadId: '31',
    squadName: 'Squad B',
    ready: true,
    editionId: '2',
    editionName: 'Fake 2'
  },{
    id: '113',
    teamName: 'team E',
    name: 'Coach E',
    nafNumber: 13100,
    faction: 'faction 2',
    squadId: '30',
    squadName: 'Squad A',
    ready: true,
    editionId: '2',
    editionName: 'Fake 2'
  },{
    id: '114',
    teamName: 'team F',
    name: 'Coach F',
    nafNumber: 14899,
    faction: 'faction 1',
    squadId: '31',
    squadName: 'Squad B',
    ready: true,
    editionId: '2',
    editionName: 'Fake 2'
  }];
};

export function fakeSummaryCoach(): SummaryCoach {
  return {
    id: '100',
    name: 'Coach A',
    nafNumber: 111,
    faction: 'faction 1',
    ready: true
  };
};

export function fake6SummaryCoachs(): SummaryCoach[] {
  return [fakeSummaryCoach(),{
    id: '101',
    name: 'Coach B',
    nafNumber: 4521,
    faction: 'faction 1',
    ready: true
  },{
    id: '111',
    name: 'Coach C',
    nafNumber: 11782,
    faction: 'faction 2',
    ready: true
  },{
    id: '112',
    name: 'Coach D',
    nafNumber: 13580,
    faction: 'faction 2',
    ready: true
  },{
    id: '113',
    name: 'Coach E',
    nafNumber: 13100,
    faction: 'faction 2',
    ready: true
  },{
    id: '114',
    name: 'Coach F',
    nafNumber: 14899,
    faction: 'faction 1',
    ready: true
  }];
};