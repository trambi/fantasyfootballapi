import type { DataLayer } from "./data-layer";
import type { Edition } from "./edition";
import type { IDataLayer } from "./i-data-layer";
import type { SquadForBulkCreation } from "./squad";
import Papa from "papaparse";

/**
 * Transforms CSV data into a squad object for bulk creation.
 *
 * @param {Edition} edition - The edition object containing information about the number of coaches per squad.
 * @returns {(input: object) => { name: string; editionId: number; teamMates: { name: string; nafNumber: number; faction: string }[] }} - 
 * A function that takes an input object and returns a squad object.
 */
function csvDataToSquadForBulkCreation(edition: Edition) {
    return function (input: object) {
        const teamMates: { name: string, nafNumber: number, faction: string }[] = [];
        for (let i = 1; i <= edition.coachPerSquad; i++) {
            teamMates.push({
                name: String(input[`coach_${i}_name` as keyof unknown]),
                nafNumber: Number(input[`coach_${i}_naf` as keyof unknown]),
                faction: String(input[`coach_${i}_race` as keyof unknown])
            });
        }
        return {
            name: String(input['coach_team_name' as keyof unknown]).trim(),
            editionId: edition.id,
            teamMates
        };
    }
}

export function cvsToSquadsForBulkCreation(content: string, edition: Edition): { data: SquadForBulkCreation[], errors: string[] } {
    const errors: string[] = [];
    const results = Papa.parse(content, {
        header: true
    });
    const data = results.data as object[];
    return { data: data.map(csvDataToSquadForBulkCreation(edition)), errors: errors };
}

// use dataLayer and save coachs then squad
export async function saveSquads(layer :IDataLayer , editionId: string, squads: SquadForBulkCreation[]) {
    // extract coachs from squads
    const coachs = squads.flatMap(s => s.teamMates);
    const coachsIdByName = new Map<string, string>();
    // save coachs
    await Promise.all(coachs.map(async (coach) => {
        const c = {...coach,editionId};
        const id = await layer.createCoachForEdition(c);
        if (id !== null) {
            coachsIdByName.set(coach.name,id);
        }else{
            console.error('id null for %s',coach.name);
        }
    }));
    //save squads
    await Promise.all(squads.map((squad) => {
        const teamMates = squad.teamMates.map((tm) => {
            const id = coachsIdByName.get(tm.name) as string;
            return {...tm,id};
        });
        const s = {...squad,teamMates};
        layer.createSquadForEdition(s);
    }));
}
