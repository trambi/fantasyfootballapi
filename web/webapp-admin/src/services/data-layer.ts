import { editionValidator, type Edition } from "./edition";
import { coachsValidator, coachValidator, type Coach, type CoachForCreation, type CoachForUpdate } from "./coach";
import { squadValidator, squadsValidator, type Squad, type SquadForCreation, type SquadForUpdate } from "./squad";
import { z } from "zod";
import { gameValidator, gamesValidator, playedGamesValidator, type Game, type GameForCreation, type GameForUpdate, type PlayedGame } from "./game";
import { type CoachRank, type SquadRank } from "./rank";
import type { IDataLayer } from "./i-data-layer";

type FetchFunction = typeof fetch;

interface HasId {
    id: string;
}

interface HasEditionId {
    editionId: string;
}

interface Validator<T> {
    safeParse: (input: string) => z.SafeParseReturnType<T, T>;
}

/**
 * DataLayer class provides a access to REST API server
 */
export class DataLayer implements IDataLayer{
    /**
     * fetch function to use for all requests
     */
    fetchFunction: FetchFunction;
    /**
     * base url of the REST API
     */
    baseUrl: string;
    /**
     * 
     * @param baseUrl base url of the REST API
     * @param fetchFunction fetch function to use for all requests
     */
    public constructor(baseUrl: string, fetchFunction: FetchFunction) {
        this.baseUrl = baseUrl;
        this.fetchFunction = fetchFunction;
    }

    public async getCurrentEdition(): Promise<Edition> {
        const targetUrl = `${this.baseUrl}/editions/current`;
        const response = await this.fetchFunction(targetUrl);
        const decoded = await response.json();
        const result = editionValidator.safeParse(decoded);
        if (!result.success) {
            const msg = "getCurrentEdition: Invalid response from server:" + result.error;
            console.error(msg);
            throw new Error(msg);
        }
        return result.data;
    }

    public async getEdition(id: string): Promise<Edition> {
        return this.getById<Edition>(editionValidator, 'editions', id);
    }

    public async getCoach(id: string): Promise<Coach> {
        return this.getById<Coach>(coachValidator, 'coachs', id);
    }

    public async getSquad(id: string): Promise<Squad> {
        return this.getById<Squad>(squadValidator, 'squads', id);
    }

    public async getGame(id: string): Promise<Game> {
        return this.getById<Game>(gameValidator, 'games', id);
    }

    public async getCoachsByEdition(id: string): Promise<Coach[]> {
        return this.getByEditionId<Coach>(coachsValidator, 'coachs', id);
    }

    public async getSquadsByEdition(id: string): Promise<Squad[]> {
        return this.getByEditionId<Squad>(squadsValidator, 'squads', id);
    }

    public async getGamesByEdition(id: string): Promise<Game[]> {
        return this.getByEditionId<Game>(gamesValidator, 'games', id);
    }

    public async getPlayedGamesByEdition(id: string): Promise<PlayedGame[]> {
        return this.getByEditionId<PlayedGame>(playedGamesValidator, 'playedGames', id);
    }

    public async getGamesByEditionAndRound(id: string, round: number): Promise<Game[]> {
        const targetUrl = `${this.baseUrl}/editions/${id}/rounds/${round}/games`;
        const response = await this.fetchFunction(targetUrl);
        const decoded = await response.json();
        const result = gamesValidator.safeParse(decoded);
        if (!result.success) {
            const msg = "getGamesByEditionAndRound : Invalid response from server:" + result.error;
            console.error(msg);
            throw new Error(msg);
        }
        return result.data;
    }


    public async getGamesBySquad(id: string): Promise<Game[]> {
        const targetUrl = `${this.baseUrl}/squads/${id}/games`;
        return this.fetchAndValidateGames(targetUrl);
    }

    public async getGamesByCoach(id: string): Promise<Game[]> {
        const targetUrl = `${this.baseUrl}/coachs/${id}/games`;
        return this.fetchAndValidateGames(targetUrl);
    }

    private async fetchAndValidateGames(targetUrl: string): Promise<Game[]> {
        const response = await this.fetchFunction(targetUrl);
        const decoded = await response.json();
        const result = gamesValidator.safeParse(decoded);
        if (!result.success) {
            const msg = `Invalid response from server for ${targetUrl}: ${result.error}`;
            console.error(msg);
            throw new Error(msg);
        }
        return result.data;
    }

    public async createEdition(edition: Edition) {
        const targetUrl = `${this.baseUrl}/editions`;
        const body = JSON.stringify(edition);
        const options = {
            method: 'POST',
            body,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        let response;
        try {
            response = await this.fetchFunction(targetUrl, options);
            console.log('response status: ' + response.status);
        } catch (e: unknown) {
            console.error(e);
            throw e;
        }

        if(response?.status === 201){
            return null;
        }
        return response.headers.get('Location');
    }

    public async createCoachForEdition(coach: CoachForCreation) {
        return this.createForEdition<CoachForCreation>('coachs', coach);
    }

    public async createSquadForEdition(squad: SquadForCreation) {
        return this.createForEdition<SquadForCreation>('squads', squad);
    }

    public async createGameForEdition(game: GameForCreation) {
        return this.createForEdition<GameForCreation>('games', game);
    }

    public async updateEdition(edition: Edition) {
        return this.update<Edition>('editions', edition);
    }

    public async updateCoach(coach: CoachForUpdate) {
        return this.update<CoachForUpdate>('coachs', coach);
    }

    public async updateCoachReadiness(id: string, readiness: boolean){
        return this.updateReadiness('coachs', id, readiness);
    }

    public async updateSquad(squad: SquadForUpdate) {
        return this.update<SquadForUpdate>('squads', squad);
    }

    public async updateSquadReadiness(id: string, readiness: boolean){
        return this.updateReadiness('squads', id, readiness);
    }

    protected async updateReadiness(path:string, id: string, readiness: boolean){
        const targetUrl = `${this.baseUrl}/${path}/${id}`;
        const body = JSON.stringify({ready:readiness});
        const options = {
            method: 'PATCH',
            body,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        let response;
        try {
            response = await this.fetchFunction(targetUrl, options);
        } catch (e: unknown) {
            console.error(e);
            throw e;
        }
        return response?.status === 204;
    }

    public async updateGame(game: GameForUpdate) {
        return this.update<GameForUpdate>('games', game);
    }

    public async deleteGame(id: string) {
        return this.delete('games', id);
    }

    public async getCoachRanking(id: string, rankingName: string) {
        const targetUrl = `${this.baseUrl}/editions/${id}/coachRankings/${rankingName}`;
        const response = await this.fetchFunction(targetUrl);
        const rawRanking = await response.json();
        const result: CoachRank[] = [];
        let position = 1;
        for (const rawRank of rawRanking) {
            const parseResult = coachValidator.safeParse(rawRank);
            if (!parseResult.success) {
                const msg = "getCoachRanking: Invalid response from server:" + parseResult.error;
                console.error(msg);
                throw new Error(msg);
            }
            const { id, name, teamName, nafNumber, faction, squadId, squadName, ready, editionId, editionName, ...otherParams } = rawRank;
            const rank = {
                id,
                name,
                teamName,
                nafNumber,
                faction,
                squadId,
                squadName,
                ready,
                editionId,
                editionName,
                position,
                params: new Map<string, number>()
            }
            // Tricky part because we dont known what will be the keys of the other parameters
            for (const param in otherParams) {
                rank.params.set(param, otherParams[param as keyof typeof otherParams]);
            }
            result.push(rank);
            position += 1;
        }
        return result;
    }

    public async getSquadRanking(id: string, rankingName: string) {
        const targetUrl = `${this.baseUrl}/editions/${id}/squadRankings/${rankingName}`;
        const response = await this.fetchFunction(targetUrl);
        const rawRanking = await response.json();
        const result: SquadRank[] = [];
        let position = 1;
        for (const rawRank of rawRanking) {
            const parseResult = squadValidator.safeParse(rawRank);
            if (!parseResult.success) {
                const msg = "getSquadRanking: Invalid response from server:" + parseResult.error;
                console.error(msg);
                throw new Error(msg);
            }
            const { id, name, editionId, teamMates, ...otherParams } = rawRank;
            const rank = {
                id,
                name,
                editionId,
                teamMates,
                position,
                params: new Map<string, number>()
            }
            // Tricky part because we dont known what will be the keys of the other parameters
            for (const param in otherParams) {
                rank.params.set(param, otherParams[param as keyof typeof otherParams]);
            }
            result.push(rank);
            position += 1;
        }
        return result
    }

    protected async getByEditionId<T>(validator: Validator<Array<T>>, path: string, id: string): Promise<Array<T>> {
        const targetUrl = `${this.baseUrl}/editions/${id}/${path}`;
        const response = await this.fetchFunction(targetUrl);
        const decoded = await response.json();
        const result = validator.safeParse(decoded);
        if (!result.success) {
            const msg = "getByEditionId(" + path + ") Invalid response from server:" + result.error
            console.error(msg);
            throw new Error(msg);
        }
        return result.data;
    }

    protected async getById<T>(validator: Validator<T>, path: string, id: string,): Promise<T> {
        const targetUrl = `${this.baseUrl}/${path}/${id}`;
        const response = await this.fetchFunction(targetUrl);
        const decoded = await response.json();
        const result = validator.safeParse(decoded);
        if (!result.success) {
            const msg = "getById(" + path + "Invalid response from server:" + result.error;
            console.error(msg);
            throw new Error(msg);
        }
        return result.data;
    }

    protected async createForEdition<T extends HasEditionId>(path: string, instance: T): Promise<string|null> {
        const targetUrl = `${this.baseUrl}/editions/${instance.editionId}/${path}`;
        const body = JSON.stringify(instance);
        const options = {
            method: 'POST',
            body,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        let response;
        try {
            response = await this.fetchFunction(targetUrl, options);
            console.log('response status: ' + response.status);
        } catch (e: unknown) {
            console.error(e);
            throw e;
        }
        if(response?.status !== 201){
            return null;
        }
        return this.extractIdFromLocation(response);
    };

    protected extractIdFromLocation = (response: Response): string | null => {
        const location = response.headers.get('Location');
        if (!location) {
            console.error('createForEdition: Missing Location header in response');
            return null;
        }
        return location.split('/').pop() ?? null;
    };
    

    protected async update<T extends HasId>(path: string, instance: T) {
        const targetUrl = `${this.baseUrl}/${path}/${instance.id}`;
        const body = JSON.stringify(instance);
        const options = {
            method: 'PUT',
            body,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        let response;
        try {
            response = await this.fetchFunction(targetUrl, options);
        } catch (e: unknown) {
            console.error(e);
            throw e;
        }
        return response?.status === 204;
    }

    protected async delete(path: string, id: string) {
        const targetUrl = `${this.baseUrl}/${path}/${id}`;
        const options = {
            method: 'DELETE',
        };
        let response;
        try {
            response = await this.fetchFunction(targetUrl, options);
        } catch (e: unknown) {
            console.error(e);
            throw e;
        }
        return response?.status === 204;
    }

}