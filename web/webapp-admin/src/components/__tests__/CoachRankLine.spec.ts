import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import { fakeCoachRank } from '@/services/rank';
import CoachRankLine from '../CoachRankLine.vue'

describe('CoachRankLine', () => {
  it('renders coach rank with fields in the right order', () => {
    // Given
    const rank = fakeCoachRank();
    const fields = ['points', 'opponentsPoints', 'netTd', 'netCasualties'];
    const wrapper = mount(CoachRankLine, { props: { rank, fields } });
    
    expect(wrapper.find('th').text()).toContain(rank.position);
    expect(wrapper.find('button:nth-child(1)').text()).toContain(rank.name);
    expect(wrapper.find('td:nth-child(3)').text()).toContain(rank.faction);
    expect(wrapper.find('td:nth-child(4)').text()).toContain(rank.params.get(fields[0]));
    expect(wrapper.find('td:nth-child(5)').text()).toContain(rank.params.get(fields[1]));
    expect(wrapper.find('td:nth-child(6)').text()).toContain(rank.params.get(fields[2]));
    expect(wrapper.find('td:nth-child(7)').text()).toContain(rank.params.get(fields[3]));

  });
  it('should emit info when clicked on coach name', async () => {
    // Given
    const rank = fakeCoachRank();
    const fields = ['points', 'opponentsPoints', 'netTd', 'netCasualties'];
    const wrapper = mount(CoachRankLine, { props: { rank, fields } });
    await wrapper.find('button:nth-child(1)').trigger('click');
    expect(wrapper.emitted()).toHaveProperty('info');
    expect(wrapper.emitted('info')).toEqual([[rank.id]]);
  });
});
