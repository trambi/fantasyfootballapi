import { describe, expect, it } from 'vitest';
import { mount } from '@vue/test-utils';
import PointSummary from '../PointSummary.vue';
import { fakePointConfiguration } from '@/services/point-configuration';

describe('PointSummary', () => {
    it('should render given point configuration', () => {
        // Given
        const pointConfiguration = fakePointConfiguration();
        const expected = [
            "Default value: 0",
            "If {TdFor} = {TdAgainst} then value: 2",
            "If {TdFor} > {TdAgainst} then value: 5",
        ];
        // When
        const wrapper = mount(PointSummary, { props: { pointConfiguration }, });
        // Then
        expect(wrapper.find('li:nth-child(1)').text()).toContain(expected[0]);
        expect(wrapper.find('li:nth-child(2)').text()).toContain(expected[1]);
        expect(wrapper.find('li:nth-child(3)').text()).toContain(expected[2]);
    });
});