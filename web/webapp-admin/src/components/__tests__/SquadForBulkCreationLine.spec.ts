import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import SquadForBulkCreationLine from '../SquadForBulkCreationLine.vue'
import { fakeSquadForBulkCreation } from '@/services/squad';


describe('SquadForBulkCreationLine', () => {
  it('renders properly with a to be created squad', () => {
    expect(true).toBeTruthy();
    /**
     @TODO
     * 
    */
  //   // Given
  //   const squad = fakeSquadForBulkCreation();
  //   const wrapper = mount(SquadForBulkCreationLine, { props: { squad } });
  //   expect(wrapper.find('td:nth-child(1)').text()).toContain(squad.name);
  //   expect(wrapper.find('td:nth-child(2)').text()).toContain(squad.teamMates[0].name);
  //   expect(wrapper.find('td:nth-child(3)').text()).toContain(squad.teamMates[0].faction);
  //   expect(wrapper.find('td:nth-child(4)').text()).toContain(squad.teamMates[0].nafNumber);
  //   expect(wrapper.find('td:nth-child(5)').text()).toContain(squad.teamMates[1].name);
  //   expect(wrapper.find('td:nth-child(6)').text()).toContain(squad.teamMates[1].faction);
  //   expect(wrapper.find('td:nth-child(7)').text()).toContain(squad.teamMates[1].nafNumber);
  //   expect(wrapper.find('td:nth-child(8)').text()).toContain(squad.teamMates[2].name);
  //   expect(wrapper.find('td:nth-child(9)').text()).toContain(squad.teamMates[2].faction);
  //   expect(wrapper.find('td:nth-child(10)').text()).toContain(squad.teamMates[2].nafNumber);
  // });
  // it('should emit delete when clicked on Supprimer', async () => {
  //   // Given
  //   const squad = fakeSquadForBulkCreation();
  //   const wrapper = mount(SquadForBulkCreationLine, { props: { squad } });
  //   // When
  //   await wrapper.find('button.btn-danger').trigger('click');
  //   // Then
  //   expect(wrapper.emitted()).toHaveProperty('delete');
  });
});
