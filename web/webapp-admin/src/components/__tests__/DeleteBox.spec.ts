import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import DeleteBox from '../DeleteBox.vue';
import type { DeleteInfo } from '@/services/deleteinfo';

describe('DeleteBox', () => {
    const fakeDeleteInfo = ()=>({
        target: 'game',
        id: '1',
        name: 'round 1 table 2 - coach 1 vs coach 2'
    } as DeleteInfo);
    const prefixId = 'test';
    it('should display if DeleteInfo is given as props', () => {
        // Given
        const deleteInfo = fakeDeleteInfo();
        // When
        const wrapper = mount(DeleteBox, { props: { deleteInfo, prefixId } });
        // Then
        expect(wrapper.find('span').text()).toEqual('Êtes-vous sûr de vouloir supprimer game \'1\' ?')
    });
    it('should not display if DeleteInfo is not given as props', () => {
        // Given
        // When
        const wrapper = mount(DeleteBox,{ props: { prefixId } });
        // Then
        expect(wrapper.find('span').exists()).toBe(false);
    });
    it('should emit cancel when non is clicked ', async () => {
        // Given
        const deleteInfo = fakeDeleteInfo();
        const wrapper = mount(DeleteBox, { props: { deleteInfo, prefixId } });
        // When
        await wrapper.find('button#test-no').trigger('click');
        // Then
        expect(wrapper.emitted()).toHaveProperty('cancel');
        expect(wrapper.emitted()['cancel'][0]).toEqual([]);
    });
    it('should emit delete with target and id when oui is clicked ', async () => {
        // Given
        const deleteInfo = fakeDeleteInfo();
        const wrapper = mount(DeleteBox, { props: { deleteInfo, prefixId } });
        // When
        await wrapper.find('button#test-yes').trigger('click');
        // Then
        expect(wrapper.emitted()).toHaveProperty('delete');
        expect(wrapper.emitted()['delete'][0]).toEqual(['game','1']);
    });
});