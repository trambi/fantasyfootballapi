import { describe, expect, it } from 'vitest';
import { mount } from '@vue/test-utils';
import PointEditor from '../PointEditor.vue';
import type { VueNode } from 'node_modules/@vue/test-utils/dist/types';
import { fakePointConfiguration } from '@/services/point-configuration';

describe('PointEditor', () => {
    const conf = fakePointConfiguration();
    const prefixId = 'test';
    const name = "example";
    it('should render given point configuration', () => {
        // Given
        const content1 = 'Default value: 0';
        // When
        const wrapper = mount(PointEditor, { props: { modelValue: conf, prefixId, name } });
        // Then
        expect(wrapper.find('summary').text()).toContain(name);
        expect(wrapper.find('summary').text()).toContain(content1);
        expect((wrapper.find('input#test-default-point').element as VueNode<HTMLInputElement>).value).toContain(conf.default);
        expect((wrapper.find('input#test-clause-0-condition').element as VueNode<HTMLInputElement>).value).toContain(conf.clauses[0].condition);
        expect((wrapper.find('input#test-clause-0-point').element as VueNode<HTMLInputElement>).value).toContain(conf.clauses[0].valueIfTrue);
        expect((wrapper.find('input#test-clause-1-condition').element as VueNode<HTMLInputElement>).value).toContain(conf.clauses[1].condition);
        expect((wrapper.find('input#test-clause-1-point').element as VueNode<HTMLInputElement>).value).toContain(conf.clauses[1].valueIfTrue);

    });

    it('should update vmodel when click on suppress on a clause', async () => {
        // Given
        const expected = {
            default: conf.default,
            clauses: [
                conf.clauses[0]
            ]
        };
        const wrapper = mount(PointEditor, { props: { modelValue: conf, prefixId, name } });
        // When
        await wrapper.find('button#test-clause-1-delete').trigger('click');
        // Then
        expect(conf).toEqual(expected);
    });
    it('should update vmodel when add a clause', async () => {
        // Given
        const expected = {
            default: conf.default,
            clauses: [
                ...conf.clauses,
                {
                    condition: '{TdFor}+1={TdAgainst}',
                    valueIfTrue: '1'
                }
            ]
        };
        const wrapper = mount(PointEditor, { props: { modelValue: conf, prefixId, name } });
        // When
        await wrapper.find('input#test-add-condition').setValue('{TdFor}+1={TdAgainst}');
        await wrapper.find('input#test-add-point').setValue('1');
        await wrapper.find('button#test-add').trigger('click');
        // Then
        // configuration has been updated
        expect(conf).toEqual(expected);
        // and reset to add clause element
        expect((wrapper.find('input#test-add-condition').element as VueNode<HTMLInputElement>).value).toContain('');
        expect((wrapper.find('input#test-add-point').element as VueNode<HTMLInputElement>).value).toContain('');
    });
    it('should update vmodel ranking when update a ranking element', async () => {
        // Given
        const expected = {
            default: conf.default,
            clauses: [
                {
                    condition: '{TdFor}={TdAgainst}',
                    valueIfTrue: '3'
                },
                conf.clauses[1]
            ]
        };
        const wrapper = mount(PointEditor, { props: { modelValue: conf, prefixId, name } });
        // When
        await wrapper.find('input#test-clause-0-condition').setValue('{TdFor}={TdAgainst}');
        await wrapper.find('input#test-clause-0-point').setValue('3')
        // Then
        expect(conf).toEqual(expected);
    });
});