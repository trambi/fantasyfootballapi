import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import ToBeCreatedConfrontations from '../ToBeCreatedConfrontations.vue'
import { fakeToBeCreatedConfrontation } from '@/services/confrontation';

describe('ToBeCreatedConfrontations', () => {
  it('renders properly', () => {
    // Given
    const confrontation1 = fakeToBeCreatedConfrontation();
    const confrontation2 = fakeToBeCreatedConfrontation();
    confrontation2.squadName1 = 'Squad 3';
    confrontation2.squadName2 = 'Squad 4';
    const confrontations = [confrontation1, confrontation2];
    // When
    const wrapper = mount(ToBeCreatedConfrontations, { props: { confrontations } });
    // Then
    expect(wrapper.findAll('h3')[0].text()).toContain(`${confrontations[0].squadName1} vs ${confrontations[0].squadName2}`);
    expect(wrapper.findAll('h3')[1].text()).toContain(`${confrontations[1].squadName1} vs ${confrontations[1].squadName2}`);
  });
  it.each([
    [0],
    [1],
  ])('emits delete with %d list of the games when clicking on a button danger',async (line:number)=>{
    // Given
    const confrontation1 = fakeToBeCreatedConfrontation();
    const confrontation2 = fakeToBeCreatedConfrontation();
    confrontation2.squadName1 = 'Squad 3';
    confrontation2.squadName2 = 'Squad 4';
    confrontation2.squadId1 = '1001';
    confrontation2.squadId2 = '1002';
    const ids = [
      '30vs31',
      '1001vs1002',
    ]
    let table = confrontation1.games.length;
    for (let i = 0; i < confrontation2.games.length; i++) {
      confrontation2.games[i].table = table;
      table +=1;
    }
    const confrontations = [confrontation1, confrontation2];
    const wrapper = mount(ToBeCreatedConfrontations, { props: { confrontations } });
    // When
    await wrapper.findAll(`button.btn-danger`)[line].trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('delete');
    expect(wrapper.emitted()['delete'][0]).toEqual([ids[line]]);
  });
});