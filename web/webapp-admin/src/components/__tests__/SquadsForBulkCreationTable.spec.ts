import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import SquadsForBulkCreationTable from '../SquadsForBulkCreationTable.vue'
import { fakeSquadForBulkCreation } from '@/services/squad';

describe('SquadsForBulkCreationTable', () => {
  it('renders properly with a to be created squad', () => {
    expect(true).toBe(true)
    /**
     @TODO
     * 
    */
    // Given
    //     // Given
    //     const squad1 = fakeSquadForBulkCreation();
    //     const squad2 = fakeSquadForBulkCreation();
    //     squad2.name = 'Squad 2';
    //     const squads = [squad1, squad2];
    //     // When
    //     const wrapper = mount(SquadsForBulkCreationTable, { props: { squads } });
    //     // Then
    //     expect(wrapper.find('tr:nth-child(1) td:nth-child(1)').text()).toContain(squad1.name);
    //     expect(wrapper.find('tr:nth-child(2) td:nth-child(1)').text()).toContain(squad2.name);

    //   });
    //   it('should emit delete when clicked on Supprimer', async () => {
    //     // Given
    //     const squad1 = fakeSquadForBulkCreation();
    //     const squad2 = fakeSquadForBulkCreation();
    //     squad2.name = 'Squad 2';
    //     const squads = [squad1, squad2];
    //     const wrapper = mount(SquadsForBulkCreationTable, { props: { squads } });
    //     // When
    //     await wrapper.findAll('button.btn-danger')[0].trigger('click');
    //     // Then
    //     expect(wrapper.emitted()).toHaveProperty('delete',squad1.name);
  });
});
