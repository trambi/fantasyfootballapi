import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import EditGameForm from '../EditGameForm.vue';
import { fakeEdition } from '@/services/edition';
import { fakeNotPlayedGame, fakePlayedGame } from '@/services/game';
import type { VueNode } from 'node_modules/@vue/test-utils/dist/types';

describe('EditGameForm', () => {
    it('should display values when a not played game is given as props', () => {
        const edition = fakeEdition();
        const game = fakeNotPlayedGame();
        const wrapper = mount(EditGameForm, { props: { edition, game } });
        expect(wrapper.find('h1').text()).toContain(`${game.coach1.name} vs ${game.coach2.name}`);
        expect(wrapper.find('h2').text()).toContain(`Round ${game.round} - Table ${game.table}`);
        expect((wrapper.find('#td1').element as VueNode<HTMLInputElement>).value).toEqual("0");
        expect((wrapper.find('#td2').element as VueNode<HTMLInputElement>).value).toEqual("0");
        expect((wrapper.find('#casualties1').element as VueNode<HTMLInputElement>).value).toEqual("0");
        expect((wrapper.find('#casualties2').element as VueNode<HTMLInputElement>).value).toEqual("0");
        expect((wrapper.find('#completions1').element as VueNode<HTMLInputElement>).value).toEqual("0");
        expect((wrapper.find('#completions2').element as VueNode<HTMLInputElement>).value).toEqual("0");
        expect((wrapper.find('#fouls1').element as VueNode<HTMLInputElement>).value).toEqual("0");
        expect((wrapper.find('#fouls2').element as VueNode<HTMLInputElement>).value).toEqual("0");
        expect((wrapper.find('#special1').element as VueNode<HTMLInputElement>).value).toEqual("");
        expect((wrapper.find('#special2').element as VueNode<HTMLInputElement>).value).toEqual("");
    });
    it('should change order of inputs when reverse is clicked', async () => {
        // Given
        const edition = fakeEdition();
        const game = fakeNotPlayedGame();
        const wrapper = mount(EditGameForm, { props: { edition, game } });
        expect(wrapper.find('h1').text()).toContain(`${game.coach1.name} vs ${game.coach2.name}`);
        expect(wrapper.find('tr:nth-child(2) td:nth-child(1) label').text()).toEqual("Td 1* :");
        expect(wrapper.find('tr:nth-child(3) td:nth-child(1) label').text()).toEqual("Casualties 1* :");
        expect(wrapper.find('tr:nth-child(4) td:nth-child(1) label').text()).toEqual("Completions 1* :");
        expect(wrapper.find('tr:nth-child(5) td:nth-child(1) label').text()).toEqual("Fouls 1* :");
        expect(wrapper.find('tr:nth-child(6) td:nth-child(1) label').text()).toEqual("Special 1* :");
        // When
        await wrapper.find('button#reverse').trigger('click');
        // Then
        expect(wrapper.find('h1').text()).toContain(`${game.coach2.name} vs ${game.coach1.name}`);
        expect(wrapper.find('tr:nth-child(2) td:nth-child(1) label').text()).toEqual("Td 2* :");
        expect(wrapper.find('tr:nth-child(3) td:nth-child(1) label').text()).toEqual("Casualties 2* :");
        expect(wrapper.find('tr:nth-child(4) td:nth-child(1) label').text()).toEqual("Completions 2* :");
        expect(wrapper.find('tr:nth-child(5) td:nth-child(1) label').text()).toEqual("Fouls 2* :");
        expect(wrapper.find('tr:nth-child(6) td:nth-child(1) label').text()).toEqual("Special 2* :");
    });
    it('should return to normal when reverse is clicked twice', async () => {
        // Given
        const edition = fakeEdition();
        const game = fakeNotPlayedGame();
        const wrapper = mount(EditGameForm, { props: { edition, game } });
        await wrapper.find('button#reverse').trigger('click');
        expect(wrapper.find('tr:nth-child(2) td:nth-child(1) label').text()).toEqual("Td 2* :");
        // When
        await wrapper.find('button#reverse').trigger('click');
        // Then
        expect(wrapper.find('tr:nth-child(2) td:nth-child(1) label').text()).toEqual("Td 1* :");
        expect(wrapper.find('tr:nth-child(3) td:nth-child(1) label').text()).toEqual("Casualties 1* :");
        expect(wrapper.find('tr:nth-child(4) td:nth-child(1) label').text()).toEqual("Completions 1* :");
        expect(wrapper.find('tr:nth-child(5) td:nth-child(1) label').text()).toEqual("Fouls 1* :");
        expect(wrapper.find('tr:nth-child(6) td:nth-child(1) label').text()).toEqual("Special 1* :");
    });
    it('should display values when a played game is given as props', () => {
        const edition = fakeEdition();
        const game = fakePlayedGame();
        const wrapper = mount(EditGameForm, { props: { edition, game } });
        expect(wrapper.find('h1').text()).toContain(`${game.coach1.name} vs ${game.coach2.name}`);
        expect(wrapper.find('h2').text()).toContain(`Round ${game.round} - Table ${game.table}`);
        expect((wrapper.find('#td1').element as VueNode<HTMLInputElement>).value).toEqual(`${game.td1}`);
        expect((wrapper.find('#td2').element as VueNode<HTMLInputElement>).value).toEqual(`${game.td2}`);
        expect((wrapper.find('#casualties1').element as VueNode<HTMLInputElement>).value).toEqual(`${game.casualties1}`);
        expect((wrapper.find('#casualties2').element as VueNode<HTMLInputElement>).value).toEqual(`${game.casualties2}`);
        expect((wrapper.find('#completions1').element as VueNode<HTMLInputElement>).value).toEqual(`${game.completions1}`);
        expect((wrapper.find('#completions2').element as VueNode<HTMLInputElement>).value).toEqual(`${game.completions2}`);
        expect((wrapper.find('#fouls1').element as VueNode<HTMLInputElement>).value).toEqual(`${game.fouls1}`);
        expect((wrapper.find('#fouls2').element as VueNode<HTMLInputElement>).value).toEqual(`${game.fouls2}`);
        expect((wrapper.find('#special1').element as VueNode<HTMLInputElement>).value).toEqual(game.special1);
        expect((wrapper.find('#special2').element as VueNode<HTMLInputElement>).value).toEqual(game.special2);
    });
    it('should emit \'submit\' with a proper content when click on submit', async () => {
        // Given
        const edition = fakeEdition();
        const game = fakeNotPlayedGame();
        const wrapper = mount(EditGameForm, { props: { edition, game } });
        const expected = {
            id: game.id,
            editionId: edition.id,
            round:game.round,
            table:game.table,
            finale: game.finale,
            played: true,
            coachId1: game.coach1.id,
            coachId2: game.coach2.id,
            td1: 1,
            td2: 2,
            casualties1: 3,
            casualties2: 4,
            completions1: 5,
            completions2: 6,
            fouls1: 7,
            fouls2: 8,
            special1: "something",
            special2: "else"
        }
        await wrapper.find('#td1').setValue(expected.td1);
        await wrapper.find('#td2').setValue(expected.td2);
        await wrapper.find('#casualties1').setValue(expected.casualties1);
        await wrapper.find('#casualties2').setValue(expected.casualties2);
        await wrapper.find('#completions1').setValue(expected.completions1);
        await wrapper.find('#completions2').setValue(expected.completions2);
        await wrapper.find('#fouls1').setValue(expected.fouls1);
        await wrapper.find('#fouls2').setValue(expected.fouls2);
        await wrapper.find('#special1').setValue(expected.special1);
        await wrapper.find('#special2').setValue(expected.special2);
        // When
        await wrapper.find('button#submit').trigger('click');
        // Expect
        expect(wrapper.emitted()).toHaveProperty('submit');
        expect(wrapper.emitted()['submit'][0]).toEqual([edition.id, expected]);
    });
});