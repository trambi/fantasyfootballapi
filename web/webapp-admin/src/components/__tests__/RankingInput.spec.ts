import { describe, expect, it } from 'vitest';
import { mount } from '@vue/test-utils';
import RankingInput from '../RankingInput.vue';
import type { VueNode } from 'node_modules/@vue/test-utils/dist/types';

describe('RankingInput', () => {
    it('should render given ranking', () => {
        // Given
        const rankingField = {
            label:"My label",
            field: "Touchdown",
            type: "For",
            descending: true
        }
        const possibleFields = ['Touchdown','Casualties'];
        const possibleTypes = ['For','Against']
        const prefixId = 'test';
        // When
        const wrapper = mount(RankingInput, { props: { possibleFields, possibleTypes, prefixId, modelValue:rankingField}, });
        // Then
        expect((wrapper.find('input#test-label').element as VueNode<HTMLInputElement>).value).toContain(rankingField.label);
        expect(wrapper.find('select#test-field').text()).toContain(rankingField.field);
        expect(wrapper.find('select#test-type').text()).toContain(rankingField.type);
        expect((wrapper.find('input#test-descending').element as VueNode<HTMLInputElement>).value).toContain(rankingField.descending?'on':'');
    });
    it('should update model rankingField when update a label', async () => {
        // Given
        const rankingField = {
            label:"My label",
            field: "Touchdown",
            type: "For",
            descending: true
        };
        const possibleFields = ['Touchdown','Casualties'];
        const possibleTypes = ['For','Against']
        const prefixId = 'test';
        const expected = {
            label:"Modified label",
            field: "Touchdown",
            type: "For",
            descending: true
        };
        const wrapper = mount(RankingInput, { props: { modelValue:rankingField, possibleFields, possibleTypes, prefixId } });
        // When
        await wrapper.find('input#test-label').setValue('Modified label');
        // Then
        expect(rankingField).toEqual(expected);
    });
});