import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import PlayedGameLine from '../PlayedGameLine.vue'
import { fakePlayedGame } from '@/services/game';

describe('PlayedGameLine', () => {
  it('should display played game with displayRound props set to false', () => {
    // Given
    const game = fakePlayedGame();
    const wrapper = mount(PlayedGameLine, { props: { game, displayRound: false, fromCoach1Perspective: false, displayCoach1: true } });
    expect(wrapper.find('td:nth-child(1)').text()).toContain(game.table);
    expect(wrapper.find('td:nth-child(2)').text()).toContain(`${game.coach1.name} vs ${game.coach2.name}`);
    expect(wrapper.find('td:nth-child(3)').text()).toContain(`${game.td1} - ${game.td2}`);
    expect(wrapper.find('td:nth-child(4)').text()).toContain(`${game.casualties1} - ${game.casualties2}`);
    expect(wrapper.find('td:nth-child(5)').text()).toContain(`${game.points1} - ${game.points2}`);
  });
  it('should display round and played game with displayRound props set to true', () => {
    // Given
    const game = fakePlayedGame();
    const wrapper = mount(PlayedGameLine, { props: { game, displayRound: true, fromCoach1Perspective: false, displayCoach1: true } });
    expect(wrapper.find('td:nth-child(1)').text()).toContain(game.round);
    expect(wrapper.find('td:nth-child(2)').text()).toContain(game.table);
    expect(wrapper.find('td:nth-child(3)').text()).toContain(`${game.coach1.name} vs ${game.coach2.name}`);
    expect(wrapper.find('td:nth-child(4)').text()).toContain(`${game.td1} - ${game.td2}`);
    expect(wrapper.find('td:nth-child(5)').text()).toContain(`${game.casualties1} - ${game.casualties2}`);
    expect(wrapper.find('td:nth-child(6)').text()).toContain(`${game.points1} - ${game.points2}`);
  });
  it('should hide coach1 name and display played game with displayCoach1 props set to false', () => {
    // Given
    const game = fakePlayedGame();
    const wrapper = mount(PlayedGameLine, { props: { game, displayRound: true, fromCoach1Perspective: false, displayCoach1: false } });
    expect(wrapper.find('td:nth-child(1)').text()).toContain(game.round);
    expect(wrapper.find('td:nth-child(2)').text()).toContain(game.table);
    expect(wrapper.find('td:nth-child(3)').text()).toContain(game.coach2.name);
    expect(wrapper.find('td:nth-child(4)').text()).toContain(`${game.td1} - ${game.td2}`);
    expect(wrapper.find('td:nth-child(5)').text()).toContain(`${game.casualties1} - ${game.casualties2}`);
    expect(wrapper.find('td:nth-child(6)').text()).toContain(`${game.points1} - ${game.points2}`);
  });
  it('should emit delete when clicked on Supprimer', async () => {
    // Given
    const game = fakePlayedGame();
    const wrapper = mount(PlayedGameLine, { props: { game, displayRound: false, fromCoach1Perspective: false, displayCoach1: true } });
    // When
    await wrapper.find('button.btn-danger').trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('delete');
  });
  it('should emit edit when clicked on Modifier', async () => {
    // Given
    const game = fakePlayedGame();
    const wrapper = mount(PlayedGameLine, { props: { game, displayRound: false, fromCoach1Perspective: false, displayCoach1: true } });
    // When
    await wrapper.find('button.btn-warning').trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('edit');
  });
});
