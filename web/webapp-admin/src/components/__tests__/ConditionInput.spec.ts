import { describe, expect, it } from 'vitest';
import { mount } from '@vue/test-utils';
import ConditionInput from '../ConditionInput.vue';
import type { VueNode } from 'node_modules/@vue/test-utils/dist/types';

describe('ConditionInput', () => {
    const prefixId = 'test';
    const label = 'Condition';
    it('should render given ranking', () => {
        // Given
        const initialValue = '{TdFor}={TdAgainst}';
        // When
        const wrapper = mount(ConditionInput, { props: { label, prefixId, modelValue: initialValue }, });
        // Then
        expect(wrapper.find('label').text()).toContain(label);
        expect((wrapper.find('input#test-condition').element as VueNode<HTMLInputElement>).value).toContain(initialValue);
    });
    it('should update model when update it', async () => {
        // Given
        const initialValue = '{TdFor}={TdAgainst}';
        const expected = '{TdFor}<{TdAgainst}';
        // To test v-model 
        // We need to implement onUpdate:modelValue
        // according to https://test-utils.vuejs.org/guide/advanced/v-model.html
        const wrapper = mount(ConditionInput, {
            props: {
                label,
                modelValue: initialValue,
                prefixId,
                'onUpdate:modelValue': (e) => wrapper.setProps({ modelValue: e })
            }
        });
        // When
        await wrapper.find('input#test-condition').setValue(expected);
        // Then
        expect(wrapper.props('modelValue')).toBe(expected);
    });
});