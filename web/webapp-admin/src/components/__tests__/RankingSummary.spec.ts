import { describe, expect, it } from 'vitest';
import { mount } from '@vue/test-utils';
import RankingSummary from '../RankingSummary.vue';

describe('RankingSummary', () => {
    it('should render given ranking', () => {
        // Given
        const ranking = [
            {
                label: "TD",
                field: "Touchdown",
                type: "For",
                descending: true,
            },
            {
                label: "Defense",
                field: "Touchdown",
                type: "Against",
                descending: false,
            },
            {
                label: "Net casualties",
                field: "Casualties",
                type: "Net",
                descending: true,
            },
        ];
        const expected = [
            "TD: Based on Touchdown For descending",
            "Defense: Based on Touchdown Against ascending",
            "Net casualties: Based on Casualties Net descending",
        ];
        // When
        const wrapper = mount(RankingSummary, { props: { ranking }, });
        // Then
        expect(wrapper.find('li:nth-child(1)').text()).toContain(expected[0]);
        expect(wrapper.find('li:nth-child(2)').text()).toContain(expected[1]);
        expect(wrapper.find('li:nth-child(3)').text()).toContain(expected[2]);
    });
});