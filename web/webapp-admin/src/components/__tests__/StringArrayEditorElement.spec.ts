import { describe, expect, it } from 'vitest';
import { mount } from '@vue/test-utils';
import StringArrayEditorElement from '../StringArrayEditorElement.vue';

describe('StringArrayEditorElement', () => {
    it('should render given string', () => {
        // Given
        const value = 'value 1';
        const prefixId = 'test';
        // When
        const wrapper = mount(StringArrayEditorElement, { props: { value,prefixId } });
        // Then
        expect(wrapper.find('#test-value').text()).toContain(value);
    });
    it('should emit "delete" when click on suppress', async () => {
        // Given
        const value = 'value 1';
        const prefixId = 'test';
        const wrapper = mount(StringArrayEditorElement, { props: { value,prefixId } });
        // When
        await wrapper.find('button#test-delete').trigger('click');
        // Then
        expect(wrapper.emitted()).toHaveProperty('delete');
        expect(wrapper.emitted()['delete'][0]).toEqual([value]);
    });
});