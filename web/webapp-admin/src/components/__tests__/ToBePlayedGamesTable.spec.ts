import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import ToBePlayedGamesTable from '../ToBePlayedGamesTable.vue'
import { fake2NotPlayedGames } from '@/services/game';

describe('ToBePlayedGamesTable', () => {

  it('renders properly with displayRound set to false', () => {
    // Given
    const games = fake2NotPlayedGames();
    // When
    const wrapper = mount(ToBePlayedGamesTable, { props: { games, displayRound:false } });
    // Then
    expect(wrapper.find('tr:nth-child(1) td:nth-child(1)').text()).toContain(games[0].table);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(2)').text()).toContain(games[0].coach1.name);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(3)').text()).toContain(games[0].coach2.name);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(1)').text()).toContain(games[1].table);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(2)').text()).toContain(games[1].coach1.name);
  });
  it('renders properly with displayRound set to true', () => {
    // Given
    const games = fake2NotPlayedGames();
    // When
    const wrapper = mount(ToBePlayedGamesTable, { props: { games, displayRound:true } });
    // Then
    expect(wrapper.find('tr:nth-child(1) td:nth-child(1)').text()).toContain(games[0].round);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(2)').text()).toContain(games[0].table);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(3)').text()).toContain(games[0].coach1.name);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(4)').text()).toContain(games[0].coach2.name);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(1)').text()).toContain(games[1].round);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(2)').text()).toContain(games[1].table);
  });
  it.each([
    ['resume', 1, 'success'],
    ['resume', 2, 'success'],
    ['delete', 2, 'danger'],
  ])('emits %s with %d of the game when clicking on a button %s', async (emitted: string, line: number, button: string) => {
    // Given
    const games = fake2NotPlayedGames();
    // When
    const wrapper = mount(ToBePlayedGamesTable, { props: { games,displayRound:false } });
    // Then
    await wrapper.findAll(`button.btn-${button}`)[line - 1].trigger('click');
    expect(wrapper.emitted()).toHaveProperty(emitted);
    expect(wrapper.emitted()[emitted][0]).toEqual([games[line - 1].id]);
  });
});