import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import CreateGameForm from '../CreateGameForm.vue';
import { fake6Coachs } from '@/services/coach';
import { fakeEdition } from '@/services/edition';

describe('CreateGameForm', () => {
    it('should emit \'submit\' with a proper content when click on submit', async () => {
        // Given
        const edition = fakeEdition();
        const availableCoachs = fake6Coachs();
        const wrapper = mount(CreateGameForm, { props: { edition, availableCoachs } });
        await wrapper.find('#round').setValue(1);
        await wrapper.find('#table').setValue(2);
        await wrapper.find('#coach1').setValue(availableCoachs[0].id);
        await wrapper.find('#coach2').setValue(availableCoachs[2].id);
        await wrapper.find('#finale').setValue(true);
        const expected = {
            editionId: edition.id,
            round:1,
            table:2,
            finale: true,
            played: false,
            coachId1: availableCoachs[0].id,
            coachId2: availableCoachs[2].id,
        }
        // When
        await wrapper.find('button').trigger('click');
        // Expect
        expect(wrapper.emitted()).toHaveProperty('submit');
        expect(wrapper.emitted()['submit'][0]).toEqual([edition.id, expected]);
    });
});