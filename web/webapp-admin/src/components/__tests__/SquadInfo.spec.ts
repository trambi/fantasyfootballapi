import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';
import SquadInfo from '../SquadInfo.vue';
import { fakeSquad } from '@/services/squad';

describe('SquadInfo', () => {
  it('renders properly with a squad', () => {
    // Given
    const squad = fakeSquad();
    // When
    const wrapper = mount(SquadInfo, { props: { squad } });
    // Then
    expect(wrapper.find('h1').text()).toContain(`Squad : ${squad.name}`);
    expect(wrapper.find('h1>span').text()).toContain(squad.id);
    expect(wrapper.find('h2').text()).toContain('Membres:');
    expect(wrapper.find('ul li:nth-child(1)').text()).toContain(squad.teamMates[0].name);
    expect(wrapper.find('ul li:nth-child(2)').text()).toContain(squad.teamMates[1].name);
  });
  it('should emit info-coach on click on a teamMember', async()=>{
    // Given
    const squad = fakeSquad();
    const wrapper = mount(SquadInfo, { props: { squad } });
    // When
    await wrapper.find('#info-100').trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('info-coach');
    expect(wrapper.emitted('info-coach')).toEqual([[squad.teamMates[0].id]]);
  });
  it('should emit edit-coach on click on a edit button for a teamMember', async()=>{
    // Given
    const squad = fakeSquad();
    const wrapper = mount(SquadInfo, { props: { squad } });
    // When
    await wrapper.find('#edit-100').trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('edit-coach');
    expect(wrapper.emitted('edit-coach')).toEqual([[squad.teamMates[0].id]]);
  });
});