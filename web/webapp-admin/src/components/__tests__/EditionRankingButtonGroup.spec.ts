import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import EditionRankingButtonGroup from '../EditionRankingButtonGroup.vue';
import { fakeEdition } from '@/services/edition';

describe('EditionRankingButtonGroup', () => {
    it('should display buttons for coach and squad ranking', () => {
        // Given
        const edition = fakeEdition();
        if(edition.rankings.coach === undefined){
            edition.rankings.coach = {};
        }
        edition.rankings.coach.td = [{ label: "tdFor", field: "Td", type: "For", descending: true }];
        // When
        const wrapper = mount(EditionRankingButtonGroup, { props: { edition } });
        // Then
        expect(wrapper.find('button:nth-child(1)').text()).toContain('Classements individuels');
        expect(wrapper.find('button:nth-child(2)').text()).toContain('Classements par équipe');

    });
    it('should display buttons for coach ranking when clicking on coach rankings button', async () => {
        // Given
        const edition = fakeEdition();
        if(edition.rankings.coach === undefined){
            edition.rankings.coach = {};
        }
        edition.rankings.coach.td = [{ label: "tdFor", field: "Td", type: "For", descending: true }];
        const wrapper = mount(EditionRankingButtonGroup, { props: { edition } });
        // When
        await wrapper.find('button:nth-child(1)').trigger('click');
        // Then
        expect(wrapper.find('button:nth-child(2)').text()).toContain('Classement individuel (main)');
        expect(wrapper.find('button:nth-child(3)').text()).toContain('Classement individuel (td)');
        expect(wrapper.find('button:nth-child(4)').text()).toContain('Classements par équipe');
    });
    it('should display buttons for coach ranking when clicking on squad rankings button', async () => {
        // Given
        const edition = fakeEdition();
        if(edition.rankings.coach === undefined){
            edition.rankings.coach = {};
        }
        edition.rankings.coach.td = [{ label: "tdFor", field: "Td", type: "For", descending: true }];
        const wrapper = mount(EditionRankingButtonGroup, { props: { edition } });
        // When
        await wrapper.find('button:nth-child(2)').trigger('click');
        // Then
        expect(wrapper.find('button:nth-child(2)').text()).toContain('Classements par équipe');
        expect(wrapper.find('button:nth-child(3)').text()).toContain('Classement par équipe (casualties)');
        expect(wrapper.find('button:nth-child(4)').text()).toContain('Classement par équipe (comeback)');
        expect(wrapper.find('button:nth-child(5)').text()).toContain('Classement par équipe (defense)');
        expect(wrapper.find('button:nth-child(6)').text()).toContain('Classement par équipe (fouls)');
        expect(wrapper.find('button:nth-child(7)').text()).toContain('Classement par équipe (main)');
        expect(wrapper.find('button:nth-child(8)').text()).toContain('Classement par équipe (td)');
        expect(wrapper.find('button:nth-child(1)').text()).toContain('Classements individuels');
    });
});