import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import EditionForm from '../EditionForm.vue';
import type { VueNode } from 'node_modules/@vue/test-utils/dist/types';
import { fakeEdition } from '@/services/edition';

describe('EditionForm', () => {
    it('should render given edition', () => {
        // Given
        const edition = fakeEdition();
        // When
        const wrapper = mount(EditionForm, { props: { edition } });
        // Then
        expect((wrapper.find('input#name').element as VueNode<HTMLInputElement>).value).toContain(edition.name);
        expect((wrapper.find('input#organizer').element as VueNode<HTMLInputElement>).value).toContain(edition.organizer);
        expect((wrapper.find('input#day-1').element as VueNode<HTMLInputElement>).value).toContain(edition.day1);
        expect((wrapper.find('input#day-2').element as VueNode<HTMLInputElement>).value).toContain(edition.day2);
        expect((wrapper.find('input#round-number').element as VueNode<HTMLInputElement>).value).toContain(edition.roundNumber);
        expect((wrapper.find('input#first-day-round').element as VueNode<HTMLInputElement>).value).toContain(edition.firstDayRound);
        expect((wrapper.find('input#coach-per-squad').element as VueNode<HTMLInputElement>).value).toContain(edition.coachPerSquad);
        expect((wrapper.find('input#full-squad').element as VueNode<HTMLInputElement>).value).toContain(edition.fullSquad?"on":"");
        expect((wrapper.find('input#use-finale').element as VueNode<HTMLInputElement>).value).toContain(edition.useFinale?"on":"");
        expect(wrapper.find('summary').text()).toContain(edition.allowedFactions.join(', '));
        expect((wrapper.find('input#coach-main-0-label').element as VueNode<HTMLInputElement>).value).toContain("points");
        expect(wrapper.find('select#coach-main-0-field').text()).toContain("Points");
        expect(wrapper.find('select#coach-main-0-type').text()).toContain("For");
        expect((wrapper.find('input#coach-main-0-descending').element as VueNode<HTMLInputElement>).value).toContain("on");
        expect((wrapper.find('input#game-points-default-point').element as VueNode<HTMLInputElement>).value).toContain(edition.gamePoints.default);
        expect((wrapper.find('input#confrontation-points-default-point').element as VueNode<HTMLInputElement>).value).toContain(edition.confrontationPoints.default);

    });
    it('should emit "submit" with a proper content with edition as props when click on submit', async () => {
        // Given
        const newEditionName = 'An unbelievable edition';
        const edition = fakeEdition();
        const wrapper = mount(EditionForm, { props: { edition } });
        const expected = { ...edition,name: newEditionName };
        // When
        await wrapper.find('input#name').setValue(newEditionName);
        await wrapper.find('button[type="submit"]').trigger('click');
        // Then
        expect(wrapper.emitted()).toHaveProperty('submit');
        expect(wrapper.emitted()['submit'][0]).toEqual([expected]);
    });
    it('should emit "submit" with a proper content without edition as props when click on submit', async () => {
        // Given
        const newEditionName = 'An unbelievable edition';
        const edition = fakeEdition();
        const wrapper = mount(EditionForm, { props: { } });
        const expected = { ...edition,name: newEditionName };
        // When
        await wrapper.find('input#name').setValue(newEditionName);
        await wrapper.find('button[type="submit"]').trigger('click');
        // Then
        expect(wrapper.emitted()).toHaveProperty('submit');
        expect(wrapper.emitted()['submit'][0]).toEqual([expected]);
    });
});