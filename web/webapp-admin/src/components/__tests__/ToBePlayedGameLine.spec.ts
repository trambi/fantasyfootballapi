import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import ToBePlayedGameLine from '../ToBePlayedGameLine.vue'
import { fakeNotPlayedGame } from '@/services/game';

describe('ToBePlayedGameLine', () => {
  it('renders properly with a not played game with displayRound set to false', () => {
    // Given
    const game = fakeNotPlayedGame();
    const wrapper = mount(ToBePlayedGameLine, { props: { game,displayRound : false } });
    expect(wrapper.find('td:nth-child(1)').text()).toContain(game.table);
    expect(wrapper.find('td:nth-child(2)').text()).toContain(game.coach1.name);
    expect(wrapper.find('td:nth-child(3)').text()).toContain(game.coach2.name);
    expect(wrapper.find('td:nth-child(4) button.btn-success').text()).toContain("Résumer");
    expect(wrapper.find('td:nth-child(4) button.btn-danger').text()).toContain("Supprimer");
  });
  it('renders properly with a not played game with displayRound set to true', () => {
    // Given
    const game = fakeNotPlayedGame();
    const wrapper = mount(ToBePlayedGameLine, { props: { game,displayRound : true } });
    expect(wrapper.find('td:nth-child(1)').text()).toContain(game.round);
    expect(wrapper.find('td:nth-child(2)').text()).toContain(game.table);
    expect(wrapper.find('td:nth-child(3)').text()).toContain(game.coach1.name);
    expect(wrapper.find('td:nth-child(4)').text()).toContain(game.coach2.name);
    expect(wrapper.find('td:nth-child(5) button.btn-success').text()).toContain("Résumer");
    expect(wrapper.find('td:nth-child(5) button.btn-danger').text()).toContain("Supprimer");
  });
  it('should emit delete when clicked on Supprimer', async () => {
    // Given
    const game = fakeNotPlayedGame();
    const wrapper = mount(ToBePlayedGameLine, { props: { game,displayRound:false } });
    // When
    await wrapper.find('button.btn-danger').trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('delete');
  });
  it('should emit resume when clicked on Résumer', async () => {
    // Given
    const game = fakeNotPlayedGame();
    const wrapper = mount(ToBePlayedGameLine, { props: { game,displayRound:false } });
    // When
    await wrapper.find('button.btn-success').trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('resume');
  });
});
