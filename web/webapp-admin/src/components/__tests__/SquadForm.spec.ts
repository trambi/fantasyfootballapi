import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import SquadForm from '../SquadForm.vue';
import { fake6Coachs } from '@/services/coach';
import { fakeEdition } from '@/services/edition';
import { fakeSquad } from '@/services/squad';
import type { VueNode } from 'node_modules/@vue/test-utils/dist/types';

describe('SquadForm', () => {
    it('should render given squad', () => {
        // Given
        const edition = fakeEdition();
        const squad = fakeSquad();
        const availableCoachs = fake6Coachs();
        // When
        const wrapper = mount(SquadForm, { props: { squad, availableCoachs, edition } });
        // Then
        expect((wrapper.find('#name').element as VueNode<HTMLInputElement>).value).toContain(squad.name);
        expect((wrapper.find('#member1').element as VueNode<HTMLInputElement>).value).toEqual(squad.teamMates[0].id)
        expect((wrapper.find('#member2').element as VueNode<HTMLInputElement>).value).toEqual(squad.teamMates[1].id)
        expect((wrapper.find('#member3').element as VueNode<HTMLInputElement>).value).toEqual(squad.teamMates[2].id)
    });
    it('should emit \'submit\' with a proper content with squad as props when click on submit', async () => {
        // Given
        const edition = fakeEdition();
        const squad = fakeSquad();
        const availableCoachs = fake6Coachs();
        const wrapper = mount(SquadForm, { props: { squad, edition, availableCoachs } });
        await wrapper.find('#name').setValue('An unbelievable name');
        await wrapper.find('#member1').setValue(availableCoachs[0].id);
        await wrapper.find('#member2').setValue(availableCoachs[1].id);
        await wrapper.find('#member3').setValue(availableCoachs[4].id);
        const expected = {
            id: squad.id,
            editionId: edition.id,
            name: 'An unbelievable name',
            teamMates: [
                { name: availableCoachs[0].name, id: availableCoachs[0].id, teamName: availableCoachs[0].teamName },
                { name: availableCoachs[1].name, id: availableCoachs[1].id, teamName: availableCoachs[1].teamName },
                { name: availableCoachs[4].name, id: availableCoachs[4].id, teamName: availableCoachs[4].teamName }
            ]
        }
        // When
        await wrapper.find('button').trigger('click');
        // Expect
        expect(wrapper.emitted()).toHaveProperty('submit');
        expect(wrapper.emitted()['submit'][0]).toEqual([edition.id, expected]);
    });

    it('should emit \'submit\' with a proper content without squad as props when click on submit', async () => {
        // Given
        const edition = fakeEdition();
        const availableCoachs = fake6Coachs();
        const wrapper = mount(SquadForm, { props: { edition, availableCoachs } });
        await wrapper.find('#name').setValue('An unbelievable name');
        await wrapper.find('#member1').setValue(availableCoachs[0].id);
        await wrapper.find('#member2').setValue(availableCoachs[1].id);
        await wrapper.find('#member3').setValue(availableCoachs[4].id);
        const expected = {
            editionId: edition.id,
            name: 'An unbelievable name',
            teamMates: [
                { name: availableCoachs[0].name, id: availableCoachs[0].id, teamName: availableCoachs[0].teamName },
                { name: availableCoachs[1].name, id: availableCoachs[1].id, teamName: availableCoachs[1].teamName },
                { name: availableCoachs[4].name, id: availableCoachs[4].id, teamName: availableCoachs[4].teamName }
            ]
        }
        // When
        await wrapper.find('button').trigger('click');
        // Expect
        expect(wrapper.emitted()).toHaveProperty('submit');
        expect(wrapper.emitted()['submit'][0]).toEqual([edition.id, expected]);
    });
});