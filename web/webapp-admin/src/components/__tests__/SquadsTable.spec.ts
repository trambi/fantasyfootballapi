import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import SquadsTable from '../SquadsTable.vue'
import { displayMembers, fakeSquads } from '@/services/squad';

describe('SquadsTable', () => {
  
  it('renders properly', () => {
    // Given
    const squads = fakeSquads();
    // When
    const wrapper = mount(SquadsTable, { props: { squads } });
    // Then
    expect(wrapper.find('tr:nth-child(1) td:nth-child(1)').text()).toContain(squads[0].name);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(2)').text()).toContain(displayMembers(squads[0]));
    expect(wrapper.find('tr:nth-child(2) td:nth-child(1)').text()).toContain(squads[1].name);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(2)').text()).toContain(displayMembers(squads[1]));
  });
  it.each([
    ['info',1,'info'],
    ['info',2,'info'],
    ['edit',2,'warning'],
    ['delete',2,'danger'],
  ])('emits %s with %d of the squad when clicking on a button %s',async (emitted:string,line:number, button:string)=>{
    // Given
    const squads = fakeSquads();
    // When
    const wrapper = mount(SquadsTable, { props: { squads } });
    // Then
    await wrapper.findAll(`button.btn-${button}`)[line-1].trigger('click');
    expect(wrapper.emitted()).toHaveProperty(emitted);
    expect(wrapper.emitted()[emitted][0]).toEqual([squads[line-1].id]);
  });
  // it('emits ready with the squad when clicking on the ready button',async ()=>{
  //   // Given
  //   // When
  //   const wrapper = mount(SquadsTable, { props: { squads:squads() } });
  //   // Then
  //   await wrapper.findAll('button.btn-primary')[0].trigger('click');
  //   expect(wrapper.emitted()).toHaveProperty('ready');
  //   expect(wrapper.emitted()['ready'][0]).toEqual([1,false]);
  // });
});