import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import SquadLine from '../SquadLine.vue'
import { displayMembers, fakeSquad } from '@/services/squad';

describe('SquadLine', () => {
  it('renders properly with a squad', () => {
    // Given
    const squad = fakeSquad();
    const wrapper = mount(SquadLine, { props: { squad } });
    expect(wrapper.find('button:nth-child(1)').text()).toContain(squad.name);
    expect(wrapper.find('td:nth-child(2)').text()).toContain(displayMembers(squad));
    expect(wrapper.find('td:nth-child(3)').text()).toContain('Prêt');
    //expect(wrapper.find('td:nth-child(4)').text()).toContain('Déclarer absent');
    expect(wrapper.find('td:nth-child(4)').text()).toContain('Supprimer');
    expect(wrapper.find('td:nth-child(4)').text()).toContain('Modifier');
  });
  // it('should emit info when clicked on coach name', async () => {
  //   // Given
  //   const coach = {
  //     id: 1,
  //     teamName: 'The team',
  //     name: 'The coach',
  //     email: 'XXXXXXXXX',
  //     nafNumber: 123,
  //     faction: 'A faction',
  //     squadId: 2,
  //     squadName: 'The Squad',
  //     ready: false
  //   }
  //   const wrapper = mount(CoachLine, { props: { coach } });
  //   await wrapper.find('button:nth-child(1)').trigger('click');
  //   expect(wrapper.emitted()).toHaveProperty('info');
  // });
  // it('should emit ready when clicked on declare present', async () => {
  //   // Given
  //   const coach = {
  //     id: 1,
  //     teamName: 'The team',
  //     name: 'The coach',
  //     email: 'XXXXXXXXX',
  //     nafNumber: 123,
  //     faction: 'A faction',
  //     squadId: 2,
  //     squadName: 'The Squad',
  //     ready: false
  //   }
  //   const wrapper = mount(CoachLine, { props: { coach } });
  //   // When
  //   await wrapper.find('button.btn-primary').trigger('click');
  //   // Then
  //   expect(wrapper.emitted()).toHaveProperty('ready');
  // });
  // it('should emit delete when clicked on Supprimer', async () => {
  //   // Given
  //   const coach = {
  //     id: 1,
  //     teamName: 'The team',
  //     name: 'The coach',
  //     email: 'XXXXXXXXX',
  //     nafNumber: 123,
  //     faction: 'A faction',
  //     squadId: 2,
  //     squadName: 'The Squad',
  //     ready: true
  //   }
  //   const wrapper = mount(CoachLine, { props: { coach } });
  //   // When
  //   await wrapper.find('button.btn-danger').trigger('click');
  //   // Then
  //   expect(wrapper.emitted()).toHaveProperty('delete');
  // });
  // it('should emit edit when clicked on Modifier', async () => {
  //   // Given
  //   const coach = {
  //     id: 1,
  //     teamName: 'The team',
  //     name: 'The coach',
  //     email: 'XXXXXXXXX',
  //     nafNumber: 123,
  //     faction: 'A faction',
  //     squadId: 2,
  //     squadName: 'The Squad',
  //     ready: true
  //   }
  //   const wrapper = mount(CoachLine, { props: { coach } });
  //   // When
  //   await wrapper.find('button.btn-warning').trigger('click');
  //   // Then
  //   expect(wrapper.emitted()).toHaveProperty('edit');
  // });
});
