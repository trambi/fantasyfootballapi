import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import AdminEdition from '../AdminEdition.vue';
import { fakeEdition } from '@/services/edition';
import { fake2NotPlayedGames, fake2PlayedGames, type NotPlayedGame, type PlayedGame } from '@/services/game';

describe('AdminEdition', () => {
    it('should render one admin menu bar when there is no games at all', () => {
        // Given
        const edition = fakeEdition();
        const toBePlayedGames: NotPlayedGame[] = [];
        const playedGames : PlayedGame[]= [];
        const round = 1;
        // When
        const wrapper = mount(AdminEdition, { props: { edition, round, toBePlayedGames,playedGames} });
        // Then
        expect(wrapper.findAll('div.menubar button:nth-child(1)')).toHaveLength(1);
    });

    it('should render two admin menu bars when there is only not played games', () => {
        // Given
        const edition = fakeEdition();
        const toBePlayedGames = fake2NotPlayedGames();
        const playedGames : PlayedGame[]= [];
        const round = 1;
        // When
        const wrapper = mount(AdminEdition, { props: { edition, round, toBePlayedGames,playedGames} });
        // Then
        expect(wrapper.findAll('div.menubar button:nth-child(1)')).toHaveLength(2);
    });
    it('should render three admin menu bars when there is played games and not played games', () => {
        // Given
        const edition = fakeEdition();
        const toBePlayedGames = fake2NotPlayedGames();
        const playedGames = fake2PlayedGames();
        const round = 1;
        // When
        const wrapper = mount(AdminEdition, { props: { edition, round, toBePlayedGames,playedGames} });
        // Then
        expect(wrapper.findAll('div.menubar button:nth-child(1)')).toHaveLength(3);
    });
});