import { describe, it, expect } from 'vitest';

import { mount } from '@vue/test-utils';
import CoachInfo from '../CoachInfo.vue'
import { fakeCoach } from '@/services/coach';

describe('CoachInfo', () => {
  it('renders properly with a ready coach', () => {
    // Given
    const coach = fakeCoach();
    const wrapper = mount(CoachInfo, { props: { coach } });
    expect(wrapper.find('h1').text()).toContain(`Coach : ${coach.name}`);
    expect(wrapper.find('h1>span').text()).toContain(coach.id);
    expect(wrapper.find('li:nth-child(1)').text()).toContain(`Numéro NAF : ${coach.nafNumber}`);
    expect(wrapper.find('li:nth-child(2)').text()).toContain(`Nom d'équipe : ${coach.teamName}`);
    expect(wrapper.find('li:nth-child(3)').text()).toContain(`Race : ${coach.faction}`);
    expect(wrapper.find('li:nth-child(4)').text()).toContain('Prêt : Oui');
  });
  it('renders without line team name if team name is empty', () => {
    // Given
    const coach = fakeCoach();
    coach.teamName = undefined;
    const wrapper = mount(CoachInfo, { props: { coach } });
    expect(wrapper.find('h1').text()).toContain(`Coach : ${coach.name}`);
    expect(wrapper.find('h1>span').text()).toContain(coach.id);
    expect(wrapper.find('li:nth-child(1)').text()).toContain(`Numéro NAF : ${coach.nafNumber}`);
    expect(wrapper.find('li:nth-child(2)').text()).not.toContain(`Nom d'équipe : `);
    expect(wrapper.find('li:nth-child(2)').text()).toContain(`Race : ${coach.faction}`);
    expect(wrapper.find('li:nth-child(3)').text()).toContain('Prêt : Oui');
  });
  it('should emit info-squad on click on a squad name', async()=>{
    // Given
    const coach = fakeCoach();
    const wrapper = mount(CoachInfo, { props: { coach } });
    // When
    await wrapper.find('button').trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('info-squad');
    expect(wrapper.emitted('info-squad')).toEqual([[coach.squadId]]);
  });
});