import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import PlayedGamesTable from '../PlayedGamesTable.vue'
import { fake2PlayedGames } from '@/services/game';

describe('PlayedGamesTable', () => {

  it('renders properly with displayRound props set to false', () => {
    // Given
    const games = fake2PlayedGames();
    // When
    const wrapper = mount(PlayedGamesTable, { props: { games, displayRound: false, fromCoach1Perspective: false, displayCoach1: true } });
    // Then
    expect(wrapper.find('tr:nth-child(1) td:nth-child(1)').text()).toContain(games[0].table);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(2)').text()).toContain(`${games[0].coach1.name} vs ${games[0].coach2.name}`);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(3)').text()).toContain(`${games[0].td1} - ${games[0].td2}`);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(4)').text()).toContain(`${games[0].casualties1} - ${games[0].casualties2}`);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(1)').text()).toContain(games[1].table);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(2)').text()).toContain(`${games[1].coach1.name} vs ${games[1].coach2.name}`);
  });
  it('should display round with displayRound props set to true', () => {
    // Given
    const games = fake2PlayedGames();
    // When
    const wrapper = mount(PlayedGamesTable, { props: { games, displayRound: true, fromCoach1Perspective: false, displayCoach1: true } });
    // Then
    expect(wrapper.find('tr:nth-child(1) td:nth-child(1)').text()).toContain(games[0].round);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(2)').text()).toContain(games[0].table);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(3)').text()).toContain(`${games[0].coach1.name} vs ${games[0].coach2.name}`);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(4)').text()).toContain(`${games[0].td1} - ${games[0].td2}`);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(5)').text()).toContain(`${games[0].casualties1} - ${games[0].casualties2}`);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(1)').text()).toContain(games[1].round);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(2)').text()).toContain(games[1].table);
  });
  it('should hide coach1.name with displayCoach1 props set to false', () => {
    // Given
    const games = fake2PlayedGames();
    // When
    const wrapper = mount(PlayedGamesTable, { props: { games, displayRound: true, fromCoach1Perspective: false, displayCoach1: false } });
    // Then
    expect(wrapper.find('tr:nth-child(1) td:nth-child(1)').text()).toContain(games[0].round);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(3)').text()).toContain(games[0].coach2.name);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(1)').text()).toContain(games[1].round);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(3)').text()).toContain(games[1].coach2.name);
  });
  it.each([
    ['resume', 1, 'warning'],
    ['resume', 2, 'warning'],
    ['delete', 2, 'danger'],
  ])('emits %s with %d of the squad when clicking on a button %s', async (emitted: string, line: number, button: string) => {
    // Given
    const games = fake2PlayedGames();
    // When
    const wrapper = mount(PlayedGamesTable, { props: { games, displayRound: false, fromCoach1Perspective: false, displayCoach1: true } });
    // Then
    await wrapper.findAll(`button.btn-${button}`)[line - 1].trigger('click');
    expect(wrapper.emitted()).toHaveProperty(emitted);
    expect(wrapper.emitted()[emitted][0]).toEqual([games[line - 1].id]);
  });
});