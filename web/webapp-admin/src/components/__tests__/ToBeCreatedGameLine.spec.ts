import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import ToBeCreatedGameLine from '../ToBeCreatedGameLine.vue'
import { fakeToBeCreatedGame } from '@/services/game';

describe('ToBeCreatedGameLine', () => {
  it('renders properly with a to be created game', () => {
    // Given
    const game = fakeToBeCreatedGame();
    const wrapper = mount(ToBeCreatedGameLine, { props: { game } });
    expect(wrapper.find('td:nth-child(1)').text()).toContain(game.table);
    expect(wrapper.find('td:nth-child(2)').text()).toContain(game.squadName1);
    expect(wrapper.find('td:nth-child(3)').text()).toContain(game.coachName1);
    expect(wrapper.find('td:nth-child(4)').text()).toContain('vs');
    expect(wrapper.find('td:nth-child(5)').text()).toContain(game.coachName2);
    expect(wrapper.find('td:nth-child(6)').text()).toContain(game.squadName2);
  });
  it('should emit delete when clicked on Supprimer', async () => {
    // Given
    const game = fakeToBeCreatedGame();
    const wrapper = mount(ToBeCreatedGameLine, { props: { game } });
    // When
    await wrapper.find('button.btn-danger').trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('delete');
  });
});
