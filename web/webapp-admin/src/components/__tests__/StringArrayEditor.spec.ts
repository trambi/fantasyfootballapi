import { describe, expect, it } from 'vitest';
import { mount } from '@vue/test-utils';
import StringArrayEditor from '../StringArrayEditor.vue';

describe('StringArrayEditor', () => {
    it('should render given string array', () => {
        // Given
        const values = [
            'value 1',
            'value 2',
            'value 3'
        ];
        const prefixId = "test";
        const placeholderForNewInput = "New value"
        // When
        const wrapper = mount(StringArrayEditor, { props: { modelValue: values, placeholderForNewInput, prefixId } });
        // Then
        expect(wrapper.find('summary').text()).toContain(values.join(', '));
        expect(wrapper.find('#test-0-value').text()).toContain(values[0]);
        expect(wrapper.find('#test-1-value').text()).toContain(values[1]);
        expect(wrapper.find('#test-2-value').text()).toContain(values[2]);
    });
    it('should update when click on suppress on a line', async () => {
        // Given
        const values = [
            'value 1',
            'value 2',
            'value 3'
        ];
        const placeholderForNewInput = "New value";
        const expected = [
            'value 2',
            'value 3'
        ];
        const prefixId = "test";
        const wrapper = mount(StringArrayEditor, { props: { modelValue: values, placeholderForNewInput, prefixId } });
        // When
        await wrapper.find('button#test-0-delete').trigger('click');
        // Then
        expect(values).toEqual(expected);
    });
    it('should update when add a string', async () => {
        // Given
        const values = [
            'value 1',
            'value 2'
        ];
        const placeholderForNewInput = "New value";
        const expected = [
            'value 1',
            'value 2',
            'value 3'
        ]
        const prefixId = "test";
        const wrapper = mount(StringArrayEditor, { props: { modelValue: values, placeholderForNewInput, prefixId } });
        // When
        await wrapper.find('input#test-add-value').setValue('value 3');
        await wrapper.find('button#test-add-button').trigger('click');
        // Then
        expect(values).toEqual(expected);
    });
    it('should update when add a string', async () => {
        // Given
        const values = [
            'value 1'
        ];
        const placeholderForNewInput = "New value";
        const expected = [
            'value 1',
            'value 2',
            'value 3'
        ]
        const prefixId = "test";
        // To test v-model 
        // We need to implement onUpdate:modelValue
        // according to https://test-utils.vuejs.org/guide/advanced/v-model.html
        const wrapper = mount(StringArrayEditor, {
            props: {
                modelValue: values,
                placeholderForNewInput,
                prefixId,
                'onUpdate:modelValue': (e) => wrapper.setProps({ modelValue: e })
            }
        });
        // When
        await wrapper.find('input#test-add-value').setValue('value 2, value 3');
        await wrapper.find('button#test-add-bulk-button').trigger('click');
        // Then
        expect(wrapper.props('modelValue')).toEqual(expected);
    });
});