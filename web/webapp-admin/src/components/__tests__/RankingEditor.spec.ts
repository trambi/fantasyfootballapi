import { describe, expect, it } from 'vitest';
import { mount } from '@vue/test-utils';
import RankingEditor from '../RankingEditor.vue';
import type { VueNode } from 'node_modules/@vue/test-utils/dist/types';

describe('RankingEditor', () => {
    const prefixId = 'test';
    const name = 'Test';
    it('should render given ranking', () => {
        // Given
        const ranking = [
            {
                label: "TD",
                field: "Touchdown",
                type: "For",
                descending: true
            },
            {
                label: "Defense",
                field: "Touchdown",
                type: "Against",
                descending: false
            },
        ];
        const possibleFields = ['Touchdown', 'Casualties'];
        const possibleTypes = ['For', 'Against']
        const expectedContent1 = 'TD: Based on Touchdown For descending';
        const expectedContent2 = 'Defense: Based on Touchdown Against ascending';
        // When
        const wrapper = mount(RankingEditor, { props: { name, modelValue:ranking, possibleFields, possibleTypes, prefixId } });
        // Then
        expect(wrapper.find('summary').text()).toContain(name);
        expect(wrapper.find('summary').text()).toContain(expectedContent1);
        expect(wrapper.find('summary').text()).toContain(expectedContent2);
        expect((wrapper.find('input#test-0-label').element as VueNode<HTMLInputElement>).value).toContain(ranking[0].label);
        expect(wrapper.find('select#test-0-field').text()).toContain(ranking[0].field);
        expect(wrapper.find('select#test-0-type').text()).toContain(ranking[0].type);
        expect((wrapper.find('input#test-0-descending').element as VueNode<HTMLInputElement>).value).toContain('on');
        expect((wrapper.find('input#test-1-label').element as VueNode<HTMLInputElement>).value).toContain(ranking[1].label);
    });
    it('should update ranking vmodel when click on suppress on an element', async () => {
        // Given
        const ranking = [
            {
                label: "TD",
                field: "Touchdown",
                type: "For",
                descending: true,
            },
            {
                label: "Defense",
                field: "Touchdown",
                type: "Against",
                descending: false,
            },
            {
                label: "Net casualties",
                field: "Casualties",
                type: "Net",
                descending: true,
            },
        ];
        const expected = [
            {
                label: "TD",
                field: "Touchdown",
                type: "For",
                descending: true,
            },
            {
                label: "Net casualties",
                field: "Casualties",
                type: "Net",
                descending: true,
            },
        ];
        const possibleFields = ['Touchdown', 'Casualties'];
        const possibleTypes = ['For', 'Against','Net'];
        const wrapper = mount(RankingEditor, { props: { name, modelValue:ranking, possibleFields, possibleTypes, prefixId } });
        // When
        await wrapper.find('button#test-1-delete').trigger('click');
        // Then
        expect(ranking).toEqual(expected);
    });
    it('should update ranking vmodel when add a ranking element', async () => {
        // Given
        const ranking = [
            {
                label: "TD",
                field: "Touchdown",
                type: "For",
                descending: true,
            },
        ];
        const expected = [
            {
                label: "TD",
                field: "Touchdown",
                type: "For",
                descending: true,
            },
            {
                label: "Defense",
                field: "Touchdown",
                type: "Against",
                descending: false,
            },
        ];
        const possibleFields = ['Touchdown', 'Casualties'];
        const possibleTypes = ['For', 'Against']

        const wrapper = mount(RankingEditor, { props: { name, modelValue:ranking, possibleFields, possibleTypes, prefixId } });
        // When
        await wrapper.find('input#test-add-label').setValue('Defense');
        await wrapper.find('select#test-add-field').setValue('Touchdown');
        await wrapper.find('select#test-add-type').setValue('Against');
        await wrapper.find('input#test-add-descending').trigger('click')
        await wrapper.find('#test-add').trigger('click');
        // Then
        // ranking has been updated
        expect(ranking).toEqual(expected);
        // and reset to add ranking element
        expect((wrapper.find('input#test-add-label').element as VueNode<HTMLInputElement>).value).toContain('');
        expect(wrapper.find('select#test-add-field').text()).toContain('');
        expect(wrapper.find('select#test-add-type').text()).toContain('');
        expect((wrapper.find('input#test-add-descending').element as VueNode<HTMLInputElement>).value).toContain('on');
    });
    it('should update vmodel ranking when update a ranking element', async () => {
        // Given
        const ranking = [
            {
                label: "TD",
                field: "Touchdown",
                type: "For",
                descending: true
            },
            {
                label: "Defense",
                field: "Touchdown",
                type: "Against",
                descending: false
            },
        ];
        const possibleFields = ['Touchdown', 'Casualties'];
        const possibleTypes = ['For', 'Against']
        const expected = [
            {
                label: "Modified td",
                field: "Touchdown",
                type: "For",
                descending: true
            },
            {
                label: "Defense",
                field: "Touchdown",
                type: "Against",
                descending: false
            },
        ];
        const wrapper = mount(RankingEditor, { props: { name, modelValue:ranking, possibleFields, possibleTypes, prefixId } });
        // When
        await wrapper.find('input#test-0-label').setValue('Modified td');
        await wrapper.find('input#test-0-label').trigger('change')
        // Then
        expect(ranking).toEqual(expected);
    });
});