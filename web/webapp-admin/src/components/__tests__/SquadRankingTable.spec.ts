import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import { fakeSquadRanking } from '@/services/rank';
import SquadRankingTable from '../SquadRankingTable.vue'
import { displayMembers } from '@/services/squad';

describe('SquadRankingTable', () => {
  it('renders properly', () => {
    // Given
    const ranking = fakeSquadRanking();
    const fields = ['points', 'opponentsPoints', 'netTd', 'netCasualties'];
    // When
    const wrapper = mount(SquadRankingTable, { props: { ranking, fields } });
    // Then
    expect(wrapper.find('thead tr th:nth-child(1)').text()).toContain('#');
    expect(wrapper.find('thead tr th:nth-child(2)').text()).toContain('Équipe');
    expect(wrapper.find('thead tr th:nth-child(3)').text()).toContain('Participants');
    expect(wrapper.find('thead tr th:nth-child(4)').text()).toContain(fields[0]);
    expect(wrapper.find('thead tr th:nth-child(5)').text()).toContain(fields[1]);
    expect(wrapper.find('thead tr th:nth-child(6)').text()).toContain(fields[2]);
    expect(wrapper.find('thead tr th:nth-child(7)').text()).toContain(fields[3]);
    expect(wrapper.find('tbody tr:nth-child(1) th').text()).toContain(ranking[0].position);
    expect(wrapper.find('tbody tr:nth-child(1) td:nth-child(3)').text()).toContain(displayMembers(ranking[0]));
    expect(wrapper.find('tbody tr:nth-child(1) td:nth-child(4)').text()).toContain(ranking[0].params.get(fields[0]));
    expect(wrapper.find('tbody tr:nth-child(1) td:nth-child(5)').text()).toContain(ranking[0].params.get(fields[1]));
    expect(wrapper.find('tbody tr:nth-child(1) td:nth-child(6)').text()).toContain(ranking[0].params.get(fields[2]));
    expect(wrapper.find('tbody tr:nth-child(1) td:nth-child(7)').text()).toContain(ranking[0].params.get(fields[3]));
    expect(wrapper.find('tbody tr:nth-child(2) th').text()).toContain(ranking[1].position);
    expect(wrapper.find('tbody tr:nth-child(2) td:nth-child(3)').text()).toContain(displayMembers(ranking[1]));
  });
  it.each([
    ['info',1,'info'],
    ['info',2,'info'],
  ])('emits %s with %d of the coach when clicking on a button %s',async (emitted:string,line:number, button:string)=>{
    // Given
    const ranking = fakeSquadRanking();
    const fields = ['points', 'opponentsPoints', 'netTd', 'netCasualties'];
    const wrapper = mount(SquadRankingTable, { props: { ranking, fields } });
    // When
    await wrapper.findAll(`button.btn-${button}`)[line-1].trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty(emitted);
    expect(wrapper.emitted()[emitted][0]).toEqual([ranking[line-1].id]);
  });
});


