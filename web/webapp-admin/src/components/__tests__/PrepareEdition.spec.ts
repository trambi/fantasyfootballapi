import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import PrepareEdition from '../PrepareEdition.vue';
import { fakeEdition } from '@/services/edition';
import { fake6Coachs, type Coach } from '@/services/coach';
import { fakeSquads, type Squad } from '@/services/squad';


describe('PrepareEdition', () => {
    it('should render one admin menu bar when there is no coachs at all', () => {
        // Given
        const edition = fakeEdition();
        const coachs: Coach[] = [];
        const squads : Squad[]= [];
        // When
        const wrapper = mount(PrepareEdition, { props: { edition, coachs, squads} });
        // Then
        expect(wrapper.findAll('div.menubar button:nth-child(1)')).toHaveLength(1);
    });
    
    it('should render two admin menu bars when there is only coachs', () => {
        // Given
        const edition = fakeEdition();
        const coachs = fake6Coachs();
        const squads : Squad[]= [];
        // When
        const wrapper = mount(PrepareEdition, { props: { edition, squads, coachs} });
        // Then
        expect(wrapper.findAll('div.menubar button:nth-child(1)')).toHaveLength(2);
    });
    it('should render three admin menu bars when there is coachs and squads', () => {
        // Given
        const edition = fakeEdition();
        const coachs = fake6Coachs();
        const squads= fakeSquads();
        // When
        const wrapper = mount(PrepareEdition, { props: { edition, squads, coachs} });
        // Then
        expect(wrapper.findAll('div.menubar button:nth-child(1)')).toHaveLength(3);
    });
    it('should emit coach-ready when click on readiness button of the first coach',async ()=>{
        // Given
        const edition = fakeEdition();
        const coachs = fake6Coachs();
        const squads : Squad[]= [];
        // When
        const wrapper = mount(PrepareEdition, { props: { edition, squads, coachs} });
        await wrapper.find('tr:nth-child(1) td:nth-child(5) button:nth-child(1)').trigger('click');
        // Then
        expect(wrapper.emitted()).toHaveProperty('coach-ready');
    });
    it('should emit squad-ready when click on readiness button of the first squad',async ()=>{
        // Given
        const edition = fakeEdition();
        const coachs: Coach[] = [];
        const squads= fakeSquads();

        // When
        const wrapper = mount(PrepareEdition, { props: { edition, squads, coachs} });
        await wrapper.find('tr:nth-child(1) td:nth-child(4) button:nth-child(1)').trigger('click');
        // Then
        expect(wrapper.emitted()).toHaveProperty('squad-ready');
    });
});


