import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import AdminEditionBar from '../AdminEditionBar.vue';
import { fakeEdition } from '@/services/edition';

describe('AdminEditionBar', () => {
    it('should display next round when round equals to zero', () => {
        // Given
        const edition = fakeEdition();
        const round = 0;
        // When
        const wrapper = mount(AdminEditionBar, { props: { edition, round } });
        // Then
        expect(wrapper.find('button:first-child').text()).not.toEqual('Ronde précédente');
        expect(wrapper.find('button:nth-child(2)').text()).toEqual('Ronde suivante');
    });
    it('should display next and previous round when round is one', () => {
        // Given
        const edition = fakeEdition();
        const round = 1;
        // When
        const wrapper = mount(AdminEditionBar, { props: { edition, round } });
        // Then
        expect(wrapper.find('button:first-child').text()).toEqual('Ronde précédente');
        expect(wrapper.find('button:nth-child(3)').text()).toEqual('Ronde suivante');
    });
    it('should display previous round when round is last round', () => {
        // Given
        const edition = fakeEdition();
        const round = 5;
        // When
        const wrapper = mount(AdminEditionBar, { props: { edition, round } });
        // Then
        expect(wrapper.find('button:first-child').text()).toEqual('Ronde précédente');
        expect(wrapper.find('button:nth-child(3)').text()).not.toEqual('Ronde suivante');
    });
});