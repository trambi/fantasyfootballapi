import { describe, expect, it } from 'vitest';
import { mount } from '@vue/test-utils';
import PointInput from '../PointInput.vue';
import type { VueNode } from 'node_modules/@vue/test-utils/dist/types';

describe('PointInput', () => {
    const prefixId = 'test';
    const label = 'Point';
    it('should render given ranking', () => {
        // Given
        const initialValue = '1';
        // When
        const wrapper = mount(PointInput, { props: { label,prefixId, modelValue:initialValue }, });
        // Then
        expect(wrapper.find('label').text()).toContain(label);
        expect((wrapper.find('input#test-point').element as VueNode<HTMLInputElement>).value).toContain(initialValue);
    });
    it('should update model rankingField when update a label', async () => {
        // Given
        const initialValue = '1';
        const expected = '2';
        // To test v-model 
        // We need to implement onUpdate:modelValue
        // according to https://test-utils.vuejs.org/guide/advanced/v-model.html
        const wrapper = mount(PointInput, { 
            props: { 
                label,
                modelValue:initialValue,
                prefixId, 
                
                'onUpdate:modelValue': (e) => wrapper.setProps({ modelValue: e }) 
            }
         });
        // When
        await wrapper.find('input').setValue(expected);
        // Then
        expect(wrapper.props('modelValue')).toBe(expected);
    });
});