import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import CoachsTable from '../CoachsTable.vue'
import { fake6Coachs } from '@/services/coach';

describe('CoachsTable', () => {
  it('renders properly', () => {
    // Given
    const coachs = fake6Coachs();
    // When
    const wrapper = mount(CoachsTable, { props: { coachs } });
    // Then
    expect(wrapper.find('tr:nth-child(1) td:nth-child(1)').text()).toContain(coachs[0].name);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(2)').text()).toContain(coachs[0].nafNumber);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(3)').text()).toContain(coachs[0].faction);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(1)').text()).toContain(coachs[1].name)
    expect(wrapper.find('tr:nth-child(2) td:nth-child(2)').text()).toContain(coachs[1].nafNumber)
    expect(wrapper.find('tr:nth-child(2) td:nth-child(3)').text()).toContain(coachs[1].faction)
  });
  it.each([
    ['info',1,'info'],
    ['info',2,'info'],
    ['edit',2,'warning'],
    ['delete',2,'danger'],
  ])('emits %s with %d of the coach when clicking on a button %s',async (emitted:string,line:number, button:string)=>{
    // Given
    const coachs = fake6Coachs();
    // When
    const wrapper = mount(CoachsTable, { props: { coachs } });
    // Then
    await wrapper.findAll(`button.btn-${button}`)[line-1].trigger('click');
    expect(wrapper.emitted()).toHaveProperty(emitted);
    expect(wrapper.emitted()[emitted][0]).toEqual([coachs[line-1].id]);
  });
  it('emits ready with the coach when clicking on the ready button',async ()=>{
    // Given
    const coachs = fake6Coachs();
    // When
    const wrapper = mount(CoachsTable, { props: { coachs } });
    // Then
    await wrapper.findAll('button.btn-primary')[0].trigger('click');
    expect(wrapper.emitted()).toHaveProperty('ready');
    expect(wrapper.emitted()['ready'][0]).toEqual([coachs[0].id,false]);
  });
});