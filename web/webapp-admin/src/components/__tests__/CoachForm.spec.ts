import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import CoachForm from '../CoachForm.vue';
import type { VueNode } from 'node_modules/@vue/test-utils/dist/types';
import { fakeEdition } from '@/services/edition';
import { fakeCoach } from '@/services/coach';

describe('CoachForm', () => {
    it('should render given coach', () => {
        // Given
        const edition = fakeEdition();
        const coach = fakeCoach();
        // When
        const wrapper = mount(CoachForm, { props: { coach, edition } });
        // Then
        expect((wrapper.find('input#name').element as VueNode<HTMLInputElement>).value).toContain(coach.name);
        expect((wrapper.find('#team').element as VueNode<HTMLInputElement>).value).toContain(coach.teamName);
        expect((wrapper.find('#nafNumber').element as VueNode<HTMLInputElement>).value).toContain(coach.nafNumber);
        expect(wrapper.find('#faction').text()).toContain(coach.faction);
    });
    it('should emit "submit" with a proper content with coach as props when click on submit', async () => {
        // Given
        const edition = fakeEdition();
        const coach = fakeCoach();
        const wrapper = mount(CoachForm, { props: { coach, edition } });
        await wrapper.find('#name').setValue('An unbelievable name');
        await wrapper.find('#team').setValue('An unbelievable team');
        await wrapper.find('#nafNumber').setValue(1230);
        await wrapper.find('#faction').setValue('Faction A');
        const expected = {
            editionId: coach.editionId,
            editionName: coach.editionName,
            id: coach.id,
            faction: coach.faction,
            teamName:coach.teamName,
            name: coach.name,
            nafNumber: coach.nafNumber,
            ready: coach.ready,
            squadId: coach.squadId,
            squadName: coach.squadName
        }
        // When
        await wrapper.find('button').trigger('click');
        // Then
        expect(wrapper.emitted()).toHaveProperty('submit');
        expect(wrapper.emitted()['submit'][0]).toEqual([edition.id, expected]);
    });
    it('should emit "submit" with a proper content without coach as props when click on submit', async () => {
        // Given
        const edition = fakeEdition();
        const wrapper = mount(CoachForm, { props: { edition } });
        await wrapper.find('#name').setValue('An unbelievable name');
        await wrapper.find('#faction').setValue(edition.allowedFactions[0]);
        const expected = {
            editionId: edition.id,
            faction: edition.allowedFactions[0],
            name: "An unbelievable name",
            nafNumber: 0,
            ready: false,
        }
        // When
        await wrapper.find('button').trigger('click');
        // Expect
        expect(wrapper.emitted()).toHaveProperty('submit');
        expect(wrapper.emitted()['submit'][0]).toEqual([edition.id, expected]);
    });
});