import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import CoachLine from '../CoachLine.vue'
import { fakeCoach } from '@/services/coach';

describe('CoachLine', () => {
  it('renders properly with a ready coach', () => {
    // Given
    const coach = fakeCoach();
    const wrapper = mount(CoachLine, { props: { coach } });
    expect(wrapper.find('button:nth-child(1)').text()).toContain(coach.name);
    expect(wrapper.find('td:nth-child(2)').text()).toContain(coach.nafNumber);
    expect(wrapper.find('td:nth-child(3)').text()).toContain(coach.faction);
    expect(wrapper.find('td:nth-child(4)').text()).toContain('Prêt');
    expect(wrapper.find('td:nth-child(5)').text()).toContain('Déclarer absent');
    expect(wrapper.find('td:nth-child(5)').text()).toContain('Supprimer');
    expect(wrapper.find('td:nth-child(5)').text()).toContain('Modifier');
  });
  it('renders properly with a not ready coach', () => {
    // Given
    const coach = fakeCoach();
    coach.ready = false;
    const wrapper = mount(CoachLine, { props: { coach } });
    expect(wrapper.find('button:nth-child(1)').text()).toContain(coach.name);
    expect(wrapper.find('td:nth-child(2)').text()).toContain(coach.nafNumber);
    expect(wrapper.find('td:nth-child(3)').text()).toContain(coach.faction);
    expect(wrapper.find('td:nth-child(4)').text()).toContain('Absent');
    expect(wrapper.find('td:nth-child(5)').text()).toContain('Déclarer présent');
    expect(wrapper.find('td:nth-child(5)').text()).toContain('Supprimer');
    expect(wrapper.find('td:nth-child(5)').text()).toContain('Modifier');
  });
  it('should emit info when clicked on coach name', async () => {
    // Given
    const coach = fakeCoach()
    const wrapper = mount(CoachLine, { props: { coach } });
    await wrapper.find('button:nth-child(1)').trigger('click');
    expect(wrapper.emitted()).toHaveProperty('info');
  });
  it('should emit ready when clicked on declare present', async () => {
    // Given
    const coach = fakeCoach();
    coach.ready = false;
    const wrapper = mount(CoachLine, { props: { coach } });
    // When
    await wrapper.find('button.btn-primary').trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('ready');
  });
  it('should emit delete when clicked on Supprimer', async () => {
    // Given
    const coach = fakeCoach();
    const wrapper = mount(CoachLine, { props: { coach } });
    // When
    await wrapper.find('button.btn-danger').trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('delete');
  });
  it('should emit edit when clicked on Modifier', async () => {
    // Given
    const coach = fakeCoach();
    const wrapper = mount(CoachLine, { props: { coach } });
    // When
    await wrapper.find('button.btn-warning').trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('edit');
  });
});
