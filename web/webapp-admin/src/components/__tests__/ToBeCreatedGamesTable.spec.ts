import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import ToBeCreatedGamesTable from '../ToBeCreatedGamesTable.vue'
import { fake2ToBeCreatedGames } from '@/services/game';

describe('ToBeCreatedGamesTable', () => {
  it('renders properly', () => {
    // Given
    const games = fake2ToBeCreatedGames();
    // When
    const wrapper = mount(ToBeCreatedGamesTable, { props: { games } });
    // Then
    expect(wrapper.find('tr:nth-child(1) td:nth-child(1)').text()).toContain(games[0].table);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(2)').text()).toContain(games[0].squadName1);
    expect(wrapper.find('tr:nth-child(1) td:nth-child(3)').text()).toContain(games[0].coachName1);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(1)').text()).toContain(games[1].table);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(5)').text()).toContain(games[1].coachName2);
    expect(wrapper.find('tr:nth-child(2) td:nth-child(6)').text()).toContain(games[1].squadName2);
  });
  it.each([
    [0],
    [1],
  ])('emits delete with %d of the game to be created when clicking on a button danger',async (line:number)=>{
    // Given
    const games = fake2ToBeCreatedGames();
    // When
    const wrapper = mount(ToBeCreatedGamesTable, { props: { games } });
    // Then
    await wrapper.findAll(`button.btn-danger`)[line].trigger('click');
    expect(wrapper.emitted()).toHaveProperty('delete');
    expect(wrapper.emitted()['delete'][0]).toEqual([games[line].table]);
  });
});