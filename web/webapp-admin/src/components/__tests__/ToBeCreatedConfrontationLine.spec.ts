import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils';
import ToBeCreatedConfrontationLine from '../ToBeCreatedConfrontationLine.vue';
import { fakeToBeCreatedConfrontation } from '@/services/confrontation';

describe('ToBeCreatedConfrontationLine', () => {
  it('renders properly with a to be created confrontation', () => {
    // Given
    const confrontation = fakeToBeCreatedConfrontation();
    const wrapper = mount(ToBeCreatedConfrontationLine, { props: { confrontation } });
    expect(wrapper.find('div.row:nth-child(1) h3').text()).toContain(`${confrontation.squadName1} vs ${confrontation.squadName2}`);
    expect(wrapper.find('div.row:nth-child(1) button.btn-danger').text()).toContain('Supprimer');
    expect(wrapper.find('div.row:nth-child(2)').text()).toContain(`Table ${confrontation.games[0].table} - ${confrontation.games[0].coachName1} vs ${confrontation.games[0].coachName2}`);
    expect(wrapper.find('div.row:nth-child(3)').text()).toContain(`Table ${confrontation.games[1].table} - ${confrontation.games[1].coachName1} vs ${confrontation.games[1].coachName2}`);
  });
  it('should emit delete when clicked on Supprimer', async () => {
    // Given
    const confrontation = fakeToBeCreatedConfrontation();
    const wrapper = mount(ToBeCreatedConfrontationLine, { props: { confrontation } });
    // When
    await wrapper.find('div.row:nth-child(1) button.btn-danger').trigger('click');
    // Then
    expect(wrapper.emitted()).toHaveProperty('delete');
  });
});
