# webapp-admin

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) to make the TypeScript language service aware of `.vue` types.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### Current status

Theme|Action|Present in previous version|Status|
-----|------|------|-----|
Coach|Add|Yes| ✅|
Coach|View|Yes|✅|
Coach|Delete|Yes|⛔|
Coach|List by edition|Yes|⛔|
Coach|Delete by edition|No|⛔|
Coach|Modify|Yes|✅|
Squad|Add|Yes|✅|
Squad|Delete|Yes|⛔|
Squad|DeleteByEdition|Yes|⛔|
Squad|List|Yes|⛔|
Squad|Load|Yes|🚧|
Squad|Modify|Yes|✅|
Squad|View|Yes|✅|
Edition|Add|Yes|⛔|
Edition|Modify|Yes|⛔|
Game|delete|Yes|✅|
Game|modify|Yes|?|
Game|resume|Yes|✅|
Game|schedule|Yes|?|
Game|summarize|Yes|⛔|
Game|summarize_team|Yes|⛔|
Game|view|Yes|⛔|
Main|backup|Yes|⛔|
Main|export|Yes|⛔|
Main|index|Yes|✅|
Main|index_not_started|Yes|✅|
Main|naf|Yes|⛔|
Main|next_round|Yes|?|
Ranking|all|Yes|⛔|
Ranking|coach|Yes|✅|
Ranking|squad|Yes|✅|
Simulate|games|Yes|⛔|